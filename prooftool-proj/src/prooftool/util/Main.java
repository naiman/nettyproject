package prooftool.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import prooftool.backend.laws.Expression;
import prooftool.backend.parser.Lexer;
import prooftool.backend.parser.Parser;
import prooftool.backend.parser.SyntaxErrorException;
import prooftool.util.objects.Dictionary;

public class Main {
	/**
	 * Converts all Unicode strings in an expression to their corresponding
	 * ASCII representation for the tool. Of course, it can only convert those
	 * which have a Unicode-ASCII mapping in Symbol.java
	 *
	 * @param expn
	 *            The expression to be converted
	 * @return The ASCII representation of the given expression(s)
	 */
	private static String oneCharReplace(String expn) {
		for (String sym : Symbol.singleCharMap.keySet()) {
			String uniSym = Symbol.singleCharMap.get(sym);
			int i = expn.indexOf(uniSym);
			while (i > -1) {
				expn = expn.substring(0, i) + sym
						+ expn.substring(i + uniSym.length());
				i = expn.indexOf(uniSym);
			}
		}

		return expn;
	}

	private static class StrLenComparator implements Comparator<String> {
		@Override
		public int compare(String o1, String o2) {
			if (o1.length() < o2.length()) {
				return 1;
			} else if (o1.length() > o2.length()) {
				return -1;
			} else {
				return o1.compareTo(o2);
			}
		}
	}

	public static String toUnicode(String expn) {
		ArrayList<String> asciiSym = new ArrayList<String>();
		asciiSym.addAll(Symbol.symbolMap.keySet());
		Collections.sort(asciiSym, new StrLenComparator());
		String lower = expn.toLowerCase();
		for (String sym : asciiSym) {

			String uniSym = Symbol.lookupUnicode(sym);
			int i = lower.indexOf(sym.toLowerCase());

			while (i > -1) {
				if (Main.prevQuotes(expn, i, false) % 2 == 0) {
					expn = expn.substring(0, i) + uniSym
							+ expn.substring(i + sym.length());
					lower = expn.toLowerCase();
					i = lower.indexOf(sym.toLowerCase());
				} else {
					i = lower.indexOf(sym.toLowerCase(), i + 1);
				}
			}
		}
		return expn;
	}

	/**
	 * Return the number of '"' characters before the character at index i.
	 * 
	 * @param s
	 * @param i
	 * @param escaped
	 * @return
	 */
	private static int prevQuotes(String s, int i, boolean escaped) {
		int j = s.indexOf("\"");
		int count = 0;
		
		while (j > -1 && j < i) {
			count++;
			j = s.indexOf("\"", j + 1);
		}
		
		return count;
	}

	/**
	 * Overloads the function addDelimiters to allow the inputs of type string
	 *
	 * @param sr
	 *            The reader from which the lines are obtained
	 * @return A string that is the same as the input, except delimited
	 * @throws Exception
	 */
	public static String addDelimiter(String file) throws Exception {
		InputStream is = new FileInputStream(file);
		return Main.addDelimiter(new InputStreamReader(is, "UTF8"));
	}

	public static Expression parseSingle(String in) throws Exception {
		String input = Main.toUnicode(oneCharReplace(in));
		Parser p = new Parser(new Lexer(new StringReader(input)));
		
		@SuppressWarnings("unchecked")
		List<Expression> expns = (List<Expression>) p.parse().value;
		return expns.get(0).makeVariables(new Dictionary());
	}

	/**
	 * This is the function that calls the parser on the input and creates a
	 * list of expressions out of it.
	 *
	 * @param in
	 *            The input text or file name
	 * @param isFile
	 *            Whether 'in' is the actual thing to parse or the file name
	 * @return A list of expressions from the input
	 * @throws Exception
	 */
	public static List<Expression> parse(String in, boolean isFile) throws Exception {
		if (in.equals("")) {
			return new ArrayList<Expression>();
		}
		
		String input;
		if (isFile) {
			input = Main.toUnicode(oneCharReplace(Main.addDelimiter(in)));
		} else {
			input = Main.toUnicode(oneCharReplace(Main.addDelimiter(new StringReader(in))));
		}

		Parser p = new Parser(new Lexer(new StringReader(input)));
		
		@SuppressWarnings("unchecked")
		List<Expression> expns = (List<Expression>) p.parse().value;

		for (int i = 0; i < expns.size(); i++) {
			expns.set(i, expns.get(i).makeVariables(new Dictionary()));			
		}

		return expns;
	}
	
	/**
	 * Add delimiters between lines for the parser to work with.
	 *
	 * @param sr
	 *            The reader from which the lines are obtained
	 * @return A string that is the same as the input, except delimited
	 * @throws Exception
	 */
	private static String addDelimiter(Reader sr) throws Exception {
		BufferedReader br = new BufferedReader(sr);
		StringBuilder ret = new StringBuilder();
		String line = br.readLine();
		char del = ExpressionUtils.lineDelimiter;
		boolean first = true;
		int cnt = 0;
		
		while (line != null) {
			cnt++;
			int i = line.indexOf(del);
			
			// check if delimiter character was used in the file
			if (i != -1) {
				// this craziness is here because '&&' was allowed for the 'and' symbol
				if ((i == 0) || ((i + 1 < line.length()) && (line.charAt(i + 1) != del))) {
					throw new SyntaxErrorException("Illegal character '" + del + "' in line " + (cnt - 1));
				}
			}
			
			if ((!line.trim().equals("")) && (line.trim().charAt(0) != '%')) {
				if (!first) {
					line = " " + del + " " + line;
				}
				first = false;
			}
			
			// input = input + line + "\n";
			ret.append(line);
			ret.append("\n");
			line = br.readLine();
		}

		return ret.toString();
	}
}
