package prooftool.util;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import prooftool.backend.direction.Direction;
import prooftool.backend.direction.ExpnDirection;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Literal;
import prooftool.backend.laws.Scope;
import prooftool.backend.laws.Variable;

public class ExpressionUtils {

	public static final Variable ALL = (Variable) parse("all");	
	public static final String prime = "′";
	public static final char lineDelimiter = '&';
	public static final Direction revImpDir = new ExpnDirection("⇐");
	public static final Direction impDir = new ExpnDirection("⇒");
	public static final Direction eqDir = new ExpnDirection("=");
	public static final Direction colonDir = new ExpnDirection(":");
	public static final Direction revColonDir = new ExpnDirection("::");
	public static final Literal top = new Literal("⊤");
	public static final Literal bottom = new Literal("⊥");
	public static final String times = "×";
	public static final String savedFieldDelim = "¶";
	public static final String savedComponentDelim = "¶¶";
	public static final String savedLineDelim = "¶¶¶";
	public static final Expression listType = parse("[*" + ALL + "]");
	
	public static Expression parse(String s) {
		try {
			return Main.parse(s, false).get(0);
		} catch (Exception e) {	
			System.err.println(s);
			e.printStackTrace();
			return null;
		}
	}
	
	public static <T> boolean containsObject(List<T> l, Object e) {
		boolean contains = false;
		Iterator<T> it = l.iterator();
		
		while (it.hasNext()) {
			if (it.next() == e) {
				return true;
			}
		}
		
		return contains;
	}
	
	/**
	 * Finds the intersection of two sorted lists
	 * @param l0
	 * @param l1
	 * @return
	 */
	public static List<Integer> intersect(List<Integer> l0, List<Integer> l1) {
		List<Integer> inter = new ArrayList<Integer>();
		
		for (Integer i: l0) {
			if (l1.contains(i)) {
				inter.add(i);
			}
		}
		
		return inter;
	}
	
	public static int lastnthIndexOf(String str, String sub, int n) {
	    int pos = str.lastIndexOf(sub, str.length());
	    
	    while (n-- > 0 && pos-1 != -1) {
	        pos = str.lastIndexOf(sub, pos-1);
	    }
	    
	    return pos;
	}

	//TODO: temporary hack. Would need to have zoomability as part of expression config
	public static boolean isZoomable(Expression expn, int part) {
		if (expn instanceof Application && ((Application)expn).getFunId().equals(Identifiers.gets) && part == 0) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * This assumes that the law body is an application, and returns a law that
	 * is similar to the input except the body is turned around
	 * 
	 * @param law
	 * @return
	 * @throws Exception
	 */
	public static Expression reverseLaw(Expression law) throws Exception {
		Expression lawClone = law.clone();
		law.copyTypes(lawClone);

		Expression body = lawClone;
		Scope parent = null;
		Expression reverse;

		while (body.is_uni_quant_scope()) {
			parent = (Scope) ((Application) body).getArgs()[0];
			body = parent.getBody();
		}
		
		Expression leftSide = ((Application) body).getChild(0);
		Expression rightSide = ((Application) body).getChild(1);
		Variable reverseDir = new Variable(body.getDirection().reverse().getIdent());
		Application reverseBody = new Application(reverseDir, rightSide, leftSide);

		if (parent != null) {
			parent.setBody(reverseBody);
			reverse = lawClone;
		} else {
			reverse = reverseBody;
		}

		return reverse;
	}
	
	/**
	 * This method takes an expression and breaks it up into pieces. It is used
	 * to turn a context expression into smaller more workable ones. If for
	 * example we have ¬(x∨y) as context, this method will return two
	 * expressions: '¬x' and '¬y'.
	 *
	 * This method performs a deep negation on the given expression is necessary
	 * and then splits it up based on the top level conjunction. If there isnt
	 * one, then just that one expression is returned. Otherwise, its sub-
	 * expressions are returned.
	 *
	 * @param expn
	 *            The context expression to be broken up
	 * @return List of smaller parts of the given expression
	 */
	public static List<Expression> completeContext(Expression expn) {
		List<Expression> extraContext = new ArrayList<Expression>();

		// do deep negation
		Expression toBreak = expn;
		if (expn.isNegation()) {
			assert expn.getChild(0) != null;
			toBreak = expn.getChild(0).deepNegate();
		}
		
		toBreak.flatten();

		// split along conjunction
		if ((toBreak instanceof Application)
				&& (((Application) toBreak).getFunction().toString()
						.equals("∧"))) {
			for (Expression arg : ((Application) toBreak).getArgs()) {
				extraContext.add(arg);
			}
		} else {
			extraContext.add(toBreak);
		}

		return extraContext;
	}
	
    /**
     * Returns the variables in vars which appear in expn and which do not
     * appear in otherSide
     *
     * @param expn First expression
     * @param otherSide Other expression
     * @param vars The variables which we are concerned with their appearance
     * @return The variables from vars unique to expn
     */
	public static List<Variable> getUniqueVars(Expression expn, Expression otherSide, List<Variable> instVars) {
		Map<Variable, Variable> expnVars = expn.getAllVarsHelper();
		Map<Variable, Variable> otherVars = otherSide.getAllVarsHelper();
		List<Variable> instUnique = new ArrayList<Variable>(instVars.size());

		for (Variable var : instVars) {
			if (expnVars.containsKey(var) && !otherVars.containsKey(var)) {
				instUnique.add(var);
			}
		}
		
		return instUnique;
	}
}
