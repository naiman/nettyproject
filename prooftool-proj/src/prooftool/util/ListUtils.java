package prooftool.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import prooftool.util.objects.Predicate;
import prooftool.util.objects.UnaryFunction;

// Utilities for working with lists.
public class ListUtils {

	// Join two lists into one.
	public static <T> List<T> join(List<T> a, List<T> b) {
		ArrayList<T> result = new ArrayList<T>(a);
		result.addAll(b);
		return result;
	}
	
	// Join a collection of strings, with a separator
	public static String join(Collection<String> s, String sep) {
		return ListUtils.join(s.toArray(new String[]{}), sep);
	}
	
	// Same as above, but for an array of strings
	// (why does Java not have a Range concept that both
	// collections and arrays satisfy?)
	public static String join(String[] s, String sep) {
		String result = "";
		
		for (int i = 0; i < s.length; ++i) {
			if (i != 0)
				result += sep;
			result += s[i];
		}
		
		return result;
	}
	
	// Filter a collection/array using the specified predicate
	public static <T> List<T> filter(Collection<T> list, Predicate<T> predicate) {
		List<T> result = new ArrayList<T>();
		
		for (T t : list) {
			if (predicate.apply(t)) {
				result.add(t);
			}
		}
		
		return result;
	}
	
	public static <T> List<T> filter(T[] a, Predicate<T> predicate) {
		return ListUtils.filter(Arrays.asList(a), predicate);
	}
	
	// Transform a collection/array using the specified unary function
	public static <T, U> List<U> transform(Collection<T> list, UnaryFunction<T, U> function) {
		List<U> result = new ArrayList<U>();
		
		for (T t : list) {
			result.add(function.apply(t));
		}
		
		return result;
	}
	
	public static <T, U> List<U> transform(T[] a, UnaryFunction<T, U> function) {
		return ListUtils.transform(Arrays.asList(a), function);
	}
	
	// Add a U to the List<U> corresponding to a T in a Map<T, List<U>>,
	// creating the mapping if it doesn't exist yet.
	public static <T, U> void listMapPut(Map<T, List<U>> m, T t, U u) {
		ListUtils.listMapGet(m, t).add(u);
	}

	// Get a List<U> corresponding to a T in a Map<T, List<U>>.
	// If the mapping doesn't exist, create it, with the empty
	// list as the initial value.
	public static <T, U> List<U> listMapGet(Map<T, List<U>> m, T t) {
		List<U> result = m.get(t);
		
		if (result == null) {
			result = new ArrayList<U>();
			m.put(t, result);
		}
		
		return result;
	}
	
	// List listMapPut/listMapGet, but for Map<T, Set<U>>
	public static <T, U> Set<U> setMapGet(Map<T, Set<U>> m, T t) {
		Set<U> result = m.get(t);
		
		if (result == null)
		{
			result = new HashSet<U>();
			m.put(t, result);
		}
		
		return result;
	}
	
	public static <T, U> void setMapPut(Map<T, Set<U>> m, T t, U u) {
		ListUtils.setMapGet(m, t).add(u);
	}
	
	public static <T, U> boolean setMapContains(Map<T, Set<U>> m, T t, U u) {
		return ListUtils.setMapGet(m, t).contains(u);
	}
	
	public static <T, U> void setMapRemove(Map<T, Set<U>> m, T t, U u) {
		ListUtils.setMapGet(m, t).remove(u);
	}
	
	// Provides a reversed view of a list, without modifying it.
	public static class ReverseView<T> implements Iterable<T> {
		private ListIterator<T> listIterator;

		public ReverseView(List<T> wrappedList) {
			this.listIterator = wrappedList.listIterator(wrappedList.size());
		}

		public Iterator<T> iterator() {
			return new Iterator<T>() { 
				public boolean hasNext() {
					return listIterator.hasPrevious();
				}

				public T next() {
					return listIterator.previous();
				}

				public void remove() {
					listIterator.remove();
				}
			};
		}
	}
	
	// Convenience function for constructing a reverse view
	public static <T> ReverseView<T> reverseView(List<T> list) {
		return new ReverseView<T>(list);
	}
}
