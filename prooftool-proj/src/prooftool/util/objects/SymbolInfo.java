package prooftool.util.objects;



import java.util.ArrayList;
import java.util.List;

import prooftool.backend.laws.Expression;



/*
 * Stores the information associated with each symbol. This always includes 
 * a unicode symbol (which might be identical), and possibly the required operand type
 * and return type. In some more complex operators these last two values cannot be 
 * determined without knowing what the operands are, and therefore they are left as null  
 */

public class SymbolInfo {
	private List<Expression> types = new ArrayList<Expression>();	
	private DirectionInfo dirInfo;
	private boolean isAssociative;
	
	public SymbolInfo (Expression type, boolean isAssociative, DirectionInfo dirInfo) {
		this.types.add(type);
		this.dirInfo = dirInfo;
	}
	
	public SymbolInfo (List<Expression> types, boolean isAssociative, DirectionInfo dirInfo) {
		this.types = types;
		this.dirInfo = dirInfo;
	}
	
	public List<Expression> getTypes() {
		return this.types;
	}
	
	public DirectionInfo getDirInfo() {
		return this.dirInfo;
	}
	
	public boolean isAssociative() {
		return this.isAssociative;
	}
}
