package prooftool.util.objects;

import java.util.HashMap;
import java.util.Map;

import prooftool.backend.laws.Identifier;
import prooftool.backend.laws.Variable;

/**
 * A mapping from Identifiers to Variables. In compilers it's also called a
 * symbol table. We might rename it such some day. (Thanks refactoring.)
 * 
 * @author robert
 * 
 */
public class Dictionary {
	private Map<Identifier, Variable> dict;
	
	public Dictionary() {
		this.dict = new HashMap<Identifier, Variable>();
	}

	public Variable get(Identifier name) {
		if (!dict.containsKey(name)) {
			dict.put(name, new Variable(name));
		}
		
		return dict.get(name);
	}
	
	public Variable get(Variable name) {
		if (!dict.containsKey(name.getId())) {
			dict.put(name.getId(), name);
		}
		
		return dict.get(name.getId());
	}

	/**
	 * Put {@param local} into the dict.
	 * 
	 * @return old value for that Identifier, if there was any.
	 */
	public Variable put(Variable local) {
		Variable old = dict.get(local.getId());
		dict.put(local.getId(), local);
		return old;
	}

	public void reset(Identifier id, Variable old) {
		if (old == null) {
			dict.remove(id);
		} else {
			dict.put(id, old);
		}
	}
}
