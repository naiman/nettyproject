package prooftool.util.objects;

public class Interval implements Comparable<Interval> {
	private int start;
	private int end;
	
	public Interval(int start, int end) {
		this.start = start;
		this.end = end;
	}
	
	@Override
	public int compareTo(Interval arg0) {			
		return this.start - arg0.start;
	}
	
	@Override
	public String toString() {
		return start + "," + end;
	}
	
	public int getStart() {
		return this.start;
	}
	
	public int getEnd() {
		return this.end;
	}
	
	public void setEnd(int newEnd) {
		this.end = newEnd;
	}
}