package prooftool.util.objects;

public class ZoomInfo {
	private int start;
	private int end;
	private Path path;
	
	public ZoomInfo(int start, int end, int expnPart) {
		this.start = start;
		this.end = end;
		this.path = new Path(expnPart);
	}
	public ZoomInfo(int start, int end, Path expnPart) {
		this.start = start;
		this.end = end;
		this.path = expnPart;
	}
	
	@Override
	public String toString() {
		return start + "," + end;
	}
	
	public int getExpnPart() {
		return path.peek();
	}
	
	public Path getPath() {
		return path;
	}
	
	public int getStart() {
		return start;
	}
	
	public int getEnd() {
		return end;
	}
	
	public void setStart(int start) {
		this.start = start;
	}
	
	public void setEnd(int end) {
		this.end = end;
	}
	
	public void setPath(Path path) {
		this.path = path;
	}
}