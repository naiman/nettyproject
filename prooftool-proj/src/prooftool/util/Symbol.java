package prooftool.util;

import java.util.*;

import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Identifier;
import prooftool.proofrepresentation.ProofRepresentationBits.MonotonicContext;
import prooftool.util.objects.DirectionInfo;
import prooftool.util.objects.SymbolInfo;

/*
 * Contains the replacement ascii keywords for Unicode characters 
 * as well as some relations for direction of proof. 
 */

public class Symbol {

	public static final Map<String, String> symbolMap = new HashMap<String, String>();
	static {
		Symbol.symbolMap.put("\\times", "×"); // \times
		
		Symbol.symbolMap.put("!=", "≠");
		Symbol.symbolMap.put("\\not", "¬");  // \not
		Symbol.symbolMap.put("/\\", "∧");
		Symbol.symbolMap.put("\\/", "∨");
		Symbol.symbolMap.put("-->", "⇒");
		Symbol.symbolMap.put("<--", "⇐");

		Symbol.symbolMap.put("\\top", "⊤");     //  \top
		Symbol.symbolMap.put("\\bot", "⊥"); //  \bot

		Symbol.symbolMap.put("\\forall", "∀"); // \forall
		Symbol.symbolMap.put("\\exists", "∃");  // \exists
		Symbol.symbolMap.put("\\prod", "∏"); // \prod
		Symbol.symbolMap.put("\\sum", "∑");   //  \sum
		Symbol.symbolMap.put("\\sol", "§"); // \sol
		Symbol.symbolMap.put("\\dom", "∆"); // \dom
		Symbol.symbolMap.put(">>", "⟩");
		Symbol.symbolMap.put("<<", "⟨");
		Symbol.symbolMap.put("->", "→");
		Symbol.symbolMap.put("|->","↦");
		Symbol.symbolMap.put(">->","↣");
		Symbol.symbolMap.put("\\cent", "¢");  // \cent
		Symbol.symbolMap.put("\\rdot","·");  // \rdot
		Symbol.symbolMap.put("\\quote","‘");  // \rdot

		Symbol.symbolMap.put("\\ltri", "⊲"); //  \ltri
		Symbol.symbolMap.put("\\rtri", "⊳"); //  \rtri
		Symbol.symbolMap.put("\\len", "⟷"); // \len

		Symbol.symbolMap.put("\\in", "∈");		// \in
		Symbol.symbolMap.put("\\cup", "⋃");   // \cup
		Symbol.symbolMap.put("\\cap", "⋂"); // \cap
		Symbol.symbolMap.put("\\pow", "⚡"); // \
		
		Symbol.symbolMap.put("\\unlist", "♮");
		Symbol.symbolMap.put("\\oplus", "⊕");
		
		Symbol.symbolMap.put("\\infty","∞"); // \infty
		Symbol.symbolMap.put("\\check","✓"); // \check
		Symbol.symbolMap.put("`", "‘"); // `

		Symbol.symbolMap.put("\\subseteq", "⊆"); // \subseteq
		Symbol.symbolMap.put("\\supseteq", "⊇"); // \supseteq
		Symbol.symbolMap.put("\\subset", "⊂"); // \subset
		Symbol.symbolMap.put("\\supset", "⊃"); // \supset
		Symbol.symbolMap.put(">=", "≥");
		Symbol.symbolMap.put("<=", "≤");
		Symbol.symbolMap.put("^", "↑");
		Symbol.symbolMap.put("_", "↓");
		Symbol.symbolMap.put("\\prime", "′");
		Symbol.symbolMap.put("`", "′");
		
		//Stuff no longer used
//		sm.put("MIN","⋀");
//		sm.put("MAX","⋁");
//		sm.put("L_FRAME", "⟦");
//		sm.put("R_FRAME", "⟧");
//		sm.put("<---", "⇐⇐");
//		sm.put("--->", "⇒⇒");
//		sm.put("/","∕");
//		sm.put("-","−");
//		sm.put("*","∗");
//		sm.put(" ", " "); // \__U+200A
	}
	
	public static final Map<String, String> singleCharMap = new HashMap<String, String>();
	static {
		Symbol.singleCharMap.put(ExpressionUtils.prime,"'");
	}
	
	public static final Map<String, SymbolInfo> operatorMap = new HashMap<String, SymbolInfo>();
	static {
		/*NOTE: although all is not really a data type, it is needed in order to give the 
		 * operands of bunch operators some type. It can also be thought of as generic type.
		 */		
		String all = ExpressionUtils.ALL.toString();
		String[] basicTypes = new String[]{"nat","int","rat","bool","char",all};		
		String[] numberTypes = new String[]{"nat","int","rat"};
		String numberFunctionTypesString = ""; // two operand number operators
		String numberDirectionTypesString = ""; // number directions
		String numberQuantifierTypesString = ""; // number quantifiers
		String equalityTypesString = "";
		
		for (int i=0;i<numberTypes.length;i++) {
			String numberQuantifierType = "(" + all + "→" + numberTypes[i] + ")→" + numberTypes[i] + "\n";
			for (int j=0;j<numberTypes.length;j++) {
				String numberType = numberTypes[i] + "→" + numberTypes[j] + "→" + numberTypes[Math.max(i, j)] + "\n";
				String numberDirectionType = numberTypes[i] + "→" + numberTypes[j] + "→" + "bool\n";

				numberFunctionTypesString += numberType;
				numberDirectionTypesString += numberDirectionType;
			}
			numberQuantifierTypesString +=numberQuantifierType;
		}

		for (int i=0;i<basicTypes.length;i++) {
			String equalityType = basicTypes[i] + "→" + basicTypes[i] + 
			"→" + "bool\n";
			equalityTypesString += equalityType;
		}

		try {
			List<Expression> numberFunctionTypes = Main.parse(numberFunctionTypesString, false); // two operand number operators
			List<Expression> numberDirectionTypes = Main.parse(numberDirectionTypesString, false); // number directions
			List<Expression> numberQuantifierTypes = Main.parse(numberQuantifierTypesString, false); // number directions
			List<Expression> equalityTypes = Main.parse(equalityTypesString, false);
			
			Symbol.operatorMap.put("×", new SymbolInfo(numberFunctionTypes,true,null));			
			Symbol.operatorMap.put("−", new SymbolInfo(numberFunctionTypes,false,null));			
			Symbol.operatorMap.put("+", new SymbolInfo(numberFunctionTypes,true,null));
			
			//these two are a bit inaccurate. Division should be rat→(rat\0)→rat
			//and exponentiation is more complicated, since we need to be careful with
			//complex numbers. At the very least we can do type checking
			Symbol.operatorMap.put("↑", new SymbolInfo(numberFunctionTypes,false,null));
			Symbol.operatorMap.put("∕", new SymbolInfo(numberFunctionTypes,false,null));
//			sm.put("↑", new SymbolInfo(ExpressionUtils.parse("rat→nat→rat"),false,null));
			
			Symbol.operatorMap.put("≥", new SymbolInfo(numberDirectionTypes,false,
					new DirectionInfo("≤",new String[]{"=","≥",">"})));
			Symbol.operatorMap.put("≤", new SymbolInfo(numberDirectionTypes,false,
					new DirectionInfo("≥",new String[]{"=","≤","<"})));
			Symbol.operatorMap.put(">", new SymbolInfo(numberDirectionTypes,false,
					new DirectionInfo("<",new String[]{"=",">"})));
			Symbol.operatorMap.put("<", new SymbolInfo(numberDirectionTypes,false,
					new DirectionInfo(">",new String[]{"=","<"})));
			Symbol.operatorMap.put("=", new SymbolInfo(equalityTypes,true,
					new DirectionInfo("=",new String[]{"="})));	
			
			Symbol.operatorMap.put("≠", new SymbolInfo(equalityTypes,false,null));
			Symbol.operatorMap.put("¬", new SymbolInfo(Main.parse("bool→bool", false),false,null));
			Symbol.operatorMap.put("∧", new SymbolInfo(Main.parse("bool→bool→bool", false),true,null));
			Symbol.operatorMap.put("∨", new SymbolInfo(Main.parse("bool→bool→bool", false),true,null));
			
			Symbol.operatorMap.put(":=", new SymbolInfo(ExpressionUtils.parse(all + "→"+ all +"→bool"),false,null));
			Symbol.operatorMap.put(".", new SymbolInfo(ExpressionUtils.parse("bool→bool→bool"),true,null));
			
			Symbol.operatorMap.put("⇒", new SymbolInfo(Main.parse("bool→bool→bool", false),false,
					new DirectionInfo("⇐",new String[]{"=","⇒"})));
			Symbol.operatorMap.put("⇐", new SymbolInfo(Main.parse("bool→bool→bool", false),false,
					new DirectionInfo("⇒",new String[]{"=","⇐"})));
			
			Symbol.operatorMap.put("∀", new SymbolInfo(Main.parse("(" + all + "→bool)→bool", false),false,null));
			Symbol.operatorMap.put("∃", new SymbolInfo(Main.parse("(" + all + "→bool)→bool", false),false,null));
			Symbol.operatorMap.put("∏", new SymbolInfo(numberQuantifierTypes,false,null));
			Symbol.operatorMap.put("∑", new SymbolInfo(numberQuantifierTypes,false,null));
			Symbol.operatorMap.put("§", new SymbolInfo(Main.parse("(" + all + "→bool)→" + all + "", false),false,null));
			
			Symbol.operatorMap.put(Identifiers.set_brackets.toString(), new SymbolInfo(ExpressionUtils.parse(all + "→{" + all + "}"),false,null));
			Symbol.operatorMap.put(Identifiers.ifthenelse.toString(), new SymbolInfo(ExpressionUtils.parse("bool→" + all + "→" + all + "→" + all),false,null));
			
			Symbol.operatorMap.put(Identifiers.colon.toString(), new SymbolInfo(ExpressionUtils.parse(all + "→" + all + "→bool"),false,
					new DirectionInfo("::",new String[]{"=",":"})));
			Symbol.operatorMap.put(Identifiers.rev_colon.toString(), new SymbolInfo(ExpressionUtils.parse(all + "→" + all + "→bool"),false,
					new DirectionInfo(":",new String[]{"=","::"})));
			
			Symbol.operatorMap.put("#", new SymbolInfo(ExpressionUtils.parse("[" + all + "]→nat"),false,null));
			Symbol.operatorMap.put("¢", new SymbolInfo(ExpressionUtils.parse(all + "→nat"),false,null));
			Symbol.operatorMap.put(",..", new SymbolInfo(ExpressionUtils.parse("nat→nat→nat"),false,null));
			//somehow put in the type for list indexing
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static final Map<String, MonotonicContext[]> monotoneMap = new HashMap<String, MonotonicContext[]>();
	static {
		MonotonicContext pos = MonotonicContext.POSITIVE;
		MonotonicContext neg = MonotonicContext.NEGATIVE;
		MonotonicContext neut = MonotonicContext.NEUTRAL;
		
		Symbol.monotoneMap.put("×", new MonotonicContext[]{pos,pos});
//		sm.put("∕", new SymbolInfo(numberFunctionTypes,false,null));
		Symbol.monotoneMap.put("−", new MonotonicContext[]{pos,neg});			
		Symbol.monotoneMap.put("+", new MonotonicContext[]{pos,pos});
		Symbol.monotoneMap.put(".", new MonotonicContext[]{pos,pos});
		
		Symbol.monotoneMap.put("≥", new MonotonicContext[]{pos,neg});
		Symbol.monotoneMap.put("≤", new MonotonicContext[]{neg,pos});	
		Symbol.monotoneMap.put("<", new MonotonicContext[]{neg,pos});
		Symbol.monotoneMap.put(">", new MonotonicContext[]{pos,neg});
		Symbol.monotoneMap.put("=", new MonotonicContext[]{neut,neut});	
		
		Symbol.monotoneMap.put("≠", new MonotonicContext[]{neut,neut});
		Symbol.monotoneMap.put("¬", new MonotonicContext[]{neg});
		Symbol.monotoneMap.put("∧", new MonotonicContext[]{pos,pos});
		Symbol.monotoneMap.put("∨", new MonotonicContext[]{pos,pos});
		
		Symbol.monotoneMap.put("#", new MonotonicContext[]{pos});
		Symbol.monotoneMap.put("⟷", new MonotonicContext[]{pos});
		
		Symbol.monotoneMap.put("¢", new MonotonicContext[]{pos});
		Symbol.monotoneMap.put(",..", new MonotonicContext[]{neg, pos});
		Symbol.monotoneMap.put(":", new MonotonicContext[]{neg, pos});
		Symbol.monotoneMap.put("::", new MonotonicContext[]{pos,neg});
		
		Symbol.monotoneMap.put("⇒", new MonotonicContext[]{neg,pos});
		Symbol.monotoneMap.put("⇐", new MonotonicContext[]{pos,neg});
		
		Symbol.monotoneMap.put("∀", new MonotonicContext[]{pos});
		Symbol.monotoneMap.put("∃", new MonotonicContext[]{pos});
		Symbol.monotoneMap.put("∏", new MonotonicContext[]{pos});
		Symbol.monotoneMap.put("∑", new MonotonicContext[]{pos});
		Symbol.monotoneMap.put("§", new MonotonicContext[]{neut});
		
		Symbol.monotoneMap.put("if  then  else  end", new MonotonicContext[]{neut, pos, pos});
	}
	
	public static final Map<String, String[]> typeDirMap = new HashMap<String, String[]>();
	static {
		String[] numberDirections = new String[]{"<","≤","=","≥",">"};
		
		Symbol.typeDirMap.put("nat", numberDirections);
		Symbol.typeDirMap.put("int", numberDirections);
		Symbol.typeDirMap.put("rat", numberDirections);
		Symbol.typeDirMap.put("char", numberDirections);
		Symbol.typeDirMap.put("bool", new String[]{null,"⇒","=","⇐",null});
		Symbol.typeDirMap.put(ExpressionUtils.ALL.toString(), new String[]{null,":","=","::",null});
		Symbol.typeDirMap.put("{" + ExpressionUtils.ALL.toString() + "}", new String[]{null,"⊆","=","⊇",null});
	}
	
	// stores the reverse direction for ordering symbols
	public static final Map<String, String> reverseDirections = new HashMap<String, String>();
	static {
		Symbol.reverseDirections.put("=", "=");
		
		Symbol.reverseDirections.put("≥", "≤");
		Symbol.reverseDirections.put("≤", "≥");
		Symbol.reverseDirections.put(">=", "<=");
		Symbol.reverseDirections.put("<=", ">=");
		
		Symbol.reverseDirections.put("⇒", "⇐");
		Symbol.reverseDirections.put("⇐", "⇒");
		Symbol.reverseDirections.put("-->", "<--");
		Symbol.reverseDirections.put("<--", "-->");
		
		Symbol.reverseDirections.put(":", "::");
		Symbol.reverseDirections.put("::", ":");
		
		Symbol.reverseDirections.put("⊆", "⊇");
		Symbol.reverseDirections.put("⊇", "⊆");
		Symbol.reverseDirections.put("SUBSET", "SUPERSET");
		Symbol.reverseDirections.put("SUPERSET", "SUBSET");
	}
	
	
	public static final Map<Identifier, Identifier> reverseConnectives = new HashMap<Identifier, Identifier>();
	static {
		Symbol.reverseConnectives.put(Identifiers.eq, Identifiers.eq);
		Symbol.reverseConnectives.put(Identifiers.colon, Identifiers.rev_colon);
		Symbol.reverseConnectives.put(Identifiers.rev_colon, Identifiers.colon);
		Symbol.reverseConnectives.put(Identifiers.gt, Identifiers.lt);
		Symbol.reverseConnectives.put(Identifiers.lt, Identifiers.gt);
		Symbol.reverseConnectives.put(Identifiers.gte, Identifiers.lte);
		Symbol.reverseConnectives.put(Identifiers.lte, Identifiers.gte);
		
		Symbol.reverseConnectives.put(Identifiers.imp, Identifiers.rev_imp);
		Symbol.reverseConnectives.put(Identifiers.rev_imp, Identifiers.imp);
		
		Symbol.reverseConnectives.put(Identifiers.supset, Identifiers.subset);
		Symbol.reverseConnectives.put(Identifiers.subset, Identifiers.supset);
		
		Symbol.reverseConnectives.put(Identifiers.supseteq, Identifiers.subseteq);
		Symbol.reverseConnectives.put(Identifiers.subseteq, Identifiers.supseteq);
	}
	
	public static final List<String> programOps = new ArrayList<String>();
	static {
		Symbol.programOps.add(".");
		Symbol.programOps.add(":=");
		Symbol.programOps.add("ok");
		Symbol.programOps.add("||");
//		ops.add("var");
//		ops.add("ivar");
//		ops.add("chan");
//		ops.add("frame");
	}
	
	public static final List<String> numberTypes = new ArrayList<String>();
	static {
		Symbol.numberTypes.add("nat");
		Symbol.numberTypes.add("int");
		Symbol.numberTypes.add("rat");
	}
	
	public static final Map<Identifier, String> messyIdents = new HashMap<Identifier, String>();
	static {
		//scopes
		Symbol.messyIdents.put(Identifiers.chan, "");
		Symbol.messyIdents.put(Identifiers.var, "");
		Symbol.messyIdents.put(Identifiers.ivar, "");
		Symbol.messyIdents.put(Identifiers.let, "");
		Symbol.messyIdents.put(Identifiers.letp, "");
		Symbol.messyIdents.put(Identifiers.result, "");
		Symbol.messyIdents.put(Identifiers.frame, "");
	}
	
	public static final Map<Identifier, String> quantifiers = new HashMap<Identifier, String>();
	static {
		Symbol.quantifiers.put(Identifiers.forall, "");
		Symbol.quantifiers.put(Identifiers.exists, "");
		Symbol.quantifiers.put(Identifiers.sum, "");
		Symbol.quantifiers.put(Identifiers.prod, "");
		Symbol.quantifiers.put(Identifiers.max, "");
		Symbol.quantifiers.put(Identifiers.min, "");
		Symbol.quantifiers.put(Identifiers.sol, "");	
	}
	
	// stores which symbols are associative
	public static Map<String, String> assocMap = new HashMap<String, String>();
	static {
		Symbol.assocMap.put("×", null);
		Symbol.assocMap.put("+", null);
	
		Symbol.assocMap.put("∨", null);
		Symbol.assocMap.put("∧", null);
		
		Symbol.assocMap.put("⋃", null);
		Symbol.assocMap.put("⋂", null);
		
		Symbol.assocMap.put(",", null);
		Symbol.assocMap.put("‘", null);
		
		Symbol.assocMap.put(";", null);
		
		Symbol.assocMap.put(".", null);
		
		Symbol.assocMap.put("=", null);
	}
	
    /**
     * A list of negations (that is also technically laws). This is used to
     * to perform a deep negation of an expression. It is more of a hack
     * rather than a list of law. It is just a convenient method to store
     * a bunch of negations
     **/
	public static List<Expression> negations;
	static {		
		String s = "∀ ⟨a:bool→           ((¬a)  ≠  a       )⟩ \n"
				+ "∀ ⟨a:bool→∀ ⟨b:bool→ ((a∨b) ≠  (¬a∧¬b) )⟩⟩ \n"
				+ "∀ ⟨a:bool→∀ ⟨b:bool→ ((a∧b) ≠  (¬a∨¬b) )⟩⟩ \n"
				+ "∀ ⟨a:bool→∀ ⟨b:bool→ ((a⇒b) ≠  (a∧¬b)  )⟩⟩ \n"
				+ "∀ ⟨a:bool→∀ ⟨b:bool→ ((a⇐b) ≠  (¬a∧b)  )⟩⟩ \n" // +
																	// "∀ a,b:bool· ((a=b) ≠  (¬(a=b))) \n"
				+ "∀ ⟨a:bool→∀ ⟨b:bool→ ((a=b) ≠  (a≠b)   )⟩⟩ \n"
				+ "∀ ⟨a:bool→∀ ⟨b:bool→ ((a≠b) ≠  (a=b)   )⟩⟩ \n"
				+ "∀ ⟨a:bool→           (a     ≠  (¬a)    )⟩ \n";
		try {
			Symbol.negations = Main.parse(s, false);
			for (Expression e : Symbol.negations) {
				e.makeTypes(new ArrayList<Expression>());
			}
		} catch (Exception e) {
			System.err.println("Parse error during negation instantiation");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	
	/**
	 * Returns the unicode representation of the given ascii symbol. If not
	 * found, or is the same, the input symbol is return as is.
	 */
	public static String lookupUnicode(String symbol) {
		String rV = Symbol.symbolMap.get(symbol);
		return rV != null ? rV : symbol;
	}

	// Return the ASCII representation of a given Unicode symbol.
	public static String lookupAscii(String unicode) {		
		for (Map.Entry<String, String> pairs: Symbol.symbolMap.entrySet()) {
			String val = pairs.getValue();
			
			if (val.equals(unicode)) {
				return pairs.getKey();
			}
		}
		
		return unicode;
	}
	
	public static String[] getTypeDir(Expression type) {
		//return bunch direction is the type is of functional form
		if (type instanceof Application 
				&& ((Application)type).getFunId() == Identifiers.fun_arrow ){
			return new String[]{null,":","=","::",null};
		} else {
			return Symbol.typeDirMap.get(type.toString());
		}
	}
}
