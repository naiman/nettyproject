package prooftool.tool;

import javax.swing.UIManager;

import prooftool.backend.suggestions.FunctionApplier;
import prooftool.backend.suggestions.ProbabilityNormalizer;
import prooftool.backend.suggestions.ProgrammingSuggestor;
import prooftool.backend.suggestions.SpecialFunApplier;
import prooftool.backend.suggestions.SpecialTypeChecker;
import prooftool.backend.suggestions.UnifyingSuggestor;
import prooftool.backend.suggestions.heuristic.HeuristicSuggestion;
import prooftool.gui.UIBits;
import prooftool.gui.mainui.MainUI;
import prooftool.gui.prooftree.ProofTree;
import prooftool.proofrepresentation.ProofRepresentationBits;

/**
 * The main class that launches the tool. The design of this class is a
 * modification of the main class for a compiler used at U of T for csc488
 * (compilers) written by Dave Wortman.
 * 
 * @author lev
 * @author evm
 *
 */
public class Main {
	/*-----------------------------------------------------------*/
	/*--- Main Program ------------------------------------------*/
	/*-----------------------------------------------------------*/
	/**
	 * The main driver for the system.
	 *
	 * @param argv
	 *            an array of strings containing command line arguments.
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 */
	public static void main(String argv[]) throws NoSuchMethodException, SecurityException {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}
		System.setProperty("awt.useSystemAAFontSettings", "on");
		System.setProperty("swing.aatext", "true");
		
		//Set up UIBits
		UIBits.contextPanelLabel.setFont(UIBits.defaultFont);
		
		//Set up reflection for UndoManager
		UIBits.proofTreeAddNewLineProofMethod = ProofTree.class.getDeclaredMethod(UIBits.proofTreeAddNewLineProofName, UIBits.proofTreeAddNewLineProofParam);
		UIBits.proofTreeAddNewLineProofLineMethod = ProofTree.class.getDeclaredMethod(UIBits.proofTreeAddNewLineProofLineName, UIBits.proofTreeAddNewLineProofLineParam);
		UIBits.proofTreeDeleteLineMethod = ProofTree.class.getDeclaredMethod(UIBits.proofTreeDeleteLineName, UIBits.proofTreeDeleteLineParam);
		UIBits.proofTreeDeleteSubProofMethod = ProofTree.class.getDeclaredMethod(UIBits.proofTreeDeleteSubProofName, UIBits.proofTreeDeleteSubProofParam);
		UIBits.proofTreeSetBaseProofMethod = ProofTree.class.getDeclaredMethod(UIBits.proofTreeSetBaseProofName, UIBits.proofTreeSetBaseProofParam);
		UIBits.proofTreeZoomInMethod = ProofTree.class.getDeclaredMethod(UIBits.proofTreeZoomInName, UIBits.proofTreeZoomInParam);
		UIBits.proofTreeZoomOutMethod = ProofTree.class.getDeclaredMethod(UIBits.proofTreeZoomOutName, UIBits.proofTreeZoomOutParam);
		UIBits.proofTreeUndoZoomOutMultiLineMethod = ProofTree.class.getDeclaredMethod(UIBits.proofTreeUndoZoomOutMultiLineName, UIBits.proofTreeUndoZoomOutMultiLineParam);
		UIBits.proofTreeEditLineMethod = ProofTree.class.getDeclaredMethod(UIBits.proofTreeEditLineName, UIBits.proofTreeEditLineParam);
		
		// Set up ProofRepresentationBits
		ProofRepresentationBits.generators.add(new HeuristicSuggestion());
		ProofRepresentationBits.generators.add(new UnifyingSuggestor());
		ProofRepresentationBits.generators.add(new FunctionApplier());
		ProofRepresentationBits.generators.add(new SpecialTypeChecker());
		ProofRepresentationBits.generators.add(new ProgrammingSuggestor());
		ProofRepresentationBits.generators.add(new SpecialFunApplier());
		ProofRepresentationBits.generators.add(new ProbabilityNormalizer());
		
		//Launch gui
		UIBits.main = new MainUI();
	} 
}
