package prooftool.gui.tabs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import prooftool.gui.UIBits;
import prooftool.gui.mainui.NewTab;

/**
 * <p>This class acts as the panel for the tabs. It shows the tab name along
 * with a exit/close button.</p>
 */
public class TabPanelControl extends JPanel implements ActionListener {

	private static final long serialVersionUID = -1536696911726009067L;

	/**
	 * The tab contents.
	 */
	private NewTab tabContents;
	
	/**
	 * The label that displayes the tab name.
	 */
	private JLabel tabLabel;
	
	/**
	 * The name of this tabe.
	 */
	private String tabName;		
	
	/**
	 * When there is multiple instances opened, a suffix is given.
	 */
	private int suffix;

	/*+
	 * Create a new TabPanel with the given name.
	 */
	public TabPanelControl(String tabName) {
		super();

		JButton closeButton = new JButton();
		closeButton.setOpaque(false);
		closeButton.setIcon(UIBits.close);
		closeButton.setBorder(null);
		closeButton.setFocusable(false);
		closeButton.addActionListener(this);

		this.tabName = tabName;
		this.suffix = 0;
		this.tabLabel = new JLabel(this.tabName);

		this.add(this.tabLabel);
		this.add(closeButton);
		this.setOpaque(false);
		this.setBorder(UIBits.noBorderTabs);
	}

	/**
	 * Returns the name of this tab, appends the following:
	 * 
	 * <ul>
	 * <li>Unsaved Changes! - When isDirty returns true</li>
	 * <li>No Changes Detected - When isDirty returns false</li>
	 * </ul>
	 */
	@Override
	public String toString() {
		return this.getDisplayName()
				+ " - "
				+ (this.tabContents.getProofTree().isDirty() ? "Unsaved Changes!"
						: "No Changes Detected");
	}

	/**
	 * Returns the tab contents associated with this.
	 * 
	 * @return NewTab
	 */
	public NewTab getTabContents() {
		return this.tabContents;
	}

	/**
	 * Sets the contents of this tab.
	 * 
	 * @param tabContents NewTab
	 */
	public void setTabContents(NewTab tabContents) {
		this.tabContents = tabContents;
	}

	/**
	 * Returns the name of this tab with out any prefixes.
	 * 
	 * @return Name of this tab as a String
	 */
	public String getTabName() {
		return this.tabName;
	}

	/**
	 * Returns the name of this tab which is currently displayed.
	 * 
	 * @return Name of this tab as displayed in the GUI as a String
	 */
	private String getDisplayName() {
		return this.tabLabel.getText();
	}

	/**
	 * Updates the name of this tab. Calling this method will cause all the tabs opened to be updated.
	 * 
	 * @param newTabName The new name of this tab.
	 */
	public void updateTabName(String newTabName) {
		UIBits.main.updateTabName(this, newTabName);
		this.tabName = newTabName;

		this.tabLabel.setText(this.suffix == 0 ? this.tabName
				: this.tabName + " " + this.suffix);
	}

	/**
	 * If there are multiple instances of this tab opened, sets the instance number (suffix).
	 * 
	 * @param i The instance number of the tab.
	 */
	public void setSuffix(int i) {
		this.suffix = i;
		this.tabLabel.setText(this.tabLabel.getText() + " " + this.suffix);
	}

	/**
	 * Returns the instance number of this tab.
	 * 
	 * @return Instance number as a int.
	 */
	public int getSuffix() {
		return this.suffix;
	}

	/**
	 * Since there is only one source where an ActionEvent can come from (closeButton),
	 * all this will do will cause the MainUI to start to close this document.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		UIBits.main.closeDocuement(this);
	}
}
