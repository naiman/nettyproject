package prooftool.gui.tabs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ScrollPaneConstants;

import net.miginfocom.swing.MigLayout;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

import prooftool.gui.UIBits;
import prooftool.gui.elements.NettyToolBar;

/*
 * @author evm
 */
public class CodeGen extends JPanel implements ActionListener {

	private static final long serialVersionUID = 8755432755498038679L;
	private static final int save = 0;
	private static final int run = 1;
	
	private RSyntaxTextArea codeArea;
	private RSyntaxTextArea outputArea;

	public CodeGen() {
		NettyToolBar toolbar = new NettyToolBar(UIBits.CodePanelToolBarImageFiles, UIBits.CodePanelToolBarTextIcons, UIBits.CodePanelToolBarToolTips, this);
		
		this.codeArea = new RSyntaxTextArea();
		this.codeArea.setLineWrap(true);
		this.codeArea.setAntiAliasingEnabled(true);
		this.codeArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CPLUSPLUS);

		this.outputArea = new RSyntaxTextArea();
		this.outputArea.setLineWrap(true);
		this.outputArea.setAntiAliasingEnabled(true);
		this.outputArea.setEditable(false);
		this.outputArea.setHighlightCurrentLine(false);

		JScrollPane outScroll = new JScrollPane(outputArea);
		outScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		RTextScrollPane codeScroll = new RTextScrollPane(codeArea);
		JSplitPane vSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
		codeScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

		vSplit.setLeftComponent(codeScroll);
		vSplit.setRightComponent(outScroll);
		vSplit.setOneTouchExpandable(true);
		vSplit.setDividerLocation(400);

		this.setLayout(new MigLayout("ins 0"));
		this.add(toolbar, "wrap, pushx, growx");
		this.add(vSplit, "push, grow");
		this.setSize(400, 600);
		this.setVisible(true);
	}
	
	public void setCode(String code) {
		this.codeArea.setText(code);
	}

	private void run() {
		// TODO be sure to erase any old code files
		try {
			save(UIBits.codePath);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Error: could not save code file.");
			return;
		}

		try {
			if (UIBits.codeCompile == null) {
				System.err.println("Compilation failed; could not find the file for compilation.");
				return;
			}

			Process compilep = Runtime.getRuntime().exec(UIBits.codeCompile);
			compilep.waitFor();

			if (compilep.exitValue() != 0) {
				BufferedReader in = new BufferedReader(new InputStreamReader(compilep.getErrorStream()));
				String line = null;
				StringBuilder out = new StringBuilder();
				System.err.println("Compilation failed: \n");
				while ((line = in.readLine()) != null) {
					out.append(line);
					out.append("\n");
					System.err.println(line);
				}

				JOptionPane.showMessageDialog(this, "Compilation failed: \n" + out.toString());
			}

			Process execute = Runtime.getRuntime().exec(UIBits.codePath + UIBits.separator + UIBits.codeOutFileName);
			// get its output stream
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						execute.getInputStream()));
				String line = null;
				StringBuilder out = new StringBuilder();

				while ((line = in.readLine()) != null) {
					out.append(line);
				}

				this.outputArea.setText(out.toString());
			} catch (IOException e0) {
				e0.printStackTrace();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (InterruptedException e2) {
			e2.printStackTrace();
		}
	}

	private void save(String path) throws IOException {
		File file;
		if (path == null) {
			JFileChooser fc = new JFileChooser("");
			int returnVal = fc.showSaveDialog(this);

			if (returnVal != JFileChooser.APPROVE_OPTION) {
				return;
			}
			file = fc.getSelectedFile();

		} else {
			file = new File(path + UIBits.separator + UIBits.codeFileName);
		}

		BufferedWriter out = new BufferedWriter(new FileWriter(file));
		out.write(this.codeArea.getText());
		out.close();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {			
		switch (Integer.parseInt(e.getActionCommand())) {
		case CodeGen.save:
			try { 
				this.save(null);
			} catch (IOException ex) {
				ex.printStackTrace();
				JOptionPane.showMessageDialog(null, "Error: could not save code file.", "Error Saving", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			break;
		case CodeGen.run:
			this.run();
			break;	            
		default:
			break;
		}
	}
}
