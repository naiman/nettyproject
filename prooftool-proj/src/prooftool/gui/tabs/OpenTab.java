package prooftool.gui.tabs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import prooftool.gui.UIBits;
import prooftool.proofrepresentation.Proof;

/**
 * <p>This deals with opening new proofs.</p>
 */
public class OpenTab extends JFileChooser implements ActionListener {
	private static final long serialVersionUID = -8416737682663359198L;
	private File defaultFolder;

	/**
	 * <p>Creates a new OpenTab that allows the user to browse the file-system,
	 * to open new Netty Proofs.</p>
	 */
	public OpenTab() {
		super();

		this.setApproveButtonText(UIBits.openTabApproveButtonText);
		this.setApproveButtonToolTipText(UIBits.openTabApproveButtonToolTip);

		this.addActionListener(this);
		this.setFileSelectionMode(JFileChooser.FILES_ONLY);
		this.setMultiSelectionEnabled(false);
		this.defaultFolder = this.getCurrentDirectory();
	}

	/**
	 * <p>Responds to the user input:
	 * <ul>
	 * <li>User hits reset button: returns the user to the default folder.</li>
	 * <li>User hits the accept button: Netty attempts to open the file into a new tab. If it fails, displays an error message.</li>
	 * </ul>
	 * </p>
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		String action = arg0.getActionCommand();

		if (action.equalsIgnoreCase(UIBits.openTabActionCommand[0])) {
			this.setCurrentDirectory(this.defaultFolder);
		} else if (action.equalsIgnoreCase(UIBits.openTabActionCommand[1])) {
			try {
				UIBits.main.openDocument(new Proof(this.readFileContents()), this.getSelectedFile().getName());
			} catch (Exception e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(this, "Load error!\n" + e.toString());
			}
		}
	}

	/**
	 * Attempts to read the selected file and returns it as a String.
	 * 
	 * @return A Netty proof as a String.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private String readFileContents() throws FileNotFoundException,
			IOException {
		InputStream in = new FileInputStream(this.getSelectedFile());
		byte[] b = new byte[(int) this.getSelectedFile().length()];
		int len = b.length;
		int total = 0;

		while (total < len) {
			int result = in.read(b, total, len - total);
			if (result == -1) {
				break;
			}
			total += result;
		}

		in.close();
		return new String(b, Charset.forName("UTF-8"));
	}
}
