package prooftool.gui.tabs;

import java.util.Map;

import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import prooftool.gui.UIBits;
import prooftool.util.Symbol;

/**
 * <p>This tab displays usage information.</p>
 * 
 * Credit: http://alvinalexander.com/blog/post/jfc-swing/how-create-simple-swing-html-viewer-browser-java
 * 
 * @author maghari1
 * 
 */
public class HelpTab extends JScrollPane {
	private static final long serialVersionUID = 3457603762305456450L;

	/**
	 * Creates a new HelpTab. 
	 */
	public HelpTab() {
		super();
		JEditorPane toDisplay = new JEditorPane();
		
		toDisplay.setEditable(false);
		
		HTMLEditorKit kit = new HTMLEditorKit();
		toDisplay.setEditorKit(kit);
		this.generateStyleSheet(kit);
		
		Document doc = kit.createDefaultDocument();
		toDisplay.setDocument(doc);
		toDisplay.setText(this.generateHTML());
		
		this.setViewportView(toDisplay);
	}
	
	private String generateHTML() {
		//Generate the table for the unicode table
		String unicodeTableHtmlString = "<table style=\"width=100%\">\n"
										+ "<tr>\n"
										+ "\t<td>What to Type</td>\n"
										+ "\t<td>Output</td>\n"
										+ "</tr>\n";
		
		Map<String, String> symbolMap = Symbol.symbolMap;
		for (Map.Entry<String, String> mapping : symbolMap.entrySet()) {
			String key = mapping.getKey();
			String value = mapping.getValue();
			
			key = key.replaceAll("<", "&lt;");
			key = key.replaceAll(">", "&gt;");
			
			unicodeTableHtmlString += "\t<tr>\n"
								+ "\t\t<td>" + key + "</td>\n"
								+ "\t\t<td>" + value + "</td>\n"
								+ "\t<tr>\n";
		}
		
		unicodeTableHtmlString += "</table>\n";

        // create some simple html as a string
        String htmlString = "<html>\n"
                          + "<body>\n"
                          + "<h2>Symbol Lookup Table</h2>\n"
                          + "<p>Here the ways to type unicode characters in Netty.</p>\n"
                          + "<br>"
                          + unicodeTableHtmlString
                          + "</body>\n"
        				  + "</html>\n";
        
        return htmlString;
	}
	
	private void generateStyleSheet(HTMLEditorKit kit) {
		// add some styles to the html
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule("body {color:#000; font-family:" + UIBits.lawLibraryFont.getFamily() + "; margin: 4px; }");
        styleSheet.addRule("h1 {color: blue;}");
        styleSheet.addRule("h2 {color: #ff0000;}");
        styleSheet.addRule("pre {font : 10px monaco; color : black; background-color : #fafafa; }");
	}
}
