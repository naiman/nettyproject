package prooftool.gui.elements;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import prooftool.gui.UIBits;

/**
 * 
 * @author evm
 *
 * <p>This is the JPanel that will hold the suggestions. 
 * This assumes the parent element is a JSplitPane.</p>
 */
public class NettySuggestionsPanel extends JPanel {

	private static final long serialVersionUID = -4438311481735112672L;

	public NettySuggestionsPanel() {
		super();
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setBackground(Color.WHITE);
		this.add(UIBits.nettySuggestionsPanelDefaultLabel);
	}
	
	@Override
	public Dimension getPreferredSize() {		
		return new Dimension((int) this.getParent().getSize().getWidth(), (int) super.getPreferredSize().getHeight());
	}
}
