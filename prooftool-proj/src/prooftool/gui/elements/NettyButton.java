package prooftool.gui.elements;

import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import prooftool.gui.UIBits;

/**
 * 
 * @author evm
 * 
 * <p>This NettyButton is based on JButton, but allows for easy access to
 * set icon, alt text, etc... at the init level.</p>
 * 
 * <p>icon - path to the icon to be used<br>
 * altText - text to use if icon does not exist<br>
 * toolTip - text to appear when mouse hovers over button<br>
 * action - action command to be used (used with actionL)<br>
 * actionL - actionListener to be added to this button (should be used with action)</p>
 *
 */
public class NettyButton extends JButton {
	
	private static final long serialVersionUID = 6043696800744753310L;
	private File iconURL;
	private String altText;
	
	/**
	 * Initializes this button with a icon and altText.
	 * 
	 * @param icon 
	 * @param altText
	 */
	public NettyButton(String icon, String altText) {	
		super();
		this.init(icon, altText, null, null, null);
	}
	
	/**
	 * Initializes this button with a icon, altText, and toolTip.
	 * 
	 * @param icon
	 * @param altText
	 * @param toolTip
	 */
	public NettyButton(String icon, String altText, String toolTip) {
		super();
		this.init(icon, altText, toolTip, null, null);
	}
	
	
	/**
	 * Initializes this button with a icon, altText, toolTip, and action. 
	 * 
	 * @param icon
	 * @param altText
	 * @param toolTip
	 * @param action
	 */
	public NettyButton(String icon, String altText, String toolTip, String action) {
		super();
		this.init(icon, altText, toolTip, action, null);
	}
	
	
	/**
	 * Initializes this button with a icon, altText, toolTip, action, and actionL.
	 * 
	 * @param icon
	 * @param altText
	 * @param toolTip
	 * @param action
	 * @param actionL
	 */
	public NettyButton(String icon, String altText, String toolTip, String action, ActionListener actionL) {
		super();
		this.init(icon, altText, toolTip, action, actionL);
	}
	
	/**
	 * Backend of this class, initializes this button.
	 * 
	 * @param icon
	 * @param altText
	 * @param toolTip
	 * @param action
	 * @param actionL
	 */
	private void init(String icon, String altText, String toolTip, String action, ActionListener actionL) {
		this.iconURL = new File(UIBits.imgPath + icon);
		this.altText = altText;
		
		//If there is an icon use it otherwise use the altText
		if (this.iconURL == null) this.setText(this.altText);
		else this.setIcon(new ImageIcon(this.iconURL.toString()));
		
		//If there is a tooltip use it
		if (toolTip != null) this.setToolTipText(toolTip);
		
		//Same thing with action
		if (action != null) this.setActionCommand(action);
		if (actionL != null) this.addActionListener(actionL);
	}
}
