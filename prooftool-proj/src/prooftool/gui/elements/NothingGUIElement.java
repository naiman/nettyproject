package prooftool.gui.elements;

import java.awt.Component;

/**
 * 
 * @author evm
 * 
 *  <p>Since when the user clicks the "+" tab, the tab does not actually open,
 *  we can save memory and cputime by making a new GUI element that does
 *  nothing or renders nothing.</p>
 *
 */
public class NothingGUIElement extends Component {
	private static final long serialVersionUID = 1039820619956581357L;
}
