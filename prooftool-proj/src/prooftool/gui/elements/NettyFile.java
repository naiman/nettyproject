package prooftool.gui.elements;

import java.io.File;

public class NettyFile {

	private File file;
	
	public NettyFile(File file) {
		this.file = file;
	}
	
	public File getFile() {
		return this.file;
	}
	
	@Override
	public String toString() {
		return this.file.getName();
	}	
}
