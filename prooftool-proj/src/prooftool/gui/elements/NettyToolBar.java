package prooftool.gui.elements;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JToolBar;

/**
 * 
 * @author evm
 * 
 * <p>NettyToolbar will build the toolbar given a set of input </p>
 * 
 * <p>ToolbarImageFiles - an string array that contains all the file paths for the icons being used<br>
 * ToolbarTextIcons - an string array that contains all the alt text string for the buttons in-case some of the icons can not be loaded<br>
 * ToolbarToolTips - an string array that contains all the tooltips for the buttons in this toolbar<br>
 * al - the ActionListener to apply on all the buttons generated in this toolbar</p>
 *
 */
public class NettyToolBar extends JToolBar {

	private static final long serialVersionUID = 3585280369245282804L;
	private ArrayList<NettyButton> buttons;
	
	/**
	 * Given ToolbarImageFiles, ToolbarTextIcons, ToolbarToolTips, and al, generates a toolbar with all the buttons added.
	 * 
	 * @param ToolbarImageFiles
	 * @param ToolbarTextIcons
	 * @param ToolbarToolTips
	 * @param al
	 */
	public NettyToolBar(String[] ToolbarImageFiles, String[] ToolbarTextIcons, String[] ToolbarToolTips, ActionListener al) {
		super();
		this.buttons = new ArrayList<NettyButton>();
		this.setFloatable(false);
		this.setFloatable(false);
		
		for (int i = 0; i != ToolbarImageFiles.length; i++) {
			NettyButton toolBarButton = new NettyButton(
					ToolbarImageFiles[i], // Image
					ToolbarTextIcons[i], // Alt Text
					ToolbarToolTips[i], // Tooltips
					Integer.toString(i), // Action Command
					al); // Action Listener
			
			this.buttons.add(toolBarButton);
			this.add(toolBarButton);
		}
	}
	
	/**
	 *  Returns all the buttons in this toolbar.
	 * 
	 * @return ArrayList<NettyButton>
	 */
	public ArrayList<NettyButton> getButtons() {
		return this.buttons;
	}
}
