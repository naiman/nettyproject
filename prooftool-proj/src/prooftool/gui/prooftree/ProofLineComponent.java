package prooftool.gui.prooftree;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.JTextComponent;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

import prooftool.backend.laws.Expression;
import prooftool.gui.Settings;
import prooftool.gui.UIBits;
import prooftool.proofrepresentation.ProofLine;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.justification.ZoomInJustification;
import prooftool.util.ProofRep;
import prooftool.util.objects.Path;
import prooftool.util.objects.ZoomInfo;

/**
 * @author xiang
 * @author evm
 *
 * <p>This is the view-only element that displays prooflines.</p>
 */
public class ProofLineComponent extends JPanel {

	private static final long serialVersionUID = 4736011362389397441L;
	
	/**
	 * zoom in adapter enable zoom in for the proof text
	 */
	private final class ZoomInListener extends MouseAdapter {
		
		@Override
		public void mouseClicked(MouseEvent e) {
			Map<Integer, ZoomInfo> zoomMap = Settings.get_Settings().isZoomType() ? spl.getDeepZoomMap(): spl.getZoomMap();

			if (e.getSource() instanceof JTextComponent) {
				JTextComponent area = (JTextComponent) e.getSource();

				if (e.getButton() == MouseEvent.BUTTON1) {
					int offset = area.getFontMetrics(UIBits.defaultFont).getWidths()[0] / 2;
					int loc = area.viewToModel(new Point(e.getX() - offset, e.getY()));
					if (zoomMap.containsKey(loc)) {
						ProofLineComponent.this.proofTree.zoomIn(spl, zoomMap.get(loc).getPath(), false);
					}
				}
			}
		}

		@Override
		/**
		 * update the highlight in the text area as the user moves the mouse inside the text area
		 */
		public void mouseMoved(MouseEvent e) {						
			Map<Integer, ZoomInfo> zoomMap = Settings.get_Settings().isZoomType() ? spl.getDeepZoomMap(): spl.getZoomMap();

			if (e.getSource() instanceof JTextComponent) {

				JTextComponent area = (JTextComponent) e.getSource();
				this.removeZoomHighlights(area);

				// temporary hack to have the selection over a character
				int offset = area.getFontMetrics(UIBits.defaultFont).getWidths()[0] / 2;
				int loc = area.viewToModel(new Point(e.getX() - offset, e.getY()));

				if (zoomMap.containsKey(loc)) {
					ZoomInfo zi = ProofRep.calcOffsets(spl.getExpn(), zoomMap.get(loc).getPath());
					this.highlight(area, zi.getStart(), zi.getEnd(), new ZoomHighlightPainter(Color.orange));
				}
			}
		}

		@Override
		public void mouseExited(MouseEvent e) {
			if (e.getSource() instanceof JTextComponent) {
				JTextComponent area = (JTextComponent) e.getSource();
				this.removeZoomHighlights(area);
			}
		}

		private void removeZoomHighlights(JTextComponent textComp) {
			Highlighter hilite = textComp.getHighlighter();
			Highlighter.Highlight[] hilites = hilite.getHighlights();

			for (int i = 0; i < hilites.length; i++) {
				if (hilites[i].getPainter() instanceof ZoomHighlightPainter) {
					hilite.removeHighlight(hilites[i]);
				}
			}
		}

		public void highlight(JTextComponent textComp, int start, int end,
				ZoomHighlightPainter hlPainter) {
			Highlighter hilite = textComp.getHighlighter();

			try {
				hilite.addHighlight(start, end, hlPainter);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		}

		class ZoomHighlightPainter extends
				DefaultHighlighter.DefaultHighlightPainter {
			public ZoomHighlightPainter(Color color) {
				super(color);
			}
		}
	}
	
	private ProofTree proofTree;
	private ProofLine spl;
	private JLabel dirLabel;
	private RSyntaxTextArea textArea;
	private ZoomInListener zil;
	private JPanel justPanel;
	private JLabel justLabel;
	private JLabel iconLabel;
	private Object zoomInTag;
	private Object zoomOutTag;
	
	private boolean inUse;

	public ProofLineComponent(ProofTree proofTree, ProofLine spl) {
		this.proofTree = proofTree;
		this.inUse = false;
		this.zoomInTag = null;
		this.zoomOutTag = null;
		
		this.spl = spl;
		this.spl.initZoomMap();

		// should depend on window dimension
		this.setBackground(UIBits.backgroundColor);

		// create and add a direction label
		this.dirLabel = new JLabel();
		this.dirLabel.setVerticalAlignment(SwingConstants.TOP);
		this.dirLabel.setFont(UIBits.defaultFont);
		this.dirLabel.setBackground(UIBits.backgroundColor);

		// text panel for displaying proof line
		this.textArea = new RSyntaxTextArea(1, 25);
		this.textArea.setEditable(false);
		this.textArea.setLineWrap(true);
		this.textArea.setWrapStyleWord(false);
		this.textArea.setAntiAliasingEnabled(true);
		this.textArea.setFont(UIBits.defaultFont);
		this.textArea.setHighlightCurrentLine(false);
		this.textArea.setTabSize(3);
		this.textArea.setBackground(UIBits.backgroundColor);
		this.textArea.setBorder(UIBits.noBorder);

		// justification panel
		this.iconLabel = new JLabel();
		this.iconLabel.setHorizontalAlignment(JLabel.CENTER);
		this.justLabel = new JLabel();
		this.justLabel.setVerticalAlignment(SwingConstants.BOTTOM);
		this.justPanel = new JPanel(new GridLayout(2, 1));
		this.justPanel.setOpaque(true);
		this.justPanel.setBackground(UIBits.backgroundColor);
		this.justPanel.add(this.iconLabel);
		this.justPanel.add(this.justLabel);

		// set layout of current panel (must be done before adding
		// sub-components to the panel)
		this.setLayout(new BorderLayout());

		this.add(dirLabel, BorderLayout.LINE_START);
		this.add(textArea, BorderLayout.CENTER);
		this.add(justPanel, BorderLayout.LINE_END);
		
		this.dirLabel.setText(this.spl.getDirection().toString());
		this.textArea.setText(this.spl.getExpn().toString().replace("·", "·\n"));
		this.setJustification(this.spl.getJustification());

		this.zil = new ZoomInListener();
	}

	/**
	 * probably need this
	 */
	private void setJustification(Justification just) {		
		//Only replace as needed!
		if (this.justLabel.getText().equals(just.toString())) {
			return;
		}
		
		this.justLabel.setText(just.toString());

		if (just.isInvalid()) {
			this.iconLabel.setIcon(UIBits.warnPic);
			this.iconLabel.setToolTipText("There is no known justification for the next line");
		} else {
			this.iconLabel.setIcon(null);
		}

		if (just.getLaw() != null) {
			justPanel.setToolTipText(((Expression) just.getLaw()).getLawBody().toString());
		}
	}

	protected void setWidth(int maxWidth) {
		Dimension labelDim = dirLabel.getPreferredSize();
		Dimension justDim = justPanel.getPreferredSize();

		dirLabel.setMaximumSize(labelDim);
		justPanel.setMaximumSize(justDim);

		int textWidth = maxWidth - labelDim.width - justDim.width - 2;
		textArea.setSize(new Dimension(textWidth, textArea.getHeight()));

		Dimension textDim = textArea.getPreferredSize();
		textArea.setSize(textDim);

		int maxHeigh = Math.max(labelDim.height, Math.max(justDim.height, textDim.height)) + 2;
		
		this.setPreferredSize(new Dimension(maxWidth, maxHeigh));
	}

	/**
	 * called when the user clicks on a sub-expression to zoom in
	 */
	protected void addCornerPainters() {
		try {
			if (spl.getJustification() instanceof ZoomInJustification) {
				Path path = ((ZoomInJustification) spl.getJustification())
						.getPath();
				assert path != null;
				ZoomInfo zi = ProofRep.calcOffsets(spl.getExpn(), path);
				this.zoomInTag = this.textArea.getHighlighter().addHighlight(
						zi.getStart(),
						zi.getEnd(),
						new CornerPainter(Color.red, false, zi.getStart(), zi
								.getEnd()));
			} else {
				Path outPath = spl.getZoomOutPath();
				if (outPath != null && outPath.get(0) != -1) {
					ZoomInfo zi = ProofRep.calcOffsets(spl.getExpn(), outPath);
					this.zoomOutTag =	this.textArea.getHighlighter().addHighlight(
							zi.getStart(),
							zi.getEnd(),
							new CornerPainter(Color.BLUE, false, zi.getStart(),
									zi.getEnd()));
				}
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * clears the zoom-in highlight
	 */
	protected void removeZoominHightlight() {
		if (this.zoomInTag != null) {
			this.textArea.getHighlighter().removeHighlight(this.zoomInTag);
		}
	}
	
	/**
	 * clears the zoom-out highlight
	 */
	protected void removeZoomoutHightlight() {		
		if (this.zoomOutTag != null) {
			this.textArea.getHighlighter().removeHighlight(this.zoomOutTag);
		}
	}

	/**
	 * enable the mouse listener from the text area to enable zooming
	 */
	protected void startZoomingProof() {
		if (this.inUse) {
			return;
		}
		
		this.inUse = true;
		this.textArea.addMouseListener(this.zil);
		this.textArea.addMouseMotionListener(this.zil);
	}

	/**
	 * remove the mouse listener from the text area to disable zooming
	 */
	protected void stopZoomingProof() {
		if (!this.inUse) {
			return;
		}
				
		this.inUse = false;		
		this.textArea.removeMouseListener(this.zil);
		this.textArea.removeMouseMotionListener(this.zil);
		
		this.zil.removeZoomHighlights(this.textArea);
	}
	
	protected void refresh() {
		//Replace text as needed
		if (!this.dirLabel.getText().equals(this.spl.getDirection().toString())) {
			this.dirLabel.setText(this.spl.getDirection().toString());
		}
		
		String expn = this.spl.getExpn().toString().replace("·", "·\n");
		if (!this.textArea.getText().equals(expn)) {
			this.textArea.setText(expn);
		}
		
		this.setJustification(spl.getJustification());
		this.addCornerPainters();
	}
}