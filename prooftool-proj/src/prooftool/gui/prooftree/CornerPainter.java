package prooftool.gui.prooftree;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;

import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;

import prooftool.gui.UIBits;

/**
 * 
 * @author evm
 * @author xiang
 * 
 * <p> This is the renderer for the corner painters. It shows up with the user zooms in (in red) or zooms out (in blue) of a subproof. </p>
 *
 */
public class CornerPainter extends DefaultHighlighter.DefaultHighlightPainter {
	private boolean top;
	private int start;
	private int end;
	 
	/**
	 * <p> This sets up a renderer with the colour color, postion top, starting from position start and ending at postion end. </p>
	 * 
	 * @param color Colour of the corner painter
	 * @param top If true, renders above the text, otherwise renders below the text.
	 * @param start Position in text where to start
	 * @param end Position in text where to end
	 */
	protected CornerPainter(Color color, boolean top, int start, int end) {
		super(color);
		this.top = top;
		this.start = start;
		this.end = end;
	}

	/**
	 * <p> Paints a portion of a highlight. </p>
	 *
	 * @param  g the graphics context
	 * @param  offs0 the starting model offset >= 0
	 * @param  offs1 the ending model offset >= offs1
	 * @param  bounds the bounding box of the view, which is not
	 *	       necessarily the region to paint.
	 * @param  c the editor
	 * @param  view View painting for
	 * @return region drawing occured in
	 */
	@Override
	public Shape paintLayer(Graphics g, int offs0, int offs1, Shape bounds, JTextComponent c, View view) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		Rectangle r = this.getDrawingArea(offs0, offs1, bounds, view);
		//TODO look up start/end coordinates of subexpression highlighted and only paint
		// corner if its the first/last line respectively
		if (r == null) {
			return null;
		}
		
		//  Do your custom painting
		Color color = getColor();
		g2d.setColor(color == null ? c.getSelectionColor() : color);

		//  Code is the same as the default highlighter except we use drawRect(...)
		if (this.top) {
			this.drawTopCorners(g2d, r, offs0, offs1);
		} else {
			this.drawBottomCorners(g2d, r, offs0, offs1);
		}
		
		// Return the drawing area
		return r;
	}
	
	private void drawTopCorners(Graphics g, Rectangle r, int offs0, int offs1) {
		int sizex = Math.min(r.width-1, UIBits.cornerPaintSize);
		int sizey = Math.min(r.height-1, UIBits.cornerPaintSize);
		if (offs0 == start) {
			g.drawLine(r.x, r.y, r.x, r.y + UIBits.cornerPaintSize);
			g.drawLine(r.x, r.y, r.x + UIBits.cornerPaintSize, r.y);
		}
		if (offs1 == end) {
			g.drawLine(r.x + r.width - 1, r.y, r.x + r.width -1, r.y + sizey);
			g.drawLine(r.x + r.width - 1, r.y, r.x + r.width -1 - sizex, r.y);
		}
	}
	
	private void drawBottomCorners(Graphics g, Rectangle r, int offs0, int offs1) {
		int sizex = Math.min(r.width-1, UIBits.cornerPaintSize);
		int sizey = Math.min(r.height-1, UIBits.cornerPaintSize);
		if (offs0 == start) {
			g.drawLine(r.x, r.y + r.height -1, r.x, r.y + r.height -1 - sizey);
			g.drawLine(r.x, r.y + r.height -1, r.x + sizex, r.y + r.height -1);
		}
		if (offs1 == end) {
			g.drawLine(r.x + r.width - 1, r.y + r.height -1, r.x + r.width - 1, r.y + r.height -1 - sizey);
			g.drawLine(r.x + r.width - 1, r.y + r.height -1, r.x + r.width - 1 - sizex, r.y + r.height -1);
		}
	}

	private Rectangle getDrawingArea(int offs0, int offs1, Shape bounds, View view) {
		// Contained in view, can just use bounds.

		if (offs0 == view.getStartOffset() && offs1 == view.getEndOffset()) {
			return (bounds instanceof Rectangle) ? (Rectangle)bounds : bounds.getBounds();
		} else {
			// Should only render part of View.
			try {
				// --- determine locations ---
				Shape shape = view.modelToView(offs0, Position.Bias.Forward, offs1,Position.Bias.Backward, bounds);
				Rectangle r = (shape instanceof Rectangle) ? (Rectangle)shape : shape.getBounds();
				return r;
			} catch (BadLocationException e) {
				// can't render
			}
		}
		// Can't render
		return null;
	}
}