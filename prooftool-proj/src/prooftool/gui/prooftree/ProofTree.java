package prooftool.gui.prooftree;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import prooftool.backend.Step;
import prooftool.backend.direction.Direction;
import prooftool.backend.direction.ExpnDirection;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Variable;
import prooftool.gui.UIBits;
import prooftool.gui.mainui.NewTab;
import prooftool.gui.undomanager.UndoManager;
import prooftool.proofrepresentation.Proof;
import prooftool.proofrepresentation.ProofElement;
import prooftool.proofrepresentation.ProofLine;
import prooftool.proofrepresentation.ProofOptions;
import prooftool.proofrepresentation.ProofRepresentationBits;
import prooftool.proofrepresentation.ProofRepresentationBits.MonotonicContext;
import prooftool.proofrepresentation.ProofRepresentationBits.ProofSubtype;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.justification.ZoomInJustification;
import prooftool.proofrepresentation.suggestion.Suggestion;
import prooftool.util.Main;
import prooftool.util.ProofRep;
import prooftool.util.Symbol;
import prooftool.util.objects.Path;

/**
 * 
 * @author evm
 * @author xiang
 * 
 * <p> This displays the proof in a tree-like structure. <br>
 * 
 * This component also keeps track of a few things:
 * <ul>
 * 	<li>proof - root node of the proof</li>
 * 	<li>selectedLine - the selected proofLine element, can be null if nothing is selected</li>
 * 	<li>editorLine - keeps track of the next line to be inputed in the proof</li>
 * 	<li>dirty - keeps track if the proof has changed since the last saved point</li>
 * 	<li>editing - keeps track if the user is currently editing a selected line</li>
 * 	<li>undoManager - the undoManager that allows the user to undo and redo verious actions</li>
 * 	<li>tab - which tab this proof belongs to</li>
 * </ul><br>
 * 
 *  In addition, to render the proof, this also deals with verious interactions with the proof (ex. addNewLine, deleteLine, etc...).
 * </p>
 */
public class ProofTree extends JComponent implements ComponentListener, MouseListener, MouseMotionListener {

	private static final long serialVersionUID = -1059556882040128848L;
	
	/**
	 * <p> Traverse states indicates where the node is with relative to the view
	 * port of the scroll pane. </p> 
	 * 
	 * <ul>
	 * <li>BEFORE: the line components is above screen</li>
	 * <li>ON: line component is showing on screen</li>
	 * <li>AFTER: line component below the screen (for adding components this can be 
	 * used to prune the rest of the search take care to only use one recursive
	 * helper at a time so the current state of the traversal is consistent</li>
	 * </ul>
	 */
	private enum TraverseState {
		BEFORE, ON, AFTER
	}
	
	/**
	 * The "root" node of the proof.
	 */
	private Proof proof;
	
	/** 
	 * The selected line of the proof.
	 */
	private ProofLine selectedLine;
	
	/**
	 * Provide the hover effect when mouse cursor is moved over a proof line
	 */
	private ProofElement higlightedElement;
	
	/**
	 * Editor node, allows user input.
	 */
	private ProofLine editorLine;
	
	/**
	 * Dirty flag, shows if the proof has changed.
	 */
	private boolean dirty;
	
	/**
	 * Editing flag, shows if this proof is currently being edited.
	 */
	private boolean editing;
	
	/**
	 * UndoManager for this proofTree.
	 */
	private UndoManager undoManager;
	
	/**
	 * The tab in which the proof lives in.
	 */
	private NewTab tab;
	
	/**
	 * Width of this panel.
	 */
	private int panelWidth;
	
	/**
	 * Height oi this panel.
	 */
	private int panelHeight;
	
	/**
	 * Offset used when drawing.
	 */
	private int verticalOffset;	
	
	/**
	 * <a href="ProofTree.TraverseState.html"> See TraverseState.</a>
	 */
	private TraverseState ts;
	
	/**
	 * <p> This display and allows interaction between the proof and the user. </p>
	 * 
	 * @param proof The proof which to display
	 * @param tab The tab which the proof belongs to.
	 */
	public ProofTree(Proof proof, NewTab tab) {
		super();
		this.proof = proof;
		
		this.tab = tab;
		this.selectedLine = null;
		this.higlightedElement = null;
		
		this.dirty = false;
		this.editing = false;
		this.undoManager = new UndoManager(this);	
		
		//EditorNode
		this.editorLine = new ProofLine();
		this.editorLine.setPanel(new ExpressionInputBox(this));
		
		//Add editornode
		this.proof.getChildren().add(this.editorLine);
		
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addComponentListener(this);
		this.initTreeCells(this.proof);
	}
	
	////////////////GUI Related Stuff Here
	/**
	 * Initializes the GUI elements in the node given.
	 * 
	 * @param proofElement
	 */
	private void initTreeCells(ProofElement proofElement) {
		// if the proof element resembles a line in the original proof
		if (proofElement instanceof ProofLine) {
			ProofLine spl = (ProofLine) proofElement;
			
			JComponent jc = spl.getPanel();
			//If the panel does not exist, then create it!
			if (jc == null) {
				ProofLineComponent plc = new ProofLineComponent(this, spl);
				spl.setPanel(plc);
				
				plc.addCornerPainters();
			} else {
				if (jc instanceof ProofLineComponent) {
					ProofLineComponent plc = (ProofLineComponent) jc;
					plc.addCornerPainters();
				}
			}		
		// if the proof element contain subproofs
		} else if (proofElement instanceof Proof) {
			Proof proof = (Proof) proofElement;
			
			for (ProofElement children : proof.getChildren()) {
				this.initTreeCells(children);
			}
		} 
	}
	
	/**
	 * initialize component positions assuming that each cell has already have
	 * an associated screen component to measure dimension from
	 */
	private void initTreeLayout() {		
	   this.panelWidth = this.getParent().getWidth();
		this.panelHeight = this.getParent().getHeight();

		this.verticalOffset = 0;
		this.initTreeLayout(this.proof, 0);
		this.setPreferredSize(new Dimension(this.panelWidth, this.panelHeight));
	}
	
	/**
	 * initialize the rectangle instance in each proof line cell that will be
	 * used as the bounding boxes
	 */
	private void initTreeLayout(ProofElement pe, int indent) {
		if (pe instanceof Proof) {
			Proof p = (Proof) pe;

			if (p.getProofOptions().isHidden()) {
				Rectangle rect = new Rectangle(indent, this.verticalOffset,	this.panelWidth - indent, UIBits.proofTreeExpandBoxHeight);
				p.setRect(rect);
				
				this.verticalOffset += UIBits.proofTreeExpandBoxHeight;
			} else {
				// preserve the original horizontal & vertical offset
				int subProofOffset = this.verticalOffset;

				for (ProofElement children : p.getChildren()) {
					this.initTreeLayout(children, indent + UIBits.proofTreeIndentAmount);
				}

				Rectangle rect = new Rectangle(indent, subProofOffset, UIBits.proofTreeIndentAmount, ProofRep.getCellHeight(p));
				p.setRect(rect);

				// it's important to not increment the vertical offset so that
				// subproofs doesn't take up extra spaces than necessary
			}
		} else {
			ProofLine pl = (ProofLine) pe;
			
			JComponent proofComponent = pl.getPanel();
			
			if (proofComponent instanceof ProofLineComponent) {
				ProofLineComponent component = (ProofLineComponent) proofComponent;
				component.setWidth(this.panelWidth - indent);
			} 
			
			Dimension d = proofComponent.getPreferredSize();
			
			//ExpressionInputBox needs a bit more space!
			if (proofComponent instanceof ExpressionInputBox) {
				d.setSize(this.panelWidth - indent + 5, d.getHeight());
				proofComponent.setSize(d);
			}

			Rectangle rect = new Rectangle(indent, this.verticalOffset, d.width, d.height);
			pl.setRect(rect);

			this.verticalOffset += d.height;
			this.panelHeight = Math.max(this.panelHeight, this.verticalOffset);
		}
	}
	
	@Override
	public void paintComponent(Graphics g) {		
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
		
		g2d.setBackground(UIBits.backgroundColor);
		g2d.clearRect(0, 0, this.getWidth(), this.getHeight());
		
		g2d.setColor(Color.black);
		this.paintElementsHelper(this.proof, g2d);

		if (this.higlightedElement != null) {
			this.paintHighlight(g2d);
		}
		
		if (this.selectedLine != null) {
			this.paintFocus(g2d);
		}
	}

	/**
	 * Recursive helper to draw the proof tree
	 */
	private void paintElementsHelper(ProofElement pe, Graphics2D g) {
		Rectangle r = pe.getRect();

		// if this is a subproof in a proof
		// the rectangle becomes the guide line
		if (pe instanceof Proof) {
			Proof p = (Proof) pe;
			
			if (!p.getProofOptions().isHidden()) {
				this.paintScopeLine(g, r);
				
				for (ProofElement children : p.getChildren()) {
					this.paintElementsHelper(children, g);
				}
			} else {
				this.paintExpandBox(g, r);
			}
		}
	}

	/**
	 * paint the scope line for a sub-proof element
	 * the rectangle associate to a sub-proof should be vertical
	 * as opposed to a normal scope line
	 */
	private void paintScopeLine(Graphics2D g, Rectangle r) {
		g.setColor(new Color(100, 100, 100));

		// outdent pixel:
		int outDent = 1;

		int x1 = r.x;
		int y1 = r.y;
		int x2 = x1 + r.width;
		int y2 = y1 + r.height;

		g.drawLine(x2 - outDent, y1, x2, y1);
		g.drawLine(x2 - outDent, y1, x2 - outDent, y2);
		g.drawLine(x2 - outDent, y2, x2, y2);
	}

	private void paintExpandBox(Graphics2D g, Rectangle r) {
		g.setColor(new Color(100, 100, 100));
		String message = "Click to expand subproof ...";
		g.drawChars(message.toCharArray(), 0, message.length(), r.x + 10, r.y + r.height - 10);
	}

	/**
	 * highlight the line being hovered over
	 */
	private void paintHighlight(Graphics2D g) {
		Rectangle r = this.higlightedElement.getRect();
		
		if (this.higlightedElement instanceof Proof) {
			Proof p = (Proof) this.higlightedElement;
			
			if (!p.getProofOptions().isHidden()) {
				// highlight scope line
				g.setColor(new Color(0, 0, 0));

				// outdent pixel:
				int outDent = 1;

				int x1 = r.x;
				int y1 = r.y;
				int x2 = x1 + r.width;
				int y2 = y1 + r.height;

				g.drawLine(x2 - outDent, y1, x2, y1);
				g.drawLine(x2 - outDent, y1, x2 - outDent, y2);
				g.drawLine(x2 - outDent, y2, x2, y2);
			}
		} else {
			g.setColor(new Color(100, 100, 100));
			g.drawRect(r.x, r.y, r.width, r.height);
		}
	}
	
	/**
	 * paint the currently selected node
	 */
	private void paintFocus(Graphics2D g) {
		Rectangle r = this.selectedLine.getRect();
		g.setColor(new Color(0, 0, 0));
		g.drawRect(r.x, r.y, r.width, r.height);
	}
	
	/**
	 * swapping rectangles with components
	 */
	private void addVisibleComponents() {		
		ts = TraverseState.BEFORE;
		this.addVisibleComponent(this.proof);
	}

	/**
	 * return the leaf component on screen right now
	 */
	private void addVisibleComponent(ProofElement pe) {
		// controls the width of the area being highlighted
		int cellPadding = 1;

		if (pe instanceof Proof) {
			Proof p = (Proof) pe;
			
			if (!p.getProofOptions().isHidden()) {
				for (ProofElement children : p.getChildren()) {
					this.addVisibleComponent(children);
					
					if (this.ts == TraverseState.AFTER) {
						return;
					}
				}
			}
		} else {
			ProofLine pl = (ProofLine) pe;
			
			Rectangle screenBound = new Rectangle(this.getSize());
			Rectangle proofLineBound = pl.getRect();
			JComponent screenComponent = pl.getPanel();
			
			if (screenBound.intersects(proofLineBound)) {
				screenComponent.setBounds(proofLineBound.x + cellPadding, proofLineBound.y + cellPadding, proofLineBound.width - 2 * cellPadding, proofLineBound.height - 2 * cellPadding);
				this.add(screenComponent);

				if (this.ts == TraverseState.BEFORE) {
					this.ts = TraverseState.ON;
				}
			} else {
				this.remove(screenComponent);

				if (this.ts == TraverseState.ON) {
					this.ts = TraverseState.AFTER;
				}
			}
		}
	}
	
	/**
	 * find the ProofLineNode in which the point p resides in
	 */
	private ProofElement detectEventSource(Point p) {
		return this.detectEventSource(this.proof, p);
	}

	/**
	 * recursive helper to find the node which mouse pointer is ina
	 */
	private ProofElement detectEventSource(ProofElement pe, Point point) {
		if (pe.getRect().contains(point)) {
			return pe;
		}

		if (pe instanceof Proof) {
			Proof p = (Proof) pe;
			
			if (!p.getProofOptions().isHidden()) {
				for (ProofElement children : p.getChildren()) {
					ProofElement temp = this.detectEventSource(children, point);
					
					if (temp != null) {
						return temp;
					}
				}
			}
		}
		
		return null;
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		ProofElement pe = this.detectEventSource(e.getPoint());
		this.higlightedElement = pe;
		this.repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		ProofElement pe = this.detectEventSource(e.getPoint());

		// clicked on nothing: no action
		if (pe == null) {
			return;
		}

		// clicked on a subproof element
		if (pe instanceof Proof) {
			Proof p = (Proof) pe;
			ProofOptions po = p.getProofOptions();
			po.setHidden(!po.isHidden());
			
			this.setSelectedLine(null);
		} else {
			// no action
			if (pe == this.selectedLine) {
				return;
			}
			
			// set the current focused node to line node
			this.setSelectedLine((ProofLine) pe);
		}

		this.refreshTree(false);
	}

	@Override
	public void componentResized(ComponentEvent e) {
		this.refreshTree(false);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void componentMoved(ComponentEvent e) {
	}

	@Override
	public void componentShown(ComponentEvent e) {
	}

	@Override
	public void componentHidden(ComponentEvent e) {
	}
	//////////////////////End of GUI Releated Stuff

	/*
	 * Returns the proof used.
	 */
	public Proof getProof() {		
		return this.proof;
	}
	
	/*
	 * Returns the selected line
	 */
	public ProofLine getSelectedLine() {
		return this.selectedLine;
	}
	
	/*
	 * Returns the UndoManager used with this proof
	 */
	public UndoManager getUndoManager() {
		return this.undoManager;
	}
	
	/*
	 * Returns true if this tab is in editing mode
	 */
	public boolean isEditing() {
		return this.editing;
	}
	
	/**
	 * Sets this proof into editing mode. It creates a new ExpressionInputBox and sets it to view.
	 */
	public void startEditingFocus() {
		if (this.selectedLine == null) {
			return;
		}
		
		ExpressionInputBox eib = new ExpressionInputBox(this);
		eib.setFields(this.selectedLine);
		this.selectedLine.setPanel(eib);		
		this.removeEditorNode();
		
		this.editing = true;		
		this.refreshTree(false);
	}
	
	/**
	 * Returns this proof into viewing mode. It re-creates the ProofLineComponent to view the edited part.
	 */
	public void stopEditingFocus() {		
		//If we are not editing do nothing or if nothing was selected do nothing!
		if (!this.editing) {
			return;
		}
		
		if (this.selectedLine == null) {
			return;
		}		
		
		ProofLineComponent plc = new ProofLineComponent(this, this.selectedLine);		
		plc.startZoomingProof();
		plc.addCornerPainters();
		
		this.selectedLine.setPanel(plc);
		this.addEditorNode();		
		
		this.editing = false;
		this.refreshTree(false);
	}
	
	private void addEditorNode() {
		//Add the editor node back
		Proof p = this.selectedLine.getParent();
		List<ProofElement> childrenOfP = p.getChildren();
		int index = childrenOfP.indexOf(this.selectedLine);
		childrenOfP.add(index + 1, this.editorLine);
	}
	
	private void removeEditorNode() {
		//Remove editor node
		Proof p = this.selectedLine.getParent();
		p.getChildren().remove(this.editorLine);
	}

	/*
	 * Returns true if this proof is dirty (contains unsaved changes)
	 */
	public boolean isDirty() {
		return this.dirty;
	}
	
	/*
	 * Sets the dirty flag
	 */
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}
	
	/*
	 * Sets the selected line in this proof
	 */
	public void setSelectedLine(ProofLine newSelectedLine) {
		//Stop editing when switching!
		if (this.isEditing()) {
			this.stopEditingFocus();
		}
		
		//Dont select editornode
		if (newSelectedLine == this.editorLine) {
			return;
		}
		
		//If we are switching lines, make sure the first editor node gets removed
		if (newSelectedLine != null) {
			this.proof.getChildren().remove(this.editorLine);
		}
		
		if (this.selectedLine != null) {
			ProofLineComponent plc = (ProofLineComponent) this.selectedLine.getPanel();
			plc.stopZoomingProof();
			plc.removeZoominHightlight();
			plc.refresh();
			
			//Remove editor node
			this.removeEditorNode();
		}
		
		this.selectedLine = newSelectedLine;
		
		if (this.selectedLine != null) {
			ProofLineComponent plc = (ProofLineComponent) this.selectedLine.getPanel();
			if (plc == null) {
				plc = new ProofLineComponent(this, this.selectedLine);
				this.selectedLine.setPanel(plc);
				
				plc.removeZoominHightlight();
				plc.addCornerPainters();
			}
			
			plc.startZoomingProof();
			plc.refresh();
			
			//Add the editor node back
			this.addEditorNode();
		} 
		
		this.refreshTree(true);
	}
	
	/*
	 * Refreshes the proofTree
	 */
	public void refreshTree(boolean refreshSuggestionsPanel) {
		this.initTreeLayout();
		this.removeAll();
		this.addVisibleComponents();
		this.revalidate();
		this.repaint();
		
		this.tab.refreshFrame(false, true, refreshSuggestionsPanel);
	}
	
	/**
	 * Use a suggestion generated by the history pane to add a new line and move
	 * the focus
	 * 
	 * @param sug
	 *            The suggestion to take
	 */
	public void useSuggestion(Suggestion sug) {
		assert this.selectedLine != null;
		
		if (this.editing) {
			this.stopEditingFocus();
		}

		Step step = sug.getStep();		
		Justification j = sug.getJust();
		// predecessor holds the justification
		this.selectedLine.setJustification(j);

		Expression focusExpn = step.getFocus().instantiate(new HashMap<Variable, Expression>());
		// build new line and put it in
		ProofLine newLine = new ProofLine(step.getDop(), focusExpn, ProofRepresentationBits.InvalidJustification);

		this.addNewLine(this.selectedLine, newLine, false);
	}
	
	/**
	 * Add a new Line under the focused line This is what is typically being
	 * called when the user type in the next line in the proof
	 */
	public boolean addNewLine(String newLineText, Direction dir) {
		if (dir.toString().equals("")) {
			JOptionPane.showMessageDialog(this.tab, "You need to input a direction", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		if (this.selectedLine == null && Symbol.reverseDirections.get(dir.toString()) == null) {
			JOptionPane.showMessageDialog(this.tab, "The following is not a valid direction:\n" + dir.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		// try parse
		Expression t;
		try {
			t = Main.parseSingle(newLineText);
			t.toString();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this.tab, "Could not parse input:\n" + newLineText, "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		// create new screen proof line
		ProofLine next = new ProofLine(dir, t, ProofRepresentationBits.InvalidJustification);
		this.addNewLine(this.selectedLine, next, false);
		
		return true;
	}
	
	public void addNewLine(ProofLine line, Proof next, Boolean fromUndoManager) {
		//get line if none given
		if (line == null) {
			line = this.selectedLine;
		}
		
		// add current state to undo stack
		if (!fromUndoManager) {
			Object[] params = { line, next, true };
			Object[] undoParam = { next, true };
			
			this.undoManager.actionPerformed(UIBits.proofTreeAddNewLineProofMethod, params, 
					UIBits.proofTreeDeleteSubProofMethod, undoParam);
		}
		
		if (this.proof.getChildren().size() == 0) {
			this.proof = next;
		} else {
			if (line == null) {
				JOptionPane.showMessageDialog(this.tab, UIBits.tabUINothingSelectedContent, UIBits.mainUIProofImportEmptyContentTitle, JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			line.getParent().addUnder(line, next);
		}
		
		this.initTreeCells(this.proof);
		this.refreshTree(true);
		this.dirty = true;
	}
	
	private void addNewLine(ProofLine where, ProofLine next, Boolean fromUndoManager) {
		assert next != null;
		boolean isBase = this.proof.getChildren().isEmpty();
		
		if (where != null) {
			where.insertAsNextLine(next);
			
			//If a justification can not be found, we assume it is an invalid justification
			Justification just = where.tryToFindJustification(this.tab.getLaws());
			if (just == null) {
				next.setJustification(ProofRepresentationBits.InvalidJustification);
			}
		} else {
			this.proof.add(next);
		}
		
		// set focused line
		this.setSelectedLine(next);
		
		//Undo/redo
		if (!fromUndoManager) {
			Method actionDid = UIBits.proofTreeAddNewLineProofLineMethod;
			Object[] params = { where, next, true };
			
			Method actionToGetBack = UIBits.proofTreeDeleteLineMethod;
			Object[] paramsToGetBack = { next, true };
			
			//If this is the first line being added, we need a different method
			// to redo
			if (isBase) {
				actionDid = UIBits.proofTreeSetBaseProofMethod;
				params = new Object[] { this.proof, this.selectedLine, true };
			}
			
			this.undoManager.actionPerformed(actionDid, params, actionToGetBack, paramsToGetBack);
		}

		this.refreshTree(true);
		
		//did something, set dirty
		this.dirty = true;
	}
	
	/*
	 * Attempts to import the subproof under the line. If no line is give, it uses the currently selected line.
	 */
	public void importSubproof(ProofLine line, Proof proof) {
		if (line == null) {
			line = this.selectedLine;
		}
		
		//If this proof is empty, then set the proof
		if (this.proof.getChildren().size() == 1) {
			ProofElement kid = this.proof.getChildren().get(0);
			if (kid instanceof ProofLine) {
				ProofLine plKid = (ProofLine) kid;
				if (plKid.getPanel() instanceof ExpressionInputBox) {
					//Empty proof use setBaseProof!
					this.setBaseProof(proof, line, false);
					return;
				}
			}
		}
		
		//Check if selectedLine exists
		if (line == null) {
			JOptionPane.showMessageDialog(this.tab, UIBits.mainUIProofImportNoLineSelected, UIBits.mainUIProofImportEmptyContentTitle, JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		//Make sure existing selectedLine does not have a zoomin
		if (line.getJustification() instanceof ZoomInJustification) {
			JOptionPane.showMessageDialog(this.tab, UIBits.mainUIProofImportZoomInContent, UIBits.mainUIProofImportEmptyContentTitle, JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		//Ensure matching proofs
		ProofLine toCheckAgainst = null;
		for (ProofElement children : proof.getChildren()) {
			if (children instanceof ProofLine) {
				ProofLine pl = (ProofLine) children;
				
				//Make sure not editor node
				if (pl.getPanel() instanceof ExpressionInputBox) {
					continue;
				}
				
				toCheckAgainst = pl;
				break;
			}
		}
		
		if (!line.getExpn().equals(toCheckAgainst.getExpn())) {
			JOptionPane.showMessageDialog(this.tab, UIBits.mainUIProofImportProofMismatch, UIBits.mainUIProofImportEmptyContentTitle, JOptionPane.ERROR_MESSAGE);
			return;
		}

		//Set Justification & Update!
		line.setJustification(toCheckAgainst.getJustification());
		if (line.getPanel() instanceof ProofLineComponent) {
			ProofLineComponent plc = (ProofLineComponent) line.getPanel();
			plc.refresh();
			plc.addCornerPainters();
		}
		
		//Add as subproof
		for (ProofElement children : proof.getChildren()) {
			if (children instanceof Proof) {
				Proof toAdd = (Proof) children;				
				this.addNewLine(line, toAdd, false);
			}
		}
	}
	
	/*
	 * This replaces the expn in the given line with a new expn and direction
	 */
	private void editLine(ProofLine line, Expression expn, Direction dir, Boolean fromUndoManager) {
		if (fromUndoManager) {
			this.startEditingFocus();
		}
		
		if (!fromUndoManager) {
			Object[] params = { line, expn, dir, true };
			Object[] paramsToGetBack = { line, line.getExpn(), line.getDirection(), true };
			
			this.undoManager.actionPerformed(UIBits.proofTreeEditLineMethod, params, 
					UIBits.proofTreeEditLineMethod, paramsToGetBack);
		}
		
		line.setExpn(expn);
		line.setDirection(dir);
		line.tryToFindJustification(this.tab.getLaws());
		
		this.dirty = true;
		this.refreshTree(true);
	}

	/**
	 * similar to add new line except that it changes the focused line's exp instead
	 */
	public boolean editFocus(ProofLine line, String newLineText, Direction dir) {		
		//If we are not give a line
		if (line == null) {
			line = this.selectedLine;
		}
		
		if (dir.toString().equals("")) {
			JOptionPane.showMessageDialog(this.tab, "You need to input a direction", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		if (line == null && Symbol.reverseDirections.get(dir.toString()) == null) {
			JOptionPane.showMessageDialog(this.tab, "The following is not a valid direction:\n" + dir.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		// try parse
		Expression expn;
		try {
			expn = Main.parseSingle(newLineText);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this.tab, "Could not parse input:\n" + newLineText, "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		// check to see if this is a subproof, ask the user what to do
		if (line.getJustification() instanceof ZoomInJustification) {
			String selection = (String) JOptionPane.showInputDialog(this.tab, 
					UIBits.NewTabEditDialogPromptContent,
					UIBits.NewTabEditDialogPromptTitle,
					JOptionPane.WARNING_MESSAGE,
					UIBits.warnPic,
					UIBits.NewTabEditDialog, 
					UIBits.NewTabEditDialog[0]);
			
			if (selection == null) {
				this.stopEditingFocus();
				return false;
			}
			
			switch (UIBits.NewTabEditDialogStringToVal(selection)) {
			case UIBits.NewTabEditDialogOpenNewTab:
				if (!this.tab.openSubproofInNewTab()) {
					//User cancelled
					this.stopEditingFocus();
					return false;
				}
				
				//After new tab has been opened del subproof so fall over to next case
			case UIBits.NewTabEditDialogDeleteSubProof:
				this.deleteSubProof((Proof) line.getNextElement(), false);
				break;
			case UIBits.NewTabEditDialogDiscard:
				this.stopEditingFocus();
				return false;
			}
		}
		
		this.editLine(line, expn, dir, false);		
		return true;
	}
	
	/*
	 * Sets the display with the given proof and focus.
	 */
	private void setBaseProof(Proof proof, ProofLine focus, Boolean fromUndoManager) {
		//Undo/redo
		if (!fromUndoManager) {
			Object[] paramsUsed = { proof, focus, true };
			Object[] paramsUsedToGetBack = { this.proof, this.selectedLine, true };
			
			this.undoManager.actionPerformed(UIBits.proofTreeSetBaseProofMethod, paramsUsed,
					UIBits.proofTreeSetBaseProofMethod, paramsUsedToGetBack);
		}
		
		this.proof = proof;
		this.selectedLine = focus;
		
		//Add back in editor node
		this.proof.getChildren().add(this.editorLine);
		
		this.dirty = true;
		
		//Redo all dim and panels
		this.initTreeCells(this.proof);
		this.refreshTree(true);
	}
	
	/*
	 * Deletes the subproof
	 */
	private void deleteSubProof(Proof proof, Boolean fromUndoManager) {
		// add current state to undo stack
		if (!fromUndoManager) {
			Object[] params = { proof, true };	
			Object[] paramsToGoBack = { this.selectedLine, proof, true };
			
			this.undoManager.actionPerformed(UIBits.proofTreeDeleteSubProofMethod, params, 
					UIBits.proofTreeAddNewLineProofMethod, paramsToGoBack);
		}
		
		proof.remove();
				
		this.dirty = true;
		this.refreshTree(true);
	}
	
	/*
	 * Deletes the subproof
	 */
	private void deleteSubProof(ProofLine line, Boolean fromUndoManager) {
		Proof proof = line.getParent();
		
		// otherwise just removing that subproof
		ProofElement e = proof.getNextElement();
		if (e instanceof ProofLine) {
			((ProofLine) e).setZoomOutPath(null);
		}
		
		proof.remove(); // TODO if no step has been applied in this
						// proof, don't touch the justification

		this.setSelectedLine(null);
		
		this.refreshTree(true);
		this.dirty = true;
		return;
	}
	
	/*
	 * Deletes the line
	 */
	private void deleteOneLine(ProofLine line, Boolean fromUndoManager) {
		ProofElement newFocus = line.getPreviousElement();
		
		//Undo
		Object[] paramsUndo = { this.selectedLine, line, true };
		Method undoAction = UIBits.proofTreeAddNewLineProofLineMethod;
		
		if (newFocus instanceof Proof) {
			Proof parent = (Proof) newFocus;
			
			if (parent.isZoomInProof() && line.isLastLineInSubProof()) {
				this.undoZoomOutMultiLine(line, parent.getLastLine());
				
				//Set the newFocus to a ProofLine now
				newFocus = parent.getLastLine();
				
				//Also make sure the correct method gets used
				undoAction = UIBits.proofTreeZoomOutMethod;
				paramsUndo = new Object[]{ newFocus, true };
			} else if (line.getJustification() instanceof ZoomInJustification) {
				// TODO delete the subproof too
				JOptionPane.showMessageDialog(this.tab, "You cannot delete the start of a zoom in proof\nwithout deleting the zoom in proof first.", "Error", JOptionPane.ERROR_MESSAGE);			
				return;
			}
		} else if (line.getParent().isProofDebt() && line.isLastLineInSubProof()) {
			JOptionPane.showMessageDialog(this.tab, "You cannot delete the last line of a proof debt. \n Proof debt must be proven", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		//Undo/redo
		if (!fromUndoManager) {
			//Redo
			Object[] paramsRedo = { line, true };
			
			this.undoManager.actionPerformed(UIBits.proofTreeDeleteLineMethod, paramsRedo,
					undoAction, paramsUndo);
		}

		if (line.getPreviousElement() != null) {
			line.getPreviousElement().setJustification(null);
		}
		
		line.remove();
		
		this.setSelectedLine((ProofLine) newFocus);
	}

	/**
	 * Delete the given line, if there is one, and the entire subproof if the
	 * first line of a subproof is selected
	 */
	public void deleteLine(ProofLine line, Boolean fromUndoManager) {			
		if (line == null) {
			line = this.selectedLine;
		}
		
		// First line deletion is whole proof deletion
		if (line.isFirstLineInProof()) {			
			Proof proof = line.getParent();

			if (proof.isBaseProof()) {				
				// deleting base proof will clear the entire proof tree
				// creates empty proof
				Proof newProof = new Proof(new ExpnDirection("="), new Justification("Root Proof", true), new ProofOptions(MonotonicContext.NEUTRAL, ProofSubtype.DIRECTPROOF, false));
				this.setBaseProof(newProof, null, fromUndoManager);
				return;
			//If this is the last line in a subproof, zoom out instead, same thing!
			} else if (line.isLastLineInSubProof()) {
				this.zoomOut(line, fromUndoManager);
			} else {
				//Undo/redo
				if (!fromUndoManager) {
					Object[] params = { line, true };
					Object[] paramsToGoBack = { this.selectedLine, line, true };
					
					this.undoManager.actionPerformed(UIBits.proofTreeDeleteLineMethod, params,
							UIBits.proofTreeAddNewLineProofLineMethod, paramsToGoBack);
				}
				
				this.deleteSubProof(line, fromUndoManager);
			}
		} else {			
			this.deleteOneLine(line, fromUndoManager);
		}		
		
		this.dirty = true;
		this.refreshTree(true);
	}

	/**
	 * Zoom in on some part of the focused expression
	 * 
	 * @param get
	 *            Which part to zoom in on
	 */
	public void zoomIn(ProofLine line, Path path, Boolean fromUndoManager) {
		if (line.getJustification() instanceof ZoomInJustification) {
			JOptionPane.showMessageDialog(this.tab, "Cannot zoom into expression for which\nthere is an extant zoom in proof.", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		// This permits us to undo the zoom in
		Proof newSubproof;
		try {
			newSubproof = line.addZoomInProof(path);
		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}
		
		//Undo/redo
		if (!fromUndoManager) {
			Object[] paramsRedo = { line, path, true };
			Object[] paramsUndo = { newSubproof.getChildren().get(0), true }; 
			
			this.undoManager.actionPerformed(UIBits.proofTreeZoomInMethod, paramsRedo,
					UIBits.proofTreeZoomOutMethod, paramsUndo);
		} else {
			//Tell the undomanager that we have a new object!
			this.undoManager.updateObject(newSubproof.getChildren().get(0));
		}

		// set the focusedLine instance variable to the first line of the
		// subproof
		this.setSelectedLine((ProofLine) newSubproof.getChildren().get(0));
		this.refreshTree(true);
		
		//set dirty
		this.dirty = true;
	}
	
	private void zoomOutOneLine(ProofLine line, Boolean fromUndoManager) {		
		Proof par = line.getParent();
		ProofLine prev = (ProofLine) par.getPreviousElement();
		ZoomInJustification just = (ZoomInJustification) prev.getJustification();
		
		//Undo/redo
		if (!fromUndoManager) {
			Object[] params = { line, true };
			Object[] paramsUndo = { prev, just.getPath(), true };
			
			this.undoManager.actionPerformed(UIBits.proofTreeZoomOutMethod, params, 
					UIBits.proofTreeZoomInMethod, paramsUndo);
		}
		
		assert prev.getJustification() instanceof ZoomInJustification;
		par.remove();

		prev.tryToFindJustification(this.tab.getLaws());
		
		//Remove zoom in highlights
		ProofLineComponent plc = (ProofLineComponent) prev.getPanel();
		plc.removeZoominHightlight();
		
		this.setSelectedLine(prev);
		
		return;
	}
	
	private void undoZoomOutMultiLine(ProofLine line, ProofLine lineToDelete) {		
		line.setJustification(ProofRepresentationBits.InvalidJustification);
		lineToDelete.remove();
		
		this.setSelectedLine(line);
		this.refreshTree(true);
	}
	
	private void zoomOutMultiLine(ProofLine line, Boolean fromUndoManager) {
		// This permits us to undo the zoom out
		ProofLine newLine = null;

		try {			
			newLine = line.finishZoomOutProof();
			newLine.tryToFindJustification(this.tab.getLaws());
		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}

		//Refresh the panel from which we are zooming out from
		ProofLineComponent plc = (ProofLineComponent) line.getPanel();
		plc.refresh();
		
		this.setSelectedLine(newLine);
		
		// undo/redo
		if (!fromUndoManager) {
			Object[] params = { line, true };
			Object[] paramsUndo = { line, newLine };
			
			this.undoManager.actionPerformed(UIBits.proofTreeZoomOutMethod, params,
					UIBits.proofTreeUndoZoomOutMultiLineMethod, paramsUndo);
		} else {
			//Tell the undomanager that we have a new object!
			this.undoManager.updateObject(newLine);
		}
	}

	public void zoomOut(ProofLine fromWhat, Boolean fromUndoManager) {
		if (fromWhat == null) {
			fromWhat = this.selectedLine;
		}
		
		//We need to make the selectedLine disappear for a bit...
		this.setSelectedLine(null);
		
		if (fromWhat.isOnlyLineInProof()) {
			this.zoomOutOneLine(fromWhat, fromUndoManager);
		} else {
			this.zoomOutMultiLine(fromWhat,fromUndoManager);
		}
		
		this.refreshTree(true);
		this.dirty = true;
	}
	
	public void doUndoRedo (boolean undo) {
		try {
			if (undo) {
				this.undoManager.doUndo();
			} else {
				this.undoManager.doRedo();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//No more undos clear dirty
		if (!this.undoManager.canUndo()) {
			this.dirty = false;
		}
	}
}
