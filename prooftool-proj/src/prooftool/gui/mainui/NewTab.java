package prooftool.gui.mainui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;
import prooftool.backend.ccode.CCodeGenerator;
import prooftool.backend.direction.Direction;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Expression;
import prooftool.backend.refinement.ContextRefinementProvider;
import prooftool.backend.refinement.SubproofRefinementProvider;
import prooftool.backend.suggestions.ProgrammingSuggestor;
import prooftool.gui.Settings;
import prooftool.gui.UIBits;
import prooftool.gui.elements.NettyButton;
import prooftool.gui.elements.NettySuggestionsPanel;
import prooftool.gui.elements.NettyToolBar;
import prooftool.gui.lawlibrary.LawLibrary;
import prooftool.gui.prooftree.ProofTree;
import prooftool.gui.tabs.CodeGen;
import prooftool.gui.tabs.PlainTextTab;
import prooftool.gui.tabs.TabPanelControl;
import prooftool.gui.undomanager.UndoManager;
import prooftool.proofrepresentation.Proof;
import prooftool.proofrepresentation.ProofLine;
import prooftool.proofrepresentation.justification.ZoomInJustification;
import prooftool.proofrepresentation.suggestion.Suggestion;
import prooftool.util.ListUtils;
import prooftool.util.ProofRep;

/**
 * 
 * @author dave
 * @author evm
 * @author xiang
 *    
 * <p>This is the UI for a proof. It is rendered as a tab and contains elements such as context panel, suggestions panel, and most importanly the proof-tree. </p> 
 */
public class NewTab extends JTabbedPane implements ActionListener, AdjustmentListener, KeyListener, Comparator<Suggestion>, ChangeListener {

	private static final long serialVersionUID = 1246482803326507978L;

	private final class NewTabToolBar extends NettyToolBar {

		private static final long serialVersionUID = -3533240970285189026L;

		private NettyButton redoButton;
		private NettyButton undoButton;
		private NettyButton zoomButton;
		private NettyButton exportButton;
		private NettyButton importButton;

		private NewTabToolBar(NewTab aL) {
			super(UIBits.NewTabToolBarImageFiles, UIBits.NewTabToolBarTextIcons, UIBits.NewTabToolBarToolTips, aL);

			for (NettyButton button : this.getButtons()) {
				switch (Integer.parseInt(button.getActionCommand())) {
				case UIBits.NewTabToolBarRedo:
					this.redoButton = button;
					break;
				case UIBits.NewTabToolBarUndo:
					this.undoButton = button;
					break;
				case UIBits.NewTabToolBarZoomOut:
					this.zoomButton = button;
					break;
				case UIBits.NewTabToolBarExportNewTab:
					this.exportButton = button;
					break;
				case UIBits.NewTabToolBarImportTab:
					this.importButton = button;
					break;
				}
			}
		}

		/**
		 * Set the undo and redo buttons to have the correct appearance
		 */
		private void setButtonStates(boolean states[]) {
			assert states.length == 6;

			this.redoButton.setEnabled(states[0]);
			this.undoButton.setEnabled(states[1]);
			this.zoomButton.setEnabled(states[2]);
			this.exportButton.setEnabled(states[3]);
			this.importButton.setEnabled(states[4]);
		}
	}
	
	private LawLibrary library;
	private NewTabToolBar toolBar;
	private NettySuggestionsPanel suggestionsPanel;
	private PlainTextTab ptt;	
	
	private CodeGen cg;
	private CCodeGenerator cCodeGenerator;
	
	// JSplitPanes
	private JSplitPane vSplit;
	private JSplitPane hSplit;
	// Used to display the context panel
	private JPanel contextPanel;
	// Model Used for displaying the proof
	private ProofTree newProofTree;
	private JScrollPane proofScroll;
	private JScrollPane contextPanelPane;
	private JScrollPane suggestionScroll;
	
	//TabControl
	private TabPanelControl tcp;

	protected NewTab(TabPanelControl tcp) {
		super();
		JPanel proofPanel = new JPanel();
		
		proofPanel.setLayout(new BorderLayout());
		this.tcp = tcp;

		this.toolBar = new NewTabToolBar(this);
		this.library = new LawLibrary();
		this.ptt = new PlainTextTab();
		this.cCodeGenerator = new CCodeGenerator();
		this.cg = new CodeGen();
		
		// Proof Tree & Scroll Pane
		this.proofScroll = new JScrollPane();
		this.proofScroll.getVerticalScrollBar().addAdjustmentListener(this);
		
		// Suggestions Panel
		this.suggestionsPanel = new NettySuggestionsPanel();
		this.suggestionScroll = new JScrollPane(this.suggestionsPanel);
		
		// Context Panel & Scroll Pane
		this.contextPanel = new JPanel();
		this.contextPanel.setLayout(new MigLayout());
		this.contextPanel.setBackground(UIBits.backgroundColor);
		this.contextPanel.add(UIBits.contextPanelLabel, "wrap");
		this.contextPanelPane = new JScrollPane(this.contextPanel);
		this.contextPanelPane.setViewportView(this.contextPanel);

		// split panes
		this.vSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, this.proofScroll, this.suggestionScroll);
		this.hSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, this.vSplit, this.contextPanelPane);
		this.vSplit.setResizeWeight(0.75);
		this.hSplit.setResizeWeight(0.8);

		proofPanel.add(this.toolBar, BorderLayout.PAGE_START);
		proofPanel.add(hSplit, BorderLayout.CENTER);
		proofPanel.addKeyListener(this);

		//Add the tabs
		this.addTab("Proof Tree", proofPanel);
		this.addTab("Law Library", this.library);
		this.addTab("Plain Text View", this.ptt);
		this.addTab("Code Generator", this.cg);
		this.addChangeListener(this);
		
		this.setFocusable(true);
		this.requestFocus();
		
		//Update view
		this.updateView();
	}
	
	protected void updateView() {
		if (Settings.get_Settings().isHideSuggestionPanel()) {
			this.vSplit.setRightComponent(null);
		} else {
			this.vSplit.setRightComponent(this.suggestionScroll);
		}
		
		if (Settings.get_Settings().isHideContextPanel()) {
			this.hSplit.setRightComponent(null);
		} else {
			this.hSplit.setRightComponent(this.contextPanelPane);
		}
	}
	
	public LawBlob getLaws() {
		return this.library.getLaws();
	}
	
	public ProofTree getProofTree() {
		return this.newProofTree;
	}
	
	public JScrollPane getProofScrollPane() {
		return this.proofScroll;
	}
	
	public void initProofView(Proof proof) {
		this.newProofTree = new ProofTree(proof, this);
		this.proofScroll.setViewportView(this.newProofTree);
		this.refreshFrame(true, true, true);
	}
	
	@Override
	public int compare(Suggestion arg0, Suggestion arg1) {
		// TODO Auto-generated method stub
		int l0 = arg0.getExpn().toString().length();
		int l1 = arg1.getExpn().toString().length();
		return l0 - l1;
	}
	
	public void refreshFrame(boolean refreshTree, boolean refreshContextPanel, boolean refreshSuggestionsPanel) {
		UndoManager undoManager = this.newProofTree.getUndoManager();
		Proof proof = this.newProofTree.getProof();
		ProofLine selectedLine = this.newProofTree.getSelectedLine();
		
		boolean[] toolBarStates = new boolean[] { undoManager.canRedo(), 
				undoManager.canUndo(), 
				(selectedLine != null && selectedLine.isZoomOutable()), 
				selectedLine != null,
				!proof.isEmptyProof()};

		if (refreshSuggestionsPanel) {
			this.refreshSuggestionsPanel();
		}
		
		if (refreshContextPanel) {
			this.refreshContextPanel();
		}
		
		this.validate();
		this.repaint();
		this.toolBar.setButtonStates(toolBarStates);

		this.requestFocus();
		
		if (refreshTree) {
			this.newProofTree.refreshTree(false);
		}
	}
	
	private void refreshSuggestionsPanel() {
		//Since this is in reference of the selected line, we need to get that
		ProofLine selectedLine = this.newProofTree.getSelectedLine();
		
		//Update suggestionsPanel
		this.suggestionsPanel.removeAll();
		
		if (selectedLine == null) {
			this.suggestionsPanel.add(UIBits.nettySuggestionsPanelDefaultLabel);
			return;
		}

		// if this line is before a zoom proof, we cannot make a new line for it
		if (selectedLine.getJustification() instanceof ZoomInJustification) {
			JLabel endMessage = new JLabel();
			endMessage.setForeground(Color.gray);
			endMessage.setText(UIBits.nettySuggestionsPanelErrorZoomIn);
			this.suggestionsPanel.add(endMessage);
			return;
		}

		// Get the permissible laws, including context.
		List<Expression> context = new ArrayList<Expression>(selectedLine.getParent().getFullContext().get(0));
		List<Expression> greyContext = new ArrayList<Expression>(selectedLine.getParent().getFullContext().get(1));

		LawBlob contextblob = new LawBlob();
		for (Expression law : context) {
			contextblob.add(law);
		}
		
		for (Expression law : greyContext) {
			contextblob.add(law);
		}

		Direction permittedDirection = selectedLine.getParent().getDirection();
		
		selectedLine.generateSuggestions(permittedDirection, this.library.getLaws(), contextblob);
		List<Suggestion> sugs = selectedLine.getSuggestions().getMostRecentSugs();
		List<Suggestion> greySugs = selectedLine.getSuggestions().getGreySugs();

		// order suggestion
		Collections.sort(sugs, this);
		Collections.sort(greySugs, this);

		for (Suggestion s : sugs) {
			this.suggestionsPanel.add(new SuggestionPanel(false, s, this));
		}

		if (!Settings.get_Settings().isGreySug()) {
			for (Suggestion s : greySugs) {
				this.suggestionsPanel.add(new SuggestionPanel(true, s, this));
			}
		}

		JLabel endMessage = new JLabel();
		endMessage.setForeground(Color.gray);
		if (sugs.size() == 0) {
			endMessage.setText(UIBits.nettySuggestionsPanelNoMatch);
		} else {
			endMessage.setText(String.format(UIBits.nettySuggestionsPanelNumberMatched, Integer.toString(sugs.size())));
		}
		
		this.suggestionsPanel.add(endMessage);
	}
	

	private void refreshContextPanel() {
		ProofLine selectedLine = this.newProofTree.getSelectedLine();
		
		this.contextPanel.removeAll();
		this.contextPanel.add(UIBits.contextPanelLabel, "wrap");

		if (selectedLine == null) {
			return;
		}

		List<List<Expression>> context = selectedLine.getParent().getFullContext();
		List<Expression> usableContext = context.get(0);
		List<Expression> greyContext = context.get(1);
		
		// TODO replace this with an RSyntaxTextArea
		for (Expression e : usableContext) {
			JLabel label = new JLabel(e.toString());
			label.setFont(UIBits.defaultFont);
			this.contextPanel.add(label, "wrap");
		}

		for (Expression e : greyContext) {
			JLabel label = new JLabel(e.toString());
			label.setFont(UIBits.defaultFont);
			label.setForeground(Color.lightGray);
			this.contextPanel.add(label, "wrap");
		}
		
		//Display parent context also
		if (context.size() == 4) {
			List<Expression> parentUsableContext = context.get(2);
			List<Expression> parentUsableGreyContext = context.get(3);
			
			for (Expression e : parentUsableContext) {
				JLabel label = new JLabel(e.toString());
				label.setFont(UIBits.defaultFont);
				label.setForeground(Color.MAGENTA);
				this.contextPanel.add(label, "wrap");
			}

			for (Expression e : parentUsableGreyContext) {
				JLabel label = new JLabel(e.toString());
				label.setFont(UIBits.defaultFont);
				label.setForeground(Color.DARK_GRAY);
				this.contextPanel.add(label, "wrap");
			}
		}
	}

	/**
	 * save proof sequence, returns true if safe was successful
	 */
	protected boolean saveProofSequence() {
		JFileChooser fc = new JFileChooser("");
		fc.setCurrentDirectory(UIBits.main.getCurrentLocation());
		
		int returnVal = fc.showSaveDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();

			try {
				BufferedWriter out = new BufferedWriter(new FileWriter(file));
				out.write(this.newProofTree.getProof().toSaveString());
				out.close();
				
				this.newProofTree.setDirty(false);
				this.tcp.updateTabName(file.getName());
				UIBits.main.updateOpenTabFolder(file);
				return true;
			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, e.toString());
				JOptionPane.showMessageDialog(this, "Save error!");
			}
		}
		
		return false;
	}

	private void saveProofAsTheorem() {
		ProofLine selectedLine = this.newProofTree.getSelectedLine();
		
		try {
			if (selectedLine == null) {
				JOptionPane.showMessageDialog(this, "No theorem has yet been proven");
				return;
			}

			Expression theorem = ProofRep.proves(selectedLine);
			if (theorem == null) {
				JOptionPane.showMessageDialog(this, "Could not get a theorem from current focus");
			} else {
				this.library.addLaw(theorem);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.requestFocus();
	}
	
	//Returns false if user cancels process
	public boolean openSubproofInNewTab() {
		/*
		 * If there isnt any subproofs selected, then this should not happen.
		 * But in case there is a bug that prevents the button from being, 
		 * disabled Netty will give an error message and do nothing.
		 */
		ProofLine selectedLine = this.newProofTree.getSelectedLine();
		
		if (selectedLine == null) {
			JOptionPane.showMessageDialog(this, UIBits.mainUIProofNotSelectedContent, UIBits.mainUIProofNotSelectedTitle, JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		//Get new tab name
		String newTabName = JOptionPane.showInputDialog(this, UIBits.mainUIProofExportNewTabNameContent);
		if (newTabName == null || newTabName.trim().isEmpty()) {
			JOptionPane.showMessageDialog(this, UIBits.mainUIProofNoNewTabNameProvidedContent, UIBits.mainUIProofNotSelectedTitle, JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		//Export the new tab
		UIBits.main.openSubProof(selectedLine, newTabName);
		return true;
	}
	
	private void exportToOtherTab() {
		/*
		 * Just in case there is a bug where the import button
		 * is not disabled.
		 */
		Proof proof = this.newProofTree.getProof();
		
		//Check for empty proof!
		if (proof.isEmptyProof()) {
			JOptionPane.showMessageDialog(this, UIBits.mainUIProofImportEmptyContent, UIBits.mainUIProofImportEmptyContentTitle, JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		UIBits.main.importSubProof(proof, this.tcp);
	}	

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (Integer.parseInt(e.getActionCommand())) {
		case UIBits.NewTabToolBarSave:
			this.saveProofSequence();
			break;
		case UIBits.NewTabToolBarUndo:
			this.newProofTree.doUndoRedo(true);
			break;
		case UIBits.NewTabToolBarRedo:
			this.newProofTree.doUndoRedo(false);
			break;
		case UIBits.NewTabToolBarZoomOut:
			this.newProofTree.zoomOut(null, false);
			break;
		case UIBits.NewTabToolBarRecheckProof:
			this.newProofTree.getProof().verifyAll(this.library.getLaws());
			break;
		case UIBits.NewTabToolBarSaveCurrent:
			this.saveProofAsTheorem();
			break;
		case UIBits.NewTabToolBarExportNewTab:
			this.openSubproofInNewTab();
			break;
		case UIBits.NewTabToolBarImportTab:
			this.exportToOtherTab();
			break;
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		// Undo command
		case KeyEvent.VK_BACK_SPACE:
			this.newProofTree.doUndoRedo(true);
			break;
		// Redo command
		case KeyEvent.VK_ENTER:
			this.newProofTree.doUndoRedo(false);
			break;
		// Save command
		case KeyEvent.VK_S:
			if (e.isControlDown())
				this.saveProofSequence();
			break;
		// Zoom Out Command
		case KeyEvent.VK_UP:
		case KeyEvent.VK_MINUS:
			this.newProofTree.zoomOut(null, false);
			break;
		// Y & Z are special
		case KeyEvent.VK_Z:
			if (e.isControlDown() && e.isShiftDown()) {
				this.newProofTree.doUndoRedo(false);
			} else if (e.isControlDown()) {
				this.newProofTree.doUndoRedo(true);
			}
			break;
		case KeyEvent.VK_Y:
			if (e.isControlDown()) {
				this.newProofTree.doUndoRedo(false);
			}
			break;
		case KeyEvent.VK_F2:
			this.newProofTree.startEditingFocus();
			break;
		case KeyEvent.VK_ESCAPE:
			this.newProofTree.stopEditingFocus();
			break;
		//Open new tab
		case KeyEvent.VK_O:
			if (e.isControlDown()) {
				UIBits.main.switchToOpenNewTab();
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Activate the sweeping option if the user presses the key for it.
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_DELETE) {
			this.newProofTree.deleteLine(null, false);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void adjustmentValueChanged(AdjustmentEvent e) {
		if (this.newProofTree == null) {
			return;
		}
		
		//Prevent stackoverflow, this keeps getting triggered by something
		this.proofScroll.getVerticalScrollBar().removeAdjustmentListener(this);
		
		this.newProofTree.refreshTree(false);
		
		//Add it back in
		this.proofScroll.getVerticalScrollBar().addAdjustmentListener(this);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		Component c = this.getSelectedComponent();
		
		if (c instanceof PlainTextTab) {
			this.ptt.setText(this.newProofTree.getProof());
		} else if (c instanceof CodeGen) {
			ProofLine selectedLine = this.newProofTree.getSelectedLine();
			Proof proof = this.newProofTree.getProof();
			
			if (selectedLine != null && selectedLine.getExpn() != null) {
				try {
					List<Expression> laws = ListUtils.join(this.library.getLaws().getInputLaws(), selectedLine.getParent().getFullContext().get(0));
					this.cg.setCode(this.cCodeGenerator.generateCCode(selectedLine.getExpn(), ProgrammingSuggestor.computeSigma(laws), Arrays.asList(new ContextRefinementProvider(laws), new SubproofRefinementProvider(proof))));
				} catch (CCodeGenerator.Exception ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(this, "Could not get a theorem from current focus");
				}
			}

			this.requestFocus();
		}
	}
}
