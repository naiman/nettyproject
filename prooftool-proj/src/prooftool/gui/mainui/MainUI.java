package prooftool.gui.mainui;

import java.awt.Component;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import prooftool.backend.direction.ExpnDirection;
import prooftool.gui.Settings;
import prooftool.gui.UIBits;
import prooftool.gui.elements.NothingGUIElement;
import prooftool.gui.tabs.DoNotSave;
import prooftool.gui.tabs.HelpTab;
import prooftool.gui.tabs.OpenTab;
import prooftool.gui.tabs.SettingsTab;
import prooftool.gui.tabs.TabPanelControl;
import prooftool.proofrepresentation.Proof;
import prooftool.proofrepresentation.ProofElement;
import prooftool.proofrepresentation.ProofLine;
import prooftool.proofrepresentation.ProofOptions;
import prooftool.proofrepresentation.ProofRepresentationBits.MonotonicContext;
import prooftool.proofrepresentation.ProofRepresentationBits.ProofSubtype;
import prooftool.proofrepresentation.justification.Justification;
/**
 * 
 * @author evm
 * 
 * <p> This is the main frame of the prover, and deals mostly with tabular. Actions
 * such as undo and zoom are abstracted away to the classes that implement them. </p>
 */
public class MainUI extends JFrame implements ChangeListener, WindowListener {
	
	private static final long serialVersionUID = -7227052246337594503L;
	
	/**
	 * keeps track of the number of proofs opened
	 */
	private int numTabs;
	
	/**
	 *  keeps track of the number of new proofs opened
	 */
	private int newProofs;
	
	/**
	 *  hashmap of proof name and tab contents
	 */
	private Map<String, ArrayList<TabPanelControl>> tabs;

	/**
	 * The display elements that shows the tabs.
	 */
	private JTabbedPane tabUIs;

	/**
	 * The tab that is responsible with program settings.
	 */
	private SettingsTab settings;
	
	/**
	 * The tab that is responsible with opening existing proofs saved on disk.
	 */
	private OpenTab open;
	
	/**
	 * The tab that is responsible with help related topics.
	 */
	private HelpTab help;

	/**
	 *  This is used to prevent the open tab from opening double tabs
	 */
	private boolean openTabStop;

	/**
	 * <p>Creates a new instance of the Netty GUI. Inserts the following tabs by default:
	 * 
	 * <ul>
	 * <li> "+" tab: Allows the user to open a new blank proof.</li>
	 * <li> OpenTab: Allows the user to open an existing proof from disk.</li>
	 * <li> SettingsTab: Allows the user to modify settings in this instance.</li>
	 * </ul></p>
	 * 
	 * <p>This also adds the new WindowListener (to modify the behavior of the close button) and sets up verious
	 * properties of how the tabs are displayed.
	 * </p>
	 */
	public MainUI() {
		super(UIBits.title);
		this.numTabs = -1;
		this.newProofs = 0;
		this.openTabStop = false;

		this.tabs = new HashMap<String, ArrayList<TabPanelControl>>();

		this.tabUIs = new JTabbedPane();
		this.tabUIs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		this.tabUIs.setTabPlacement(JTabbedPane.LEFT);

		this.settings = new SettingsTab();
		this.open = new OpenTab();
		this.help = new HelpTab();

		// window property for the main window
		this.setSize(UIBits.mainWindowDimension);
		this.setFocusable(true);

		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		// adding screen elements
		this.add(this.tabUIs);

		this.openNewDocument();
		this.tabUIs.add(new NothingGUIElement(), "+");
		this.tabUIs.add(this.open, "Open");
		this.tabUIs.add(this.settings, "Settings");
		this.tabUIs.add(this.help, "Help");
		this.tabUIs.addChangeListener(this);

		this.addWindowListener(this);

		this.setLocationRelativeTo(null);
		this.setEnabled(true);
		this.setVisible(true);
	}
	
	protected void updateOpenTabFolder(File file) {
		this.open.setCurrentDirectory(file);
	}
	
	protected File getCurrentLocation() {
		return this.open.getCurrentDirectory();
	}
	
	/**
	 * <p>Switches to the OpenTab, usually happens when some other part of Netty gets an user input.
	 * (Such as user pressing Ctrl-O on the Keyboard).</p>
	 */
	public void switchToOpenNewTab() {
		this.tabUIs.setSelectedComponent(this.open);
	}

	/**
	 * <p> This builds a new proof, then opens the proof in a new tab.</p>
	 */
	private void openNewDocument() {
		this.newProofs++;

		// Build the root proof
		Proof proof = new Proof(new ExpnDirection("="), new Justification("Root Proof", true), new ProofOptions(MonotonicContext.POSITIVE, ProofSubtype.DIRECTPROOF, false));

		this.openDocument(proof, "new proof " + this.newProofs);
		this.openTabStop = false;
	}

	/**
	 * <p> Given:
	 * <ol>
	 * <li>The roof node of a proof.</li>
	 * <li>The name of the new tab being opened.</li>
	 * </ol></p>
	 * 
	 * <p> Creates the tab with the name tabName, and loads the proof proof
	 * in the newly created tab </p>
	 * 
	 * <p> If there already exists an exiting tab with the name tabName, then
	 * a suffix is appended, and the tabs are grouped. </p>
	 * 
	 * @param proof The root node of the proof to open in Netty.
	 * @param tabName The name of the proof being opened.
	 */
	public void openDocument(Proof proof, String tabName) {
		// Prevent new empty proofs from being opened
		this.openTabStop = true;
		int tabPos = this.numTabs < 0 ? 0 : this.numTabs + 1;

		TabPanelControl newTab = new TabPanelControl(tabName);
		NewTab tab = new NewTab(newTab);
		
		newTab.setTabContents(tab);
		this.tabUIs.add(newTab.getTabContents(), tabPos);
		this.tabUIs.setTabComponentAt(tabPos, newTab);
		this.tabUIs.setSelectedIndex(tabPos);		
			
		// Add this to the hashmap
		if (this.tabs.containsKey(tabName)) {
			ArrayList<TabPanelControl> addTabGroup = this.tabs.get(tabName);
			newTab.setSuffix(addTabGroup.get(addTabGroup.size() - 1).getSuffix() + 1);
			addTabGroup.add(newTab);
			// If does not exist make a new entry
		} else {
			ArrayList<TabPanelControl> newTabGroup = new ArrayList<TabPanelControl>();
			newTabGroup.add(newTab);
			this.tabs.put(tabName, newTabGroup);
		}
		
		//Init proofView
		tab.initProofView(proof);

		this.numTabs++;
		this.openTabStop = false;
	}
	
	protected void openSubProof(ProofLine proofLine, String tabName) {				
		ProofLine cloned = proofLine.clone();

		// Create the base proof
		Proof newProof = new Proof(new ExpnDirection("="), new Justification("Root Proof", true), new ProofOptions(MonotonicContext.POSITIVE, ProofSubtype.DIRECTPROOF, false));
		ProofLine newProofLine = new ProofLine(cloned, newProof);
		
		//Deal with context
		newProof.setParentContext(proofLine.getParent().getFullContext());
		newProof.add(newProofLine);

		// Add the children
		int index = proofLine.getParent().getChildren().indexOf(proofLine);
		if (index != proofLine.getParent().getChildren().size() - 2) {
			//Offset by 2 because of EditorNode
			ProofElement pe = proofLine.getParent().getChildren().get(index + 2);
			
			if (pe instanceof Proof) {
				Proof children = (Proof) pe;
				newProof.add(children.clone());
				
				//Regen context
				children.getFullContext();
			}
		}
		
		//Regen context
		newProof.getFullContext();
		this.openDocument(newProof, tabName);
	}

	protected void importSubProof(Proof proof, TabPanelControl tcp) {
		HashMap<String, ArrayList<TabPanelControl>> cloned = new HashMap<String, ArrayList<TabPanelControl>>();
		cloned.putAll(this.tabs);

		// Make the list for the user to select which tab
		ArrayList<TabPanelControl> tabList = new ArrayList<TabPanelControl>();
		for (Map.Entry<String, ArrayList<TabPanelControl>> group : cloned
				.entrySet()) {
			tabList.addAll(group.getValue());
		}

		// Do not want to import to self
		tabList.remove(tcp);

		// Check if an tab exists to import to
		if (tabList.isEmpty()) {
			JOptionPane.showMessageDialog(this,
					UIBits.mainUIProofImportNoTabsContent,
					UIBits.mainUIProofImportSelectTabTitle,
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		TabPanelControl importTo = (TabPanelControl) JOptionPane
				.showInputDialog(this,
						UIBits.mainUIProofImportSelectTabContent,
						UIBits.mainUIProofImportSelectTabTitle,
						JOptionPane.INFORMATION_MESSAGE, null,
						tabList.toArray(), tabList.get(0));

		if (importTo == null) {
			return;
		}

		importTo.getTabContents().getProofTree().importSubproof(null, proof.clone());
		this.tabUIs.setSelectedComponent(importTo.getTabContents());
	}

	public void closeDocuement(TabPanelControl tab) {		
		if (tab.getTabContents().getProofTree().isDirty()) {
			switch (JOptionPane.showConfirmDialog(this, 
					String.format(UIBits.mainUISavePrompt, tab.getTabName()),
					UIBits.title, JOptionPane.YES_NO_CANCEL_OPTION)) {
			case JOptionPane.YES_OPTION:
				tab.getTabContents().saveProofSequence();
			case JOptionPane.NO_OPTION:
				break;
			case JOptionPane.CANCEL_OPTION:
				return;
			}
		}

		ArrayList<TabPanelControl> dups = this.tabs.get(tab.getTabName());
		if (dups.size() == 1)
			this.tabs.remove(tab.getTabName());
		else
			dups.remove(tab);

		this.tabUIs.remove(tab.getTabContents());

		// Make sure there is at least one doc open
		if (--this.numTabs == -1) {
			this.openTabStop = true;
			this.openNewDocument();
		} else {
			this.tabUIs.setSelectedIndex(this.numTabs);
		}
		
		//Force the system to do garbage collection
		//It will prob find some memeory that needs to be
		//cleaned!
		System.gc();
	}

	public void updateTabName(TabPanelControl oldTab, String newName) {
		// If only one instance was opened this is easy.
		ArrayList<TabPanelControl> dups = this.tabs.get(oldTab.getTabName());

		// Remove oldname from the list
		// If only one instance remove the whole set from the map
		// Otherwise remove from the list
		if (dups.size() == 1) {
			this.tabs.remove(oldTab.getTabName());
		} else {
			dups.remove(oldTab);
		}

		// Determine where to place it
		// If a unq name then make a new list
		// Otherwise add the existing list
		if (this.tabs.containsKey(newName)) {
			ArrayList<TabPanelControl> toAddTo = this.tabs.get(newName);
			oldTab.setSuffix(toAddTo.get(toAddTo.size() - 1).getSuffix() + 1);
			toAddTo.add(oldTab);
		} else {
			ArrayList<TabPanelControl> newGroup = new ArrayList<TabPanelControl>();
			newGroup.add(oldTab);
			this.tabs.put(newName, newGroup);
		}
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		// Important to remove change listener to prevent inf events going off
		this.tabUIs.removeChangeListener(this);
		
		int selectedIndex = this.tabUIs.getSelectedIndex();

		// User clicked on the +
		if (selectedIndex == this.numTabs + 1 && !this.openTabStop) {
			this.openNewDocument();
		// User clicked on the the open button, refresh filesystem view
		} else if (selectedIndex == this.numTabs + 2 && !this.openTabStop) {
			this.open.updateUI();
		}
		
		// In case they updated view, update it
		Component c = this.tabUIs.getSelectedComponent();
		if (c instanceof NewTab) {
			NewTab nT  = (NewTab) c;
			nT.updateView();
		}		

		// Add it back in
		this.tabUIs.addChangeListener(this);
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	// Used to ask the user if they are sure they want to exit
	public void windowClosing(WindowEvent e) {
		HashMap<String, ArrayList<TabPanelControl>> cloned = new HashMap<String, ArrayList<TabPanelControl>>();
		cloned.putAll(this.tabs);

		eachGroup: 
		for (Map.Entry<String, ArrayList<TabPanelControl>> group : cloned.entrySet()) {
			String tabName = group.getKey();
			ArrayList<TabPanelControl> dups = group.getValue();

			// First check if any of them are dirty, if none are do nothing!
			// Also check to see if there is only one instance with unsaved
			// changes
			int numDirty = 0;
			TabPanelControl lastDirty = null;

			for (TabPanelControl tpc : dups) {				
				if (tpc.getTabContents().getProofTree().isDirty()) {
					++numDirty;
					lastDirty = tpc;
				}
			}

			// Easy, do nothing!
			if (numDirty == 0)
				continue eachGroup;
			// Also easy, ask if they want to save that single instance
			if (numDirty == 1) {
				switch (JOptionPane.showConfirmDialog(this, String.format(
						UIBits.mainUIPromptSaveNonDupContent, tabName),
						UIBits.mainUIPromptSaveNonDupTitle,
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.WARNING_MESSAGE)) {
				// Give the prompt for the user to save
				case JOptionPane.YES_OPTION:
					if (!lastDirty.getTabContents().saveProofSequence())
						return;
					continue eachGroup;
					// Do nothing go on
				case JOptionPane.NO_OPTION:
					continue eachGroup;
					// Stops the progress, returns from the function
				case JOptionPane.CANCEL_OPTION:
					return;
				}
			}
			
			// Option for not saving any of the instances
			DoNotSave doNotSave = new DoNotSave();
			dups.add(doNotSave);

			// We have multi instances with unsaved changes, ask the user which
			// one they want saved
			TabPanelControl toSave = (TabPanelControl) JOptionPane.showInputDialog(this, 
							String.format(UIBits.mainUIPromptSaveDupContent, tabName,tabName), 
							UIBits.mainUIPromptSaveDupTitle,
							JOptionPane.WARNING_MESSAGE, 
							UIBits.warnPic, 
							dups.toArray(), 
							dups.get(0));

			dups.remove(doNotSave);
			
			// If the user hits cancel, then we return! Otherwise we save
			if (toSave == null)
				return;

			// If the user did not want to save any
			if (toSave == doNotSave)
				continue eachGroup;

			if (!toSave.getTabContents().saveProofSequence())
				return;
			continue eachGroup;
		}
		
		//Save the settings
		Settings.save_Settings();

		System.exit(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}
}
