package prooftool.gui.lawlibrary;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Expression;
import prooftool.gui.Settings;
import prooftool.gui.UIBits;
import prooftool.gui.elements.NettyFile;
import prooftool.util.Main;


/**
 * 
 * @author maghari1
 *
 */
public class LawLibrary extends JSplitPane implements ActionListener, ListSelectionListener {

	private static final long serialVersionUID = -6581020332550105030L;

	//All the laws
	private LawBlob allLaws;
	
	//Map between Files and List of Expressions
	private Map<File, List<Expression>> nameToLaw;
	
	//GUI that lists the files that are loaded
	private JList<NettyFile> fileList;
	private DefaultListModel<NettyFile> fileListModel;
	
	//GUI that lists the laws
	private JTable lawTable;
	
	public LawLibrary() {		
		//File List
		this.fileListModel = new DefaultListModel<NettyFile>();
		this.fileList = new JList<NettyFile>(this.fileListModel);
		this.fileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.fileList.addListSelectionListener(this);
		
		//Load laws
		this.allLaws = new LawBlob();
		this.nameToLaw = new HashMap<File, List<Expression>>();
		this.initLawList();
		
		//Left Side
		JPanel leftSide = new JPanel();
		leftSide.setLayout(new BorderLayout());
		
		JScrollPane fileListScroll = new JScrollPane();
		fileListScroll.setViewportView(this.fileList);
		
		//Add/Remove Buttons
		JButton add = new JButton(UIBits.lawLibraryAdd);
		add.setActionCommand(UIBits.lawLibraryAdd);
		add.addActionListener(this);
		
		JButton remove = new JButton(UIBits.lawLibraryRemove);
		remove.setActionCommand(UIBits.lawLibraryRemove);
		remove.addActionListener(this);
		
		JButton reloadLibrary = new JButton(UIBits.lawLibraryReloadLibrary);
		reloadLibrary.setActionCommand(UIBits.lawLibraryReloadLibrary);
		reloadLibrary.addActionListener(this);
		
		JButton saveLibrary = new JButton(UIBits.lawLibrarySaveLibrary);
		saveLibrary.setActionCommand(UIBits.lawLibrarySaveLibrary);
		saveLibrary.addActionListener(this);
		
		JPanel addremovePanel = new JPanel();
		addremovePanel.setLayout(new GridLayout(2, 2, 0, 0));
		addremovePanel.add(add);
		addremovePanel.add(remove);
		addremovePanel.add(reloadLibrary);
		addremovePanel.add(saveLibrary);
		
		leftSide.add(new JLabel(UIBits.lawLibraryLabel), BorderLayout.PAGE_START);
		leftSide.add(fileListScroll, BorderLayout.CENTER);
		leftSide.add(addremovePanel, BorderLayout.PAGE_END);
		
		//Right Side
		this.lawTable = new JTable();
		this.lawTable.setFont(UIBits.lawLibraryFont);
		this.lawTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JScrollPane lawTableScroll = new JScrollPane();
		lawTableScroll.setViewportView(this.lawTable);
		
		this.setLeftComponent(leftSide);
		this.setRightComponent(lawTableScroll);
		this.setDividerLocation(300);
		
		//Set default selections
		this.fileList.setSelectedIndex(0);
		this.setTable(0);
	}
	
	public LawBlob getLaws() {
		return this.allLaws;
	}
	
	public void addLaw(Expression theorem) {
		JFileChooser fc = new JFileChooser();

		fc.setCurrentDirectory(UIBits.currentDir);
		JTextArea l = new JTextArea(theorem.toString());
		l.setEditable(false);
		l.setColumns(30);
		l.setLineWrap(true);
		fc.setAccessory(l);

		if (fc.showDialog(this, "Add Law") == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			UIBits.currentDir = file.getParentFile();
			FileWriter fstream;
			try {
				fstream = new FileWriter(file, true);
				BufferedWriter out = new BufferedWriter(fstream);
				out.write("\n" + theorem.toString());
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void saveLibrary() {
		ArrayList<File> newList = new ArrayList<File>();
		for (int i = 0; i < this.fileListModel.getSize(); i++) {
			newList.add(this.fileListModel.getElementAt(i).getFile());
		}
		
		Settings.get_Settings().setLawFiles(newList);
		Settings.save_Settings();
	}
	
	private void initLawList() {
		List<File> fileList = Settings.get_Settings().getLawFiles();
		
		for (File file: fileList) {			
			List<Expression> toAdd = this.parseLaws(file);
			
			if (toAdd == null) {
				continue;
			}
			
			this.nameToLaw.put(file, toAdd);
			this.allLaws.addAllLaws(toAdd);
			this.fileListModel.addElement(new NettyFile(file));
		}
	}
	
	private List<Expression> parseLaws(File file) {
		try {
			return Main.parse(file.toString(), true);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Unable to load law file: " + file.toString(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		
		return null;
	}
	
	private void setTable(int index) {
		if (this.fileListModel.isEmpty()) {
			this.lawTable.setModel(new LawTableModel(new ArrayList<Expression>()));
		} else {
			this.lawTable.setModel(new LawTableModel(this.nameToLaw.get(this.fileListModel.get(index).getFile())));
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//We need to remove and selectionlistener to prevent switching
		//while stuff is happening
		this.fileList.removeListSelectionListener(this);
		String action = e.getActionCommand();		
		
		if (action.equalsIgnoreCase(UIBits.lawLibraryAdd)) {
			// open a file choose
			JFileChooser fc = new JFileChooser();
			fc.setCurrentDirectory(UIBits.currentDir);
			
			int returnVal = fc.showDialog(this, "Add");

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File selectedFile = fc.getSelectedFile();				
				List<Expression> newLaws = this.parseLaws(selectedFile);
				
				if (newLaws != null) {
					this.nameToLaw.put(selectedFile, newLaws);
					this.allLaws.addAllLaws(newLaws);
					this.fileListModel.addElement(new NettyFile(selectedFile));
					
					this.fileList.setSelectedValue(selectedFile, true);
					
					//Set newly loaded laws to view
					int selected = this.fileList.getSelectedIndex();
					this.setTable(selected < 0 ? 0 : selected);
				}
				
				UIBits.currentDir = fc.getCurrentDirectory();
			}
		} else if (action.equalsIgnoreCase(UIBits.lawLibraryRemove)) {
			//Lawblob does not actually have the ability to remove laws
			//so we create a new one and readd the remaining laws
			File selected = this.fileList.getSelectedValue().getFile();
			int selectedIndex = this.fileList.getSelectedIndex();
			
			if (selected == null) {
				return;
			}

			this.fileListModel.remove(selectedIndex);
			this.nameToLaw.remove(selected);
			
			//There is no way to remove law file from lawblob so we have to regenerate the it
			this.allLaws = new LawBlob();
			for (File f: this.nameToLaw.keySet()) {
				this.parseLaws(f);
			}
			
			selectedIndex = selectedIndex <= 0 ? 0 : selectedIndex - 1;
			this.fileList.setSelectedIndex(selectedIndex);
			this.setTable(selectedIndex);
		} else if (action.equalsIgnoreCase(UIBits.lawLibrarySaveLibrary)) {
			this.saveLibrary();
		} else if (action.equalsIgnoreCase(UIBits.lawLibraryReloadLibrary)) {
			this.nameToLaw.clear();
			this.fileListModel.removeAllElements();
			this.allLaws = new LawBlob();
			
			this.initLawList();
			this.fileList.setSelectedIndex(0);
			this.setTable(0);
		}
		
		//Add it back in
		this.fileList.addListSelectionListener(this);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {		
		this.setTable(this.fileList.getSelectedIndex());
	}
}
