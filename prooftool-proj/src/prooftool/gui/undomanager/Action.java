package prooftool.gui.undomanager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 
 * @author evm
 * 
 * <p> This invokes an action with a set of parameters. Used in state to define an action to either undo/redo an action in Netty. <br>
 * Please note that since this is using Java reflections, if a method is called with an incorrect number of params or wrong params types or etc... then this will raise an expection. </p>
 *
 */
public class Action {

	private Method actionTodo;
	private Object[] params;
	
	/**
	 * Defines an action with method actionTodo and parameters params.
	 * 
	 * @param actionTodo Method used in action
	 * @param params Parameters used to call the method actionTodo.
	 */
	protected Action(Method actionTodo, Object[] params) {		
		this.actionTodo = actionTodo;
		this.params = params;
	}
	
	/**
	 * <p> Invokes the method actionTodo on object onWhat with parameters params. <br>
	 * 
	 * If the method is protected or private, setAccessible is set to true and then set back to what it was before.<br>
	 * 
	 * <strong>If there is a security manager implemeted, make sure setAccessible is allowed, otherwise the Undo Manager will break.</strong>
	 * </p>
	 * 
	 * @param onWhat
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	protected void invokeAction(Object onWhat) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		System.out.println("know invoking method: " + this.actionTodo);
		System.out.print("params: ");
		for (Object o: this.params) {
			if (o == null) System.out.print("NULL");
			else System.out.print(o.getClass().getName() + "\t" + o);
		}
		System.out.println("");
		
		//Just in case we are dealing with a private or protected method		
		boolean before = this.actionTodo.isAccessible();
		this.actionTodo.setAccessible(true);		
		this.actionTodo.invoke(onWhat, this.params);
		this.actionTodo.setAccessible(before);
	}
	
	/**
	 * 
	 * @return The parameters used as an Object array.
	 */
	protected Object[] getParams() {
		return this.params;
	}
	
	/**
	 * This replaces the old objects with the newobjs from the param list.
	 * 
	 * @param orgObj
	 * @param newObj
	 */
	protected void updateParams(Object orgObj, Object newObj) {		
		for (int i = 0; i != this.params.length; i++) {
			if (this.params[i] == orgObj) {
				this.params[i] = newObj;
				return;
			}
		}
	}
}
