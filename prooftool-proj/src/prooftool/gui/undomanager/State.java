package prooftool.gui.undomanager;

import java.lang.reflect.InvocationTargetException;


/**
 * 
 * @author evm
 *
 */
public class State {

	private State nextState;
	private State prevState;
	
	private Action nextAction;
	private Action prevAction;
	
	protected State(State prev, Action prevAction) {
		this.nextState = null;
		this.nextAction = null;
		
		this.prevState = prev;
		this.prevAction = prevAction;
	}
	
	/*
	 * Used to construct the first state in the proof. Where no action has occured
	 */
	protected State() {
		this.nextState = null;
		this.nextAction = null;
		
		this.prevState = null;
		this.prevAction = null;
	}
	
	protected void setNextState(State next, Action nextAction) {
		this.nextState = next;
		this.nextAction = nextAction;
	}
	
	protected State getPrevState() {
		return this.prevState;
	}
	
	protected State getNextState() {
		return this.nextState;
	}
	
	protected Action getPrevAction() {
		return this.prevAction;
	}
	
	protected Action getNextAction() {
		return this.nextAction;
	}
	
	protected void invokeNext(Object onWhat) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		this.nextAction.invokeAction(onWhat);
	}
	
	protected void invokePrev(Object onWhat) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		this.prevAction.invokeAction(onWhat);
	}
}
