package prooftool.gui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

public class Settings {
	//Options from SetttingsTab
	@Expose private boolean greySug;
	@Expose private boolean zoomType;
	@Expose private boolean hideContextPanel;
	@Expose private boolean hideSuggestionPanel;
	@Expose private boolean showGeneratorSource;
	@Expose private List<String> lawFiles;
	
	private static Settings settings;
	private static File settingsFile = new File("netty.conf");
	private static Gson parser;
	static {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder = gsonBuilder.setPrettyPrinting();
		gsonBuilder = gsonBuilder.excludeFieldsWithModifiers(Modifier.STATIC);
		gsonBuilder = gsonBuilder.excludeFieldsWithoutExposeAnnotation();
		Settings.parser = gsonBuilder.create();
	}
	
	/**
	 * Gets the settings for Netty
	 * 
	 * @return Settings object thats contains all the settings used for this server
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Settings get_Settings() {
		if (Settings.settings != null) {
			return Settings.settings;
		}
		
		if (Settings.settingsFile.exists()) {
			BufferedReader io = null;
			
			try {
				io = new BufferedReader(new FileReader(Settings.settingsFile));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			Settings.settings = Settings.parser.fromJson(io, Settings.class);
			
			try {
				io.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return Settings.settings;
		}
		
		Settings.settings = new Settings();
		Settings.save_Settings();

		return Settings.settings;		
	}
	
	public static void save_Settings() {
		BufferedWriter io;
		
		try {
			io = new BufferedWriter(new FileWriter(Settings.settingsFile));
			io.write(Settings.parser.toJson(Settings.settings));
			io.flush();
			io.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private Settings() {
		this.greySug = false;
		this.zoomType = false;
		this.hideContextPanel = false;
		this.hideSuggestionPanel = false;
		this.showGeneratorSource = false;
		this.lawFiles = new ArrayList<String>();
	}	
	
	public boolean isGreySug() {
		return greySug;
	}

	public void setGreySug(boolean greySug) {
		this.greySug = greySug;
	}

	public boolean isZoomType() {
		return zoomType;
	}

	public void setZoomType(boolean zoomType) {
		this.zoomType = zoomType;
	}

	public boolean isHideContextPanel() {
		return hideContextPanel;
	}

	public void setHideContextPanel(boolean hideContextPanel) {
		this.hideContextPanel = hideContextPanel;
	}

	public boolean isHideSuggestionPanel() {
		return hideSuggestionPanel;
	}

	public void setHideSuggestionPanel(boolean hideSuggestionPanel) {
		this.hideSuggestionPanel = hideSuggestionPanel;
	}

	public boolean isShowGeneratorSource() {
		return showGeneratorSource;
	}

	public void setShowGeneratorSource(boolean showGeneratorSource) {
		this.showGeneratorSource = showGeneratorSource;
	}
	
	public List<File> getLawFiles() {
		List<File> fileList = new ArrayList<File>();
		for (String file: this.lawFiles) {
			fileList.add(new File(file));
		}
		
		return fileList;
	}
	
	public void setLawFiles(ArrayList<File> newLawFiles) {
		this.lawFiles = new ArrayList<String>();
		for (File f: newLawFiles) {
			this.lawFiles.add(f.toString());
		}
	}
}
