package prooftool.backend.direction;

import prooftool.backend.laws.Identifier;

public interface Direction {
	
	public String toString();
	public boolean isCompatible(Direction anotherDirection);		
	public Direction reverse();
	public boolean equals(Direction other);
	public Identifier getIdent();
}
