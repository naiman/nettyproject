package prooftool.backend.lawblob;

import prooftool.backend.laws.Variable;

/**
 * 
 * @author lev
 *
 */
public class VarIndex {
	private Integer i;
	private Variable v;

	protected VarIndex(Integer i, Variable v) {
		this.i = i;
		this.v = v;
	}

	protected VarIndex(int i, Variable v) {
		this.i = new Integer(i);
		this.v = v;
	}

	@Override
	public String toString() {
		return "(" + i + "," + v + ")";
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof VarIndex) {
			return this.equals((VarIndex) o);
		} else {
			return false;
		}
	}

	private boolean equals(VarIndex vi) {
		return this.i.equals(vi.i) && this.v.getId().equals(vi.v.getId());
	}
}
