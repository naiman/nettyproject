package prooftool.backend.lawblob;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import prooftool.backend.Step;
import prooftool.backend.Substitution;
import prooftool.backend.direction.Direction;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Scope;
import prooftool.backend.laws.Variable;

/**
 * 
 * @author maghari1
 *
 */
public class Unify implements Callable<Step> {
	//Maps an index to the expression to instantiate, direction, and free variables
	private static Map<Integer, LawInfo> lawMap;

	// mapping of INSTANTIABLE LAW variables to the expressions they unified
	// with
	private static Map<VarIndex, Expression> subst;

	// mapping of LOCAL variables to the expressions they unified with
	private static Map<VarIndex, Expression> scopeSubst;

	// This keeps track of variables we FUNCTIONIZED. All these variables are
	// also in subst
	private static Map<VarIndex, Scope> functionized;
	
	private Integer i;
	private Direction dir;

	protected Unify (Integer i, Direction dir) {
		this.i = i;
		this.dir = dir;
	}
	
	protected static void init_unify(Map<Integer, LawInfo> lawMap, Map<VarIndex, Expression> subst, Map<VarIndex, Expression> scopeSubst, Map<VarIndex, Scope> functionized) {
		Unify.lawMap = lawMap;
		Unify.subst = subst;
		Unify.scopeSubst = scopeSubst;
		Unify.functionized = functionized;
	}
	
	@Override
	public Step call() {
		//clone the law with variables we can keep track of
		LawInfo info = Unify.lawMap.get(this.i);		
		if (!dir.isCompatible(Unify.lawMap.get(i).getDirection())) {
			return null;
		}

		Expression law = info.getLaw();
		Map<Variable, Variable> varMap = law.getAllVarsHelper();
		Expression focus = law.clone(varMap);

		// get all the instantiables of this law
		Map<Variable, Expression> instMap = new HashMap<Variable, Expression>();
		Map<Variable, Scope> funMap = new HashMap<Variable, Scope>();
		Map<Variable, Variable> scMap = new HashMap<Variable, Variable>();
		
		// will be more efficient if we record instantiables
		for (Variable v : varMap.keySet()) {
			VarIndex vi = new VarIndex(i, v);
			if (Unify.subst.containsKey(vi)) {
				instMap.put(varMap.get(v), Unify.subst.get(vi));
			}

			// get the local var fun of this law also
			if (Unify.functionized.containsKey(vi)) {
				assert Unify.subst.containsKey(vi);
				funMap.put(varMap.get(v), Unify.functionized.get(vi));
			}

			if (scMap.containsKey(vi)) {
				scMap.put(varMap.get(v), (Variable) Unify.scopeSubst.get(vi));
			}
		}

		// temporary hack so that I don't have to write a new instantiate method
		Substitution sub = new Substitution(new ArrayList<Variable>(), instMap, scMap, funMap);

		if (this.appearsLocal(sub)) {
			return null;
		}

		focus = focus.instantiate(sub);

		return new Step(info.getDirection(), focus, info.getUnbound(), info.getOriginalLaw(), sub);
	}
	
	/**
	 * TODO: this method is actually a hack. will have to fix it later
	 * 
	 * @param sub
	 * @return
	 */
	private boolean appearsLocal(Substitution sub) {
    	for (Variable v: sub.getScopeSubst().keySet()) {
    		Variable u = sub.getScopeSubst().get(v);
    		assert u != null;
    		
    		for (Variable x: sub.getSubst().keySet()) {
    			Expression y = sub.getSubst().get(x);   
    			
    			if (y != null && y.contains(u)) {
    				return true;
    			}
    		}
    	}
    	
    	return false;
    }	
}
