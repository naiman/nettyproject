package prooftool.backend.lawblob;

import java.util.ArrayList;
import java.util.List;

import prooftool.backend.direction.Direction;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Variable;
import prooftool.util.ExpressionUtils;

public class LawInfo {
	private Expression law;
	private Expression originalLaw;
	
	private List<Variable> unbound;
	private Direction dir;

	protected LawInfo(Expression law, Expression originalLaw, List<Variable> unbound, Direction dir) {
		this.law = law;
		this.originalLaw = originalLaw;
		this.unbound = unbound;
		this.dir = dir;
	}

	protected LawInfo(Expression law, Expression originalLaw) {
		this(law, originalLaw, new ArrayList<Variable>(), ExpressionUtils.eqDir);
	}

	@Override
	public String toString() {
		return law.toString();
	}
	
	protected Expression getLaw() {
		return this.law;
	}
	
	protected Expression getOriginalLaw() {
		return this.originalLaw;
	}
	
	protected Direction getDirection() {
		return this.dir;
	}
	
	protected List<Variable> getUnbound() {
		return this.unbound;
	}
}