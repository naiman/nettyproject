package prooftool.backend.suggestions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import prooftool.backend.Step;
import prooftool.backend.direction.Direction;
import prooftool.backend.direction.ExpnDirection;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Identifier;
import prooftool.backend.laws.Literal;
import prooftool.backend.laws.Variable;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.suggestion.Suggestion;
import prooftool.util.ExpressionUtils;
import prooftool.util.Identifiers;
import prooftool.util.Symbol;

public class ProgrammingSuggestor implements SuggestionGenerator {
	
	private static char primeChar = ExpressionUtils.prime.charAt(0); 
	private static final String nattime = "nattime";
	private static final String realtime = "realtime";
	private static HashMap<String,Expression> sigma = null;
	
	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, List<Expression> allLaws) {
		List<Suggestion> sug = new ArrayList<Suggestion>();
		List<Suggestion> ret = new ArrayList<Suggestion>();
		LawBlob context = new LawBlob();
		LawBlob laws = new LawBlob();
		
		for(Expression law:allLaws) {
			laws.add(law);
		}
		
		sigma = null;
		sug.add(assignmentExpansion(focus, laws, context));
		sug.add(assignmentExpansion(focus, laws, context, "×", " (Probability)"));
		sug.add(substitutionLaw(focus, laws, context));
		sug.add(dependentComposition(focus, laws, context));
		sug.add(dependentComposition(focus, laws, context, "∑", "×", " (Probability)"));
		sug.add(okExpand(focus, laws, context));
		sug.add(okExpand(focus, laws, context, "×", " (Probability)"));
		sug.add(okContract(focus, laws, context));
		sug.add(independentComposition(focus, laws, context));
		
		for (Suggestion s:sug) {
			if (s!=null) {
				ret.add(s);
			}
		}
		
		return ret;
	}
	

	/** look at all laws (which includes context), and whenever we
	 * have a law of the form 'identifier':'domain' and a similar law
	 * where the identifier is primed (and the former is unprimed), we can 
	 * conclude that the identifier is a state variable. 
	 * 
	 * We allow only the pair of single-primed and unprimed pair of variables
	 * to represent a state variable.  
	 * 
	 */
	private static HashMap<String,Expression> computeSigma(LawBlob laws, LawBlob context) {		
		return computeSigma(context.getInputLaws());
	}
	
	public static HashMap<String,Expression> computeSigma(List<Expression> laws) {				
		if (sigma!=null) {
			return sigma;
		}
		
		HashMap<String,Expression> sigmaMap = new HashMap<String,Expression>(); 
		HashMap<String,String> unprimedMap = new HashMap<String,String>();
		HashMap<String,String> primedMap = new HashMap<String,String>();
		
		// determine all the unprimed an primed declarations
		for (Expression law:laws) {
			if ((law instanceof Application)
					&&(((Application)law).getFunction().toString().equals(":"))
					&& (((Application)law).getChild(0) instanceof Variable)) {
				String var = ((Application)law).getChild(0).toString();
				String type = ((Application)law).getChild(1).toString();
				
				if (var.charAt(var.length()-1) == primeChar) {
					assert var.length() >=2;			
					if (isSinglePrimed(var)) {
						primedMap.put(var.substring(0, var.length()-1), type);
						continue;
					}
				} else { 
					assert var.indexOf(primeChar) == -1;
					unprimedMap.put(var, type);
				}
			}
		}		
		
		// with the primed and unprimed lists populated, determine where there
		// is a match, allowing us to conclude that we found a state variable
		for (String var:primedMap.keySet()) {
			if (unprimedMap.containsKey(var) 
					&& primedMap.get(var).equals(unprimedMap.get(var))) {
				Expression type = ExpressionUtils.parse(primedMap.get(var));
				
				sigmaMap.put(var, type);
			}
		}
		
		sigma = sigmaMap;
		return sigmaMap;
	}
	
	private static boolean isSinglePrimed(String name) {
		return name.indexOf(primeChar) == name.length()-1;
	}
	
	private Suggestion assignmentExpansion(Expression focus, LawBlob laws, LawBlob context) {
		return assignmentExpansion(focus, laws, context, "∧", "");
	}
	
	private Suggestion assignmentExpansion(Expression focus, LawBlob laws, LawBlob context, String op, String just) {
		if ((focus instanceof Application)
				&&(((Application)focus).getFunction().toString().equals(":="))
				&& focus.getChild(0) instanceof Variable) {	//TODO add support for array assignment		
			List<String> sigma = new ArrayList<String>(ProgrammingSuggestor.computeSigma(laws, context).keySet());
			
			//anything that is being assigned to must itself be a state variable
			//hence if it is not, no suggestion is generated			
			if (sigma.contains(focus.getChild(0).toString())) {

				String expandedAssignment = "(" + focus.getChild(0) + "" + primeChar + "=" + focus.getChild(1) + ")";
				for (String stateVar:sigma) {
					if (!stateVar.equals(focus.getChild(0).toString())) {
						expandedAssignment += op + "(" + stateVar + primeChar + "=" + stateVar +")";
					}
				}

				Expression expanded = ExpressionUtils.parse(expandedAssignment);
				Direction newDir = new ExpnDirection("=");	            			
				Justification j = new Justification("Assignment" + just, true);
				return new Suggestion(new Step(newDir, expanded), j, this.getGeneratorName());
			}
		}
		
		return null;
	}	
	
	private Suggestion substitutionLaw(Expression focus, LawBlob laws, LawBlob context) {		
		if ((focus instanceof Application)
				&&(((Application)focus).getFunction().toString().equals("."))) {
			Expression right = focus.getChild(1);
			Expression left = focus.getChild(0);
			
			if (left instanceof Application
					&&((Application)left).getFunction().toString().equals(":=")
					&& noProgram(right)) {
				List<String> sigma = new ArrayList<String>(ProgrammingSuggestor.computeSigma(laws, context).keySet());
				Variable v = (Variable) left.getChild(0);
								
				if (sigma.contains(v.toString()) && this.allUnprimed(left.getChild(1), sigma)) {
					Expression e = left.getChild(1);
					Expression typeInContext = this.typeInContext(v, context.getInputLaws()); //TODO fix this
					Expression dom = (typeInContext == null? ExpressionUtils.ALL:typeInContext);
					Expression sub = ExpressionUtils.parse("⟨" + v + ":" + dom + "→" + right + "⟩ " + "(" + e + ")");
					Direction newDir = new ExpnDirection("=");
					Justification j = new Justification("Substitution", true);
					return new Suggestion(new Step(newDir, sub), j, this.getGeneratorName());
				}			
			}
		}
		
		return null;
	}
	
	private Suggestion okExpand(Expression focus, LawBlob laws, LawBlob context) {
		return okExpand(focus, laws, context, "∧", "");
	}
	
	private Suggestion okExpand(Expression focus, LawBlob laws, LawBlob context, String op, String just) {
		Expression clone = focus.clone();
		clone.flatten();
		if (focus.equals(Literal.ok)) {
			Expression expanded = this.computeOk(laws, context, op);
			if (expanded!=null) {
				Direction newDir = ExpressionUtils.eqDir;	            			
				Justification j = new Justification("Expand ok" + just, true);

				return new Suggestion(new Step(newDir, expanded), j, this.getGeneratorName());
			}
		}
		return null;
	}
	
	private Expression computeOk(LawBlob laws, LawBlob context) {
		return computeOk(laws, context, "∧");
	}
	
	private Expression computeOk(LawBlob laws, LawBlob context, String op) {
		HashMap<String,Expression> sigma = ProgrammingSuggestor.computeSigma(laws, context);
		if(sigma.isEmpty()) {
			return null;
		}
		
		StringBuilder expanded = new StringBuilder();
		int cnt = 0;
		for (String stateVar:sigma.keySet()) {
			expanded.append("(");
			expanded.append(stateVar);
			expanded.append(primeChar);
			expanded.append("=");
			expanded.append(stateVar);
			expanded.append(")");
			
			if (cnt+1<sigma.keySet().size()) {
				expanded.append(op);
			}
			cnt++;
		}
		
		return ExpressionUtils.parse(expanded.toString());
	}
	
	private Suggestion okContract(Expression focus, LawBlob laws, LawBlob context) {
		Expression ok = this.computeOk(laws, context);
		Expression ok2 = this.computeOk(laws, context, "×");
		
		if (ok !=null && focus!=null && ok.equals(focus) || ok2 != null && ok2.equals(focus)) {			
			Direction newDir = ExpressionUtils.eqDir;	            			
			Justification j = new Justification("Contract ok", true);
			return new Suggestion(new Step(newDir, Literal.ok), j, this.getGeneratorName());			
		}
		
		return null;
	}
	
	private Suggestion dependentComposition(Expression focus, LawBlob laws, LawBlob context) {
		return dependentComposition(focus, laws, context, "∃", "∧", "");
	}
	
	private Suggestion dependentComposition(Expression focus, LawBlob laws, LawBlob context, String quant, String op, String just) {	
		//check if the focus is a dependent composition
		if ((focus instanceof Application)
				&&(((Application)focus).getFunction().toString().equals("."))) {			
			
			// check if both sides contain no programming notation
			if (noProgram(focus.getChild(0)) && noProgram(focus.getChild(1))) {
				HashMap<String,Expression> sigma = ProgrammingSuggestor.computeSigma(laws, context);						
				
				if(sigma.isEmpty()) {
					return null;
				}
				
				// get a string of primed and unprimed variable declarations
				StringBuilder stateVars = new StringBuilder();
				StringBuilder doublePrimed = new StringBuilder();
				for (String s:sigma.keySet()) {
					stateVars.append("∀" + s + "," + s + primeChar + ":" + sigma.get(s) + "·");
					doublePrimed.append(quant + s + primeChar + primeChar + ":" + sigma.get(s) + "·");
				}
				
				//inefficient hack to be able to instantiate the right variables				
				// but it works!
				Expression hack = ExpressionUtils.parse(stateVars.toString() + focus);

				//get the law vars, and create a mapping of unprimed to double primed, and primed to double primed
				List<Variable> vars = hack.getLawVars();
				HashMap<Variable, Expression> leftInst = new HashMap<Variable, Expression>();
				HashMap<Variable, Expression> rightInst = new HashMap<Variable, Expression>();
				List<Variable> dprimed = new ArrayList<Variable>();
				for (Variable v:vars) {
					if (!v.toString().contains(primeChar+"")) {
						Variable dp = (Variable)ExpressionUtils.parse(v.toString() + primeChar + primeChar);
						dprimed.add(dp);
						rightInst.put(v, dp);
						for (Variable u:vars) {
							if ((v.toString() + primeChar).equals(u.toString())) {
								leftInst.put(u, dp);
							}
						}
					}
				}
				
				// instantiate the left side with the primed to double, and right side with unprimed to double
				Expression body = hack.getLawBody();
				Expression left = body.getChild(0).instantiate(leftInst);
				Expression right = body.getChild(1).instantiate(rightInst);				
				
				
				Expression expanded = ExpressionUtils.parse(doublePrimed.toString() 
						+ "(" + left + ")" + op + "(" + right + ")");
				
				
				Direction newDir = ExpressionUtils.eqDir;	            			
//				Justification j = new Justification(newDir, "Dependent Composition");
				Justification j = new Justification("Dependent Composition" + just, true);
				return new Suggestion(new Step(newDir, expanded), j, this.getGeneratorName());
		
			}
		}
		return null;
	}
	
	/**
	 * Determines the time variable in the current context, if any. We consider
	 * the time variable to be the most recently declared state variable, and 
	 * hence this method stops when it finds the first occurrence.
	 * 
	 * @param laws The current laws, which usually include the context
	 * @return
	 */
	private String getTimeIndex(LawBlob laws, LawBlob context) {
		List<String> vars = new ArrayList<String>(computeSigma(laws, context).keySet());
		
		for (Expression law:context.getInputLaws()) {
			if ((law instanceof Application)
					&& ((Application)law).getFunction().toString().equals(":")
					&& (((Application)law).getChild(0) instanceof Variable)) {
				Variable v = (Variable)((Application)law).getChild(0);
				String type = ((Application)law).getChild(1).toString();
				if ((type.equals(nattime)||type.equals(realtime))&&vars.contains(v.toString())) {
					return v.toString();
				}
			}
		}

		return null;
	}
	
	private Suggestion independentComposition(Expression focus, LawBlob laws, LawBlob context) {	
		//check if the focus is a dependent composition
		if ((focus instanceof Application)
				&&(((Application)focus).getFunction().toString().equals("||"))) {			
			
			// check if both sides contain no programming notation
			if (noProgram(focus.getChild(0)) && noProgram(focus.getChild(1))) {
				HashMap<String,Expression> sigma = ProgrammingSuggestor.computeSigma(laws, context);
				String time = this.getTimeIndex(laws, context);								
								
				
				String expanded = "";
				Expression left = focus.getChild(0);
				Expression right = focus.getChild(1);
				
				if(time == null) {
					expanded = left + "∧" + right;
				} else {
					expanded = "∃ tP,tQ:" + sigma.get(time) + "·" 
						+ "⟨" + time + primeChar + ":" + sigma.get(time) + "→" + left + "⟩ tP"
						+ "∧"
						+ "⟨" + time + primeChar + ":" + sigma.get(time) + "→" + right + "⟩ tQ"
						+ "∧" + time + primeChar + "= max tP tQ";
				}	
				//var x:nat·var y:nat·var t:nattime·x′=3∧t′=t+1||y′=4∧t′=t+2				
				
				Direction newDir = new ExpnDirection("=");	            			
				Justification j = new Justification("Independent Composition", true);
				return new Suggestion(new Step(newDir, ExpressionUtils.parse(expanded)), j, this.getGeneratorName());
		
			}
		}
		
		return null;
	}
	
	/**
	 * Determine whether the given expression has all of its variables unprimed.
	 * We only ensure that there are no unprimed state variables, since any 
	 * other variable would not matter.
	 * 
	 * @param expn
	 * @return
	 */
	private boolean allUnprimed(Expression expn, List<String> sigma) {		
		for (String s:sigma) {
			Identifier id = Identifiers.get(s + ExpressionUtils.prime);
			
			if (id!=null && expn.contains(id)) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Given a variable and a list of laws (which can include context laws, 
	 * since really its the same), find the type of the variable. 
	 * @param v Variable for which the type is needed
	 * @param laws Current laws
	 * @return
	 */
	private Expression typeInContext(Variable v, List<Expression> laws) {
		String ctype = "";
		
		for (Expression l:laws) {
			if ((l instanceof Application) && (((Application)l).getFunction().toString().equals(":"))){
				if (l.getChild(0).equals(v)) {
					ctype += ((Expression)l).getChild(1).toString() + "‘";
				}
			}
		}
		
		if (ctype.equals("")) {
			return null;
		} else {
			return ExpressionUtils.parse(ctype.substring(0, ctype.length()-1));
		}
	}
	
	/**
	 * Determine if this expression has any programming operators. This is 
	 * needed to implement the substitution law
	 * 
	 * @param p
	 * @return
	 */
	private boolean noProgram(Expression expn) {
		for (String s:Symbol.programOps) {
			Identifier id = Identifiers.get(s);
			if (id!=null && expn.contains(id)) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir,
			LawBlob laws, List<SuggestionGenerator> gens) {
		return generate(focus, requiredDir, laws, new LawBlob());
	}


	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir,
			LawBlob laws) {
		return generate(focus, requiredDir, laws, new LawBlob());
	}


	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir,
			LawBlob laws, LawBlob context) {
		List<Suggestion> sug = new ArrayList<Suggestion>();
		List<Suggestion> ret = new ArrayList<Suggestion>();
		
		sigma = null;
		sug.add(assignmentExpansion(focus, laws, context));
		sug.add(assignmentExpansion(focus, laws, context, "×", " (Probability)"));
		sug.add(substitutionLaw(focus, laws, context));
		sug.add(dependentComposition(focus, laws, context));
		sug.add(dependentComposition(focus, laws, context, "∑", "×", " (Probability)"));
		sug.add(okExpand(focus, laws, context));
		sug.add(okExpand(focus, laws, context, "×", " (Probability)"));
		sug.add(okContract(focus, laws, context));
		sug.add(independentComposition(focus, laws, context));
		
		for (Suggestion s:sug) {
			if (s!=null) {
				ret.add(s);
			}
		}
		
		return ret;
	}


	@Override
	public String getGeneratorName() {
		return "Programming Suggestor";
	}	
}
