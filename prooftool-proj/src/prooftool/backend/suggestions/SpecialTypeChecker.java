package prooftool.backend.suggestions;

import java.util.ArrayList;
import java.util.List;

import prooftool.backend.Step;
import prooftool.backend.direction.Direction;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.suggestion.Suggestion;
import prooftool.util.ExpressionUtils;
import prooftool.util.Identifiers;

public class SpecialTypeChecker implements SuggestionGenerator{

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, List<Expression> laws) {
		List<Suggestion> sug = new ArrayList<Suggestion>();
		if ((focus instanceof Application) &&(((Application)focus).getFunId() == Identifiers.colon)){
			
			focus.makeTypes(laws); // TODO remove when all expressions are typed			
			
			Expression t1 = focus.getChild(1).getType();
			Expression t0 = focus.getChild(0).getType();
			
			if (t0!=null && t1!=null && t1.typeCheck(t0, laws)) {
				Direction newDir = ExpressionUtils.eqDir;	            
				Expression top = ExpressionUtils.top;
	            Justification j = new Justification(/*newDir,*/ "Type Checking", true);
	            sug.add(new Suggestion(new Step(newDir, top), j, this.getGeneratorName()));
			}
		}
		
		return sug;
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir,
			LawBlob laws, List<SuggestionGenerator> gens) {
		return generate(focus, requiredDir, laws.getInputLaws());
	}
	
	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir,
			LawBlob laws) {
		return generate(focus, requiredDir, laws.getInputLaws());
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, LawBlob context) {
		List<Suggestion> sug = new ArrayList<Suggestion>();
		
		if ((focus instanceof Application)
				&&(((Application)focus).getFunId() == Identifiers.colon)){
			
			focus.makeTypes(laws.getInputLaws()); // TODO remove when all expressions are typed, and change to blob version			
			focus.makeTypes(context.getInputLaws()); 
			Expression t1 = focus.getChild(1).getType();
			Expression t0 = focus.getChild(0).getType();

			if (t0!=null && t1!=null 
					&& (t1.typeCheck(t0, laws.getInputLaws()) || (t1.typeCheck(t0, context.getInputLaws())))) {
				Direction newDir = ExpressionUtils.eqDir;	            
				Expression top = ExpressionUtils.top;
	            Justification j = new Justification(/*newDir,*/ "Type Checking", true);
	            sug.add(new Suggestion(new Step(newDir, top), j, this.getGeneratorName()));
			}
		}
		
		return sug;
	}

	@Override
	public String getGeneratorName() {
		return "Special Type Checker";
	}

}
