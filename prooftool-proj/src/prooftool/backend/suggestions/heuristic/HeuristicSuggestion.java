package prooftool.backend.suggestions.heuristic;

import java.util.ArrayList;
import java.util.List;

import prooftool.backend.direction.Direction;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Expression;
import prooftool.backend.suggestions.ProbabilityNormalizer;
import prooftool.backend.suggestions.SuggestionGenerator;
import prooftool.backend.suggestions.UnifyingSuggestor;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.suggestion.Suggestion;
import prooftool.util.ExpressionUtils;

public class HeuristicSuggestion extends RecursiveSuggestionGenerator {
	private int limit = 100;
	private LawBlob laws = null;
	private LawBlob context = null;

	public HeuristicSuggestion() {
		super();		
		gens.add(new UnifyingSuggestor());
		this.recJust = new Justification("Simplify", true);
		
		Heuristic hs = new Heuristic();
		this.laws = hs.getLaws();
		this.context = new LawBlob();		
	}

	@Override
	public Expression preProcess(Expression expression, Direction dir, LawBlob laws) {
		ProbabilityNormalizer norm = new ProbabilityNormalizer();
		List<Suggestion> l = norm.generate(expression, dir, laws.getInputLaws());
		if (!l.isEmpty() && l.get(0).getExpn() != null) {
			return l.get(0).getExpn();
		} else {
			return expression;
		}
	}


	@Override
	public Expression postProcess(Expression expression) {			
		return expression;
	}
	
	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, LawBlob context) {
			return super.generate(focus, requiredDir, this.laws, this.context);
	}

	@Override
	public String getGeneratorName() {
		return "Heuristic Suggestion";
	}

	@Override
	/**
	 * This method unifies the expression with the law blob and picks 
	 * the minimally ranked suggestion. If a suggestion is available, then
	 * it is applied, and modify is done recursively to children.
	 * 
	 * Because modify always decreases the lexicographical order, this is guaranteed
	 * to terminate, unless I put some odd bug in there. In any case, the code stops
	 * applying laws when it hits the application limit.
	 * 
	 */
	public Expression modify(Expression expression, Direction dir, LawBlob laws, LawBlob context) {
		int lawsApplied = 0;
		Expression current = expression;
		Direction currentDir = dir;
		Suggestion sug;
		boolean match = true;

		while (lawsApplied<limit && match) {
			match = false;				
			sug = this.applyTakeMin(current, currentDir, laws);

			if (sug!=null && this.improves(current, sug.getStep().getFocus())) {
				//the heuristic is for laws that reduce the size of the expression
				//and to have no uninstantiated variables

				current = sug.getStep().getFocus();
				if (currentDir != ExpressionUtils.eqDir) {
					currentDir = sug.getStep().getDop();
				}
				lawsApplied++;
				match = true;
			}
		}
		return current;
	}

	public Suggestion applyTakeMin(Expression focus, Direction dir, LawBlob laws) {
		List<Suggestion> sug = new ArrayList<Suggestion>();
		Suggestion min = null;
		for (SuggestionGenerator gen : this.gens) {
			sug.addAll(gen.generate(focus, dir, laws));
		}
		for (Suggestion s:sug) {
			if (min == null || this.improves(min.getStep().getFocus(), s.getStep().getFocus())) {
				min = s;
			}
		}
		return min;
	}

	public boolean improves(Expression current, Expression suggested) {
		if (suggested.toString().length() == current.toString().length()) {
			return suggested.toString().compareTo(current.toString()) < 0;
		}
		
		return (suggested.toString().length() < current.toString().length());
	}
}
