package prooftool.backend.suggestions.heuristic;

import java.io.File;
import java.util.List;

import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Expression;
import prooftool.util.Main;

public class Heuristic {

	private static String sep = System.getProperty("file.separator");
	private static File heuURL;

	private LawBlob laws;
	
	protected Heuristic () {
		try {
			//Get resources
			Heuristic.heuURL = new File("resources" + sep + "heuristic");
			
			if (!Heuristic.heuURL.exists()) {
				Heuristic.heuURL.createNewFile();
			}
			
			this.laws = new LawBlob();
			
			List<Expression> laws = Main.parse(Heuristic.heuURL.toString(), true);
			for (Expression law: laws) {
				this.laws.addHeuristic(law);
			}
		} catch (Exception e) {
			System.err.println("Error in reading heuristic laws");
			e.printStackTrace();
		}			
	}	
	
	protected LawBlob getLaws() {
		return this.laws;
	}
}
