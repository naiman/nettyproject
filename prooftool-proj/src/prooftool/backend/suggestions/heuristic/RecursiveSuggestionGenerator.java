package prooftool.backend.suggestions.heuristic;

import java.util.ArrayList;
import java.util.List;

import prooftool.backend.Step;
import prooftool.backend.direction.Direction;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Expression;
import prooftool.backend.suggestions.SuggestionGenerator;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.suggestion.Suggestion;
import prooftool.util.ExpressionUtils;
import prooftool.util.ProofRep;

abstract public class RecursiveSuggestionGenerator implements SuggestionGenerator {
	protected Justification recJust;	 
	protected List<SuggestionGenerator> gens;	
	
	public RecursiveSuggestionGenerator() {
		this.gens = new ArrayList<SuggestionGenerator>();
		this.recJust = new Justification("Recursive", true);
	}
	
	protected abstract Expression preProcess(Expression focus, Direction dir, LawBlob laws);
	protected abstract Expression postProcess(Expression focus);
	protected abstract Expression modify(Expression focus, Direction requiredDir, LawBlob laws, LawBlob context);

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, List<Expression> laws) {
		return new ArrayList<Suggestion>();
	}
	
	
	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, LawBlob context) {
		//TODO include context
		return generate(focus, requiredDir, laws, new ArrayList<SuggestionGenerator>());
	}
	
	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws) {
		return generate(focus, requiredDir, laws, new ArrayList<SuggestionGenerator>());
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, List<SuggestionGenerator> gens) {
		List<Suggestion> sug = new ArrayList<Suggestion>();
		List<Suggestion> ret = new ArrayList<Suggestion>();
		
		
		sug.add(this.recursiveSuggestion(focus, ExpressionUtils.eqDir, laws, new LawBlob()));
		
		for (Suggestion s:sug) {			
			if (s!=null) {
				ret.add(s);
			}
		}
		
		return ret;
	}
	
	@Override
	public String getGeneratorName() {
		return "Recursive Suggestion Generator";
	}	
	
	private Suggestion recursiveSuggestion(Expression focus, Direction requiredDir, LawBlob laws, LawBlob context) {
		//to preserve the focus might want to clone it and remake types by doing
		// focus = focus.clone();
		//and then making types
		
		focus = this.preProcess(focus, requiredDir, laws);
		Step step = new Step(ExpressionUtils.eqDir, this.modificationHelper(focus, requiredDir, laws, context));
		focus = this.postProcess(focus);
		
		return new Suggestion(step, recJust, this.getGeneratorName());
	}
	

	private Expression modificationHelper(Expression focus, Direction requiredDir, LawBlob laws, LawBlob context) {		
		for (int i = 0 ; ; i++) {
			Expression child = focus.getChild(i);
			
			if (child == null) {
				break;
			}
			
			//change direction appropriately
			requiredDir = ProofRep.modifyDirection(requiredDir, focus, i);
			Expression replacement = this.modificationHelper(child, requiredDir, laws, context);
			
			focus.setChild(i, replacement);
			
		}

		return this.modify(focus, requiredDir, laws, context);
	}
}


