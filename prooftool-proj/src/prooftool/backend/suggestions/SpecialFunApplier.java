package prooftool.backend.suggestions;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import prooftool.backend.Step;
import prooftool.backend.direction.Direction;
import prooftool.backend.direction.ExpnDirection;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.AbbreviatedScope;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Identifier;
import prooftool.backend.laws.Literal;
import prooftool.backend.laws.Scope;
import prooftool.backend.laws.Variable;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.suggestion.Suggestion;
import prooftool.util.ExpressionUtils;
import prooftool.util.Identifiers;
import prooftool.util.Symbol;

public class SpecialFunApplier implements SuggestionGenerator {

	public List<Suggestion> generate(Expression focus, Direction requiredDir, List<Expression> laws) {
		List<Suggestion> sug = new ArrayList<Suggestion>();
		List<Suggestion> ret = new ArrayList<Suggestion>();

		sug.add(arithmetic(focus));
		sug.add(arithmeticProbability(focus));
		sug.add(recursiveArithmetic(focus, ""));
		sug.add(sumExpansion(focus));
		sug.add(fullSumExpansion(focus));
		sug.add(multiplicativeIdentity(focus));
		sug.add(simplifyDistribution(focus));
		sug.add(otherUnaryNumberFunctions(focus));

		for (Suggestion s : sug) {
			if (s != null) {
				ret.add(s);
			}
		}

		return ret;
	}

	// stolen from StackOverflow, based on Euclid's algorithm
	// http://stackoverflow.com/questions/4201860/how-to-find-gcf-lcm-on-a-set-of-numbers
	private static int gcd(int a, int b) {
		a = a > 0 ? a : -a;
		b = b > 0 ? b : -b;
		
		while (b > 0) {
			int temp = b;
			b = a % b; // % is remainder
			a = temp;
		}
		return a;
	}

	private static int lcm(int a, int b) {
		a = a > 0 ? a : -a;
		b = b > 0 ? b : -b;
		
		return a * (b / gcd(a, b));
	}

	private static int lcm(int[] input) {
		input[0] = input[0] > 0 ? input[0] : -input[0];
		int result = input[0];
		
		for (int i = 1; i < input.length; i++) {
			result = lcm(result, input[i]);
		}
		
		return result;
	}
	
	/**
	 * attempt to convert a decimal number to a fraction.
	 * note that due to limited precisions, some decimals cannot be correctly converted due to the fact that 
	 * some double arithmetics will not return the desired result (i.e. "1.6−1" will evaluate to "0.6000000000000001", 
	 * and "2.3−2" will return "0.2999999999999998") 
	 */
	private Expression toFraction(Expression focus) {
		// need to wrap identifiers with variables
		Variable div = new Variable(Identifiers.divide);

		double x = Double.valueOf(((Literal) focus).getValue().toString());
		double error = 0;
		int maxIteration = 10000;
		double n = (int) Math.floor(x);
		x -= n;
		
		if (x == 0) {
			// fractional part = 0, focus : int; write in the form of focus/1
			return new Application(div, new Literal(n), new Literal(1));
		}

		Expression expr = null;

		if (x < error) {
			expr = new Application(div, new Literal(n), new Literal(1));
		} else if (1 - error < x) {
			expr = new Application(div, new Literal(n + 1), new Literal(1));
		}

		if (expr != null) {
			return expr;
		}

		int lowerDenominator = 1, lowerNumerator = 0, upperDenominator = 1, upperNumerator = 1;
		int middleDenominator = 0, middleNumerator = 0;

		// "binary searching" the fraction that representing the decimal
		// will converge to the decimal value
		for (int i = 0; i < maxIteration; i++) {
			middleNumerator = lowerNumerator + upperNumerator;
			middleDenominator = lowerDenominator + upperDenominator;

			if (middleDenominator * (x + error) < middleNumerator) {
				upperNumerator = middleNumerator;
				upperDenominator = middleDenominator;
			} else if (middleNumerator < (x - error) * middleDenominator) {
				lowerNumerator = middleNumerator;
				lowerDenominator = middleDenominator;
			} else {
				expr = new Application(div, new Literal(n * middleDenominator + middleNumerator), new Literal(middleDenominator));
				break;
			}
		}
		
		return expr;
	}

	/**
	 * simplifying a fraction rational to the simplest form by dividing both the 
	 * numerator and the denominator with their gcd
	 * if the denominator is 1 then simply return an int literal representing the quantity
	 */
	private Expression simplifyFraction(Expression focus) {
		if (focus instanceof Application && ((Application) focus).getFunId().equals(Identifiers.divide)) {
			int left = (Integer) ((Literal) focus.getChild(0)).getValue();
			int right = (Integer) ((Literal) focus.getChild(1)).getValue();
			int gcd = gcd(left, right);
			left /= gcd;
			right /= gcd;
			
			if (right == 1) {
				return new Literal(left);
			} else {
				focus = focus.clone();
				((Literal) focus.getChild(0)).setValue(left);
				((Literal) focus.getChild(1)).setValue(right);
				return focus;
			}
		} else if (focus instanceof Application && ((Application) focus).getFunId().equals(Identifiers.unary_minus)) {
			Expression child = (Application) focus.getChild(0);
			Expression ret = simplifyFraction(child);
			return ret != null ? new Application(new Variable(Identifiers.unary_minus), ret) : null;
		}
		
		return null;
	}

	/**
	 * evaluate the sum of a series of fraction
	 */
	private Expression addFraction(int numerator0, int denom0, int numerator1, int denom1) {
		int lcm = lcm(new int[] {denom0, denom1});
		int numerator = numerator0 * (lcm / denom0) + numerator1 * (lcm / denom1);
		Application result = new Application(new Variable(Identifiers.divide), new Literal(numerator), new Literal(lcm));
		return result;
	}
	
	private Expression subFraction(int numerator0, int denom0, int numerator1, int denom1) {
		int lcm = lcm(denom0, denom1);
		int numerator = numerator0 * (lcm / denom0) - numerator1 * (lcm / denom1);
		Application result = new Application(new Variable(Identifiers.divide), new Literal(numerator), new Literal(lcm));
		return result;
	}
	
	/**
	 * evaluate the product of a series of fraction
	 */
	private Expression multFraction(int numerator0, int denom0, int numerator1, int denom1) {
		int numerator = numerator0 * numerator1;
		int denominator = denom0 * denom1;
		Application result = new Application(new Variable(Identifiers.divide), new Literal(numerator), new Literal(denominator));
		return result;
	}
	
	private Expression divFraction(int numerator0, int denom0, int numerator1, int denom1) {
		int numerator = numerator0 * denom1;
		int denominator = numerator1 * denom0;
		Application result = new Application(new Variable(Identifiers.divide), new Literal(numerator), new Literal(denominator));
		return result;
	}
	
	/**
	 * compare the 2 given fractions
	 * 1 if the first fraction is greater
	 * 0 if the 2 fractions are equal
	 * -1 if the second fraction is greater
	 */
	private int compFraction(int numerator0, int denom0, int numerator1, int denom1) {
		int lcm = lcm(denom0, denom1);
		int compare = numerator0 * (lcm / denom0) - numerator1 * (lcm / denom1);
		
		if (compare > 0) {
			return 1;
		} else if (compare < 0) {
			return -1;
		} else {
			return 0;
		}
	}
	
	/**
	 * returns true if a given expression object is a fraction consists of only integer numerators
	 * and denominators
	 */
	private boolean isBasicFraction(Expression focus) {
		boolean isDivisison = (focus instanceof Application) && 
				(((Application) focus).getFunId() == Identifiers.divide) && 
				(((Application) focus).arity() == 2);
		
		if (isDivisison) {
			Expression child0 = ((Application) focus).getChild(0);
			Expression child1 = ((Application) focus).getChild(1);
			
			return (child0 instanceof Literal) && 
					((Literal) child0).getType().toString().equals("nat") &&
					(child1 instanceof Literal) && 
					((Literal) child1).getType().toString().equals("nat");
		} else {
			return false;
		}
	}
	
	/**
	 * takes an expression composed of fractions and attempt to evaluate its value as return value
	 * the returned expression object value won't necessarily be a fraction type 
	 */
	private Expression evalArith(Expression focus) {
		// check if current simplifier is application
		if (focus instanceof Literal) {
			String type = focus.getType().toString();

			if (Symbol.numberTypes.contains(type)) {
				Expression f = toFraction(focus);
				return f == null ? focus : f;
			} else {
				return focus;
			}
		} else if (focus instanceof Application) {
			if (((Application) focus).arity() == 2) {
				Identifier fun = ((Application) focus).getFunId();
				
				// calculate the lcms of all the fractions
				Expression child0 = focus.getChild(0);
				Expression child1 = focus.getChild(1);
				
				// don't re-evaluate if its already basic fraction fraction
				if (fun == Identifiers.divide) {
					if (child0 instanceof Literal && child1 instanceof Literal) {
						return focus.clone();
					}
				}
				
				Expression eval0 = evalArith(child0);
				Expression eval1 = evalArith(child1);
				
				if (eval0 != null && eval1 != null) {
					if ((isBasicFraction(eval0) || (eval0 instanceof Literal)) &&
						(isBasicFraction(eval1) || (eval1 instanceof Literal))) {
						// expression can be evaluated directly
						
						// if both argument are numeric
						int numerator0, denom0, numerator1, denom1;
						
						if (isBasicFraction(eval0)) {
							numerator0 = (int) ((Literal)((Application) eval0).getChild(0)).getValue();
							denom0 = (int) ((Literal)((Application) eval0).getChild(1)).getValue();
						} else {
							// eval0 instanceof Literal
							Expression fraction = toFraction(eval0);
							numerator0 = (int) ((Literal)((Application) fraction).getChild(0)).getValue();
							denom0 = (int) ((Literal)((Application) fraction).getChild(1)).getValue();
						}
						
						if (isBasicFraction(eval1)) {
							numerator1 = (int) ((Literal)((Application) eval1).getChild(0)).getValue();
							denom1 = (int) ((Literal)((Application) eval1).getChild(1)).getValue();
						} else {
							// eval1 instanceof Literal
							Expression fraction = toFraction(eval1);
							numerator1 = (int) ((Literal)((Application) fraction).getChild(0)).getValue();
							denom1 = (int) ((Literal)((Application) fraction).getChild(1)).getValue();
						}
						
						// evaluating the arithmetic expression
						// if the given expression is boolean returns either 1 or 0
						Literal o = new Literal(1);
						Literal z = new Literal(0);
						Expression result;
						if (fun == Identifiers.plus) {
							result = addFraction(numerator0, denom0, numerator1, denom1);
						} else if (fun == Identifiers.infix_minus) {
							result = subFraction(numerator0, denom0, numerator1, denom1);
						} else if (fun == Identifiers.times) {
							result = multFraction(numerator0, denom0, numerator1, denom1);
						} else if (fun == Identifiers.divide) {
							result = divFraction(numerator0, denom0, numerator1, denom1);
						} else if (fun == Identifiers.gt) {
							result = compFraction(numerator0, denom0, numerator1, denom1) > 0 ? o : z;
						} else if (fun == Identifiers.gte) {
							result = compFraction(numerator0, denom0, numerator1, denom1) >= 0 ? o : z;
						} else if (fun == Identifiers.eq) {
							result = compFraction(numerator0, denom0, numerator1, denom1) == 0 ? o : z;
						} else if (fun == Identifiers.lte) {
							result = compFraction(numerator0, denom0, numerator1, denom1) <= 0 ? o : z;
						} else if (fun == Identifiers.lt) {
							result = compFraction(numerator0, denom0, numerator1, denom1) < 0 ? o : z;
						} else {
							result = focus;
						}
	
						if (isBasicFraction(result)) {
							result = simplifyFraction(result);
						}
	
						return result;
					} else {
						
						if (isBasicFraction(eval0)) {
							eval0 = simplifyFraction(eval0);
						}
						
						if (isBasicFraction(eval1)) {
							eval1 = simplifyFraction(eval1);
						}
						
						if (eval0 != null && eval1 != null) {
							Expression result = new Application(new Variable(fun), eval0, eval1);
							return result;
						} else {
							return focus;
						}
					}
				}
			} else {
				//Identifier fun = ((Application) focus).getFunId();
				Expression focusClone = focus.clone();
				for (int i = 0; i < focus.getChildren().size(); i++) {
					Expression c = focus.getChild(i);
					Expression s = evalArith(c);
					if (s == null) {
						return null;
					}
					focusClone.setChild(i, s);
				}
				return focusClone;
			}
		} else if (focus instanceof Scope) {
			// evaluating the body of a scope function
			Scope focusClone = ((Scope) focus.clone());
			Expression x = evalArith(focusClone.getBody());
			
			if (x != null) {
				focusClone.setBody(x);
				return focusClone;
			} else {
				return null;
			}
		}
		
		return focus;
	}
	
	/**
	 * return "best-effort" evaluation suggestions for fractional arithmetics
	 * attempt to evaluate the numerical components of an expression (leaving the rest of the expression)
	 * suggestion should ALWAYS be in fractional form, even if the input is in mixed decimals/fractions
	 * (note that this is not always possible since it's hard to convert some long decimals to fractions) 
	 * the value of boolean expression will be simplified as 1s and 0s for simplicity when dealing with probabilities
	 * given expression:
	 * "if 0.2 then 1/2+1/3 else 1/4+1/5 end" will be evaluated to "if 1/5 then 5/6 else 9/20 end",
	 * "(1≤2)/4+(2≤2)/4+(3≤2)/4+(4≤2)/4" will be evaluated to "1/2",
	 * "⟨a:int→(2+4)×x⟩" will be evaluated to ⟨a:int→6×x⟩,
	 * the simplifier should return null if it fails at evaluation (same goes for other simplifiers)
	 */
	private Suggestion recursiveArithmetic(Expression focus, String ext) {
		Justification just = new Justification("Arithmetic", true);
		Direction newDir = new ExpnDirection("=");

		// attempt to evaluating given expression
		Expression expanded = null;
		try {
			expanded = evalArith(focus);
		} catch (NullPointerException npx) {
			return null;
		} catch (ClassCastException ccx) {
			return null;
		}
		
		if (expanded == null) {
			return null;
		} else {
			return new Suggestion(new Step(newDir, expanded), just, this.getGeneratorName());
		}
	}
	
	/**
	 * 
	 */
	private Suggestion multiplicativeIdentity(Expression focus) {
		// create a simplified version of focus
		if (focus instanceof Application && (((Application) focus).getFunId().equals(Identifiers.times))) {
			focus = focus.clone();
			focus.flatten();
			String calc, result = "";
			boolean changed = false;
			
			for (Expression child : focus.getChildren()) {
				try {
					calc = this.arithmetic(child, "1", "0", "").getExpn().toString();
					if (calc.equals("0") || child.toString().equals("0")) {
						// if one of the terms evaluates to zero, the whole
						// expression is zero
						Expression zero = ExpressionUtils.parse("0");
						Direction newDir = new ExpnDirection("=");
						Justification j = new Justification("Zero Element", true);
						return new Suggestion(new Step(newDir, zero), j, this.getGeneratorName());
					} else if (calc.equals("1")) {
						changed = true;
					} else {
						result += "(" + child.toString() + ")×";
					}
				} catch (NullPointerException e) {
					result += "(" + child.toString() + ")×";
				}
			}

			if (result.equals("")) {
				result = "1";
			} else {
				// strip last multiplication off
				result = result.substring(0, result.length() - 1);
			}

			if (changed) {
				Expression expr = ExpressionUtils.parse(result);
				Direction newDir = ExpressionUtils.eqDir;
				Justification j = new Justification("Identity", true);
				return new Suggestion(new Step(newDir, expr), j, this.getGeneratorName());
			}
		}
		
		return null;
	}

	private Suggestion simplifyDistribution(Expression focus) {
		if (focus instanceof Application && ((Application) focus).getFunId().equals(Identifiers.plus)) {
			focus = focus.clone();
			focus.flatten();
			String result = "";
			boolean changed = false;
			Suggestion product;

			for (Expression child : focus.getChildren()) {
				product = this.multiplicativeIdentity(child);
				if (product != null) {
					changed = true;
					if (!product.getExpn().toString().equals("0")) {
						result += "(" + product.getExpn().toString() + ")+";
					}
				} else {
					product = this.arithmetic(child, "1", "0", "");
					if (product != null && product.getExpn().toString().equals("1")) {
						changed = true;
						result += "1+";
					} else {
						result += "(" + child.toString() + ")" + "+";
					}
				}
			}

			if (result.equals("")) {
				result = "0";
			} else {
				// strip last addition off
				result = result.substring(0, result.length() - 1);
			}

			if (changed) {
				Expression expr = ExpressionUtils.parse(result);
				Direction newDir = ExpressionUtils.eqDir;
				Justification j = new Justification("Simplify Distribution", true);
				return new Suggestion(new Step(newDir, expr), j, this.getGeneratorName());
			}
		}
		return null;
	}

	private Suggestion sumExpansion(Expression focus) {
		focus = focus.clone();
		if ((focus instanceof Application) && (((Application) focus).getFunId() == Identifiers.sum) && (((Application) focus).getChild(0) instanceof Scope)) {
			Scope sc = (Scope) ((Application) focus).getChild(0);
			Expression dom = sc.getDomain();
			if ((dom instanceof Application) && regScope(sc) && ((Application) dom).getFunId() == Identifiers.commadotdot) {
				Double leftVal = this.valueOf(((Application) dom).getChild(0));
				Double rightVal = this.valueOf(((Application) dom).getChild(1));

				if (leftVal != null && rightVal != null && leftVal.doubleValue() == leftVal.intValue() && rightVal.doubleValue() == rightVal.intValue() && leftVal < rightVal) {

					Map<Variable, Expression> subst = new IdentityHashMap<Variable, Expression>();
					subst.put(sc.getDummy(), ((Application) dom).getChild(0));
					dom.setChild(0, new Literal(leftVal + 1));
					focus = focus.clone();
					Expression newExpn = sc.getBody().instantiate(subst);

					String just = "Sum Expand";
					String result = "(" + newExpn + ")" + "+" + "(" + focus + ")";
					Expression expanded = ExpressionUtils.parse(result);
					Direction newDir = ExpressionUtils.eqDir;
					Justification j = new Justification(just, true);
					return new Suggestion(new Step(newDir, expanded), j, this.getGeneratorName());
				}
			}
		}

		return null;
	}

	private Suggestion fullSumExpansion(Expression focus) {
		int upper_limit = 10; // 10
		focus = focus.clone();
		if ((focus instanceof Application) && (((Application) focus).getFunId() == Identifiers.sum) && (((Application) focus).getChild(0) instanceof Scope)) {
			Scope sc = (Scope) ((Application) focus).getChild(0);
			Expression dom = sc.getDomain();
			if ((dom instanceof Application) && regScope(sc) && ((Application) dom).getFunId() == Identifiers.commadotdot) {
				Double leftVal = this.valueOf(((Application) dom).getChild(0));
				Double rightVal = this.valueOf(((Application) dom).getChild(1));

				if (leftVal != null && rightVal != null && leftVal.doubleValue() == leftVal.intValue() && rightVal.doubleValue() == rightVal.intValue() && rightVal - leftVal <= upper_limit && leftVal < rightVal) {
					String just = "Sum Expand";
					StringBuilder result = new StringBuilder();
					Map<Variable, Expression> subst = new IdentityHashMap<Variable, Expression>();
					Expression newExpn;
					Scope clone;
					for (int i = leftVal.intValue(); i < rightVal.intValue(); i++) {
						clone = (Scope) sc.clone();
						// System.out.println(new Literal(i));
						subst.put(clone.getDummy(), new Literal(i));
						newExpn = clone.getBody().instantiate(subst);
						result.append("(" + newExpn + ")" + "+");
					}
					result.deleteCharAt(result.length() - 1);

					Expression expanded = ExpressionUtils.parse(result.toString());
					Direction newDir = ExpressionUtils.eqDir;
					Justification j = new Justification(just, true);
					return new Suggestion(new Step(newDir, expanded), j, this.getGeneratorName());
				}
			}
		}

		return null;
	}

	private Suggestion arithmetic(Expression focus) {
		return arithmetic(focus, ExpressionUtils.top.toString(), ExpressionUtils.bottom.toString(), "");
	}
	
	private Suggestion arithmeticProbability(Expression focus) {
		return arithmetic(focus, "1", "0", " (Probability)");
	}

	private Suggestion arithmetic(Expression focus, String top, String bottom, String just_ext) {
		if ((focus instanceof Application) && (((Application) focus).arity() == 2)) {
			Double leftVal = this.valueOf(focus.getChild(0));
			Double rightVal = this.valueOf(focus.getChild(1));
			if (leftVal != null && rightVal != null) {
				String just = "";
				String result = "";
				String funString = ((Application) focus).getFunction().toString();
				Identifier fun = ((Application) focus).getFunId();

				if (funString.equals(Symbol.lookupUnicode(("+")))) {
					result = (leftVal + rightVal) + "";
					just = "Addition";
				} else if (fun == Identifiers.infix_minus) {
					result = (leftVal - rightVal) + "";
					just = "Subtraction";
				} else if (funString.equals(Symbol.lookupUnicode("/"))) {
					if (rightVal != 0) {
						result = (leftVal / rightVal) + "";
						just = "Division";
					}
				} else if (funString.equals(Symbol.lookupUnicode(ExpressionUtils.times))) {
					result = (leftVal * rightVal) + "";
					just = "Multiplication";
				} else if (fun == Identifiers.power) {
					result = Math.pow(leftVal, rightVal) + "";
					just = "Exponentiation";
				} else if (fun == Identifiers.oplus) {
					result = (leftVal + rightVal) % 2 + "";
					just = "Exclusive Or";
				} else if (funString.equals(Symbol.lookupUnicode(">"))) {
					result = (leftVal > rightVal) ? top : bottom;
					just = "Ordering";
				} else if (funString.equals(Symbol.lookupUnicode("<"))) {
					result = (leftVal < rightVal) ? top : bottom;
					just = "Ordering";
				} else if (funString.equals(Symbol.lookupUnicode("<="))) {
					result = (leftVal <= rightVal) ? top : bottom;
					just = "Ordering";
				} else if (funString.equals(Symbol.lookupUnicode(">="))) {
					result = (leftVal >= rightVal) ? top : bottom;
					just = "Ordering";
				} else if (funString.equals(Symbol.lookupUnicode("="))) {
					result = (leftVal.equals(rightVal)) ? top : bottom;
					just = "Equality";
				} else if (funString.equals(Symbol.lookupUnicode("!="))) {
					result = (!leftVal.equals(rightVal)) ? top : bottom;
					just = "Equality";
				}

				if (result.equals("")) {
					return null;
				}
				Expression expanded = ExpressionUtils.parse(result);
				Direction newDir = new ExpnDirection("=");
				Justification j = new Justification(/* newDir, */just + just_ext, true);
				return new Suggestion(new Step(newDir, expanded), j, this.getGeneratorName());
			}
		}

		return null;
	}

	private Suggestion otherUnaryNumberFunctions(Expression focus) {
		if ((focus instanceof Application) && (((Application) focus).getFunction().toString().equals(" "))) {
			String top = ExpressionUtils.top.toString();
			String bottom = ExpressionUtils.bottom.toString();
			Double val = this.valueOf(focus.getChild(1));
			if (val != null) {
				String just = "";
				String result = "";

				if (focus.getChild(0).toString().equals("log")) {
					if (val > 0) {
						result = Math.log10(val) + "";
						just = "Function Evaluation";
					}
				} else if (focus.getChild(0).toString().equals("lg")) {
					if (val > 0) {
						result = Math.log(val) / Math.log(2) + "";
						just = "Function Evaluation";
					}
				} else if (focus.getChild(0).toString().equals("ceil")) {
					result = Math.ceil(val) + "";
					just = "Function Evaluation";
				} else if (focus.getChild(0).toString().equals("floor")) {
					result = Math.floor(val) + "";
					just = "Function Evaluation";
				} else if (focus.getChild(0).toString().equals("even")) {
					if (val.equals(Math.floor(val))) {
						result = top;
					} else {
						result = bottom;
					}
				} else if (focus.getChild(0).toString().equals("odd")) {
					if (!val.equals(Math.floor(val))) {
						result = top;
					} else {
						result = bottom;
					}
				}

				if (result.equals("")) {
					return null;
				}
				Expression expanded = ExpressionUtils.parse(result);
				Direction newDir = new ExpnDirection("=");
				Justification j = new Justification(/* newDir, */just, true);
				return new Suggestion(new Step(newDir, expanded), j, this.getGeneratorName());

			}
		}
		return null;
	}

	/**
	 * Determine the value of this expression if it is a number. We check if
	 * this is a literal of number type, or a negation of such a literal. We
	 * return the value or null if the expression does not satisfy this
	 * definition.
	 * @param expn
	 * @return
	 */
	private Double valueOf(Expression expn) {
		Double val = null;
		boolean negate = false;
		// check if this is a negation
		if (expn instanceof Application && ((Application) expn).getFunId() == Identifiers.unary_minus) {
			expn = expn.getChild(0);
			negate = true;
		}

		if (expn instanceof Literal) {
			if (Symbol.numberTypes.contains(expn.getType().toString())) {
				val = Double.valueOf(((Literal) expn).getValue().toString());
				if (negate) {
					val = -val;
				}
			}
		}
		return val;
	}

	private boolean regScope(Scope scope) {
		return (scope instanceof AbbreviatedScope) && ((AbbreviatedScope) scope).getId() == Identifiers.scope;
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, List<SuggestionGenerator> gens) {
		return generate(focus, requiredDir, laws.getInputLaws());
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws) {
		return generate(focus, requiredDir, laws.getInputLaws());
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, LawBlob context) {
		return generate(focus, requiredDir, laws.getInputLaws());
	}

	@Override
	public String getGeneratorName() {
		return "SpecialFunApplier";
	}

}
