package prooftool.backend;

import java.util.List;

import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Variable;

// Represents an expression parameterized by some variables.
public class ParameterizedExpression {
	private Expression expression;
	private List<Variable> parameters;
	
	public ParameterizedExpression(Expression expression, List<Variable> parameters) {
		this.expression = expression;
		this.parameters = parameters;
	}
	
	public Expression getExpression() {
		return expression;
	}
	
	public List<Variable> getParameters() {
		return parameters;
	}
}
