package prooftool.backend.laws;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import prooftool.backend.Substitution;
import prooftool.backend.ccode.ExpressionVisitor;
import prooftool.util.ExpressionUtils;
import prooftool.util.Identifiers;
import prooftool.util.Symbol;
import prooftool.util.objects.Dictionary;
import prooftool.util.objects.Path;
import prooftool.util.objects.SymbolInfo;

/**
 * Represents a function application of arbitrary arity. This includes all
 * operators, regardless of fixities (i.e infix, postfix, etc.)
 * 
 * @author robert
 * @author evm
 *
 */

public class Application extends Expression {
	private static final long serialVersionUID = 5308520905698036012L;
	private Expression fun;
	private Expression[] args;
	
	public Application(Expression fun, Expression... args) {
		this.fun = fun;	
		this.args = args;
	}
	
	/**
	 * Creates a list of expression parts returned are at one zoom level down. 
	 * 
	 * @return A list of String and Object, where the object parts are zoomable
	 *         parts and the String parts are operators/keywords.
	 */
	@Override
	public Object[] toParts() {
		if (fun instanceof Identifier) {
			// only for debugging, if we print before running ‘make_variables’
			return ((Identifier) fun).toParts(args);
		} else {
			return ((Variable) fun).getId().toParts(args);
		}		
	}
	

	/**
	 * Turn every identifier in this expression into its corresponding variable.
	 * The variable it refers to depends on scope (since an identical 
	 * identifier within a different scope is really a different variable).
	 * 
	 * This method changed all identifiers that refer to the same variable to 
	 * actually be that same variable by making them all the same object.
	 * 
	 * @param A dictionary containing a mapping of identifiers to variables
	 * @return This expression, with all its identifiers turned into variables
	 */
	@Override
	public Expression makeVariables(Dictionary d) {
		fun = fun.makeVariables(d);
		for (int i=0; i<args.length; i++) {
			args[i] = args[i].makeVariables(d);
		}
		return this;
	}
	
	@Override
	public Expression syncVariables(Dictionary d){
		fun = fun.syncVariables(d);
		for (int i=0; i<args.length; i++) {
			args[i] = args[i].syncVariables(d);
		}
		return this;
	}
	
	@Override
	public void makeTypes(List<Expression> context) {
		assert !Symbol.operatorMap.isEmpty();
		SymbolInfo info = Symbol.operatorMap.get(this.fun.toString());
		
		if (info == null) { 						
			if (this.fun.toString().equals(" ")) {
				 this.funAppType(context);
			} else {
				this.setType(null);
			}
		} else {
			//TODO try to simplify the type if possible (ex. nat,int --> int)
			//this is important for if-expressions
			for (Expression arg: this.args) {					
				arg.makeTypes(context);
				
				if (arg.getType() == null) {
					this.setType(null);
					return;
				}
			}
			
			for (Expression t : info.getTypes()) {				
				if (argumentNumber(t) == this.args.length) {					
					this.setType(this.checkApplicationType(t, 0, context));
				}
				
				if (this.getType() != null) {
					this.ifhack(context);
					break;
				}
			}
			//SIMPLIFY
		}		
	}
	
	/**
	 * Returns whether this function application is a universal quantification.
	 * This is used to determine whether new variables are introduced (since
	 * scope introduces new variables).
	 */
	@Override
	public boolean is_uni_quant_scope() {
		if (fun.is_operator()) {
			Variable f = (Variable) fun;
			Identifier id = f.getId();
			
			//is for all symbol?
			if (Symbol.lookupUnicode(id.getKeywords()[0]).equals("∀")) {
				return (args[0] instanceof Scope);
			}
		}
		
		return false;
	}
	
	/**
	 * Returns whether the given variable x is in this expression (either
	 * dummy, domain or body). Note that it must be the same object in order
	 * to be found.
	 * 
	 * @return Whether x is in this expression
	 */
	@Override
	public boolean contains(Variable x) {
		for (Expression e : args) {
			if (e.contains(x)) {
				return true;
			}
		}
		
		return fun.contains(x);
	}
	
	@Override
	public Expression instantiate(Map<Variable, Expression> subst) {				
		fun = fun.instantiate(subst);
		
		for (int i = 0; i < args.length; i++) {			
			args[i] = args[i].instantiate(subst);
		}
		
		return this;
	}
	
	@Override
	public Expression instantiate(Substitution s) {
		boolean functionized = this.is_fun_application() && (args[0] instanceof Variable) && s.getFunctionized((Variable)args[0])!=null;
		
		fun = fun.instantiate(s);
		
		for (int i = 0; i < args.length; i++) {			
			args[i] = args[i].instantiate(s);
		}		
		
		// at this point args[0] has been replaced by its instantiation
		if (functionized && args.length == 2) {
			Scope sc = (Scope) args[0];
			Map<Variable, Expression> subst = new IdentityHashMap<Variable, Expression>();
			subst.put(sc.getDummy(), args[1]);
			boolean type_ok = sc.getDomain().typeCheck(args[1].getType(), new ArrayList<Expression>());
			
			if (type_ok) {
				return sc.getBody().instantiate(subst);
			} else {
				return this;
			}
		} else {
			return this;
		}
	}
	
	@Override
	public Map<Variable,Expression> getAllVars(Map<Variable,Expression> currentVars){
		currentVars.putAll(fun.getAllVars(currentVars));
		
		for(Expression child: args){
			currentVars.putAll(child.getAllVars(currentVars));
		}
		
		return currentVars;
	}
	
	@Override
	public Map<Variable,Variable> getAllVarsHelper(){
		Map<Variable,Variable> vars = fun.getAllVarsHelper();
		
		for(Expression child: args){
			vars.putAll(child.getAllVarsHelper());
		}
		
		return vars;
	}	

	@Override
	public void setChild(int i, Expression replacement) {
		if (fun.is_operator()) {
	 		args[i] = replacement;
		} else {
			args[i+1] = replacement;
		}
	}
	
	@Override
	public Expression getChild(int i) {
		if (i >=0 && i < args.length) {
			return args[i];
		}
		
		return null;
	}
	
	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<Expression>(args.length);
		
		for(Expression arg : args) {
			children.add(arg);
		}
		
		return children;
	}
	
	/**
	 * flatten the current instance of application
	 * i.e "(1+2)+3" will be flatten to "1+2+3" if the operator is associative
	 * otherwise simply flatten each argument individually
     * (refer to Symbol.initAssoc to find a list of associated operators)
	 */
	@Override
	public void flatten() {
		// check if this function has an associative operator
		if (this.isAssociative()) {
			List<Expression> newArgs = new ArrayList<Expression>();
			
			for (int i=0; i<args.length; i++) {
				args[i].flatten();
				
				// check if a sub-expression has the same operator add all its
				// sub expression to this expression's list of arguments
				if ((args[i] instanceof Application) &&((Application) args[i]).getFunction().equalsMult(fun)) {
					for (Expression subExpn : ((Application) args[i]).args) {
						newArgs.add(subExpn);
					}					
				} else {
					newArgs.add(args[i]);
				}
			}
			// convert to array
			args = (Expression []) newArgs.toArray(args);
			
		} else { // otherwise flatten all operands and do nothing yourself 
			fun.flatten();	
			
			for (int i = 0; i < args.length; i++) {				
				args[i].flatten();
			}
		}
	}

	@Override
	public boolean contains(Identifier x) {
		for (Expression e : args) {
			if (e.contains(x)) {
				return true;
			}
		}
		
		return fun.contains(x);
	}
	
	@Override
	protected Expression cloneHelper(Map<Variable,Variable> m) {
		Expression[] newArgs = new Expression[args.length];
		
		for (int i = 0; i < args.length; i++) {
			newArgs[i] = args[i].clone(m);			
		}
		
		return new Application(fun.clone(m), newArgs);
	}
	
	@Override
	public String getTreeRep() {
		String result = "(application " + fun.getTreeRep();
		
		for (Expression arg : args) {
			result += " " + arg.getTreeRep();
		}
		
		return result + ")";
	}

	@Override
	public <T> T acceptVisitor(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}	
	
	/**
	 * Returns the function in this application. That is, it returns the 
	 * operator. If this is a juxtaposition (white space infix operator in 
	 * aPToP) that is used to apply a function to an operand, it returns the
	 * expression representing that whitespace.
	 *  
	 * @return
	 */
	public Expression getFunction() {
		return this.fun;
	}
	
	public Expression[] getArgs() {
		return this.args;
	}
	
	public int arity() { 
		return this.args.length; 
	}
	
	/**
	 * We treat any operator as a function application, with that operator being
	 * the function. Here we want to determine if the operator is the actual 
	 * juxtaposition (white space infix operator in aPToP) that is used to apply
	 * a function to an operand.
	 *  
	 * @return Is this expression a function application
	 */
	public boolean is_fun_application() {
		if (fun.is_operator()) {
			return ((Variable)fun).getId() == Identifiers.get_infix(1, " ");
		}	
		return false;
	}
	
	public Identifier getFunId() {
		if (fun instanceof Identifier) {
			return (Identifier)fun;
		} else {
			return ((Variable)fun).getId();
		}
	}
	
	/**
	 * Utility function that returns the precedence of this application's
	 * operator (if there is one). If there is no operator in this application,
	 * return -1 for precedence.
	 * 
	 * @return Precedence of operator
	 */
	protected int getPrecedence() {
		if (fun.is_operator()) {					
			if (fun instanceof Identifier) {
				return ((Identifier)fun).getPrecedence();
			} else {
				return ((Variable)fun).getId().getPrecedence();
			}
		} 
		return -1;
	}
	
	private void funAppType(List<Expression> context) {
		for (Expression arg: args) {
			arg.makeTypes(context);
			
			if (arg.getType() == null) {						
				return;
			}
		}
		
		Expression funtype = args[0].getType();
		Expression argtype = args[1].getType();
		
		//determine the type of a function applied to an argument
		if ((funtype instanceof Application) 
				&& (((Application)funtype).fun.toString().equals("→"))){
			if (((Application)funtype).getChild(0).typeCheck(argtype, context)) {
				this.setType(((Application)funtype).getChild(1));
				return;
			}
			//temporary hack, this can actually lead to an inconsistency with finite string domains
			//determine the type of a list being indexed
		} else if (ExpressionUtils.listType.typeCheck(funtype, new ArrayList<Expression>())) {			
			Expression d = funtype.at(new Path(new int[]{0,0}));			
			if (d.typeCheck(argtype, context)) {
				this.setType(d);
				return;
			}
		}
		
		this.setType(null);
	}
	
	private void ifhack(List<Expression> context) {
		if (this.fun.toString().equals(Identifiers.ifthenelse.toString())) {
			Expression thenpart = this.args[1].getType();
			Expression elsepart = this.args[2].getType();
			
			if (thenpart.typeCheck(elsepart, context)) {
				this.setType(thenpart);
			} else if (elsepart.typeCheck(thenpart, context)) {
				this.setType(elsepart);
			}
		}
	}
	
	private int argumentNumber (Expression type) {
		if ((type instanceof Application) && (((Application)type).fun.toString().equals("→"))) {
			assert ((Application) type).arity() == 2;
			return 1 + argumentNumber(type.getChild(1));
		} else {
			return 0;
		}
	}
	
	private Expression checkApplicationType(Expression type, int i, List<Expression> context) {
		if ((type instanceof Application) && (((Application)type).fun.toString().equals("→"))){
			assert ((Application) type).arity() == 2;

			if (type.getChild(0).typeCheck(this.args[i].getType(), context)) {				
				return this.checkApplicationType(type.getChild(1), i+1, context);
			} else {
				return null;
			}
		} else {
			return type;
		}
	}
	
	/**
	 * Is the operator of this application associative 
	 * @return
	 */
	private boolean isAssociative() {
		return (fun.is_operator()&&(Symbol.assocMap.containsKey(fun.toString())));			
	}
}
