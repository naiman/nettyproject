package prooftool.backend.laws;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import prooftool.backend.Substitution;
import prooftool.backend.ccode.ExpressionVisitor;
import prooftool.util.Identifiers;
import prooftool.util.objects.Dictionary;

public class Variable extends Expression {
	private static final long serialVersionUID = 3321082539539071883L;
	private Identifier id; // non-null

	@Override
	public boolean is_operator() {
		return id.is_operator();
	}

	public Variable(Identifier id) {
		super();
		this.id = id;
	}

	
	/**
	 * Creates a list of expression parts returned are at one zoom level down.
	 * Here its simply the variable's string representation. 
	 * 
	 * @return A list of String and Object, where the object parts are zoomable
	 *         parts and the String parts are operators/keywords.
	 */
	@Override
	public Object[] toParts() {
		return new Object[]{id.toString()};
	}

	/**
	 * This method should never be called on a variable. It is called initially
	 * on an expression to convert identifiers to variables.
	 */
	@Override
	public Expression makeVariables(Dictionary d) {
		// make_variables only runs before there are any variables.
		return d.get(this); // this way we dont have to worry about safety of calls
	}
	
	@Override
	public Expression syncVariables(Dictionary d) {
		return d.get(this);
	}
	
	/**
	 * Returns whether the given variable x is in this expression. Here its a 
	 * simple equality comparison. Note that it must be the same object in order
	 * to be found.
	 * 
	 * @return Whether x is in this expression
	 */
	@Override
	public boolean contains(Variable x) {
		return this == x;
	}
	
	@Override
	public Expression instantiate(Map<Variable, Expression> subst) {
		if (subst.get(this)!=null) {
			return subst.get(this);
		} else {
			return this;
		}
	}
	
	@Override
	public Expression instantiate(Substitution s) {
		if (s.get(this)!=null) {
			return s.get(this);
		} else {
			return this;
		}
	}
	
	@Override
	public Map<Variable,Expression> getAllVars(Map<Variable,Expression> currentVars){
		currentVars.put(this, this);
		return currentVars;
	}
	
	@Override
	public Map<Variable,Variable> getAllVarsHelper(){
		Map<Variable,Variable> vars = new IdentityHashMap<Variable,Variable>();
		vars.put(this, new Variable(id));
		return vars;
	}

	@Override
	/**
	 * Should never be called, since variables have no children
	 */
	public void setChild(int i, Expression replacement) {
		throw new UnsupportedOperationException();		
	}
	
	@Override
	public Expression getChild(int i) {	
		return null;
	}
	
	@Override
	public List<Expression> getChildren() {	
		return new ArrayList<Expression>();
	}

	@Override
	public void flatten() {		
	}
	
	/**
	 * Since all identifiers are unique, we can check object equality
	 */
	@Override
	public boolean contains(Identifier x) {
		return this.id == x;
	}
	
	@Override
	protected Expression cloneHelper(Map<Variable,Variable> m) {
		assert m.containsKey(this);
		return m.get(this);
	}

	/*All variables must be introduced in the following ways:
	 * 0. Within the scope of a function - variable identifier
	 * 1. Using the 'let' statement - constant identifier
	 * 2. Neither 'let' or scope, but rather just being there - name of a bunch (or type)
	 * 
	 * The type of the variable is the declared type in case 0, or the type of
	 * the expression in the 'let' statement in case 1. In those two cases the
	 * declaration takes care of determining the type.
	 * 
	 * Hence in case 2, where we come to this method and the type is still null,
	 * it means no declaration has been made for this variable. Such as example
	 * is 'nat'. Since type in our system simply means the bunch this variable 
	 * is in, the only thing we can conclude for variable 'a' about its type is
	 * that 'a:a'. 
	 */
	@Override
	public void makeTypes(List<Expression> context) {
		if (this.getType() == null) {
			this.setType(this);		
		}

		// try to get the type from context
		Expression t = this.typeInContext(this, context);
		
		if (t != null) {
			this.setType(t);
		}
	}
	
	@Override
	public String getTreeRep() {
		return "(variable " + id.getTreeRep() + ")"; 
	}

	@Override
	public <T> T acceptVisitor(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean is_uni_quant_scope() {
		return false;
	}
	
	public Identifier getId() {
		return id;
	}

	public void setId(Identifier id) {
		this.id = id;
	}
		
	private Expression typeInContext(Variable v, List<Expression> context) {		
		Expression ctype = null;
		
		for (Expression l:context) {
			if ((l instanceof Application)
					&&(((Application)l).getFunId() == Identifiers.colon)){
				if (l.getChild(0).equals(v)) {
					ctype = ((Expression)l).getChild(1).clone();
					break;
				}
			}
		}
		return ctype;
	}
}
