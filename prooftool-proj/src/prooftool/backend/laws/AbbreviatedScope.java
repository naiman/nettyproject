package prooftool.backend.laws;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import prooftool.backend.Substitution;
import prooftool.backend.ccode.ExpressionVisitor;
import prooftool.util.ExpressionUtils;
import prooftool.util.Identifiers;
import prooftool.util.objects.Dictionary;

/**
 * This class represents an abbreviated scope expression. It is used as a shorthand for
 * introducing variables, which is especially useful when programming.
 * 
 * @author lev
 *
 */

public class AbbreviatedScope extends Scope
{
	private static final long serialVersionUID = 5986949884449213616L;
	private Identifier id;
	private Expression dummyPrime;
	
	public AbbreviatedScope(Identifier id, Expression dummy, Expression domain, Expression body) {
		super(dummy, (domain == null? ExpressionUtils.ALL: domain), false, body);
		assert dummy instanceof Identifier || (dummy instanceof Variable && !((Variable)dummy).is_operator());
		
		this.id = id;
		
		if (this.id == Identifiers.var || this.id == Identifiers.result) {				
			dummyPrime = Identifiers.get(0, false, false, dummy.toString() + ExpressionUtils.prime);
		}
	}
	
	public AbbreviatedScope(Identifier id, Variable dummy, Expression domain, Expression body) {
		super(dummy, (domain == null? ExpressionUtils.ALL: domain), false, body);

		
		if (this.id == Identifiers.var || this.id == Identifiers.result) {				 
			dummyPrime = new Variable(Identifiers.getName(dummy.toString() + ExpressionUtils.prime));
		}
	}
	
	/**
	 * During parsing, ‘dummy’ can be an Identifier, but after parsing it must be
	 * a Variable.
	 * ‘dummy’ lives and dies with the scope and it is referenced from within ‘body’
	 * but must not be referenced from anywhere else. 
	 */
		
	// Scopes with several dummies are syntactic sugar which the book
	// uses only when the domain is not explicit. We leave this sugar to the
	// presentation layer and exclude it from the abstract syntax.

	public Variable getDummyPrime() { 
		return (Variable) this.dummyPrime; 
	}
	
	public Identifier getId() { 
		return this.id;
	}
	
	@Override
	/**
	 * Creates a list of expression parts returned are at one zoom level down. 
	 * 
	 * @return A list of String and Object, where the object parts are zoomable
	 *         parts and the String parts are operators/keywords.
	 */
	public Object[] toParts() {
		assert this.id.getKeywords().length == 4;
		String [] k = id.getKeywords();
		
		if (domain instanceof Application && !((Application)domain).getFunId().isBracketing()) {			
			return new Object[] {k[0], dummy.toString(), k[1], "(", domain, ")", k[2], body, k[3]};
		} else {
			return new Object[] {k[0], dummy.toString(), k[1], domain, k[2], body, k[3]};
		}
	}

	@Override
	/**
	 * Turn every identifier in this expression into its corresponding variable.
	 * The variable it refers to depends on scope (since an identical 
	 * identifier within a different scope is really a different variable).
	 * 
	 * This method changed all identifiers that refer to the same variable to 
	 * actually be that same variable by making them all the same object.
	 * 
	 * The dummy introduces a new variable, and hence it is placed in the 
	 * dictionary with the identifier. At this point it might displace any 
	 * previous mapping of the same identifier, and this means that we have
	 * the same identifier name in an internal scope. 
	 * 
	 * @param A dictionary containing a mapping of identifiers to variables
	 * @return This expression, with all its identifiers turned into variables
	 */
	public Expression makeVariables(Dictionary d) {	
		domain = domain.makeVariables(d);
		
		if ((id == Identifiers.var || id == Identifiers.result)) {
			Variable dummy_var = new Variable((Identifier) dummy);
			Variable dummyPrime_var = new Variable((Identifier)dummyPrime);
			Variable old_dummy = d.put(dummy_var);
			Variable old_dummyPrime = d.put(dummyPrime_var);
			
			body = body.makeVariables(d);
			
			d.reset(dummy_var.getId(), old_dummy);
			d.reset(dummyPrime_var.getId(), old_dummyPrime);
			dummy = dummy_var;
			dummyPrime = dummyPrime_var;
		} else {
			Variable dummy_var = new Variable((Identifier) dummy);			
			Variable old_dummy = d.put(dummy_var);
			
			body = body.makeVariables(d);
			
			d.reset((Identifier) dummy, old_dummy);			
			dummy = dummy_var;			
		}
		
		return this;
	}
	
	@Override
	public Expression syncVariables(Dictionary d) {
		domain = domain.makeVariables(d);
		
		if ((id == Identifiers.var || id == Identifiers.result)) {
			Variable old_dummy = d.put((Variable)dummy);
			Variable old_dummyPrime = d.put((Variable)dummyPrime);

			body = body.makeVariables(d);
			
			d.reset(((Variable)dummy).getId(), old_dummy);
			d.reset(((Variable)dummyPrime).getId(), old_dummyPrime);
		} else {					
			Variable old_dummy = d.put((Variable)dummy);
			
			body = body.makeVariables(d);
			
			d.reset(((Variable)dummy).getId(), old_dummy);
		}
		
		return this;
	}
	
	
	@Override
	public void makeTypes(List<Expression> context) {			
		domain.makeTypes(context);
		
		if (this.id == Identifiers.let || this.id == Identifiers.letp) {
			dummy.setType(domain.getType());
		} else {
			dummy.setType(domain);
		}
		
		if ((id == Identifiers.var || id == Identifiers.result)) {
			assert dummyPrime!=null;
			dummyPrime.setType(domain);
		}
		
		body.makeTypes(context);

		if (body.getType() == null) {
			this.setType(null);
		} else {
			if ((this.id == Identifiers.var)||((this.id == Identifiers.ivar))) {
				if (body.getType().toString().equals("bool")) {
					this.setType(ExpressionUtils.parse("bool"));
				} else {
					this.setType(null);
				}
			} else if (this.id == Identifiers.let || this.id == Identifiers.letp) {
				this.setType(body.getType());
			} else if (this.id == Identifiers.scope) {
				this.setType(ExpressionUtils.parse("(" + dummy.getType() + ")" + "→" + "(" + body.getType() + ")"));
			} else if (this.id == Identifiers.procedure_scope) {
				this.setType(ExpressionUtils.parse("(" + dummy.getType() + ")" + "↣" + "(" + body.getType() + ")"));
			} else if (this.id == Identifiers.bunch_scope) {
				this.setType(ExpressionUtils.parse("(" + dummy.getType() + ")" + "↦" + "(" + body.getType() + ")"));
			} else if (this.id == Identifiers.result) {
				this.setType(domain);
			}
		}	
	}

	/**
	 * Returns whether the given variable x is in this expression (either
	 * dummy, domain or body). Note that it must be the same object in order
	 * to be found.
	 * 
	 * @return Whether x is in this expression
	 */
	@Override
	public boolean contains(Variable x) {
		if ( x.getId() == getDummy().getId() )
			return false;
		if ( domain != null )
			if ( domain.contains(x) )
				return true;
		return body.contains(x);
	}
	
	/**
	 * This is for determining if a variable with the same name exists in this 
	 * expression. If the scope declares a variable with the same name, then it
	 * is not contained, since its actually a different variable
	 */
	@Override
	public boolean contains(Identifier x) {		
		if ((domain != null) && (domain.contains(x))) {
			return true;
		}
		if (x == getDummy().getId()) {
			return false;
		}
		if ((dummyPrime!=null)&&(x==((Variable)dummyPrime).getId())) {
			return false;
		}
		return body.contains(x);
	}

	@Override
	public Expression instantiate(Map<Variable, Expression> subst) {
		domain = domain.instantiate(subst);				
		body = body.instantiate(subst);		
		dummy = dummy.instantiate(subst);//this should never do anything
		return this;
	}
	
	@Override
	public Expression instantiate(Substitution s) {
		domain = domain.instantiate(s);				
		body = body.instantiate(s);		
		dummy = dummy.instantiate(s);//this should never do anything
		return this;
	}
	
	@Override
	public Map<Variable,Expression> getAllVars(Map<Variable,Expression> currentVars){
		
		currentVars.putAll(domain.getAllVars(currentVars));		
		if (dummy instanceof Variable) {
			currentVars.put((Variable)dummy, dummy);
		}
		if ((id == Identifiers.var || id == Identifiers.result) && dummyPrime instanceof Variable) {
			currentVars.put((Variable)dummyPrime, dummyPrime);
		}
		currentVars.putAll(body.getAllVars(currentVars));		
		return currentVars;
	}
	
	@Override
	public Map<Variable,Variable> getAllVarsHelper(){
		Map<Variable,Variable> vars = new IdentityHashMap<Variable,Variable>();
		
		if (domain != null) {
			vars.putAll(domain.getAllVarsHelper());
		}
		
		if (dummy instanceof Variable) {
			vars.put((Variable)dummy, new Variable(((Variable)dummy).getId()));
		}
		
		if ((id == Identifiers.var || id == Identifiers.result) && dummyPrime instanceof Variable) {
			vars.put((Variable)dummyPrime, new Variable(((Variable)dummyPrime).getId()));
		}
		
		vars.putAll(body.getAllVarsHelper());
		return vars;
	}	
	
	@Override
	public void setChild(int i, Expression replacement) {
		assert ((i==0)||(i==1));
		
		if (i==0) {
			domain = replacement;
		} else if (i==1) {
			body = replacement;
		}
	}
	
	@Override
	public Expression getChild(int i) {
		if (i==0) {
			return domain;
		} else if (i==1) {
			return body;
		}
		return null;
	}
	
	@Override
	public void flatten() {		
		domain.flatten();				
		body.flatten();		
	}
	
	@Override
	protected Expression cloneHelper(Map<Variable,Variable> m) {
		AbbreviatedScope sc = new AbbreviatedScope(id, dummy.clone(m), domain.clone(m), body.clone(m));
		
		if ((sc.id == Identifiers.var || sc.id == Identifiers.result)) {
			sc.dummyPrime = dummyPrime.clone(m);
		}
		
		return sc;
	}
	
	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<Expression>(2);
		children.add(domain);
		children.add(body);
		return children;
	}

	@Override
	public <T> T acceptVisitor(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public String getTreeRep() {
		return "(scope " + id.getTreeRep() + " " + domain.getTreeRep() + " " + body.getTreeRep() + ")";
	}

	@Override
	public boolean is_uni_quant_scope() {
		return false;
	}
}
