package prooftool.backend.laws;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import prooftool.backend.Substitution;
import prooftool.backend.ccode.ExpressionVisitor;
import prooftool.util.ListUtils;
import prooftool.util.Symbol;
import prooftool.util.objects.Dictionary;
import prooftool.util.objects.Predicate;
import prooftool.util.objects.UnaryFunction;

/**
 * Identifiers are the syntactic representation of variables. Since everything
 * but scopes are variables, the entire concrete syntax is handled by this class
 * (and possibly sub-classes).
 * 
 * Here's a general taxonomy of Fixities. Let | stand for a keyword of the
 * identifier and · stand for an operand or argument. #keywords arity pre
 * pattern name example 1 1 ⊥ ·| postfix signaling c! 1 1 ⊤ |· prefix -1 1 2 ⊥
 * ·|· infix 1+2 2 1 ⊤ |.| bracket {1} 3 2 ⊤ |.|.| multi while 1 do 2 wend 4 3 ⊤
 * |.|.|.| multi if 1 then 2 else 3 fi
 * 
 * There are other fixities conceivable (just think of Hoare's ‘infix ternary
 * if’ with the pattern ·|·|·), but we want to keep it simple for now. Also,
 * signaling is the only postfix operator in aPToP and we can easily omit if for
 * now. Prefix-Identifiers that apply to more than one argument are always
 * curried so they apply to just one argument at a time.
 * 
 * TODO: there's more documentation on this in other places that has to be
 * merged. consider putting it to wiki.
 * 
 *  @author unknown
 *  @author evm
 */
public class Identifier extends Expression {
	private static final long serialVersionUID = 7332087386770750128L;
	
	private enum Fixities {
		POST, PRE, IN, BRACK, NONE;
	}
	
	private static Fixities fixityOf(String first, String second) {
		if (first.equals(emptyKeyword) && second.equals(emptyKeyword)) {
			return Fixities.IN;
		} else if (first.equals(emptyKeyword)) {
			return Fixities.POST;
		} else if (second.equals(emptyKeyword)) {
			return Fixities.PRE;
		} else {
			return Fixities.BRACK;
		}
	}
	
	protected static final String emptyKeyword = "";
	protected static final String hashEmptyKeyword = "&";
	
	/**
	 * Used for pretty printing to determine whether explicit parentheses are
	 * needed.
	 */
	private int precedence;

	/**
	 * See long comment on Expression.is_operator().
	 */
	private boolean is_operator;
	
	/**
	 * Used for pretty printing.
	 * @assert is_infix ==> is_operator
	 */
	private boolean is_infix;
	private Fixities fixity;
	
	private String keywords[];
	
	/**
	 * Multi-keyword Identifiers are always operators and never infix. (Most
	 * also have precedence 0, since that bracket their arguments, but the
	 * triangles need this parameter.)
	 * 
	 * @param name
	 */
	public Identifier(int precedence, String... keywords) {
		assert precedence>=0;
		assert keywords.length > 0;
		this.is_operator = true;
		this.is_infix = false;
		this.precedence = precedence;
		this.keywords = keywords;
		this.fixity = Identifier.fixityOf(keywords[0], keywords[keywords.length-1]);
	}

	/**
	 * Identifiers are unique objects (one per name/keywords) and only created
	 * in their factory class Identifiers.
	 */
	public Identifier(int precedence, boolean is_operator, boolean is_infix, String name) {
		assert precedence >= 0;
		
		this.precedence = precedence;
		this.is_operator = is_operator;
		this.is_infix = is_infix;		
		
		if (is_operator) {
			if (is_infix) { // infix
				keywords = new String[3];
				keywords[0] = "";
				keywords[1] = name;
				keywords[2] = "";
				fixity = Fixities.IN;
			} else { // prefix
				keywords = new String[2];
				keywords[0] = name;
				keywords[1] = "";
				fixity = Fixities.PRE;
			} // for now, no postfix
		} else {
			assert !this.is_infix;
			keywords = new String[1];
			keywords[0] = name;
			fixity = Fixities.NONE;
		}
	}
	
	@Override
	public boolean is_operator() {
		return is_operator;
	}
	
	/** 
	 * For operators this is only used for debugging purposes.
	 * For all other expressions it's used for printing too.
	 * 
	 */
	@Override
	public String toString() {			
		String result = "";
		
		for (int i = 0; i < keywords.length; i++) {
			result = result + Symbol.lookupUnicode(keywords[i]);
		}
		
		return result;
	}
	
	@Override
	public boolean equals(Expression id) {
		return (id instanceof Identifier) && this.toString().equals(id.toString()) && this.keywords.length == ((Identifier) id).keywords.length;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Identifier) {
			return equals((Identifier) o);
		} else {
			return false;
		}
	}
	
	/** Only for debugging purposes, 
	 * since Identifiers will not be seen as Expressions after parse is complete
	 */
	@Override
	public Object[] toParts(){
		return new Object[] { toString() };
	}
	
	/**
	 * Turn every identifier in this expression into its corresponding variable.
	 * The variable it refers to depends on scope (since an identical 
	 * identifier within a different scope is really a different variable).
	 * 
	 * This method changed all identifiers that refer to the same variable to 
	 * actually be that same variable by making them all the same object.
	 * 
	 * @param A dictionary containing a mapping of identifiers to variables
	 * @return This expression, with all its identifiers turned into variables
	 */
	@Override
	public Expression makeVariables(Dictionary d) {
		return d.get(this);
	}
	
	@Override
	public boolean contains(Variable x) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Expression syncVariables(Dictionary d) {
		//should never be called, but does no evident harm if it does
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Expression instantiate(Map<Variable, Expression> subst) {		
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Expression instantiate(Substitution s) {		
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Map<Variable,Expression> getAllVars(Map<Variable,Expression> currentVars){
		return currentVars;
	}
	
	@Override
	public Map<Variable,Variable> getAllVarsHelper(){		
		return new IdentityHashMap<Variable,Variable>();
	}
	
	/**
	 * Should never be called, since identifiers have no children
	 */
	@Override
	public void setChild(int i, Expression replacement) {
		throw new UnsupportedOperationException();		
	}
	
	@Override
	public Expression getChild(int i) {	
		throw new UnsupportedOperationException();
	}
	
	@Override
	public List<Expression> getChildren() {	
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void flatten() {		
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean contains(Identifier x) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	protected Expression cloneHelper(Map<Variable,Variable> m) {
		return this;
	}
	

	/**
	 * makeTypes is only called after make_variables. Since there
	 * will be only Variable objects and no Identifier objects nothing need be done  
	 */		
	@Override
	public void makeTypes(List<Expression> context) {	
		throw new UnsupportedOperationException();
	}

	@Override
	public String getTreeRep() {
		Predicate<String> nonempty = new Predicate<String>(){ public boolean apply(String s) { return !s.isEmpty(); }};
		UnaryFunction<String, String> toAscii = new UnaryFunction<String, String>(){ public String apply(String s) { return Symbol.lookupAscii(s); }};
		return ListUtils.join(ListUtils.transform(ListUtils.filter(keywords, nonempty), toAscii), " ");
	}

	@Override
	public <T> T acceptVisitor(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean is_uni_quant_scope() {
		return false;
	}
	
	public boolean isPrefix() {
		boolean prefix = keywords.length == 1 
		&& !keywords[0].equals(emptyKeyword) 
		&&  keywords[1].equals(emptyKeyword);

		return prefix;
	}
	
	public boolean isBracketing() {
		return this.fixity == Fixities.BRACK;
	}

	public String name() {
		assert keywords.length == 1;
		return Symbol.lookupUnicode(keywords[0]);
	}
	
	protected int getPrecedence() {
		return this.precedence;
	}
	
	protected String[] getKeywords() {
		return this.keywords;
	}
	
	/**
	 * Called by Application to get the syntactic order of the Application which
	 * depends on the fixity of the Identifier.
	 * 
	 * Precondition This identifier is_operator()
	 * 
	 * @param args
	 * @return Returns a list of String and Expression, where the Expressions
	 *         represent the zoomable parts of the Application.
	 */
	protected Object[] toParts(Expression[] args) {	
		// allowing flat associative structures
		boolean is_associative = (is_infix) && (Symbol.assocMap.containsKey(this.toString())) && args.length > 2;
		
		//if it is associative treat it differently
		Object[] l = new Object[is_associative ? 2 * args.length + 1 : args.length + this.keywords.length];
		
		for (int i = 0; i < args.length; i++) {
			l[2 * i] = keywords[i];
			l[2 * i + 1] = args[i];
		}
		
		if (is_associative) {
			l[0] = keywords[0];
			l[2 * args.length] = keywords[keywords.length - 1];
		} else {
			l[2 * args.length] = keywords[args.length];
		}
		
		// add parens if necessary
		List<Object> parened = new ArrayList<Object>();
		
		for (int i = 0; i < l.length; i++) {
			Object part = l[i];
			boolean toBracket = false;
			
			if (part instanceof Application) {
				toBracket = this.shouldBracket((Application)part, l[i - 1].toString(), l[i + 1].toString());
			} else if (part instanceof AbbreviatedScope) {
				toBracket = this.shouldBracket((AbbreviatedScope)part, l[i - 1].toString(), l[i + 1].toString());
			} else if (part instanceof String) {
				part = Symbol.lookupUnicode((String)part);
			}
			
			if (toBracket){
				parened.add("(");
				parened.add(part);
				parened.add(")");
			} else {
				parened.add(part);
			}
		}
		
		return parened.toArray();		
	}
	
	private boolean shouldBracket(Application part, String leftKeyword, String rightKeyword) {
		int partPrec = ((Application)part).getPrecedence();
		Expression fun = ((Application)part).getFunction();
		
		Fixities myFix = Identifier.fixityOf(leftKeyword, rightKeyword);
		Identifier subId = (fun instanceof Variable)? (((Variable)fun).getId()) : ((Identifier)fun);
		Fixities subFix = Identifier.fixityOf(subId.keywords[0], subId.keywords[subId.keywords.length-1]);
		
		//these two are here because of the current shortcomings of the grammar
		boolean messyId = Symbol.messyIdents.containsKey(subId);
		boolean quantBrackets = Symbol.quantifiers.containsKey(this) && partPrec > 2;
		
		return (fun.is_operator() 
				&& (partPrec != -1)  
				&& (precedence <= partPrec)
				&& !(this.equals(subId) && !Symbol.assocMap.containsKey(toString()))
			   && ((myFix != Fixities.BRACK)||(this.is_infix))
			   && (!(myFix == Fixities.PRE && subFix == Fixities.PRE && this.isPrefix() && !messyId))
			   && (!(myFix == Fixities.POST && subFix == Fixities.POST && !messyId)))||quantBrackets;
	}
	
	private boolean shouldBracket(AbbreviatedScope part, String leftKeyword, String rightKeyword) {
		int partPrec = part.getId().precedence;

		Fixities myFix = Identifier.fixityOf(leftKeyword, rightKeyword);
		Identifier subId = part.getId();
		Fixities subFix = Identifier.fixityOf(subId.keywords[0], subId.keywords[subId.keywords.length-1]);

		boolean messyId = Symbol.messyIdents.containsKey(subId);
		assert partPrec != -1;
		return ((precedence <= partPrec)
			    && ((myFix != Fixities.BRACK)||(this.is_infix))
			    && (!(myFix == Fixities.PRE && subFix == Fixities.PRE && !messyId))
			    && (!(myFix == Fixities.POST && subFix == Fixities.POST && !messyId)));
	}
}
