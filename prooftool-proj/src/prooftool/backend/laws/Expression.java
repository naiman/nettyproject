package prooftool.backend.laws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import prooftool.backend.Substitution;
import prooftool.backend.ccode.ExpressionVisitor;
import prooftool.backend.direction.Direction;
import prooftool.backend.direction.ExpnDirection;
import prooftool.util.Identifiers;
import prooftool.util.Main;
import prooftool.util.ExpressionUtils;
import prooftool.util.Symbol;
import prooftool.util.objects.Dictionary;
import prooftool.util.objects.Interval;
import prooftool.util.objects.Path;
import prooftool.util.objects.ZoomInfo;

/**
 * General type for logical/math/programming expressions to be manipulated.
 * 
 * Subclasses are Application, Scope, Variable, and Identifier. Identifiers are
 * only there after the first parsing step. At every point after parsing is
 * complete, there will be only Application, Scope, and Variable!
 * 
 * In addition, there is the value class, for special data types that we will 
 * have some built in operations (such as numbers). That part is not implemented 
 * yet.
 * 
 * @author unknown
 * @author evm
 */
public abstract class Expression implements Serializable, Cloneable {

	private static final long serialVersionUID = 8089312367454731995L;
	private Expression type = null;
	private String lawName = "Unnamed Law";

	/**
	 * Returns an array that has this expression separated into parts. The two
	 * types of parts are strings, which include brackets and operators, and the
	 * direct sub-expression of this one. For example, for the expression
	 * '2*(x+y)' the parts will be [2,*,(,x+y,)], where '2' and 'x+y' are
	 * expressions and the rest are strings.
	 *
	 * This method is generally used to get the relevant expression parts for
	 * display on the screen.
	 */
	public abstract Object[] toParts();

	/**
	 * Helper function used by make_variables()
	 *
	 * @param d
	 * @return
	 */
	public abstract Expression makeVariables(Dictionary d);

	public abstract Expression syncVariables(Dictionary d);

	public abstract void makeTypes(List<Expression> context);
	
	/**
	 * Returns the child expression at index i. This is necessary in order to
	 * keep track of zoom.
	 *
	 * Note: scopes will have child 0 be the domain, and child 1 as the body
	 * (even if there is no domain).
	 *
	 * Precondition: none Postcondition: The expression returned is the child
	 * expression at index i, or 'null' if there is no child at that index.
	 *
	 * @param i
	 *            The index of the child expression requested
	 * @return The child expression at index i, or 'null' if there is no child
	 *         at that index.
	 */
	public abstract Expression getChild(int i);
	
	/**
	 * Replaced sub-expression i with the given replacement
	 *
	 * @param i
	 *            Index to replace at
	 * @param replacement
	 *            The expression to replace sub-expression i
	 */
	public abstract void setChild(int i, Expression replacement);
	
	/**
	 * Returns whether this expression contains the given variable
	 * 
	 * @param x
	 * @return Does ‘this’ contain ‘x’?
	 */
	public abstract boolean contains(Variable x);

	/**
	 * Returns whether this expression contains the given identifier
	 * 
	 * @param x
	 * @return Does ‘this’ contain ‘x’?
	 */
	public abstract boolean contains(Identifier x);

	public abstract List<Expression> getChildren();
	
	/**
	 * Gets the structure of associative operators to be be flat. This method
	 * mutates the expression.
	 *
	 * @return The flattened version of this expression
	 */
	public abstract void flatten();
	
	/**
	 * This method takes a mapping of variables of this expression to other
	 * expressions, and performs instantiation by replacing them with whatever
	 * they were mapped to.
	 *
	 * This should generally be called only with an IdentityHashMap, because
	 * doing otherwise will change any variable whose identifier matches one in
	 * the map. However, in some instances, such as renaming, we do want to do
	 * exactly that.
	 *
	 * @param subst
	 *            The variable to expression mapping
	 * @return
	 */
	public abstract Expression instantiate(Map<Variable, Expression> subst);

	/**
	 * Removes the clutter a bit for instantiating directly with a Substitution
	 * 
	 * @param s
	 *            The Substitution, usually after unification
	 * @return
	 */
	public abstract Expression instantiate(Substitution s);
	
    /**
     * This function is implemented in Application. This is a utility function
     * that is used after parsing. Clearly, it returns false for all other
     * expressions. 
     *
     * @return Is ‘this’ of the form ∀⟨_→_⟩ ?
     */
	public abstract boolean is_uni_quant_scope();
	
	protected abstract Map<Variable, Expression> getAllVars(Map<Variable, Expression> currentVars);

	public abstract Map<Variable, Variable> getAllVarsHelper();
	
	// Accept an ExpressionVisitor
	public abstract <T> T acceptVisitor(ExpressionVisitor<T> visitor);
	
	// Get expressions's tree representation in string form.
	public abstract String getTreeRep();
	
	protected abstract Expression cloneHelper(Map<Variable, Variable> m);
	
	@Override
	public String toString() {
		Object[] parts = this.toParts();
		StringBuilder ret = new StringBuilder();
		
		for (Object x : parts) {
			ret.append(x.toString());
		}
		
		return ret.toString();
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Expression)
			return equals((Expression) o);
		else
			return false;
	}
	
	@Override
	public Expression clone() {
		// Expression clone = structClone().make_variables();
		// WARNING we should really create a new variable for each of the
		// current ones
		// however, getAllVarsHelper does that, so we are good for now
		return this.clone(this.getAllVarsHelper());
	}
    
	/**
     * 
     */
	public Expression clone(Map<Variable, Variable> m) {
		Expression clone = this.cloneHelper(m);

		clone.makeTypes(new ArrayList<Expression>());
		clone.lawName = this.lawName;
		clone.type = this.type;
		
		return clone;
	}

	public Expression getType() {
		return this.type;
	}
	
	public void setType(Expression newType) {
		this.type = newType;
	}
	
	/**
	 * This method returns the context that is gained if we were to zoom into
	 * the ith sub-expression of 'this' expression. Currently, this method
	 * breaks up context expressions as far down as possible (see
	 * completeContext comment).
	 *
	 * @param i
	 *            The sub-expression at which we are to determine context
	 * @return The context gain at sub-expression i
	 */
	public List<Expression> getContext(int i) {
		List<Expression> context = new ArrayList<Expression>();
		
		for (Expression contextExpn : this.basicContext(i)) {
			context.addAll(ExpressionUtils.completeContext(contextExpn));
		}
		
		return context;
	}
	
	public String getLawName() {
		return lawName;
	}

	public void setLawName(Object name) {
		lawName = name.toString();
	}
	
    /**
     * Operators are those Identifier on which we don't want to zoom. For
     * instance, in ⊤∧(f x = 1) it makes sense to zoom on ⊤ x f 1 (because there
     * could be laws which have them on one side), but it doesn't make sense to
     * zoom on ∧ or = because there'd be no step leading away from this focus
     * expression. (Special note for Rick: that's not meant to forbid the user
     * from zooming there, but meant to make the good zooming places an easier
     * target for the little mouse pointer.)
     *
     * For programming convenience (save type casts), we generalize the notion
     * of operator to expressions by saying that operators are Variables whose
     * Identifier is an Operator. (Thus, Applications, Scopes, and Literals are
     * operators which is why this defaults to ⊥ .)
     */
	public boolean is_operator() {
		return false;
	}

	public void copyTypes(Expression clone) {
		clone.type = this.type;
		
		for (int i = 0; i < this.getChildren().size(); i++) {
			Expression child = this.getChild(i);
			child.copyTypes(clone.getChild(i));
		}
	}
  
	public int offset(Path path) {
		if (path.isEmpty()) {
			return 0;
		}
		
		if (this.at(path.clone()) != null) {
			return offsetHelper(path.reverse(), 0);
		} else {
			return -1;
		}
	}
    
	private int offsetHelper(Path p, int offset) {
		int childCount = 0;
		
		for (Object part : toParts()) {
			if (part instanceof Expression) {
				if (childCount == p.peek()) {
					p.pop();
					
					if (!p.isEmpty()) {
						offset = ((Expression) part).offsetHelper(p, offset);
					}
					
					break;
				} else {
					offset += part.toString().length();
				}
				childCount++;
			} else {
				offset += part.toString().length();
			}
		}
		
		return offset;
	}
	
    /**
     * Determine whether the given type is equivalent or included in this
     * expression. Although it can be made much more complicated, it is
     * currently first a simple unification where the only instantiable is 
     * 'all' in this expression. If this fails, then we check if there is a law
     * that says our type inclusion is a theorem.
     *
     * @param type
     * @return
     */
	public boolean typeCheck(Expression type, List<Expression> context) {
		// hack to get the variable 'all' to be instantiable
		Expression all = ExpressionUtils.ALL;
		String s = "∀" + "⟨" + all + ":" + all + "→" + this.toString() + "⟩";
		Expression thisType = ExpressionUtils.parse(s);
		boolean included = thisType.match_with(type) != null;

		if (!included) {
			for (Expression law : context) {

				if ((law instanceof Application)	&& (((Application) law).getFunction().toString().equals(":"))) {
					assert ((Application) law).arity() == 2;

					Expression left = ExpressionUtils.parse("∀"
							+ ExpressionUtils.ALL + ":all·"
							+ ((Application) law).getArgs()[0]);
					Expression right = ExpressionUtils.parse("∀"
							+ ExpressionUtils.ALL + ":all·"
							+ ((Application) law).getArgs()[1]);

					if (left.match_with(type) != null
							&& right.match_with(this) != null) {
						included = true;
						break;
					}
				}
			}
		}

		return included;
	}
    
	/**
	 * Perform a matching (unification) of this whole expression with the whole
	 * of the input expression.
	 *
	 * We treat 'this' expression as a law, and the input expression as the one
	 * to be unified with
	 *
	 * @param x
	 *            The expression to be matched with
	 * @return A mapping of variables of 'this' expression to parts of the input
	 */
	public Substitution match_with(Expression x) {
		return this.getLawBody().unify_with(x, this.getLawVars());
	}
    
    /**
     * Unifies ‘this’ with ‘x’, using ‘vars’ as instantiatables. This method
     * returns upon successful unification a mapping of variables to the
     * expressions that they were unified with.
     *
     * @param x
     * @param vars
     * @return Unifying substitution on success, null otherwise.
     */
	public Substitution unify_with(Expression x, List<Variable> vars) {
		Substitution s = new Substitution(vars);
		
		if (s.unify(this, x)) {
			return s;
		} else {
			return null;
		}
	}

    /**
     * We allow in the grammar to have a shorthand for variable introduction
     * that groups them together (ex. ⟨x,y,z→...body...⟩). However, formally
     * variables are introduced one at a time, and this also makes
     * implementation simpler.
     *
     * This method turns a grouped variable introduction into an expression
     * that has a separate scope for each one.
     *
     * @param quant Q
     * @param vars An expression denoting a non-null left-associated bunch of
     *            Identifiers.
     * @param body
     * @return An expression of the form Q⟨x→Q⟨y→Q⟨z→...body...⟩⟩⟩.
     */
	public static Expression quant_scope(Identifier quant, Expression vars, Expression scopeId, Expression domain, Expression body) {
		assert scopeId != null;
		Expression result = body;
		Identifier id = (scopeId instanceof Variable) ? ((Variable) scopeId).getId() : (Identifier) scopeId;

		while (vars != null) {
			if (vars instanceof Identifier || vars instanceof Variable) {
				result = new Application(quant, new AbbreviatedScope(id, vars, domain, result));
				vars = null;
			} else {
				Application a = (Application) vars;

				assert a.getArgs().length == 2;
				assert a.getArgs()[1] instanceof Identifier || a.getArgs()[1] instanceof Variable;

				result = new Application(quant, new AbbreviatedScope(id, a.getArgs()[1], domain, result));

				vars = a.getArgs()[0];
			}
		}
		
		return result;
	}

    /**
     * We allow in the grammar to have a shorthand for variable introduction
     * that groups them together (ex. ⟨x,y,z→...body...⟩). However, formally
     * variables are introduced one at a time, and this also makes
     * implementation simpler.
     *
     * This method turns a grouped variable introduction into an expression
     * that has a separate scope for each one.
     *
     * @param quant Q
     * @param vars An expression denoting a non-null left-associated bunch of
     *            Identifiers.
     * @param body
     * @return An expression of the form ⟨x→⟨y→⟨z→...body...⟩⟩⟩.
     */
	public static Expression multi_scope(Expression vars, Expression scopeId, Expression domain, Expression body) {
		assert scopeId != null;
		Expression result = body;
		Identifier id = (scopeId instanceof Variable) ? ((Variable) scopeId).getId() : (Identifier) scopeId;
		
		while (vars != null) {			
			if (vars instanceof Identifier || vars instanceof Variable) {
				result = new AbbreviatedScope(id, vars, domain, result);
				vars = null;
			} else {
				Application a = (Application) vars;
				
				assert a.getArgs().length == 2;
				assert a.getArgs()[1] instanceof Identifier || a.getArgs()[1] instanceof Variable;
				
				result = new AbbreviatedScope(id, a.getArgs()[1], domain, result);
				vars = a.getArgs()[0];
			}
		}
		
		return result;
	}

	/**
	 * Get the expression at at the given path. If no expression exists, return
	 * 'null'. If an empty path is given, it means that this expression will be
	 * returned.
	 *
	 * The path is a stack of integers which contain the indexes we must follow
	 * from the root to the expression we want. The bottom of the stack contains
	 * the first index we must follow from the root, and the top of the stack
	 * would contain the last.
	 *
	 * Commented out below is a more efficient, but more convoluted
	 * implementation.
	 *
	 * Postcondition: The given stack will be empty (so pass in clones if you
	 * care about the stack afterwards).
	 *
	 * @param p
	 *            The path at which to find and return an expression
	 * @return The expression at the given path
	 */
	public Expression at(Path p) {
		if (p.isEmpty()) {
			return this;
		}

		Expression child = getChild(p.get(0));
		if (child == null) {
			return null;
		}

		p.remove(0);
		return child.at(p);
	}

	/**
	 * Returns whether two expressions are the same. This is true if they have
	 * identical string representations
	 *
	 * @param expn
	 * @return
	 */
	public boolean equals(Expression expn) {
		if (expn != null) {
			return this.toString().equals(expn.toString());
		} else {
			return false;
		}
	}
    
	public boolean equalsMult(Expression expn) {
		return (this.equals(expn) || expn != null && ((this.toString().equals("∧") || this.toString().equals("×")) && (expn.toString().equals("∧") || expn.toString().equals("×"))));
	}

	/**
	 * Replaces the sub-expression at the given path with the given replacement
	 * expression. This does NOT mutate THIS expression.
	 *
	 * @param path
	 * @param replacement
	 * @return
	 */
	public Expression replace(Path path, Expression replacement) {
		Expression clone = this.clone();
		
		if (!path.isEmpty()) {
			Path pathClone = path.clone();
			int lastIndex = pathClone.pop();
			clone.at(pathClone.clone()).setChild(lastIndex, replacement);
			
			return clone;
		} else {
			// we assume that empty replacement path means do nothing.
			// we clone just to be safe
			return this.clone();
		}
	}

	/**
	 * Return the direction (left to right) of this expression. If there is no
	 * direction at all (say in an expression such as "⊤" or "a∨¬a"), return
	 * null.
	 *
	 * @return Direction of this expression
	 */
	public Direction getDirection() {
		Expression body = this.getLawBody();
		
		if (body instanceof Application) {
			Identifier fun = ((Application) body).getFunId();
			if (fun.is_operator()) {
				// a hack to check if the operator is a direction: if a reverse
				// direction exists, then it is itself a direction
				if (Symbol.reverseConnectives.get(fun) != null) {
					return new ExpnDirection(fun);
				}
			}
		}
		
		return null;
	}

	/**
	 * Under the assumption that this expression is a law, returns the body. The
	 * assumption is that an variables introduced by this expression can be
	 * instantiated during unification, and hence the scopes that introduce them
	 * are not part of the body.
	 *
	 * @return The body if this expression if it treated as a law
	 */
	public Expression getLawBody() {
		Expression body = this;
		
		while (body.is_uni_quant_scope()) {
			body = ((Scope) ((Application) body).getArgs()[0]).getBody();
		}
		
		return body;
	}
    
    /**
	 * Opposite to the 'getLawBody()' method, this returns the variables that
	 * are introduced by this expression. However, they also have to be
	 * universally quantified in a law to be considered as instantiables. This
	 * is because a law is true for all values that it is used with, and the
	 * notation we use at the back of the book is really a short-hand.
	 *
	 * @return The variables introduced by this expression
	 */
	public List<Variable> getLawVars() {
		List<Variable> vars = new ArrayList<Variable>();
		Expression body = this;
		
		while (body.is_uni_quant_scope()) {
			Scope s = ((Scope) ((Application) body).getArgs()[0]);
			vars.add(s.getDummy());
			body = s.getBody();
		}
		
		return vars;
	}

	/**
	 * If this expression is a negation, propagate the negation throughout the
	 * expression tree.
	 *
	 * Note: Returned expressions are not flat. Re-flatten afterwards if you
	 * wish them to be so.
	 *
	 * @return
	 * @throws Exception
	 */
	public Expression deepNegate() {
		Expression copy = this.clone();
		Expression negated = null;

		for (Expression n : Symbol.negations) {
			Expression negation = n.clone();
			Expression body = negation.getLawBody();
			Expression leftSide = ((Application) body).getChild(0);
			Expression rightSide = ((Application) body).getChild(1);

			Substitution sub = leftSide.unify_with(copy, negation.getLawVars());
			if (sub != null) {
				negated = rightSide.instantiate(sub);

				// do deep negation if needed
				if (negated instanceof Application) {
					Expression fun = ((Application) negated).getFunction();
					
					if ((fun.toString().equals("∧")) || (fun.toString().equals("∨"))) {
						Expression[] args = ((Application) negated).getArgs();
						
						for (int i = 0; i < args.length; i++) {
							if (args[i].isNegation()) {
								args[i] = args[i].getChild(0).deepNegate();
							}
						}
					}
				}
				
				break;
			}
		}
		
		assert negated != null;
		return negated;
	}

	/**
	 * Returns whether this expression is a boolean negation at top level
	 * 
	 * @return Is this expression a negation
	 */
	public boolean isNegation() {
		return (this instanceof Application) && ((Application) this).getFunction().toString().equals("¬");
	}
    
	public List<Variable> getAllVars() {
		Set<Variable> varSet = getAllVarsHelper().keySet();

		// convert set to list
		List<Variable> vars = new ArrayList<Variable>(varSet.size());
		
		for (Variable v : varSet) {
			vars.add(v);
		}
		
		return vars;
	}

	/**
	 * Just like 'toBasicParts()', except it makes all parts returned strings
	 * except for the input list of variables.
	 *
	 * @param inst
	 *            The variables we want to keep of type Expression
	 * @return
	 */
	public List<Object> toBasicParts(List<Variable> inst) {
		List<Object> basicParts = new ArrayList<Object>();
		boolean allString = false;
		
		for (Object part : this.toParts()) {
			basicParts.add(part);
		}

		while (!allString) {
			allString = true;
			List<Object> updatedParts = new ArrayList<Object>();
			
			for (Object part : basicParts) {
				if ((part instanceof Expression) && (!ExpressionUtils.containsObject(inst, part))) {
					allString = false;
					
					for (Object subPart : ((Expression) part).toParts()) {
						updatedParts.add(subPart);
					}
				} else if (!part.equals(Identifier.emptyKeyword)) {
					updatedParts.add(part);
				}
			}
			basicParts = updatedParts;
		}

		return basicParts;
	}

	// Get an ASCII string representation of the expression
	public String toAsciiString() {
		String result = toString();
		
		for (String ascii : Symbol.symbolMap.keySet()) {
			String unicode = Symbol.symbolMap.get(ascii);
			result = result.replace(unicode, ascii);
		}
		
		return result;
	}
     
	public Map<Interval, Path> allIntervals() {
		Map<Interval, Path> intervals = new HashMap<Interval, Path>();
		List<ZoomInfo> l = new ArrayList<ZoomInfo>();
		this.allIntervalsHelper(0, new Path(), l);
		int i = 0;

		while (i < l.size()) {
			ZoomInfo zi = l.get(i);
			
			if (zi.getStart() != zi.getEnd()) {
				Interval inter = new Interval(zi.getStart(), zi.getEnd());
				Path path = zi.getPath();

				while (i + 1 < l.size() && l.get(i + 1).getPath().equals(path)) {
					inter.setEnd(l.get(i + 1).getEnd());
					i++;
				}

				intervals.put(inter, path);
			}
			i++;
		}

		return intervals;
	}
    
	private void allIntervalsHelper(int off, Path path, List<ZoomInfo> intervalsSoFar) {
		int childCount = 0;
		
		for (Object part : this.toParts()) {
			if (part instanceof Expression) {
				Path sub = path.clone();
				sub.push(childCount);

				((Expression) part).allIntervalsHelper(off, sub, intervalsSoFar);
				// update off with the end index of the last one
				childCount++;
			} else {
				int lastEnd = intervalsSoFar.isEmpty() ? -1 : intervalsSoFar.get(intervalsSoFar.size() - 1).getEnd();

				if (lastEnd >= 0) {
					off = intervalsSoFar.get(intervalsSoFar.size() - 1).getEnd();
				}
				
				intervalsSoFar.add(new ZoomInfo(off, off + ((String) part).length(), path));
				assert part instanceof String;
				off = intervalsSoFar.get(intervalsSoFar.size() - 1).getEnd();
			}
		}
	}
    
    /**
	 * Returns all expressions we would have in context if we zoomed into child
	 * i of this expression. Note that context is returned for one zoom level
	 * only. To get context at a deeper level, simply collect context at each
	 * level as you zoom.
	 *
	 * Context collection is done for '∨','∧','×','⇒', 'if then else' and scopes
	 *
	 * @param i
	 *            The sub-expression index to which we are zooming and gaining
	 *            context at
	 * @return A list of the context expressions gained
	 */
	public List<Expression> basicContext(int i) {
		// ⟨P:T→P⟩
		List<Expression> context = new ArrayList<Expression>();

		if ((this instanceof Application) && (getChild(i) != null)) {
			Application flat = (Application) this.clone();
			assert flat != null;
			flat.flatten();
			Expression fun = flat.getFunction();

			// conjunction (incl. bool multiplication) and disjunction
			if ((fun.toString().equals("∨")) || fun.toString().equals("∧")
					|| fun.toString().equals("×")) {
				String op, op2;
				boolean neg;
				boolean mult = false;
				if (fun.toString().equals("∨")) {
					op = "∨";
					op2 = "∨";
					neg = true;
				} else {
					op = "∧";
					op2 = "×";
					neg = false;
				}
				if (fun.toString().equals("×")) {
					mult = true;
				}

				Expression noContext = this.getChild(i).clone();
				noContext.flatten();
				int start = 0;
				int end = -1;

				for (int k = 0; k < i; k++) {
					Expression e = this.getChild(k).clone();
					e.flatten();
					if ((e instanceof Application)
							&& ((((Application) e).getFunction().toString()
									.equals(op)) || (((Application) e)
									.getFunction().toString().equals(op2)))) {
						start += ((Application) e).arity();
					} else {
						start++;
					}
				}

				if ((noContext instanceof Application)
						&& ((((Application) noContext).getFunction().toString()
								.equals(op)) || (((Application) noContext)
								.getFunction().toString().equals(op2)))) {
					end = start + ((Application) noContext).arity() - 1;
				} else {
					end = start;
				}

				for (int j = 0; j < flat.arity(); j++) {
					if ((j < start) || (j > end)) {
						flat.getChild(j).makeTypes(new ArrayList<Expression>());
						if (neg) {
							context.add(flat.getChild(j).addNegation());
						} else if (!mult
								|| flat.getChild(j).type != null
								&& (flat.getChild(j).type.typeCheck(
										ExpressionUtils.parse("bool"),
										new ArrayList<Expression>()))
								|| ((flat.getChild(j)) instanceof Application && (((Application) (flat
										.getChild(j))).getFunction().toString().equals(
										"=")
										|| ((Application) (flat.getChild(j))).getFunction()
												.toString().equals("<")
										|| ((Application) (flat.getChild(j))).getFunction()
												.toString().equals(">")
										|| ((Application) (flat.getChild(j))).getFunction()
												.toString().equals("≤")
										|| ((Application) (flat.getChild(j))).getFunction()
												.toString().equals("≥") || ((Application) (flat
											.getChild(j))).getFunction().toString()
										.equals("∨")))) {
							context.add(flat.getChild(j));
						}
					}
				}

				// implication
			} else if (fun.toString().equals("⇒")) {
				if (i == 0) {
					context.add(flat.getChild(1).addNegation());
				} else {
					assert i == 1;
					context.add(flat.getChild(0));
				}

			} else if (fun.toString().equals("⇐")) {
				if (i == 0) {
					context.add(flat.getChild(1));
				} else {
					assert i == 1;
					context.add(flat.getChild(0).addNegation());
				}
				// 'if then else'
			} else if ((fun instanceof Variable)
					&& (((Variable) fun).getId() == Identifiers.ifthenelse)) {
				if (i == 0) {
					Expression contextExpn;
					try {
						contextExpn = ExpressionUtils.parse("(" + getChild(1)
								+ ")" + "≠" + "(" + getChild(2) + ")");
						contextExpn.makeTypes(new ArrayList<Expression>());
						context.add(contextExpn);
					} catch (Exception e) {
						System.err.println("Parse error during basicContext");
					}
				} else if (i == 1) {
					context.add(flat.getChild(0));
				} else {
					assert i == 2;
					context.add(flat.getChild(0).addNegation());
				}
			}

			// scope domain context x:D
		} else if ((this instanceof Scope)
				&& (getChild(i) == ((Scope) this).getBody())) {

			if (((Scope) this).domain != null) {
				boolean islet = false;
				if (this instanceof AbbreviatedScope) {
					Identifier id = ((AbbreviatedScope) this).getId();
					if (id == Identifiers.let || id == Identifiers.letp) {
						islet = true;
						Expression varEqualsDomain;
						Expression dom = ((Scope) this).getDomain();
						varEqualsDomain = ExpressionUtils.parse(((Scope) this)
								.getDummy() + "=" + "(" + dom + ")");
						context.add(varEqualsDomain);
						if (dom.getType() != null) {
							context.add(ExpressionUtils.parse(((Scope) this)
									.getDummy()
									+ ":"
									+ "("
									+ dom.getType()
									+ ")"));
						}
						return context;
					} else if (id == Identifiers.var
							|| id == Identifiers.result) {
						Expression varPrimeInDomain;
						// error TODO do not add type law if domain contains the
						// dummy
						varPrimeInDomain = ExpressionUtils.parse(((Scope) this)
								.getDummy()
								+ ExpressionUtils.prime
								+ ":"
								+ ((Scope) this).getDomain());
						context.add(varPrimeInDomain);
					}
				}
				// error TODO do not add type law if domain contains the dummy
				Expression varInDomain;
				varInDomain = ExpressionUtils.parse(((Scope) this).getDummy()
						+ ":"
						+ "("
						+ (islet ? ((Scope) this).getDomain().type
								: ((Scope) this).getDomain()) + ")");
				context.add(varInDomain);

			}
		}
		return context;
	}
	
	/**
	 * Adds a negation to the given expression. Used when creating context
	 * 
	 * @return This expression, but negated
	 */
	private Expression addNegation() {
		Expression neg = null;
		try {
			neg = Main.parse("¬(" + toString() + ")", false).get(0);
			neg.makeTypes(new ArrayList<Expression>());
		} catch (Exception e) {
			System.err.println("Parse error during addNegation");
		}
		return neg;
	}
}
