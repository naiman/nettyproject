package prooftool.backend;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import prooftool.backend.laws.AbbreviatedScope;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Identifier;
import prooftool.backend.laws.Literal;
import prooftool.backend.laws.Scope;
import prooftool.backend.laws.Variable;
import prooftool.util.Identifiers;

/** 
 * This is a type alias for code clarity.
 * It represents the substitution that's result of a unification.
 * @author robert
 *
 */
public class Substitution {	

	private Map<Variable, Expression> subst;
	private Map<Variable, Variable> scopeSubst;
	private Map<Variable, Scope> functionized;

	/** we're tempted to let the constructor do the unification, 
	  * but that doesn't allow the recursion we need.
	  * likewise we'd have the abstract factory (Expression.unify)
	  * contain the algorithm, but then it would have to pass the substitution
	  * and the variables in every recursive call which also sucks.
	  * so, while this design is not totally straight-forward, 
	  * it turns out to be totally neat.
	  **/
	public Substitution(List<Variable> vars, Map<Variable, Expression> subst, Map<Variable, Variable> scopeSubst, Map<Variable, Scope> functionzied) {
		this.subst = subst;		
		this.scopeSubst = scopeSubst;
		this.functionized = functionzied;
		
		for (Variable v : vars) {
			this.subst.put(v, null);
		}
	}
	
	public Substitution(List<Variable> vars) {
		this.subst = new IdentityHashMap<Variable, Expression>();		
		this.scopeSubst = new IdentityHashMap<Variable, Variable>();
		this.functionized = new IdentityHashMap<Variable, Scope>();
		
		for (Variable v : vars) {
			this.subst.put(v, null);
		}
	}
	
	public Map<Variable, Expression> getSubst() {
		return subst;
	}
	
	public Map<Variable, Variable> getScopeSubst() {
		return scopeSubst;
	}
	
	public Scope getFunctionized(Variable x) {
		return functionized.get(x);
	}
	
	public Expression get(Variable x) {
		return subst.get(x);
	}
	
	public boolean unify(Expression law, Expression focus) {
		law.makeTypes(new ArrayList<Expression>());
		return unify_helper(law,focus);
	}	
	
	private boolean unify_helper(Expression law, Expression focus) {
		if (focus == law) { //hash.get(x)	
			return true;
		}
		
		if (law instanceof Application) { 
			if (localVarFunction((Application)law)) {			
				return uni_local((Application) law,focus);
			}
			
			if ( focus instanceof Application) {
				return uni_app((Application) law, (Application) focus);
			} 
			
		}		
		
		if (law instanceof Scope && focus instanceof Scope) {
			return uni_scop((Scope) law, (Scope) focus);
		}
		
		if (law instanceof Literal && focus instanceof Literal) {
			return uni_lit((Literal) law, (Literal) focus);
		}		
		
		if (law instanceof Variable) {
			if ( uni_var((Variable) law, focus) ) {
				return true;
			}
		}
		
		return false;
	}

	private boolean uni_local(Application law, Expression focus) {	
		Variable f = (Variable) ((Application)law).getArgs()[0];
		Variable localLawVar = (Variable) ((Application)law).getArgs()[1];
		Variable localVar = scopeSubst.get(localLawVar);
		
		if (subst.get(f)!=null) { // if f is already unified with something
			// then if that something is not a scope, or a scope that is not the same as x, we fail 
			if (!(subst.get(f) instanceof Scope) || !((Scope)subst.get(f)).getBody().equals(focus)) {
				return false;
			}
		}		
		Scope f_uni = this.functionize(localVar, localVar.getType(), focus);
		subst.put(f,f_uni);
		functionized.put(f,f_uni); 
			
		return true;
	}
	
	/**
	 * Create a scope whose body is 'body' and domain is 'dom' with variable 'var'
	 * parameterized (it would be replaced in the body with a new variable of the same
	 * identifier).
	 * 
	 * @param var
	 * @param dom
	 * @param body
	 * @return
	 */
	private Scope functionize(Variable var, Expression dom, Expression body) {
		Map<Variable,Expression> m = new IdentityHashMap<Variable,Expression>();
		Variable varClone = (Variable)var.clone();
		m.put(var, varClone);
		body.instantiate(m);
		Scope s = new AbbreviatedScope(Identifiers.scope, varClone, dom, body);
		
		return s;
	}
	
	private boolean uni_lit(Literal law, Literal focus) {
		return law.getValue().equals(focus.getValue());
	}

	private boolean uni_var(Variable law, Expression focus)
	{
		if (subst.containsKey(law))  // is x instantiatable?
		{
			if ((subst.get(law) != null) && (subst.get(law).equals(focus))) {				
				return true;
			}
			
			if (subst.get(law) == null) {
				if ( focus.contains(law) ) {	// check for occurrence 
					return false;
				}

				subst.put(law, focus);
				return true;
			}
		} else {
			//if both are non-instantiable, check if they are the same
			assert !subst.containsKey(focus);
			
			if (!subst.containsKey(focus)) {  
				return law.equals(focus);
			}
		}
		return false;
	}

	private boolean uni_scop(Scope law, Scope focus) {
		boolean idOk = false;
		
		if ((law.getDomain() == null) != (focus.getDomain() == null)) {
			return false;
		}
		
		if (law.getDomain() != null && !unify_helper(law.getDomain(), focus.getDomain())) {
			return false;
		}	
		
		if ((law instanceof AbbreviatedScope)&&(focus instanceof AbbreviatedScope)) {
			if (((AbbreviatedScope)law).getId() != ((AbbreviatedScope)focus).getId()) {
				return false; // check if the type of scope is the same
			}
			
			idOk = true;
			Identifier id = ((AbbreviatedScope)law).getId(); //in the case of hidden variables, add them too
			
			if (id == Identifiers.var || id == Identifiers.result) {
				subst.put(((AbbreviatedScope)law).getDummyPrime(), ((AbbreviatedScope)focus).getDummyPrime());
				scopeSubst.put(((AbbreviatedScope)law).getDummyPrime(), ((AbbreviatedScope)focus).getDummyPrime());
			}
		}
		
		//MISSING dummy prime for var
		subst.put(law.getDummy(), focus.getDummy());
		scopeSubst.put(law.getDummy(), focus.getDummy());
		boolean result = unify_helper(law.getBody(), focus.getBody());
		subst.remove(law.getDummy());
		
		if (idOk) {
			subst.remove(((AbbreviatedScope)law).getDummyPrime());
		}
		
		return result;
	}

	private boolean uni_app(Application law, Application focus)	{
		if (law.arity() != focus.arity()) {
			return false;
		}
		
		assert law.getFunction() instanceof Variable && focus.getFunction() instanceof Variable;
		if (!unify_helper(law.getFunction(), focus.getFunction())) {
			return false;
		}
		
		for (int i = 0; i < law.arity(); i++) {
			if (!unify_helper(law.getArgs()[i], focus.getArgs()[i])) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Is app a function application of an instantiable to a local var?
	 * @param law
	 * @return
	 */
	private boolean localVarFunction(Application law) {
		return law.is_fun_application() && subst.containsKey(law.getArgs()[0]) && scopeSubst.containsKey(law.getArgs()[1]);
	}
}
