package prooftool.backend.ccode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import prooftool.backend.ParameterizedExpression;
import prooftool.backend.ParameterizedRefinement;
import prooftool.backend.Substitution;
import prooftool.backend.laws.AbbreviatedScope;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Identifier;
import prooftool.backend.laws.Literal;
import prooftool.backend.laws.Scope;
import prooftool.backend.laws.Variable;
import prooftool.backend.refinement.RefinementProvider;
import prooftool.util.Identifiers;
import prooftool.util.ListUtils;
import prooftool.util.Symbol;

// Examples for copy/paste:
//   var x : int · x >= 0 --> x′ = 0
//   var a : bool · a := a \/ a
//   if x = 0 then j else x := x - 1 . x > 0 --> x′ = 0 end
//
//  ( var x:int·if (x=2∧x>3∨x≤5) then ((x:=2.x:=3).x:=4) else (if (x>0) then (x:=1) else (x:=9) end) end ) [Zoom In]
// ( if (x=2∧x>3∨x≤5) then ((x:=2.x:=3).x:=4) else (if (x>0) then (x:=1) else (x:=9) end) end ) [No justification]
//
// var x : int · (((x > 0 --> (x := 0)) <-- (if x = 0 then ok else x := x - 1 . x > 0 --> (x := 0) end)) /\ (if x = 0 then ok else x := x - 1 . x > 0 --> (x := 0) end))
// var x : int ·  (if x = 0 then ok else x := x - 1 . x > 0 --> (x := 0) end)
// var x : int ·  ((x > 0 --> (x := 0)) <-- (if x = 0 then ok else x := x - 1 . x > 0 --> (x := 0) end))
// var x : int · (if x = 0 then ok else x := x - 1 . x > 0 --> (x := 0) end)
// var x : int · (if x = 0 then ok else x := x - 1 . x > 0 --> (x := 0) end)
// var x : int · let double = ⟨ z : int · z + z ⟩ · double x
// var x : int · let double = <<z:int->2>>· x := double x
// var x : int · x := <<z:int->z+1>> 2
// let f = << x : int -> << y : int -> x + y >> >> · var x : int · x := f 1 2
// var x : int · x := 3 . let f = ⟨ a : int → a + x ⟩ · x := x + 1 . let y = f 2 · ok    [needs some parenthesizing]
// var x : int · (x := 0 . (let y = x · (var x : int · (x := 1 . (var z : bool · (z := (y = 0)))))))
// var n : int · var L : [*int] · if n = #L then ok else ok end
// Chapter 4 examples:
//   list summation:
//     var s : int · var n : int · var L : [*int] · ((s′ = s + (∑L[n;..#L])) <-- (if n = #L then ok else (s := s + L n . n := n + 1 . s′ = s + (∑L[n;..#L])) end)) /\ (L := [1;2;3;4] . s := 0 . n := 0 . s′ = s + (∑L[n;..#L]) . println s)
//     var L : [*int] · L := [1;2;3;4] . (var s : int · s′ = (∑L) . println s)
//      var n : int · s := 0 . n := 0 . s′ = s + (∑L[n;..#L])
//      (if n = #L then ok else (s := s + L n . n := n + 1 . s′ = s + (∑L[n;..#L])) end)
//   linear search:
//     var x : int · var h : int · var L : [*int] · (R <-- (if h = #L then ok else if L h = x then ok else (h := h + 1 . R) end end)) /\ (L := [1;2;3;4] . x := 2 . h := 0 . R)
//   binary search:
//     var x : int · var L : [*int] · var h : int · var p : bool · var i : int · var j : int · (R <-- (if j - h = 1 then p := L h = x else (i := (div (h + j) 2) . (if L i <= x then h := i else j := i end) . R) end)) /\ (L := [1;2;3;4] . x := 2 . h := 0 . j := #L . R . println h . println p)
// Channels:
//   chan c : int · c! 2 . c? . println c
//   println (stdin = "q")
//   var x : int · x := 0 . P
//     P refined by (stdin?) . if stdin = "q" then ok else (println x . x := x + 1 . P) end
//   chan c : [*int] · println(✓c) . c! [1;2;3] . println(✓c) . c! [4;5;6] . c? . c? . (var e : [*int] · e := c . println (e 2) . println(✓c) . (c !) . println(✓c) . c? . println(✓c))
//   chan c : [*int] · println(✓c) . c! [1;2;3] . println(✓c) . c! [4;5;6] . c? . c? . println(c 2) . println(✓c) . (c !) . println(✓c) . c? . println(✓c)
//   (c ! 2) . (c !)
// Procedure:
//   var x : int · << y : int >-> x := y >> 5 . println x
//   << y : int >-> println y >> 5
//   var x : int · x := 0 . (var f : int >-> bool · f := << y : int ↣ println x >> . x := x + 1 . f 42)
//   var x : int · x := 0 . (letp f = ⟨ y : int ↣ x := x + 1 ⟩ · x := x + 1 . f 42 . println x)
//   var x : int · x := 0 . (letp f = (x := x + 1) · x := x + 1 . f . println x)
//   var x : int · x := 0 . (var f : int >-> bool · f := ⟨ y : int ↣ x := x + 1 ⟩ . x := x + 1 . f 42 . println x)
//   var x : int · x := 0 . (letp incrementX = (x := x + 1) · println x . incrementX . println x)
// Let:
//   var x : int · x := 0 . (let xPlusOne = x + 1 · println xPlusOne . x := x + 1 . println xPlusOne)
// Result:
//   result x : int · x := 0
//   var y : int · y := 0 . (var x : int · x := (result r : int · (y := y + 1 . r := y)) . println x . println y)

// var proc := (result r : int ↣ bool · var x : int := 0 . r := ⟨ unused : int ↣ x := x + 1 ⟩) . proc null
// Functions:
//   let mod = ⟨x:nat →⟨y:nat →(x - ((div x y) × y)) ⟩⟩ · let divides = ⟨x:nat →⟨y:nat →(mod y x) = 0 ⟩⟩ · let even = divides 2 · println (even 42)
//   var mod : nat → nat → nat · mod := ⟨x:nat →⟨y:nat →(x - ((div x y) × y)) ⟩⟩ . (var divides : nat → nat → nat · divides := ⟨x:nat →⟨y:nat →(mod y x) = 0 ⟩⟩ . (var even : nat → nat · even := divides 2 . println (even 42)))
// Fibonacci
//   var x : int · var y : int · var n : int · P
//     P refined by if n = 0 then (x := 0 . y := 1) else (n := n - 1 . P . n := x . x := y . y := n + y) end
// Letp:
//   var L : [*int] · var i : int · var item : int · letp Q=(i′:0,..#L∧L i′=item) ∨ i′=#L· Q
//    Q      refined by i:=0. i≤#L⇒Q
//    i≤#L⇒Q refined by if i=#L then ok else i<#L⇒Q end
//    i<#L⇒Q refined by if L i=item then ok else i:=i+1. i≤#L⇒Q  end
//  variation:
//   var L : [*int] · var i : int · var item : int · letp Q=(i′:0,..#L∧L i′=item) ∨ i′=#L· Q
//    Q      refined by i:=0. i≤#L⇒Q
//    i≤#L⇒Q refined by if i=#L ∨ L i=item then ok else i:=i+1. i≤#L⇒Q end
// parametric refinement (binary search):
// var L : [*int] · var found : bool · L := [1; 2; 3; 4] . bsearch L 0 (#L) 2 . println found
// var L : [*int] · var myfound : bool · let bsearch2 = ⟨L : [*int] → ⟨h : int → ⟨j : int → ⟨x : int → result found : bool · bsearch L h j x⟩⟩⟩⟩ · L := [1; 2; 3; 4] . myfound := bsearch2 L 0 (#L) 2 . println myfound 
public class CCodeGenerator 
{
	// PUBLIC TYPES
	
	// An exception class representing exceptions during the code generation process.
	@SuppressWarnings("serial")
	public static class Exception extends RuntimeException
	{
		Exception(String s)
		{
			super(s);
		}
	}
	
	// An exception class thrown when attempting to generate C code for an expression
	// that is not a program.
	@SuppressWarnings("serial")
	public static class NotProgramException extends Exception
	{
		public NotProgramException(String reason)
		{
			this(reason, false);
		}
		
		public NotProgramException(String reason, boolean fatal)
		{
			super("Expression is not a program: " + reason);
			this.fatal = fatal;
		}
		
		private boolean fatal;
		
		public boolean isFatal()
		{
			return fatal;
		}
	}
	
	// PUBLIC METHODS
	
	// Attempt to generate code for the given expression in the context of the given laws.
	// Throws CCodeGenerator.Exception upon failure.
	public String generateCCode(Expression focus, Map<String, Expression> sigma, List<RefinementProvider> refinementProviders)
	{
		// Some random debugging output
		
		//System.out.println("Generating C code for: " + focus.toAsciiString());
		System.out.println("Generating C code for: " + focus.getTreeRep());
		
//		System.out.println("The type of the focus expression is: ");
//		focus.makeTypes();
//		if (focus.type !=null) {
//			System.out.println(focus.type.getTreeRep());
//		}
		
		// The actual thing
		
		return prettyPrint(compile(focus, sigma, refinementProviders));
	}
	
	// HELPER FUNCTIONS
	
	// Compile a program to C code.
	private CGlobalScope compile(Expression e, Map<String, Expression> sigma, List<RefinementProvider> refinementProviders)
	{
		return new Compiler(refinementProviders).compileTopLevel(e, sigma);
	}
	
	// Pretty-print a C program
	private String prettyPrint(CGlobalScope e)
	{
		return new PrettyPrinter().prettyPrint(e);
	}
	
	// PRIVATE TYPES
	
	// The base class for all C program elements.
	private static interface CProgramElement
	{
		public abstract <T> T acceptVisitor(CProgramElementVisitor<T> visitor);
	}
	
	// A visitor class for the CProgramElement hierarchy
	private static interface CProgramElementVisitor<T>
	{
		public T visit(CGlobalScope cGlobalScope);
		public T visit(CIncludeDirective cIncludeDirective);
		public T visit(CFunction cFunction);
		public T visit(CSimpleName cSimpleName);
		public T visit(CXXQualifiedName cQualifiedName);
		public T visit(CTypeQualifier cTypeQualifier);
		public T visit(CSimpleType cSimpleType);
		public T visit(CQualifiedType cQualifiedType);
		public T visit(CXXTemplateType cxxTemplateType);
//		public T visit(CXXReferenceType cxxReferenceType);
		public T visit(CFunctionType cFunctionType);
		public T visit(CIdentifier cIdentifier);
		public T visit(CFunctionParameter cFunctionParameter);
		public T visit(CBlock cLocalScope);
		public <CElementType extends CProgramElement> T visit(CElementList<CElementType> cElementList);
		public T visit(CExpressionStatement cExpressionStatement);
		public T visit(CUnaryOperatorExpression cUnaryOperatorExpression);
		public T visit(CBinaryOperatorExpression cBinaryOperatorExpression);
		public T visit(CConditionalOperatorExpression cConditionalOperatorExpression);
		public T visit(COperator cOperator);
		public T visit(CIfElseStatement cIfElseStatement);
		public T visit(CVariableExpression cVariableExpression);
		public T visit(CLiteralExpression cLiteralExpression);
//		public T visit(CLabel cLabel);
//		public T visit(CLabelledStatement cLabelledStatement);
//		public T visit(CGotoStatement cGotoStatement);
		public T visit(CEmptyStatement cEmptyStatement);
		public T visit(CVariableDeclarationStatement cVariableDeclarationStatement);
		public T visit(CVariableInitializationStatement cVariableInitializationStatement);
		public T visit(CReturnStatement cReturnStatement);
		public T visit(CFunctionCallExpression cFunctionCallExpression);
		public T visit(CCastExpression cCastExpression);
		public T visit(CXXObjectMemberExpression cxxObjectMemberExpression);
		public T visit(CXXLambdaExpression cxxLambdaExpression);
		public T visit(CWhileLoop cWhileLoop);
	}
	
	// A C program element that represents the global scope
	private static class CGlobalScope implements CProgramElement
	{
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CElementList<CProgramElement> getElementList()
		{
			return elementList;
		}
		
		private CElementList<CProgramElement> elementList = new CElementList<CProgramElement>();
	}
	
	// A C program element that represents an include directive
	private static class CIncludeDirective implements CProgramElement
	{
		CIncludeDirective(String path)
		{
			this.path = path;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public String getPath()
		{
			return path;
		}
		
		private String path;
	}
	
	// A C program element that represents a function
	private static class CFunction implements CProgramElement
	{
		public CFunction(CIdentifier name, CType returnType, CFunctionParameter... parameters)
		{
			this.name = name;
			this.returnType = returnType;
			this.parameters = Arrays.asList(parameters);
			this.body = new CBlock();
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}

		public CIdentifier getName()
		{
			return name;
		}
		public CType getReturnType()
		{
			return returnType;
		}
		public List<CFunctionParameter> getParameters()
		{
			return parameters;
		}
		public CBlock getBody()
		{
			return body;
		}
		
		private CIdentifier name;
		private CType returnType;
		private List<CFunctionParameter> parameters;
		private CBlock body;
	}
	
	// A C program element that represents a name
	private static abstract class CName implements CProgramElement
	{
		public abstract boolean equals(CName other);
		
		@Override
		public boolean equals(Object other)
		{
			return equals((CName)other);
		}
	}
	
	// A C program element that represents a simple name (i.e. a name consisting of a single identifier)
	private static class CSimpleName extends CName
	{
		public CSimpleName(CIdentifier identifier)
		{
			this.identifier = identifier;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		@Override
		public boolean equals(CName other)
		{
			return other instanceof CSimpleName
				&& identifier.equals(((CSimpleName)other).identifier);
		}

		public CIdentifier getIdentifier()
		{
			return identifier;
		}
		
		private CIdentifier identifier;
	}
	
	// A C++ program element that represents a qualified name
	private static class CXXQualifiedName extends CName
	{
		public CXXQualifiedName(CIdentifier qualifier, CName unqualifiedName)
		{
			this.qualifier = qualifier;
			this.unqualifiedName = unqualifiedName;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		@Override
		public boolean equals(CName other)
		{
			return other instanceof CXXQualifiedName
				&& qualifier.equals(((CXXQualifiedName)other).qualifier)
				&& unqualifiedName.equals(((CXXQualifiedName)other).unqualifiedName);
		}

		CIdentifier getQualifier()
		{
			return qualifier;
		}
		
		CName getUnqualifiedName()
		{
			return unqualifiedName;
		}
		
		CIdentifier qualifier;
		CName unqualifiedName;
	}
	
	// A C program element that represents a type
	private static abstract class CType implements CProgramElement
	{
		public CType makeConst()
		{
			return new CQualifiedType(new CTypeQualifier(CTypeQualifierKind.CONST), this);
		}
		
		public abstract boolean equals(CType other);
		
		@Override
		public boolean equals(Object other)
		{
			return equals((CType)other);
		}
	}	
	
	// The type of a C type qualifier - either 'const' or 'volatile'.
	private static enum CTypeQualifierKind
	{
		CONST,
		VOLATILE
	}

	// A C program element that represents a type qualifier
	private static class CTypeQualifier implements CProgramElement
	{
		public CTypeQualifier(CTypeQualifierKind kind)
		{
			this.kind = kind;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		@Override
		public boolean equals(Object other)
		{
			return kind == ((CTypeQualifier)other).kind;
		}
		
		public CTypeQualifierKind getKind()
		{
			return kind;
		}
		
		CTypeQualifierKind kind;
	}
	
	// A C program element that represents a qualified type
	private static class CQualifiedType extends CType
	{
		public CQualifiedType(CTypeQualifier qualifier, CType unqualifiedType)
		{
			this.qualifier = qualifier;
			this.unqualifiedType = unqualifiedType;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		@Override
		public boolean equals(CType other)
		{
			return other instanceof CQualifiedType
				&& qualifier.equals(((CQualifiedType)other).qualifier)
				&& unqualifiedType.equals(((CQualifiedType)other).unqualifiedType);
		}

		public CTypeQualifier getQualifier()
		{
			return qualifier;
		}
		
		public CType getUnqualifiedType()
		{
			return unqualifiedType;
		}
		
		private CTypeQualifier qualifier;
		private CType unqualifiedType;
	}
	
	// A C program element that represents a simple type (i.e. a type consisting of a name only)
	private static class CSimpleType extends CType
	{
		public CSimpleType(CName name)
		{
			this.name = name;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		@Override
		public boolean equals(CType other)
		{
			return other instanceof CSimpleType
				&& name.equals(((CSimpleType)other).name);
		}
		
		public CName getName()
		{
			return name;
		}
		
		private CName name;
	}
	
	// A C++ program element that represents a template type
	private static class CXXTemplateType extends CType
	{
		public CXXTemplateType(CName templateName, CType... templateArguments)
		{
			this.templateName = templateName;
			this.templateArguments = Arrays.asList(templateArguments);
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		@Override
		public boolean equals(CType other)
		{
			return other instanceof CXXTemplateType
				&& templateName.equals(((CXXTemplateType)other).templateName)
				&& templateArguments.equals(((CXXTemplateType)other).templateArguments);
		}
		
		public CName getTemplateName()
		{
			return templateName;
		}
		
		public List<CType> getTemplateArguments()
		{
			return templateArguments;
		}
		
		private CName templateName;
		private List<CType> templateArguments;
	}
	
	// A C++ program element that represents a reference type
//	private static class CXXReferenceType extends CType
//	{
//		public CXXReferenceType(CType referredType)
//		{
//			this.referredType = referredType;
//		}
//		
//		@Override
//		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
//		{
//			return visitor.visit(this);
//		}
//
//		@Override
//		public boolean equals(CType other)
//		{
//			return other instanceof CXXReferenceType
//				&& referredType.equals(((CXXReferenceType)other).referredType);
//		}
//
//		public CType getReferredType()
//		{
//			return referredType;
//		}
//		
//		private CType referredType;
//	}
	
	// A C program element that represents a function type
	private static class CFunctionType extends CType
	{
		public CFunctionType(CType returnType, CType... argumentTypes)
		{
			this(returnType, Arrays.asList(argumentTypes));
		}
		public CFunctionType(CType returnType, List<CType> argumentTypes)
		{
			this.returnType = returnType;
			this.argumentTypes = argumentTypes;
		}

		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}

		@Override
		public boolean equals(CType other)
		{
			return other instanceof CFunctionType
				&& returnType.equals(((CFunctionType)other).returnType)
				&& argumentTypes.equals(((CFunctionType)other).argumentTypes);
		}
		
		public CType getReturnType()
		{
			return returnType;
		}
		
		public List<CType> getArgumentTypes()
		{
			return argumentTypes;
		}
		
		private CType returnType;
		private List<CType> argumentTypes;
	}
	
	// Built-in C types used during compilation
	private static class CBuiltinTypes
	{
		public static final CType int_ = new CSimpleType(new CSimpleName(new CIdentifier("int")));
		public static final CType double_ = new CSimpleType(new CSimpleName(new CIdentifier("double")));
		public static final CType bool = new CSimpleType(new CSimpleName(new CIdentifier("bool")));
		public static final CType void_ = new CSimpleType(new CSimpleName(new CIdentifier("void")));
	}
	
	// Built-in C++ types used during compilation
	private static class CXXBuiltinTypes
	{
		public static final CType auto = new CSimpleType(new CSimpleName(new CIdentifier("auto")));
	}
	
	// Names from the standard library
	private static class StandardNames
	{
		public static final String functional = "functional";
		private static final CIdentifier stdNamespace = new CIdentifier("std");
		public static final CName function = new CXXQualifiedName(stdNamespace, new CSimpleName(new CIdentifier("function")));
	}
	
	// Names defined in netty.hpp
	private static class NettyNames
	{
		public static final String nettyHpp = "netty.hpp";
		private static final CIdentifier nettyNamespace = new CIdentifier("netty");
		
		// built-in types that correspond to Netty types
		public static final CName string = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("string")));
		public static final CName list = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("list")));
		public static final CName channel = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("channel")));
		
		// built-in functions that correspond to Netty operators 
		public static final CName make_string = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("make_string")));
		public static final CName make_list = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("make_list")));
		
		// built-in variables that correspond to Netty literals
		public static final CName nil = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("nil")));
		
		// built-in functions that can be used from Netty code
		public static final CName div = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("div")));
		public static final CName print = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("print")));
		public static final CName println = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("println")));
		
		// built-in channels
		public static final CName stdin = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("stdin")));
		public static final CName stdout = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("stdout")));
		public static final CName stderr = new CXXQualifiedName(nettyNamespace, new CSimpleName(new CIdentifier("stderr")));
		
		// string/list member function names
		public static final CIdentifier length = new CIdentifier("length");
		public static final CIdentifier replace = new CIdentifier("replace");
		public static final CIdentifier contents = new CIdentifier("contents");
		public static final CIdentifier subscript = new CIdentifier("subscript");
		//public static final CIdentifier at = new CIdentifier("at");
		
		// channel member function names
		public static final CIdentifier get = new CIdentifier("get");
		public static final CIdentifier input = new CIdentifier("input");
		public static final CIdentifier signal = new CIdentifier("signal");
		public static final CIdentifier output = new CIdentifier("output");
		public static final CIdentifier check = new CIdentifier("check");
	}
	
	// C operators used during compilation
	private static class COperators
	{
		// unary
		public static final COperator negate = new COperator("-");
		public static final COperator not = new COperator("!");
		
		// binary
		public static final COperator and = new COperator("&&");
		public static final COperator or = new COperator("||");
		public static final COperator equal = new COperator("==");
		public static final COperator notEqual = new COperator("!=");
		public static final COperator plus = new COperator("+");
		public static final COperator minus = new COperator("-");
		public static final COperator multiplies = new COperator("*");
		public static final COperator divides = new COperator("/");
		public static final COperator lessEq = new COperator("<=");
		public static final COperator less = new COperator("<");
		public static final COperator greaterEq = new COperator(">=");
		public static final COperator greater = new COperator(">");
		
		public static final COperator assign = new COperator("=");
	}
	
	// A C program element that represents an identifier
	private static class CIdentifier implements CProgramElement
	{
		public CIdentifier(String name)
		{
			this.name = name;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		@Override
		public boolean equals(Object other)
		{
			return name.equals(((CIdentifier)other).name);
		}
		
		public String getName()
		{
			return name;
		}
		
		private String name;
	}
	
	
	// A C program element that represents a function parameter
	private static class CFunctionParameter implements CProgramElement
	{
		public CFunctionParameter(CType type, CIdentifier name)
		{
			this.type = type;
			this.name = name;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CType getType()
		{
			return type;
		}
		
		public CIdentifier getName()
		{
			return name;
		}
		
		private CType type;
		private CIdentifier name;
	}
	
	// A C program element that represents a brace-enclosed block
	private static class CBlock implements CProgramElement
	{
		public CBlock(CStatement... statements)
		{
			this.statementList = new CStatementList(statements);
		}
		public CBlock(CStatementList statements)
		{
			this.statementList = statements;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CStatementList getStatementList()
		{
			return statementList;
		}
		
		private CStatementList statementList;
	}
	
	// A C program element that represents a list of program elements
	private static class CElementList<CElementType extends CProgramElement> implements CProgramElement
	{
		public CElementList(CElementType... elements)
		{
			this.elements.addAll(Arrays.asList(elements));
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public List<CElementType> getElements()
		{
			return elements;
		}
		public void addElement(CElementType element)
		{
			elements.add(element);
		}
		public void addElements(CElementList<CElementType> elements)
		{
			this.elements.addAll(elements.getElements());
		}
		
		protected List<CElementType> elements = new ArrayList<CElementType>();
	}
	
	private static class CStatementList extends CElementList<CStatement>
	{
		public CStatementList(CStatement... statements)
		{
			super(statements);
		}
		
		// Note: need (first, rest) rest to disambiguate no-arg constructor
		public CStatementList(CStatementList first, CStatementList... rest)
		{
			addStatements(first);
			for (CStatementList statementList : rest)
				addStatements(statementList);
		}

		// Label the first statement in the statement list with the given label.
		// If there are no labels, add an empty statement (a label in C cannot 
		// stand on its own, it must be a prefix of a statement).
//		public void labelWith(CLabel label)
//		{
//			if (elements.isEmpty())
//				addStatement(new CEmptyStatement());
//			elements.set(0, new CLabelledStatement(label, elements.get(0)));
//		}
		

		public void addStatement(CStatement statement)
		{
			addElement(statement);
		}
		public void addStatements(CStatementList statements)
		{
			addElements(statements);
		}
	}
	
	// A C program element that represents a statement
	private static interface CStatement extends CProgramElement
	{
	}
	
	// A C program element that represents the empty statement
	private static class CEmptyStatement implements CStatement
	{
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
	}
	
	// A C program element that represents an expression statement
	private static class CExpressionStatement implements CStatement
	{
		public CExpressionStatement(CExpression expression)
		{
			this.expression = expression;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CExpression getExpression()
		{
			return expression;
		}
		
		private CExpression expression;
	}
	
	// A C program element that represents an expression
	private static interface CExpression extends CProgramElement
	{
	}
	
	// A C program element that represents a variable expression
	private static class CVariableExpression implements CExpression
	{
		public CVariableExpression(CName variableName)
		{
			this.variableName = variableName;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CName getVariableName()
		{
			return variableName;
		}
		
		private CName variableName;
	}
	
	// A C program element that represents a literal expression
	private static class CLiteralExpression implements CExpression
	{
		public CLiteralExpression(String value)
		{
			this.value = value;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public String getValue()
		{
			return value;
		}
		
		private String value;
	}
	
	// A C program element that represents a unary operator expression
	private static class CUnaryOperatorExpression implements CExpression
	{
		public CUnaryOperatorExpression(COperator operator, CExpression expression)
		{
			this.operator = operator;
			this.expression = expression;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public COperator getOperator()
		{
			return operator;
		}
		
		public CExpression getExpression()
		{
			return expression;
		}
		
		private COperator operator;
		private CExpression expression;
	}
	
	// A C program element that represents a binary operator expression
	private static class CBinaryOperatorExpression implements CExpression
	{
		public CBinaryOperatorExpression(COperator operator, CExpression leftExpression, CExpression rightExpression)
		{
			this.operator = operator;
			this.leftExpression = leftExpression;
			this.rightExpression = rightExpression;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public COperator getOperator()
		{
			return operator;
		}
		
		public CExpression getLeftExpression()
		{
			return leftExpression;
		}
		
		public CExpression getRightExpression()
		{
			return rightExpression;
		}
		
		private COperator operator;
		private CExpression leftExpression;
		private CExpression rightExpression;
	}
	
	// A C program element that represents a conditional operator expression
	private static class CConditionalOperatorExpression implements CExpression
	{
		public CConditionalOperatorExpression(CExpression condition, CExpression ifExpression, CExpression elseExpression)
		{
			this.condition = condition;
			this.ifExpression = ifExpression;
			this.elseExpression = elseExpression;
		}

		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CExpression getCondition()
		{
			return condition;
		}
		
		public CExpression getIfExpression()
		{
			return ifExpression;
		}
		
		public CExpression getElseExpression()
		{
			return elseExpression;
		}
		
		private CExpression condition;
		private CExpression ifExpression;
		private CExpression elseExpression;
	}
	
	// A C program element that represents an operator
	private static class COperator implements CProgramElement
	{
		public COperator(String symbol)
		{
			this.symbol = symbol;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public String getSymbol()
		{
			return symbol;
		}
		
		private String symbol;
	}
	
	// A C program element that represents an if/else statement
	private static class CIfElseStatement implements CStatement
	{
		public CIfElseStatement(CExpression condition, CStatement ifBranch, CStatement elseBranch)
		{
			this.condition = condition;
			this.ifBranch = ifBranch;
			this.elseBranch = elseBranch;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CExpression getCondition()
		{
			return condition;
		}
		
		public CStatement getIfBranch()
		{
			return ifBranch;
		}
		
		public CStatement getElseBranch()
		{
			return elseBranch;
		}

		private CExpression condition;
		private CStatement ifBranch;
		private CStatement elseBranch;
	}
	
	
	// A C program element that represents a block statement
	// (a block used in a statement context)
	private static class CBlockStatement extends CBlock implements CStatement
	{
		public CBlockStatement(CStatementList statements)
		{
			super(statements);
		}
	}
	
	// A C program element that represents a label
//	private static class CLabel implements CProgramElement
//	{
//		public CLabel(String labelText)
//		{
//			this.labelText = labelText;
//		}
//		
//		@Override
//		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
//		{
//			return visitor.visit(this);
//		}
//		
//		public String getLabelText()
//		{
//			return labelText;
//		}
//		
//		private String labelText;
//	}
	
	// A C program element that represents a labelled statement
	// (a statement prefixed by a label)
//	private static class CLabelledStatement implements CStatement
//	{
//		public CLabelledStatement(CLabel label, CStatement statement)
//		{
//			this.label = label;
//			this.statement = statement;
//		}
//		
//		@Override
//		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
//		{
//			return visitor.visit(this);
//		}
//		
//		public CLabel getLabel()
//		{
//			return label;
//		}
//		
//		public CStatement getStatement()
//		{
//			return statement;
//		}
//		
//		private CLabel label;
//		private CStatement statement;
//	}
	
	// A C program element that represents a goto statement
//	private static class CGotoStatement implements CStatement
//	{
//		public CGotoStatement(CLabel label)
//		{
//			this.label = label;
//		}
//		
//		@Override
//		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
//		{
//			return visitor.visit(this);
//		}
//		
//		public CLabel getLabel()
//		{
//			return label;
//		}
//		
//		private CLabel label;
//	}
	
	// A C program element that represents a variable declaration statement
	private static class CVariableDeclarationStatement implements CStatement
	{
		public CVariableDeclarationStatement(CType type, CIdentifier name)
		{
			this.type = type;
			this.name = name;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CType getType()
		{
			return type;
		}
		
		public CIdentifier getName()
		{
			return name;
		}
		
		private CType type;
		private CIdentifier name;
	}
	
	// A C program element that represents a variable declaration+initialization statement 
	private static class CVariableInitializationStatement implements CStatement
	{
		public CVariableInitializationStatement(CType type, CIdentifier name, CExpression initialValue)
		{
			this.type = type;
			this.name = name;
			this.initialValue = initialValue;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CType getType()
		{
			return type;
		}
		
		public CIdentifier getName()
		{
			return name;
		}

		public CExpression getInitialValue()
		{
			return initialValue;
		}
		
		private CType type;
		private CIdentifier name;
		private CExpression initialValue;
	}
	
	// A C program element that represents a return statement
	private static class CReturnStatement implements CStatement
	{
		public CReturnStatement(CExpression returnValue)
		{
			this.returnValue = returnValue;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CExpression getReturnValue()
		{
			return returnValue;
		}
		
		private CExpression returnValue;
	}
	
	// A C program element that represents a function call expression
	private static class CFunctionCallExpression implements CExpression
	{
		public CFunctionCallExpression(CExpression functionExpression, List<CExpression> argumentExpressions)
		{
			this.functionExpression = functionExpression;
			this.argumentExpressions = argumentExpressions;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CExpression getFunctionExpression()
		{
			return functionExpression;
		}
		
		public List<CExpression> getArgumentExpressions()
		{
			return argumentExpressions;
		}
		
		private CExpression functionExpression;
		private List<CExpression> argumentExpressions; 
	}
	
	// A C program element that represents a cast expression
	private static class CCastExpression implements CExpression
	{
		public CCastExpression(CType castType, CExpression expression)
		{
			this.castType = castType;
			this.expression = expression;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor) 
		{
			return visitor.visit(this);
		}

		public CType getCastType()
		{
			return castType;
		}
		
		public CExpression getExpression()
		{
			return expression;
		}
		
		private CType castType;
		private CExpression expression;
	}
	
	// A C++ program element that represents an object member expression
	private static class CXXObjectMemberExpression implements CExpression
	{
		public CXXObjectMemberExpression(CExpression objectExpression, CIdentifier memberName)
		{
			this.objectExpression = objectExpression;
			this.memberName = memberName;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}

		public CExpression getObjectExpression()
		{
			return objectExpression;
		}
		
		public CIdentifier getMemberName()
		{
			return memberName;
		}
		
		private CExpression objectExpression;
		private CIdentifier memberName;
	}

	// The kind of capture used for variables in a lambda
	private enum CaptureKind
	{
		BY_VALUE,
		BY_REFERENCE
	}
	
	// The mutability of capture used for variables in a lambda
	private enum CaptureMutability
	{
		IMMUTABLE,
		MUTABLE
	}
	
	// A C++ program element that represents a lambda expression 
	private static class CXXLambdaExpression implements CExpression
	{
		public CXXLambdaExpression(CaptureKind captureKind, List<CFunctionParameter> parameters, CaptureMutability captureMutability, CBlock body)
		{
			this.captureKind = captureKind;
			this.parameters = parameters;
			this.captureMutability = captureMutability;
			this.body = body;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CaptureKind getCaptureKind()
		{
			return captureKind;
		}
		
		public List<CFunctionParameter> getParameters()
		{
			return parameters;
		}
		
		public CaptureMutability getCaptureMutability()
		{
			return captureMutability;
		}
		
		public CBlock getBody()
		{
			return body;
		}
		
		private CaptureKind captureKind;
		private List<CFunctionParameter> parameters;
		private CaptureMutability captureMutability;
		private CBlock body;
	}

	private static class CWhileLoop implements CStatement
	{
		public CWhileLoop(CExpression condition, CBlock body)
		{
			this.condition = condition;
			this.body = body;
		}
		
		@Override
		public <T> T acceptVisitor(CProgramElementVisitor<T> visitor)
		{
			return visitor.visit(this);
		}
		
		public CExpression getCondition()
		{
			return condition;
		}
		
		public CBlock getBody()
		{
			return body;
		}
		
		private CExpression condition;
		private CBlock body;
	}
	
	// Utility functions shared by Compiler and ExpressionCompiler
	private static class CompilerUtils
	{
		private static Set<String> cxxKeywords = new HashSet<String>();
		
		static
		{
			cxxKeywords.addAll(Arrays.asList("and", "and_eq", "alignas", "alignof", "asm", "auto", "bitand", "bitor", "bool", "break",
					"case", "catch", "char", "char16_t", "char32_t", "class", "compl", "const", "constexpr", "const_cast", "continue",
					"decltype", "default", "delete", "double", "dynamic_cast", "else", "enum", "explicit", "export", "extern", "false",
					"float", "for", "friend", "goto", "if", "inline", "int", "long", "mutable", "namespace", "new", "noexcept", "not",
					"not_eq", "nullptr", "operator", "or", "or_eq", "private", "protected", "public", "register", "reinterpret_cast",
					"return", "short", "signed", "sizeof", "static", "static_assert", "static_cast", "struct", "switch", "template",
					"this", "thread_local", "throw", "true", "try", "typedef", "typeid", "typename", "union", "unsigned", "using",
					"virtual", "void", "volatile", "wchar_t", "while", "xor", "xor_eq"));
		}
		
		// Convert a Netty variable to a C identifier
		public static CIdentifier toCIdentifier(Variable var)
		{
			return new CIdentifier(sanitize(var.getId().name()));
		}
		
		// Lookup a variable in the given stack of variables.
		// See comments under 'variables' field in Compiler for details.
		public static VariableInfo lookupVariable(String name, Stack<VariableInfo> variables)
		{
			for (VariableInfo var : ListUtils.reverseView(variables))
				if (var.name.equals(name))
					return var;
			return null;
		}
		
		// Sanitize a Netty variable name, to make it a valid C variable name.
		public static String sanitize(String nettyVarName)
		{
			// Note: variable names beginning with an underscore are reserved
			//       for use by the code generator.
			if (cxxKeywords.contains(nettyVarName))
				return "_kw_" + nettyVarName;
			else
				return nettyVarName;
		}
	}
	
	// The kind of a variable.
	// An immediate variable is represented simply as a C variable.
	// A delayed variable is represented as a function that returns the initial
	// value, and references to the variable are represented as calls to this function.
	// This means that immediate variables have "by-value" semantics while
	// delayed variables have "by-reference" semantics.
	// A channel variable behaves like an immediate variable, but references to it
	// are represented as calls to the channel's get() function.
	private enum VariableKind
	{
		IMMEDIATE,
		DELAYED,
		CHANNEL
	}
	
	// The type of program element (expression or program) represented by a variable.
	// Expression-context variables can only be used in an expression context.
	// Program-context variables can be only be used in a program context.
	private enum VariableContext
	{
		EXPRESSION,
		PROGRAM
	}
	
	// Encapsulates information about a variable.
	private static class VariableInfo
	{
		public VariableInfo(String name, VariableKind kind, VariableContext context)
		{
			this.name = name;
			this.kind = kind;
			this.context = context;
		}
		
		public String name;  	         // the variable's name
		public VariableKind kind;        // whether the variable is immediate or delayed
		public VariableContext context;  // the context in which the variable is allowed to be used 
	}
	
	// A class used to compile Netty expressions to C types
	private static class TypeCompiler implements ExpressionVisitor<CType>
	{
		@SuppressWarnings("serial")
		public static class InvalidTypeException extends NotProgramException
		{
			InvalidTypeException(Expression type)
			{
				super("Invalid type: " + type.toAsciiString());
			}
		}
		
		public TypeCompiler(Set<String> requiredFiles)
		{
			this.requiredFiles = requiredFiles;
		}
		
		@Override
		public CType visit(AbbreviatedScope abbreviatedScope)
		{
			// An abbreviated scope is never a type.
			throw new InvalidTypeException(abbreviatedScope);
		}

		@Override
		public CType visit(Application application)
		{
			Expression function = application.getFunction();
			if (!(function instanceof Variable))
				throw new InvalidTypeException(application);
			Variable var = (Variable)function;
			if (!var.is_operator())
				throw new InvalidTypeException(application);
			Identifier id = var.getId();
			if (id.toAsciiString().equals("*"))
			{
				Expression itemType;
				if (application.getArgs().length == 1)
					itemType = application.getArgs()[0];
				else if (application.getArgs().length == 2)
					itemType = application.getArgs()[1];
				else
					throw new InvalidTypeException(application);
				requiredFiles.add(NettyNames.nettyHpp);
				return new CXXTemplateType(NettyNames.string, itemType.acceptVisitor(this));
			}
			else if (id.equals(Identifiers.list_brackets))
			{
				assert application.getArgs().length == 1;
				requiredFiles.add(NettyNames.nettyHpp);
				// If the inner type is a string<T> we unwrap it to just T, because
				// we don't want to end up with list<string<T>>, we want just list<T>.
				return new CXXTemplateType(NettyNames.list, unwrapString(application.getArgs()[0].acceptVisitor(this)));
			}
			else if (id.toAsciiString().equals("->"))  // function type
			{
				requiredFiles.add(StandardNames.functional);
				assert application.getArgs().length == 2;
				return new CXXTemplateType(
						StandardNames.function, 
						new CFunctionType(
								application.getArgs()[1].acceptVisitor(this), 
								application.getArgs()[0].acceptVisitor(this)));
			}
			// UPDATE: we don't allow procedure-typed variables
			// (Note: this also means that a function can't return a procedure,
			//        and a "result" expression cannot evaluate to a procedure).
//			else if (id.toAsciiString().equals(">->"))  // procedure type
//			{
//				// We ignore the type on the right side of the arrow, 
//				// because a procedure doesn't return anything.
//				// The C++ return type will be 'void'.
//				requiredFiles.add(StandardNames.functional);
//				assert application.args.length == 2;
//				return new CXXTemplateType(
//						StandardNames.function,
//						new CFunctionType(
//								CBuiltinTypes.void_,
//								application.args[0].acceptVisitor(this)));
//			}
			else
				throw new InvalidTypeException(application);
		}

		@Override
		public CType visit(Identifier identifier)
		{
			// Should never get here
			assert false;
			return null;
		}

		@Override
		public CType visit(Literal literal)
		{
			// A literal is never a type.
			throw new NotProgramException("Invalid type: " + literal.toAsciiString());
		}

		@Override
		public CType visit(Scope scope)
		{
			// A scope is never a type
			throw new NotProgramException("Invalid type: " + scope.toAsciiString());
		}

		@Override
		public CType visit(Variable variable)
		{
			String nettyTypename = variable.getId().name();
			if (nettyTypename.equals("nat"))
				return CBuiltinTypes.int_;
			else if (nettyTypename.equals("int"))
				return CBuiltinTypes.int_;
			else if (nettyTypename.equals("rat"))
				return CBuiltinTypes.double_;
			else if (nettyTypename.equals("real"))
				return CBuiltinTypes.double_;
			else if (nettyTypename.equals("bool"))
				return CBuiltinTypes.bool;
			else
				throw new NotProgramException("Invalid type: " + nettyTypename);
		}
		
		// Data members
		
		private Set<String> requiredFiles;
		
		// Helper functions
		
		// Unwrap a string<T> type into just T. Other types are unchanged.
		CType unwrapString(CType t)
		{
			if (!(t instanceof CXXTemplateType)) return t;
			CXXTemplateType tt = (CXXTemplateType)t;
			if (!tt.getTemplateName().equals(NettyNames.string)) return t;
			assert tt.getTemplateArguments().size() == 1;
			return tt.getTemplateArguments().get(0);
		}
	}
	
	// A class used to compile Netty expressions to C programs
	private static class Compiler implements ExpressionVisitor<CStatementList>
	{
		// Constructors
		
		public Compiler(List<RefinementProvider> refinementProviders)
		{
			this.refinementProviders = refinementProviders;
			this.typeCompiler = new TypeCompiler(requiredFiles);
			this.expressionCompiler = new ExpressionCompiler(this, typeCompiler, builtins, variables, requiredFiles);
			
			// Initialize builtins
			this.builtins.put("div", NettyNames.div);
			this.builtins.put("print", NettyNames.print);
			this.builtins.put("println", NettyNames.println);
			this.builtins.put("stdin", NettyNames.stdin);
			this.builtins.put("stdout", NettyNames.stdout);
			this.builtins.put("stderr", NettyNames.stderr);
		}
		
		// Public methods

		public CGlobalScope compileTopLevel(Expression e, Map<String, Expression> sigma)
		{
			// Create the global scope.
			CGlobalScope globalScope = new CGlobalScope();
			
			// Set up the main function.
			CFunction main = new CFunction(new CIdentifier("main"), CBuiltinTypes.int_);
			CStatementList mainBodyStatements = main.getBody().getStatementList();
			
			// Add variable declarations for the implicit program state variables at the top of main.
			// At the same time, we add them to the stack of variables that are in scope.
			for (String variableName : sigma.keySet())
			{
				variables.push(new VariableInfo(variableName, VariableKind.IMMEDIATE, VariableContext.EXPRESSION));
				mainBodyStatements.addStatement(new CVariableDeclarationStatement(
						compileType(sigma.get(variableName)), 
						new CIdentifier(CompilerUtils.sanitize(variableName))));
			}
			
			// Compile the expression into a list of statements and add them to main.
			mainBodyStatements.addStatements(compile(e));
			
			// Add necessary include directives into global scope
			for (String requiredFile : requiredFiles)
				globalScope.getElementList().addElement(new CIncludeDirective(requiredFile));
			
			// Emit main into the global scope.
			globalScope.getElementList().addElement(main);
			
			return globalScope;
		}
		
		public CStatementList compile(Expression e)
		{
			return compile(e, true);
		}
		
		public CStatementList compile(Expression e, boolean subexpression)
		{
			try
			{
				if (subexpression)
					++currentSubexpressionDepth;
				return compileImpl(e);
			}
			finally
			{
				if (subexpression)
					--currentSubexpressionDepth;
			}
		}
		
		public CStatementList compileImpl(Expression e)
		{
			System.out.println("compiling expression: " + e.toAsciiString());
			
			
			// If we've already determined that this expression is refined by a program, 
			// and we've emitted that program into a function, just emit a call to that function.
			for (ParameterizedExpression pe : expressionsRefinedByPrograms.keySet())
			{
				System.out.println("considering expression already refined by program: " + pe.getExpression().toAsciiString());
				
				List<CExpression> arguments = new ArrayList<CExpression>();
				
				// If there are no instantiables, an exact match is required
				if (pe.getParameters().isEmpty())
				{
					if (!pe.getExpression().equals(e))
						continue;
					System.out.println("exact match!");
				}
				
				// Otherwise, we try to unify
				else
				{
					Substitution subst = pe.getExpression().unify_with(e.clone(), pe.getParameters());
					if (subst == null)
						continue;
					System.out.println("unifies!");
					
					try
					{
						for (Variable var : subst.getSubst().keySet())
						{
							Expression argE = subst.getSubst().get(var);
							if (argE == null)
							{
								System.out.println("when unifying: " + pe.getExpression().toAsciiString());
								System.out.println("  with: " + e.toAsciiString());
								System.out.print("  over instantiables: ");
								for (Variable uvar : pe.getParameters())
									System.out.print(uvar.toAsciiString() + ", ");
								System.out.println();
								System.out.println(" the variable: " + var.toAsciiString() + " has no substitution");
								throw new NotProgramException("Unexpected error: unification failed");
							}
							arguments.add(compileExpression(argE));
						}
					}
					catch (NotProgramException ex)
					{
						// For parametric refinements, it might be that even though the refinement is a program,
						// the refined expression unified with the expression we're trying to compile in such
						// a way that the substituted "expressions" are programs rather than expressions
						// (i.e. they cannot be compiled in expression context). We do not support such refinemements
						// right now, so we discard the refinement.
						// TODO: support this
						System.out.println("Note: discarding refinement because: " + ex);
						continue;
					}
				}
				
				return new CStatementList(
						new CExpressionStatement(
								new CFunctionCallExpression(
										new CVariableExpression(
												new CSimpleName(expressionsRefinedByPrograms.get(pe))), 
										arguments)));
			}
			
			// Otherwise, attempt to compile the expression itself as a program
			// If that fails, what we do depends on whether or not the exception is fatal.
			// A fatal exception means "there's no way this expression or any refinement of it
			// can be a program" - in this case we don't try to look for refinements, we just
			// propagate the exception.
			// A nonfatal exception means "this expression is not a program, but a refinement
			// of it might be", so we save the exception and try looking for refinements that
			// are programs. If we end up not finding any, we'll throw the saved exception.
			NotProgramException whyNot;
			try
			{
				return e.acceptVisitor(this);
			}
			catch (NotProgramException ex)
			{
				if (ex.isFatal())
					throw ex;
				else
					whyNot = ex;
			}
			
			System.out.println("failed to compile expression, looking for refinements");
			
			// If compiling the expression as a program failed, 
			// look in the list of laws to see if there is a law
			// that states that this expression is refined by something,
			// and if that something is a program, then compile that program
			// and emit it.
			for (ParameterizedRefinement refinement : getRefinementsOf(e))
			{
				// If refinement is off limits, throw it out right away
				if (ListUtils.setMapContains(offLimitsRefinements, currentSubexpressionDepth, refinement.getRefinement()))
				{
					System.out.println("Note: discarding refinement to avoid infinite recursion");
					continue;
				}
				
				System.out.println("Considering refinement:");
				System.out.println("  expression: " + refinement.getExpression().toAsciiString());
				System.out.println("  refinement: " + refinement.getRefinement().toAsciiString());
				
				Expression uninstantiatedExpr = refinement.getExpression();
				Set<Variable> parameters = refinement.getSubstitution().getSubst().keySet();

				//System.out.println("considering refinements: " + refinement.toAsciiString());
				
				// Attempt to compile the refinement to see whether it is a program.
				// To support recursive programs, occurrences of this
				// expression inside the refinement must be treated as programs.
				// To that end, we invent a function representing the current expression,
				// and add it to the list of expressions refined by programs.
				// rather than just 'e'
				CIdentifier function = generateName();
				expressionsRefinedByPrograms.put(new ParameterizedExpression(uninstantiatedExpr, new ArrayList<Variable>(parameters)), function);
				
				// While compiling the refinement, its parameter variables are in scope
				for (Variable parameter : parameters)
					variables.push(new VariableInfo(parameter.getId().name(), VariableKind.IMMEDIATE, VariableContext.EXPRESSION));

				// Attempt to compile the refinement.
				CStatementList program;
				try
				{
					ListUtils.setMapPut(offLimitsRefinements, currentSubexpressionDepth, e);
					program = compile(refinement.getRefinement(), false);
				}
				catch (NotProgramException ex)
				{
					System.out.println("Note: discarding refinement because: " + ex);
					
					// Refinement is not a program.
					// Remove the expression -> label mapping, 
					// and move on to the next candidate refinement.
					expressionsRefinedByPrograms.remove(uninstantiatedExpr);
					continue;
				}
				finally
				{
					// Whether or not the compilation succeeded, remove the refinement's parameter variables
					for (@SuppressWarnings("unused") Variable parameter : parameters)
						variables.pop();
					ListUtils.setMapRemove(offLimitsRefinements, currentSubexpressionDepth, e);
				}
				
				// Refinement *is* a program. Emit a function containing its code, and
				// a call to this function.
				System.out.println("Refinement checks out!");

				// Prepare the list of parameters to the function and their types - one per refinement parameter
				List<CType> functionParameterTypes = new ArrayList<CType>();
				List<CFunctionParameter> functionParameters = new ArrayList<CFunctionParameter>();
				for (Variable parameter : parameters)
				{
					CType parameterType = compileType(parameter.getType());
					functionParameterTypes.add(parameterType);
					functionParameters.add(new CFunctionParameter(parameterType, CompilerUtils.toCIdentifier(parameter)));
				}
				
				// Prepare the list of arguments to the function.
				List<CExpression> functionArguments = new ArrayList<CExpression>();
				try
				{
					for (Variable parameter : parameters)
						functionArguments.add(compileExpression(refinement.getSubstitution().getSubst().get(parameter)));
				}
				catch (NotProgramException ex)
				{
					// For parametric refinements, it might be that even though the refinement is a program,
					// the refined expression unified with the expression we're trying to compile in such
					// a way that the substituted "expressions" are programs rather than expressions
					// (i.e. they cannot be compiled in expression context). We do not support such refinemements
					// right now, so we discard the refinement.
					// TODO: support this
					System.out.println("Note: discarding refinement because: " + ex);
					expressionsRefinedByPrograms.remove(uninstantiatedExpr);
					continue;
				}
				System.out.println("Substituted expressions check out!");
				
				requiredFiles.add(StandardNames.functional);
				return new CStatementList(
						new CVariableInitializationStatement(
								new CXXTemplateType(
										StandardNames.function,
										new CFunctionType(CBuiltinTypes.void_, functionParameterTypes)),
								function, 
								new CXXLambdaExpression(
										CaptureKind.BY_REFERENCE, 
										functionParameters, 
										CaptureMutability.MUTABLE,
										new CBlock(program))),
						new CExpressionStatement(
								new CFunctionCallExpression(
										new CVariableExpression(
												new CSimpleName(function)), 
										functionArguments)));
			}
			
			// If the expression is neither a program nor refined by one, throw
			// the original exception explaining why it's not a program
			throw whyNot;
		}
		
		// Methods implemented from ExpressionVisitor
		
		@Override
		public CStatementList visit(Application application)
		{
			Expression function = application.getFunction();
			if (!(function instanceof Variable))
				throw new NotProgramException("Compound expression for function in program context: " + function.toAsciiString());
			Variable var = (Variable)function;
			if (var.is_operator())
			{
				Identifier id = var.getId();
				if (id.toAsciiString().equals(":="))
				{
					assert application.getArgs().length == 2;
					return new CStatementList(
								new CExpressionStatement(
									new CBinaryOperatorExpression(
										COperators.assign, 
										compileExpression(application.getArgs()[0]),
										compileExpression(application.getArgs()[1]))));
				}
				else if (id.toAsciiString().equals("."))
				{
					assert application.getArgs().length >= 2;
					CStatementList result = new CStatementList();
					for (Expression arg : application.getArgs())
						result.addStatements(compile(arg));
					return result;
				}
				else if (id.equals(Identifiers.ifthenelse))
				{
					application.getArgs()[0].makeTypes(new ArrayList<Expression>());
					if (application.getArgs()[0].getType() != null && !compileType(application.getArgs()[0].getType()).equals(CBuiltinTypes.bool))
						throw new NotProgramException("Non-boolean condition in 'if' expression: " + application.getArgs()[0].toAsciiString());
					assert application.getArgs().length == 3;
					return new CStatementList(
							new CIfElseStatement(compileExpression(application.getArgs()[0]),
								                 new CBlockStatement(compile(application.getArgs()[1])), 
								                 new CBlockStatement(compile(application.getArgs()[2]))));
				}
				else if (id.toString().equals(" "))  // procedure call
				{
					System.out.println("compiling procedure call");
					assert application.getArgs().length >= 1;
					List<CExpression> arguments = new ArrayList<CExpression>();
					for (int i = 1; i < application.getArgs().length; ++i)
						arguments.add(compileExpression(application.getArgs()[i]));
					return new CStatementList(
							new CExpressionStatement(
									new CFunctionCallExpression(
											compileExpression(application.getArgs()[0]), 
											arguments)));
				}
				else if (id.equals(Identifiers.post_exclamation))  // channel signal
				{
					assert application.getArgs().length == 1;
					requiredFiles.add(NettyNames.nettyHpp);
					if (application.getArgs().length == 1)
						return new CStatementList(
								new CExpressionStatement(
										new CFunctionCallExpression(
												new CXXObjectMemberExpression(
														compileChannelVariable(application.getArgs()[0]), 
														NettyNames.signal), 
												new ArrayList<CExpression>())));
				}
				else if (id.equals(Identifiers.infix_exclamation))  // channel output
				{
					assert application.getArgs().length == 2;
					requiredFiles.add(NettyNames.nettyHpp);
					return new CStatementList(
							new CExpressionStatement(
									new CFunctionCallExpression(
											new CXXObjectMemberExpression(
													compileChannelVariable(application.getArgs()[0]), 
													NettyNames.output), 
											Arrays.asList(compileExpression(application.getArgs()[1])))));
				}
				else if (id.equals(Identifiers.post_question))  // channel input
				{
					assert application.getArgs().length == 1;
					requiredFiles.add(NettyNames.nettyHpp);
					return new CStatementList(
							new CExpressionStatement(
									new CFunctionCallExpression(
											new CXXObjectMemberExpression(compileChannelVariable(application.getArgs()[0]), NettyNames.input), 
											new ArrayList<CExpression>())));
				}
				else if (id.equals(Identifiers.whiledo))  // while ... do ... end loop
				{
					assert application.getArgs().length == 2;
					return new CStatementList(
							new CWhileLoop(
									compileExpression(application.getArgs()[0]),
									new CBlock(compile(application.getArgs()[1]))));
				}
				throw new NotProgramException("Unsupported operator in program context: " + function.toAsciiString());
			}
			else
				throw new NotProgramException("Non-operator function in program context: " + var.toAsciiString());
		}

		@Override
		public CStatementList visit(Identifier identifier)
		{
			// We should never get here.
			assert false;
			return null;
		}

		@Override
		public CStatementList visit(Literal literal)
		{
			// The only literal that is a program is "ok".
			// The code we generate for it is nothing,
			// represented as an empty sequence of elements.
			if (literal.toString().equals("ok"))
				return new CStatementList();
			else
				throw new NotProgramException("Literal in program context: " + literal);
		}

		@Override
		public CStatementList visit(Scope scope)
		{
			// A Scope is never a program
			throw new NotProgramException("Unsupported scope in program context: " + scope.toAsciiString());
		}
		
		@Override
		public CStatementList visit(AbbreviatedScope abbreviatedScope)
		{
			Identifier id = abbreviatedScope.getId();
			
			// channel declaration
			if (id.equals(Identifiers.chan))
			{
				requiredFiles.add(NettyNames.nettyHpp);
				Variable channelVar = abbreviatedScope.getDummy();
				return compileVariableDeclAndBody(
						channelVar, 
						VariableKind.CHANNEL,
						VariableContext.EXPRESSION,
						new CVariableDeclarationStatement(
								new CXXTemplateType(NettyNames.channel, compileType(abbreviatedScope.getDomain())), 
								CompilerUtils.toCIdentifier(channelVar)), 
						abbreviatedScope.getBody());
			}
			
			// "var", "let", and "letp" expressions
			else if (id.equals(Identifiers.var) || id.equals(Identifiers.let) || id.equals(Identifiers.letp))
			{
				CStatement varDeclStmt;
				VariableKind kind;
				VariableContext context;
				CIdentifier name = CompilerUtils.toCIdentifier(abbreviatedScope.getDummy()); 
				
				if (id.equals(Identifiers.var))
				{
					varDeclStmt = new CVariableDeclarationStatement(
							compileType(abbreviatedScope.getDomain()),
							name);
					kind = VariableKind.IMMEDIATE;
					context = VariableContext.EXPRESSION;
				}
				else if (id.equals(Identifiers.let))
				{
					// Note: unlike in "var", the variable is not part of the program state,
					//       so it cannot be modified. As a result, we declare it 'const'.
					varDeclStmt = new CVariableInitializationStatement(
							CXXBuiltinTypes.auto.makeConst(),
							name,
							new CXXLambdaExpression(
									CaptureKind.BY_REFERENCE, 
									new ArrayList<CFunctionParameter>(), 
									CaptureMutability.IMMUTABLE,
									new CBlock(new CReturnStatement(compileExpression(abbreviatedScope.getDomain())))));
					kind = VariableKind.DELAYED;
					context = VariableContext.EXPRESSION;
				}
				else  // 'letp'
				{
					Expression initExpr = abbreviatedScope.getDomain();
					CExpression compiledInitExpr;
					
					boolean isProcedure = initExpr instanceof AbbreviatedScope
							           && ((AbbreviatedScope)initExpr).getId().equals(Identifiers.procedure_scope); 
					// procedure
					if (isProcedure)
					{
						compiledInitExpr = compileExpression(initExpr);
						kind = VariableKind.IMMEDIATE;
						context = VariableContext.EXPRESSION;
					}
					
					// program
					else
					{
						compiledInitExpr = new CXXLambdaExpression(
								CaptureKind.BY_REFERENCE, 
								new ArrayList<CFunctionParameter>(), 
								CaptureMutability.MUTABLE, 
								new CBlock(compile(abbreviatedScope.getDomain())));
						kind = VariableKind.DELAYED;
						context = VariableContext.PROGRAM;
					}

					// Note: we cannot declare variables representing programs as const,
					//       because their values are mutable lambdas, and you cannot
					//       call a mutable lambda that's also const (the const qualifies
					//       the lamba object, and the mutable specifies that its operator()
					//       member is not const).
					varDeclStmt = new CVariableInitializationStatement(
							isProcedure ? CXXBuiltinTypes.auto.makeConst() : CXXBuiltinTypes.auto, 
							name, 
							compiledInitExpr);
				}
				
				return compileVariableDeclAndBody(abbreviatedScope.getDummy(), kind, context, varDeclStmt, abbreviatedScope.getBody());
			}
			
			// Other AbbreviatedScopes are not programs. In particular, function scopes are fatally
			// not programs (no refinement of them can be a program).
			else
				throw new NotProgramException("Unsupported scope in program context: " + abbreviatedScope.toAsciiString(),
						                      id.equals(Identifiers.scope) /* is fatal */);
		}

		@Override
		public CStatementList visit(Variable variable)
		{
			// A Variable can be a program if it's a program-context variable.
			String varname = variable.getId().name();
			VariableInfo var = CompilerUtils.lookupVariable(varname, variables); 
			if (var == null)
				throw new NotProgramException("Variable not declared: " + variable.toAsciiString());
			if (var.context != VariableContext.PROGRAM)
				throw new NotProgramException("Non-program-typed identifier not allowed in program context: " + variable.toAsciiString());
			assert var.kind == VariableKind.DELAYED;
			return new CStatementList(
					new CExpressionStatement(
							new CFunctionCallExpression(
									new CVariableExpression(new CSimpleName(new CIdentifier(CompilerUtils.sanitize(var.name)))), 
									new ArrayList<CExpression>())));
		}
		
		// Instance variables
		
		// The list of refinement providers that we can query to get refinements for expressions
		private List<RefinementProvider> refinementProviders;
		
		// Built-in identifiers. Netty names mapped to C(++) names.
		private Map<String, CName> builtins = new HashMap<String, CName>();
		
		// The program's variables, in order of declaration.
		// During compilation of local scopes, local variables are pushed onto this stack,
		// (and then popped off when the local scope is exited).
		// The stack is allowed to contain multiple variables with the same name;
		// the one nearest to the top is the visible one.
		private Stack<VariableInfo> variables = new Stack<VariableInfo>();
		
		// The include directives required by the program
		private Set<String> requiredFiles = new HashSet<String>();
		
		// Used to perform compilation in a type context
		private TypeCompiler typeCompiler;
		
		// Used to perform compilation in an expression context
		private ExpressionCompiler expressionCompiler;
		
		// Non-program expressions that we know are programs and for which program statements
		// have already been emitted, mapped to the name of the function into which they were emitted.
		private Map<ParameterizedExpression, CIdentifier> expressionsRefinedByPrograms = new HashMap<ParameterizedExpression, CIdentifier>();
		
		// Used to generate unique label names.
		private int nextLabel = 0;
		
		// The depth of the current subexpression being compiled
		private int currentSubexpressionDepth = 0;
		
		// Refinements that are off-limits at each subexpression depth
		// (because we are currently compiling *their* refinements).
		private Map<Integer, Set<Expression>> offLimitsRefinements = new HashMap<Integer, Set<Expression>>();
		
		// Helper functions
		
		// Compile an implemented Netty expression to a C expression
		private CExpression compileExpression(Expression e)
		{
			return e.acceptVisitor(expressionCompiler);
		}
		
		// Convert a Netty type name (e.g. 'nat') to a C type
		private CType compileType(Expression e)
		{
			return e.acceptVisitor(typeCompiler);
		}
		
		// Compile a channel variable expression in a context where it's supposed
		// to evaluate to the channel itself, not the current value at the read cursor
		private CExpression compileChannelVariable(Expression e)
		{
			return expressionCompiler.compileChannelVariable(e);
		}
		
		// Look through the list of laws and get all expressions that are
		// refinements of the given expression.
		private List<ParameterizedRefinement> getRefinementsOf(Expression e)
		{
			List<ParameterizedRefinement> result = new ArrayList<ParameterizedRefinement>();
			for (RefinementProvider refinementProvider : refinementProviders)
				result.addAll(refinementProvider.getRefinementsOf(e));
			
			//System.out.println("Here are the refinements of " + e.toAsciiString());
			//for (ParameterizedRefinement ref : result)
			//	System.out.println(ref.getExpression().toAsciiString());
			
			return result;
		}
		
		// Generate a name that has not been used yet.
		// Note: identifiers beginning with an underscore are reserved
		//       for use by the code generator.
		private CIdentifier generateName()
		{
			return new CIdentifier("_function" + nextLabel++);
		}
		
		// Generate code for a variable declaration/initialization followed by code that uses the variable.
		private CStatementList compileVariableDeclAndBody(Variable var, VariableKind kind, VariableContext context, CStatement varDeclStmt, Expression body)
		{
			// Emit the variable declaration/initialization statement.
			CStatementList result = new CStatementList(varDeclStmt);
			
			// Determine whether we need to add a new scope block because the declared variable
			// shadows an existing variable in the outer scope.
			String newVarName = var.getId().name();
			boolean shadows = CompilerUtils.lookupVariable(newVarName, variables) != null;
			
			// Emit the body statements. 
			// Inside the body, the declared variable is part of the state space.
			variables.push(new VariableInfo(newVarName, kind, context));
			result.addStatements(compile(body));
			variables.pop();
			
			// If the newly introduced variable shadows a previous variable, wrap
			// the statements into a local scope block. Otherwise the C compiler
			// will complain that a variable name is being re-declared.
			if (shadows)
				result = new CStatementList(new CBlockStatement(result));
			
			return result;
		}
	}
	
	// This class is used to compile "implemented Netty expressions"
	// (a subset of Netty expressions) to C expressions.
	private static class ExpressionCompiler implements ExpressionVisitor<CExpression>
	{
		// Static variables 
		
		private static Map<String, COperator> unaryOperators = new HashMap<String, COperator>();
		private static Map<String, COperator> binaryOperators = new HashMap<String, COperator>();
		
		// Static constructor
		
		static
		{
			unaryOperators.put(Identifiers.unary_minus.toString(), COperators.negate);
			unaryOperators.put(Symbol.lookupAscii("¬"), COperators.not);
			
			binaryOperators.put(Symbol.lookupAscii("∧"), COperators.and);
			binaryOperators.put(Symbol.lookupAscii("∨"), COperators.or);
			binaryOperators.put(Symbol.lookupAscii("="), COperators.equal);
			binaryOperators.put(Symbol.lookupAscii("≠"), COperators.notEqual);
			binaryOperators.put(Symbol.lookupAscii("+"), COperators.plus);
			binaryOperators.put(Identifiers.infix_minus.toString(), COperators.minus);
			binaryOperators.put(Symbol.lookupAscii("×"), COperators.multiplies);
			binaryOperators.put(Symbol.lookupAscii("≤"), COperators.lessEq);
			binaryOperators.put(Symbol.lookupAscii("<"), COperators.less);
			binaryOperators.put(Symbol.lookupAscii("≥"), COperators.greaterEq);
			binaryOperators.put(Symbol.lookupAscii(">"), COperators.greater);
		}
		
		// Constructor
		
		public ExpressionCompiler(Compiler compiler, TypeCompiler typeCompiler, Map<String, CName> builtins, 
				                  Stack<VariableInfo> variables, Set<String> requiredFiles)
		{
			this.compiler = compiler;
			this.typeCompiler = typeCompiler;
			this.builtins = builtins;
			this.variables = variables;
			this.requiredFiles = requiredFiles;
		}
		
		// Methods implemented from ExpressionVisitor
		
		@Override
		public CExpression visit(Application application)
		{
			// In an implemented expression, all applications
			// must be those of operators.
			Expression function = application.getFunction();
			if (!(function instanceof Variable))
				throw new NotProgramException("Unimplemented function: " + function.toAsciiString());
			Variable v = (Variable)function;
			if (!v.is_operator())
				throw new NotProgramException("Unimplemented function: " + function.toAsciiString());
			
			// Handle function application operator.
			// Note that this may be a call to a function, or a list access, but we generate
			// a function call expression either way. The list data structure has an overloaded 
			// functin call operator that makes this possible.
			if (v.getId().toString().equals(" "))
			{
				assert application.getArgs().length >= 1;
				List<CExpression> arguments = new ArrayList<CExpression>();
				for (int i = 1; i < application.getArgs().length; ++i)
					arguments.add(application.getArgs()[i].acceptVisitor(this));
				return new CFunctionCallExpression(application.getArgs()[0].acceptVisitor(this), arguments);
			}	
			
			// If/then/else operator
			else if (v.getId().equals(Identifiers.ifthenelse))
			{
				assert application.getArgs().length == 3;
				return new CConditionalOperatorExpression(
						application.getArgs()[0].acceptVisitor(this), 
						application.getArgs()[1].acceptVisitor(this),
						application.getArgs()[2].acceptVisitor(this));
			}
			
			// String concatenation operator
			else if (v.getId().toString().equals(";"))
			{
				assert application.getArgs().length == 2;
				requiredFiles.add(NettyNames.nettyHpp);
				return new CFunctionCallExpression(
						new CVariableExpression(NettyNames.make_string),
						Arrays.asList(application.getArgs()[0].acceptVisitor(this),
									  application.getArgs()[1].acceptVisitor(this)));
			}
			
			// String subscript operator
			else if (v.getId().toAsciiString().equals("_"))
			{
				assert application.getArgs().length == 2;
				requiredFiles.add(NettyNames.nettyHpp); 
				return new CFunctionCallExpression(
						new CXXObjectMemberExpression(
								application.getArgs()[0].acceptVisitor(this),
								NettyNames.subscript),
						Arrays.asList(application.getArgs()[0].acceptVisitor(this)));
			}
			
			// String item replacement operator (a.k.a. "triangles" operator)
			else if (v.getId().equals(Identifiers.triangles))
			{
				assert application.getArgs().length == 3;
				requiredFiles.add(NettyNames.nettyHpp);
				return new CFunctionCallExpression(
						new CXXObjectMemberExpression(
								application.getArgs()[0].acceptVisitor(this), 
								NettyNames.replace),
						Arrays.asList(
								application.getArgs()[1].acceptVisitor(this),
								application.getArgs()[2].acceptVisitor(this)));
			}
			
			// String length operator
			else if (v.getId().toAsciiString().equals("\\len"))
			{
				assert application.getArgs().length == 1;
				return new CFunctionCallExpression(
						new CXXObjectMemberExpression(application.getArgs()[0].acceptVisitor(this), NettyNames.length),
						new ArrayList<CExpression>());
			}
			
			// List construction operator
			else if (v.getId().equals(Identifiers.list_brackets))
			{
				assert application.getArgs().length == 1;
				requiredFiles.add(NettyNames.nettyHpp);
				return new CFunctionCallExpression(
						new CVariableExpression(NettyNames.make_list),
						Arrays.asList(application.getArgs()[0].acceptVisitor(this)));
			}
			
			// List length operator
			else if (v.getId().toString().equals("#"))
			{
				assert application.getArgs().length == 1;
				requiredFiles.add(NettyNames.nettyHpp);
				return new CFunctionCallExpression(
						new CXXObjectMemberExpression(application.getArgs()[0].acceptVisitor(this), NettyNames.length),
						new ArrayList<CExpression>());
			}
			
			// List contents operator
			else if (v.getId().toAsciiString().equals("\\unlist"))
			{
				assert application.getArgs().length == 1;
				requiredFiles.add(NettyNames.nettyHpp);
				return new CFunctionCallExpression(
						new CXXObjectMemberExpression(application.getArgs()[0].acceptVisitor(this), NettyNames.contents),
						new ArrayList<CExpression>());
			}
			
			// List indexing operator
			// TODO: This cannot be implemented right now because the *type* of the result,
			//       depends on the *value* of the right hand side, and you can't do that
			//       in C++.
//			else if (v.id.toString().equals("@"))
//			{
//				assert application.args.length == 2;
//				requiredFiles.add(NettyNames.nettyHpp);
//				return new CFunctionCallExpression(
//						new CXXObjectMemberExpression(application.args[0].acceptVisitor(this), NettyNames.at),
//						Arrays.asList(application.args[1].acceptVisitor(this)));
//			}
			
			// Channel "check" operator
			else if (v.getId().toAsciiString().equals("\\check"))
			{
				assert application.getArgs().length == 1;
				requiredFiles.add(NettyNames.nettyHpp);
				return new CFunctionCallExpression(
						new CXXObjectMemberExpression(compileChannelVariable(application.getArgs()[0]), NettyNames.check), 
						new ArrayList<CExpression>());
			}
			
			// Division operator
			// This is not mapped directly to the C division operator, because in C,
			// division between integers results in an integer result (the decimal
			// part is truncated), but division between integers in Netty results in
			// a rational result (represented as floating-point in C).
			else if (v.getId().toString().equals("/"))
			{
				assert application.getArgs().length == 2;
				return new CBinaryOperatorExpression(
						COperators.divides, 
						new CCastExpression(
								CBuiltinTypes.double_, 
								application.getArgs()[0].acceptVisitor(this)),
                        application.getArgs()[1].acceptVisitor(this));
			}
			
			// List item replacement operator (really a special case of selective union,
			// but we don't currently implement selective union in the general case).
			else if (v.getId().toAsciiString().equals("|"))
			{
				// Only accept this is the LHS expression is of the form 'index -> replacement'
				assert application.getArgs().length == 2;
				Expression replacementExpr = application.getArgs()[0];
				Expression listExpr = application.getArgs()[1];
				if (replacementExpr instanceof Application)
				{
					Application replacementExprA = (Application)replacementExpr;
					Expression replacementExprFunction = replacementExprA.getFunction();
					if (replacementExprFunction instanceof Variable)
					{
						Variable replacementExprFunctionV = (Variable)replacementExprFunction;
						if (replacementExprFunctionV.is_operator() && replacementExprFunctionV.getId().toAsciiString().equals("->"))
						{
							assert replacementExprA.getArgs().length == 2;
							Expression indexExpr = replacementExprA.getArgs()[0];
							Expression replaceExpr = replacementExprA.getArgs()[1];
							return new CFunctionCallExpression(
									new CXXObjectMemberExpression(listExpr.acceptVisitor(this), NettyNames.replace), 
									Arrays.asList(indexExpr.acceptVisitor(this), replaceExpr.acceptVisitor(this)));
						}
					}
				}
				throw new NotProgramException("Sorry, selective union is not implemented in the general case"); 
			}
			
			// Other operators map directory to C operators.
			// So far, all supported operators are either unary or binary.
			else if (application.getArgs().length == 1)
				return new CUnaryOperatorExpression(convertUnaryOperator(v), 
												    application.getArgs()[0].acceptVisitor(this));
			else if (application.getArgs().length == 2)
			{
				return new CBinaryOperatorExpression(convertBinaryOperator(v),
						                             application.getArgs()[0].acceptVisitor(this),
						                             application.getArgs()[1].acceptVisitor(this));
			}
			else
				throw new NotProgramException("Operator with unimplemented arity: " + function.toAsciiString());
		}

		@Override
		public CExpression visit(Identifier identifier)
		{
			// We should never get here.
			assert false;
			return null;
		}

		@Override
		public CExpression visit(Literal literal)
		{
			System.out.println("type is: " + literal.getValue().getClass());
			String value = literal.getValue().toString();
			if (value.equals("nil"))
				return new CVariableExpression(NettyNames.nil);
			else if (value.equals("⊤"))
				return new CLiteralExpression("true");
			else if (value.equals("⊥"))
				return new CLiteralExpression("false");
			// hack: turn a one-character string literal into a character literal
			else if (   literal.getValue() instanceof String 
					 && value.charAt(0) == '"' 
					 && value.charAt(value.length() - 1) == '"'
					 && value.length() == 3)
				return new CLiteralExpression("'" + value.substring(1, 2) + "'");
			else
				// TODO: Do some more checking here
				return new CLiteralExpression(value);
		}

		@Override
		public CExpression visit(Scope scope)
		{
			// Non-Abbreviated Scopes are not valid expressions.
			throw new NotProgramException("Non-abbreviated scope in expression context");
		}

		@Override
		public CExpression visit(AbbreviatedScope abbreviatedScope)
		{
			// "result" is special AbbreviatedScope
			if (abbreviatedScope.getId().equals(Identifiers.result))
			{
				// "result v : T · body" gets compiled into a C++(11) lambda
				// whose body consists of
				//  1) the declaration of "v"
				//  2) the statements of "body"
				//  3) a return statement that returns "v"
				// This lambda is then immediately called.
				// Creating a lambda and then calling it immediately accomplishes two things:
				//  1) We stay in expression context, as required.
				//  2) Because the lambda captures variables from the surrounding scope by value,
				//     we ensure that nothing gets changed.
				Variable var = abbreviatedScope.getDummy();
				CStatementList body = new CStatementList(
						new CVariableDeclarationStatement(
								compileType(abbreviatedScope.getDomain()),
								CompilerUtils.toCIdentifier(var)));
				variables.push(new VariableInfo(var.getId().name(), VariableKind.IMMEDIATE, VariableContext.EXPRESSION));
				body.addStatements(compiler.compile(abbreviatedScope.body));
				variables.pop();
				body.addStatement(
						new CReturnStatement(
								new CVariableExpression(
										new CSimpleName(new CIdentifier(CompilerUtils.sanitize(var.getId().name()))))));
				return new CFunctionCallExpression(
						new CXXLambdaExpression(
								CaptureKind.BY_VALUE, 
								new ArrayList<CFunctionParameter>(),
								CaptureMutability.MUTABLE,
								new CBlock(body)),
						new ArrayList<CExpression>());
			}

			// Otherwise, we've got a function or a procedure scope
			if (!(abbreviatedScope.getId().equals(Identifiers.scope) || abbreviatedScope.getId().equals(Identifiers.procedure_scope)))
			{
				System.out.println("abbreviated scope id is: " + abbreviatedScope.getId());
				throw new NotProgramException("Unsupported scope in expression context");
			}
			
			// A Scope compiles into a function, implemented using C++(11) lambdas.
			
			// Compile the function's body. While we are in the function body, the function's parameter is in scope.
			CStatementList body;
			String paramName = abbreviatedScope.getDummy().getId().name();
			variables.push(new VariableInfo(paramName, VariableKind.IMMEDIATE, VariableContext.EXPRESSION));

			// The body of a function is compiled as an expression (and the lambda's body becomes a return statement 
			// returning that expression); the body of a procedure is compiled as a program and the lambda's body
			// consists of the program's statements.
			boolean isFunction = abbreviatedScope.getId().equals(Identifiers.scope);  // as opposed to procedure 
			if (isFunction) // function
				body = new CStatementList(new CReturnStatement(abbreviatedScope.body.acceptVisitor(this)));
			else                                                    // procedure
				body = compiler.compile(abbreviatedScope.body);
			
			variables.pop();
			
			// Emit the lambda expression.
			// Note: procedures capture variables in the surrounding scope by reference. 
			return new CXXLambdaExpression(
					(isFunction ? CaptureKind.BY_VALUE : CaptureKind.BY_REFERENCE),
					Arrays.asList(new CFunctionParameter(compileType(abbreviatedScope.getDomain()), CompilerUtils.toCIdentifier(abbreviatedScope.getDummy()))),
					(isFunction ? CaptureMutability.IMMUTABLE : CaptureMutability.MUTABLE),
					new CBlock(body));
		}

		@Override
		public CExpression visit(Variable variable)
		{
			return compileVariable(variable, null, null, null);
		}
		
		// Instance variables
		
		private Compiler            compiler;
		private TypeCompiler        typeCompiler;
		private Map<String, CName>  builtins;
		private Stack<VariableInfo> variables;
		private Set<String>         requiredFiles;
		
		// Helper functions

		// Compile a channel variable expression in a context where it's supposed
		// to evaluate to the channel itself, not the current value at the read cursor
		public CExpression compileChannelVariable(Expression e)
		{
			if (e instanceof Variable)
				// override the variable kind to VariableKind.IMMEDIATE, otherwise compileVariable()
				// will append a .get() to it
				return compileVariable((Variable)e, VariableKind.IMMEDIATE, VariableKind.CHANNEL,
						"Expected a channel variable, got: " + e.toAsciiString());
			else
				throw new NotProgramException("Excepted channel, got: " + e.toAsciiString());
		}
		
		// Compile a Variable expression
		public CExpression compileVariable(Variable variable, VariableKind overrideVariableKind,
				VariableKind ensureVariableKind, String wrongVariableKindError)
		{
			String varname = variable.getId().name();
			VariableInfo var = CompilerUtils.lookupVariable(varname, variables); 
			if (var == null)
			{
				// Try treating it as a built-in
				if (builtins.containsKey(varname))
				{
					requiredFiles.add(NettyNames.nettyHpp);
					return new CVariableExpression(builtins.get(varname));
				}
				else
					throw new NotProgramException("Variable not declared: " + variable.toAsciiString());
			}
			if (var.context != VariableContext.EXPRESSION)
				throw new NotProgramException("Program-typed identifier not allowed in expression context: " + variable.toAsciiString());
			CVariableExpression varExpr = new CVariableExpression(new CSimpleName(new CIdentifier(CompilerUtils.sanitize(var.name))));
			if (ensureVariableKind != null && var.kind != ensureVariableKind)
				throw new NotProgramException(wrongVariableKindError);
			VariableKind varKind = overrideVariableKind == null ? var.kind : overrideVariableKind;
			if (varKind == VariableKind.IMMEDIATE)
				return varExpr;
			else if (varKind == VariableKind.DELAYED)
				return new CFunctionCallExpression(varExpr, new ArrayList<CExpression>());
			else  // VariableKind.CHANNEL
				return new CFunctionCallExpression(
						new CXXObjectMemberExpression(varExpr, NettyNames.get),
						new ArrayList<CExpression>());
		}
		
		// Convert a Netty type name (e.g. 'nat') to a C type
		private CType compileType(Expression e)
		{
			return e.acceptVisitor(typeCompiler);
		}
		
		private COperator convertUnaryOperator(Variable op)
		{
			COperator result = unaryOperators.get(Symbol.lookupAscii(op.getId().toString()));
			if (result == null)
				throw new NotProgramException("Unimplemented unary operator: " + op.toAsciiString());
			return result;
		}
		
		private COperator convertBinaryOperator(Variable op)
		{
			COperator result = binaryOperators.get(Symbol.lookupAscii(op.getId().toString()));
			if (result == null)
				throw new NotProgramException("Unimplemented binary operator: " + op.toAsciiString());
			return result;
		}
	}
	
	// A class used to pretty-print C programs to strings
	private static class PrettyPrinter implements CProgramElementVisitor<String>
	{
		public String prettyPrint(CProgramElement e)
		{
			// Sometimes pretty-printing can produce artifacts that are always undesirable.
			// We patch them up here.
			String result = e.acceptVisitor(this).replaceAll("\n;", ";");
			
			// Sanity checks
			assert indentationLevel == 0;
			assert subexpressionContext == 0;
			assert inlineContext == 0;
			
			return result;
		}
		
		// Methods implemented from CProgramElementVisitor
		
		@Override
		public String visit(CGlobalScope cGlobalScope)
		{
			indentationLevel = 0;
			return cGlobalScope.getElementList().acceptVisitor(this);
		}

		@Override
		public String visit(CIncludeDirective cIncludeDirective)
		{
			return indent() + "#include <" + cIncludeDirective.getPath() + ">\n";
		}

		@Override
		public String visit(CFunction cFunction)
		{
			return indent() 
				 + cFunction.getReturnType().acceptVisitor(this) 
				 + " "
				 + cFunction.getName().acceptVisitor(this)
				 + "("
				 + ListUtils.join(visitAll(cFunction.getParameters()), ", ")
				 + ")\n"
				 + cFunction.getBody().acceptVisitor(this);
		}

		@Override
		public String visit(CIdentifier cIdentifier)
		{
			return cIdentifier.getName();
		}

		@Override
		public String visit(CSimpleName cSimpleName)
		{
			return cSimpleName.getIdentifier().acceptVisitor(this);
		}

		@Override
		public String visit(CXXQualifiedName cQualifiedName)
		{
			return cQualifiedName.getQualifier().acceptVisitor(this) 
				 + "::"
				 + cQualifiedName.getUnqualifiedName().acceptVisitor(this);
		}

		@Override
		public String visit(CTypeQualifier cTypeQualifier)
		{
			switch (cTypeQualifier.getKind())
			{
				case CONST: return "const";
				case VOLATILE: return "volatile";
			}
			assert false;
			return null;
		}

		@Override
		public String visit(CSimpleType cSimpleType)
		{
			return cSimpleType.getName().acceptVisitor(this);
		}

		@Override
		public String visit(CQualifiedType cQualifiedType)
		{
			return cQualifiedType.getQualifier().acceptVisitor(this)
				 + " "
				 + cQualifiedType.getUnqualifiedType().acceptVisitor(this);
		}
		
		@Override
		public String visit(CXXTemplateType cxxTemplateType)
		{
			return cxxTemplateType.getTemplateName().acceptVisitor(this)
				 + "<"
				 + ListUtils.join(visitAll(cxxTemplateType.getTemplateArguments()), ", ")
				 + ">";
		}
		
//		@Override
//		public String visit(CXXReferenceType cxxReferenceType)
//		{
//			return cxxReferenceType.getReferredType().acceptVisitor(this) + "&";
//		}
		
		@Override
		public String visit(CFunctionType cFunctionType)
		{
			return cFunctionType.getReturnType().acceptVisitor(this)
				 + "("
				 + ListUtils.join(visitAll(cFunctionType.getArgumentTypes()), ", ")
				 + ")";
		}
		
		@Override
		public String visit(CFunctionParameter cFunctionParameter)
		{
			 return cFunctionParameter.getType().acceptVisitor(this) 
				  + " " 
				  + cFunctionParameter.getName().acceptVisitor(this);
		}

		@Override
		public String visit(CBlock cBlock)
		{
			String result = indent() + "{" + maybeNewline();
			indentationLevel += tabstop;
			result += cBlock.getStatementList().acceptVisitor(this);
			indentationLevel -= tabstop;
			result += indent() + "}" + maybeNewline("");
			return result;
		}
		
		@Override
		public <CElementType extends CProgramElement> String visit(CElementList<CElementType> cElementList)
		{
			return ListUtils.join(visitAll(cElementList.getElements()), "");
		}

		@Override
		public String visit(CExpressionStatement cExpressionStatement)
		{
			return simpleStatement(cExpressionStatement.getExpression().acceptVisitor(this));
		}

		@Override
		public String visit(CUnaryOperatorExpression cUnaryOperatorExpression)
		{
			return maybeLeftParen() 
		         + cUnaryOperatorExpression.getOperator().acceptVisitor(this) 
		         + " " 
		         + enterSubexpressionContext()
		         + cUnaryOperatorExpression.getExpression().acceptVisitor(this)
		         + exitSubexpressionContext()
		         + maybeRightParen();
		}

		@Override
		public String visit(CBinaryOperatorExpression cBinaryOperatorExpression)
		{
			return maybeLeftParen()
				 + enterSubexpressionContext()
				 + cBinaryOperatorExpression.getLeftExpression().acceptVisitor(this)
				 + " "
			     + cBinaryOperatorExpression.getOperator().acceptVisitor(this) 
			     + " " 
			     + cBinaryOperatorExpression.getRightExpression().acceptVisitor(this)
			     + exitSubexpressionContext()
			     + maybeRightParen();
		}
		
		@Override
		public String visit(CConditionalOperatorExpression cConditionalOperatorExpression)
		{
			return maybeLeftParen()
				 + enterSubexpressionContext()
				 + cConditionalOperatorExpression.getCondition().acceptVisitor(this)
				 + " ? "
				 + cConditionalOperatorExpression.getIfExpression().acceptVisitor(this)
				 + " : "
				 + cConditionalOperatorExpression.getElseExpression().acceptVisitor(this)
				 + exitSubexpressionContext()
				 + maybeRightParen();
		}

		@Override
		public String visit(COperator cOperator)
		{
			return cOperator.getSymbol();
		}
		
		@Override
		public String visit(CIfElseStatement cIfElseStatement)
		{
			return indent()
				 + "if ("
				 + cIfElseStatement.getCondition().acceptVisitor(this)
				 + ")"
				 + maybeNewline()
				 + cIfElseStatement.getIfBranch().acceptVisitor(this)
				 + indent()
				 + "else"
				 + maybeNewline()
				 + cIfElseStatement.getElseBranch().acceptVisitor(this);
		}
		
		@Override
		public String visit(CVariableExpression cVariableExpression)
		{
			return cVariableExpression.getVariableName().acceptVisitor(this);
		}
		
		@Override
		public String visit(CLiteralExpression cLiteralExpression)
		{
			return cLiteralExpression.getValue();
		}
		
//		@Override
//		public String visit(CLabel cLabel)
//		{
//			return cLabel.getLabelText();
//		}
//
//		@Override
//		public String visit(CLabelledStatement cLabelledStatement)
//		{
//			return indent(indentationLevel - 1)
//				 + cLabelledStatement.getLabel().acceptVisitor(this)
//				 + ":"
//				 + maybeNewline()
//				 + cLabelledStatement.getStatement().acceptVisitor(this);
//		}
//		
//		@Override
//		public String visit(CGotoStatement cGotoStatement)
//		{
//			return simpleStatement("goto " + cGotoStatement.getLabel().acceptVisitor(this));
//		}
		
		@Override
		public String visit(CEmptyStatement cEmptyStatement)
		{
			return simpleStatement("");
		}
		
		@Override
		public String visit(CVariableDeclarationStatement cVariableDeclarationStatement)
		{
			return simpleStatement(
					  cVariableDeclarationStatement.getType().acceptVisitor(this)
					+ " "
					+ cVariableDeclarationStatement.getName().acceptVisitor(this));
		}
		
		@Override
		public String visit(CVariableInitializationStatement cVariableInitializationStatement)
		{
			CExpression initialValue = cVariableInitializationStatement.getInitialValue(); 
			return simpleStatement(
					  cVariableInitializationStatement.getType().acceptVisitor(this)
					+ " "
					+ cVariableInitializationStatement.getName().acceptVisitor(this)
					+ " = "
					// if initializer is a lambda, write its body as a block rather than on separate lines
					// note: the indented lambda context will be automatically exited upon emitting the lambda
					+ ((initialValue instanceof CXXLambdaExpression) ? enterIndentedLambdaContext() : "")
					+ cVariableInitializationStatement.getInitialValue().acceptVisitor(this));
		}
		
		@Override
		public String visit(CReturnStatement cReturnStatement)
		{
			CExpression returnValue = cReturnStatement.getReturnValue();
			return simpleStatement(
					  "return " 
					+ ((returnValue instanceof CXXLambdaExpression) ? enterIndentedLambdaContext() : "")
			        + returnValue.acceptVisitor(this));
		}
		
		@Override
		public String visit(CFunctionCallExpression cFunctionCallExpression)
		{
			return cFunctionCallExpression.getFunctionExpression().acceptVisitor(this)
				 + "("
				 + ListUtils.join(visitAll(cFunctionCallExpression.getArgumentExpressions()), ", ")
				 + ")";
		}
		
		@Override
		public String visit(CCastExpression cCastExpression) 
		{
			return "("
				 + cCastExpression.getCastType().acceptVisitor(this)
				 + ")"
				 + cCastExpression.getExpression().acceptVisitor(this);
		}
		
		@Override
		public String visit(CXXObjectMemberExpression cxxObjectMemberExpression)
		{
			return cxxObjectMemberExpression.getObjectExpression().acceptVisitor(this)
				 + "."
				 + cxxObjectMemberExpression.getMemberName().acceptVisitor(this);
		}
		
		@Override
		public String visit(CXXLambdaExpression cxxLambdaExpression)
		{
			boolean indentNicely = (inlineContext == 0 && indentedLambdaContext == true);
			indentedLambdaContext = false;
			return "["
				 + (cxxLambdaExpression.getCaptureKind() == CaptureKind.BY_VALUE ? "=" : "&")
				 + "]("
				 + ListUtils.join(visitAll(cxxLambdaExpression.getParameters()), ", ")
				 + ")"
				 + (cxxLambdaExpression.getCaptureMutability() == CaptureMutability.MUTABLE ? " mutable " : "")
				 + (indentNicely ? "\n" : enterInlineContext())
				 + cxxLambdaExpression.getBody().acceptVisitor(this)
				 + (indentNicely ? "" : exitInlineContext());
		}
		
		@Override
		public String visit(CWhileLoop cWhileLoop)
		{
			return indent()
			     + "while ("
				 + cWhileLoop.getCondition().acceptVisitor(this)
				 + ")"
				 + maybeNewline()
				 + cWhileLoop.getBody().acceptVisitor(this);
		}

		// Constants
		
		private static final int tabstop = 4; 

		// Instance variables
		
		private int indentationLevel = 0;
		
		private int subexpressionContext = 0;
		
		private int inlineContext = 0;
		
		private boolean indentedLambdaContext = false;
		
		// Helper functions
		
		private String indent()
		{
			return inlineContext > 0 ? "" : indent(indentationLevel);
		}
		
		private String indent(int indentationLevel)
		{
			char[] indent = new char[indentationLevel];
			for (int i = 0; i < indentationLevel; ++i)
				indent[i] = ' ';
			return new String(indent);	
		}
		
		private String enterSubexpressionContext()
		{
			++subexpressionContext;
			return "";
		}
		
		private String exitSubexpressionContext()
		{
			--subexpressionContext;
			return "";
		}
		
		private String enterInlineContext()
		{
			++inlineContext;
			return "";
		}
		
		private String exitInlineContext()
		{
			--inlineContext;
			return "";
		}
		
		private String enterIndentedLambdaContext()
		{
			indentedLambdaContext = true;
			return "";
		}
		
		private String maybeLeftParen()
		{
			return subexpressionContext > 0 ? "(" : "";
		}
		
		private String maybeRightParen()
		{
			return subexpressionContext > 0 ? ")" : "";
		}
		
		private String maybeNewline()
		{
			return maybeNewline(" ");
		}
		
		private String maybeNewline(String alternative)
		{
			return inlineContext > 0 ? alternative : "\n";
		}
		
		private String simpleStatement(String body)
		{
			return indent() + body + ";" + maybeNewline();
		}
		
		private <CElementType extends CProgramElement> List<String> visitAll(List<CElementType> elements)
		{
			List<String> result = new ArrayList<String>();
			for (CElementType element : elements)
				result.add(element.acceptVisitor(this));
			return result;
		}
	}
}
