package prooftool.backend.ccode;

import prooftool.backend.laws.AbbreviatedScope;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Identifier;
import prooftool.backend.laws.Literal;
import prooftool.backend.laws.Scope;
import prooftool.backend.laws.Variable;

public interface ExpressionVisitor<T> {
	public T visit(AbbreviatedScope abbreviatedScope);
	public T visit(Application application);
	public T visit(Identifier identifier);
	public T visit(Literal literal);
	public T visit(Scope scope);
	public T visit(Variable variable);
}
