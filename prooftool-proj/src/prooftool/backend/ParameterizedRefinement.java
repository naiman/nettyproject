package prooftool.backend;

import prooftool.backend.laws.Expression;

// Represents an expression E, together with a substitution S, such that S applied to E
// constitutes a refinement of some original expression P.
public class ParameterizedRefinement {
	private Expression expression;
	private Expression refinement;
	private Substitution substitution;
	
	public ParameterizedRefinement(Expression expression, Expression refinement, Substitution substitution) {
		this.expression = expression;
		this.refinement = refinement;
		this.substitution = substitution;
	}
	
	public Expression getExpression() {
		return expression;
	}
	
	public Expression getRefinement() {
		return refinement;
	}
	
	public Substitution getSubstitution() {
		return substitution;
	}
}
