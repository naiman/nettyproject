package prooftool.backend;

import java.util.ArrayList;
import java.util.List;

import prooftool.backend.direction.Direction;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Variable;

/**
 * 
 * @author evm
 *
 */
public class Step {
	
	private Direction dop;
	private Expression focus;
	private List<Variable> instantiables;
	private Expression law;
	private Substitution sub;

	public Step(Direction dop, Expression focus) {
		this.dop = dop;
		this.focus = focus;
		this.instantiables = new ArrayList<Variable>();
	}
	
	public Step(Direction dop, Expression focus, List<Variable> instantiables, Expression law, Substitution sub) {
		this(dop, focus);
		this.instantiables = instantiables;
		this.law = law;
		this.sub = sub;
	}
	
	@Override
	public String toString() {
		return this.law.toString();
	}
	
	public Direction getDop() {
		return this.dop;
	}

	public Expression getFocus() {
		return this.focus;
	}
	
	public Expression getLaw() {
		return this.law;
	}
	
	public List<Variable> getInstantiables() {
		return this.instantiables;
	}
	
	public Substitution getSubtitution() {
		return this.sub;
	}
}
