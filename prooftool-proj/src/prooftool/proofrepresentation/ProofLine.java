package prooftool.proofrepresentation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.swing.JComponent;
import javax.swing.tree.TreeNode;

import prooftool.backend.Step;
import prooftool.backend.direction.Direction;
import prooftool.backend.direction.ExpnDirection;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Variable;
import prooftool.backend.suggestions.SpecialTypeChecker;
import prooftool.backend.suggestions.SuggestionGenerator;
import prooftool.gui.prooftree.ExpressionInputBox;
import prooftool.proofrepresentation.ProofRepresentationBits.MonotonicContext;
import prooftool.proofrepresentation.ProofRepresentationBits.ProofSubtype;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.justification.ZoomInJustification;
import prooftool.proofrepresentation.suggestion.Suggestion;
import prooftool.proofrepresentation.suggestion.Suggestions;
import prooftool.util.ExpressionUtils;
import prooftool.util.Identifiers;
import prooftool.util.Main;
import prooftool.util.ProofRep;
import prooftool.util.objects.Interval;
import prooftool.util.objects.Path;
import prooftool.util.objects.ZoomInfo;

/**
 *
 * @author dave
 * @author evm
 */
public class ProofLine extends ProofElement implements Cloneable {

	private Expression expn;
	private Path zoomOutPath;
	private Suggestions suggestions;
	
	private JComponent panel;
	
	//map a char position in the text area to highlight ranges and sub-expression paths
	private Map<Integer,ZoomInfo> zoomMap = new HashMap<Integer,ZoomInfo>();
	private Map<Integer, ZoomInfo> deepZoomMap = new HashMap<Integer,ZoomInfo>();

	/**
	 * Make a new proofline with no justification
	 * 
	 * @param expn
	 *            The expression in the line
	 * @param dir
	 *            The direction for the line
	 * @param j
	 *            The justification this proofline gives for the next line
	 */
	public ProofLine(Direction dir, Expression expn, Justification j) {
		super.setDirection(dir);
		this.setJustification(j);
		this.expn = expn;
		
		this.suggestions = new Suggestions();
	}
	
	public ProofLine(ProofLine proofLine, Proof parent) {
		// might need to clone the justification. We try without doing it here    	
		super.setDirection(proofLine.getDirection());
		this.expn = proofLine.getExpn();
		this.setJustification(proofLine.getJustification());
   
		this.setParent(parent);		
		this.suggestions = new Suggestions();
	}
	
	public ProofLine (String s) throws Exception{		
		this.suggestions = new Suggestions();
		this.loadElement(s);
	}
	
	public ProofLine() {
		this.suggestions = new Suggestions();
	}

	/**
	 * Make a nicely formatted string of this element. If this is the first line
	 * in a subproof, we don't include the direction in the returned string.
	 * 
	 * @param isFirst
	 *            Indicate that "=" should not precede the expression
	 * @return A nuce unicode string for the expression this line represents
	 */
	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		// Put in the preamble bit
		if (isFirstLineInProof()) {
			ret.append("  ( ");
		} else {
			ret.append(super.getDirection() + " ( ");
		}

		// Add in the expression and closing parenthesis
		ret.append(this.getExpn().toString());
		ret.append(" )");
		ret.append(" [");
		
		// WARNING: somehow justification is null after zoom-in. temp bug fix
		if (super.getJustification() != null) { 
			ret.append(super.getJustification().getLawName());
		} else {
			ret.append("No Justification");
		}

		ret.append("]");
		return ret.toString();
	}

	@Override
	public ProofLine clone() {
		Justification nj;
		if (super.getJustification() != null) {
			nj = (Justification) super.getJustification().clone();
		} else {
			nj = null;
		}

		ProofLine ret = new ProofLine(this.getDirection(), getExpn().clone(), nj);

		ret.zoomOutPath = zoomOutPath == null ? null : this.zoomOutPath.clone();
		return ret;
	}
	
    /**
     * Remove this from the parent proof it is attached to, and set the
     * predecessor proofelement's justification to Invalid
     */
    @Override
    public void remove() {
		ProofElement prev = this.getPreviousElement();

		if (prev != null) {
			prev.setJustification(ProofRepresentationBits.InvalidJustification);
		}

		assert super.getParent() != null;
		assert super.getParent().getChildren().contains(this);

		super.getParent().getChildren().remove(this);
    }
    
	@Override
	public String toSaveString() {
		StringBuilder ret = new StringBuilder();
		this.appendFields(ret);
		return ret.toString();
	}

	@Override
	public void loadElement(String s) throws Exception {
		String[] comps = s.split(ExpressionUtils.savedComponentDelim);
		StringTokenizer st = new StringTokenizer(comps[0], ExpressionUtils.savedFieldDelim);

		this.expn = Main.parseSingle(st.nextToken());
		String outPath = st.nextToken();

		if (outPath.equals("null")) {
			this.zoomOutPath = null;
		} else {
			Scanner pathScan = new Scanner(outPath);
			pathScan.useDelimiter(",");
			this.zoomOutPath = new Path();
			while (pathScan.hasNext()) {
				this.zoomOutPath.push(Integer.parseInt(pathScan.next()));
			}

			// Close the scanner - otherwise potential for resource leak
			pathScan.close();
		}

		this.setDirection(new ExpnDirection(st.nextToken()));
		this.setJustification(ProofRep.loadJustification(comps[1]));
	}
	
	//Methods needed to be implemented from TreeNode
	@Override
	public TreeNode getChildAt(int childIndex) {
		return null;
	}

	@Override
	public int getChildCount() {
		return -1;
	}

	@Override
	public int getIndex(TreeNode node) {		
		return -1;
	}

	@Override
	public boolean getAllowsChildren() {
		return false;
	}

	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public Enumeration<ProofElement> children() {
		return null;
	}
	//End of Methods needed by TreeNode

	/**
	 * Is this element the first line in a proof?
	 * 
	 * @return Whether this is the first element in the proof that it's an
	 *         immediate member of.
	 */
	public boolean isFirstLineInProof() {
		assert super.getParent() != null;
		assert super.getParent().getChildren() != null;
		assert super.getParent().getChildren().contains(this);// this is
																// actually not
																// necessary

		return (super.getParent().getChildren().indexOf(this) == 0);
	}

	/**
	 * Determine whether this is the only line in a subproof
	 * 
	 * @return Whether this is the inly line in the proof in which it resides
	 */
	public boolean isOnlyLineInProof() {
		return isFirstLineInProof()
				&& super.getParent().getChildren().size() == 1;
	}

	/**
	 * Is this line the last line in the proof of which it is a component?
	 * 
	 * @return Whether this is the last line in some subproof
	 */
	public boolean isLastLineInSubProof() {
		assert super.getParent() != null;
		assert super.getParent().getChildren() != null;
		assert super.getParent().getChildren().contains(this);

		//Check for editor node:
		int offset = 1;
		ProofElement toCheck = super.getParent().getChildren().get(super.getParent().getChildren().size() - 1);
		
		if (toCheck instanceof ProofLine) {
			ProofLine pl = (ProofLine) toCheck;
			
			if (pl.getPanel() instanceof ExpressionInputBox) {
				offset = 2;
			}
		}
		
		return (super.getParent().getChildren().indexOf(this) == super.getParent().getChildren().size() - offset);
	}

	/**
	 * Get the expression held by this line
	 * 
	 * @return the expression on this line
	 */
	public Expression getExpn() {
		return expn;
	}
	
	/*
	 * Used by the ProofTree to get access to the panel controlling this expr
	 */
	public JComponent getPanel() {
		return this.panel;
	}
	
	/*
	 * Set the panel (from the ProofTree)
	 */
	public void setPanel(JComponent panel) {
		this.panel = panel;
	}
	
	public void setExpn(Expression expn) {
		this.expn = expn;
	}

	public Path getZoomOutPath() {
		return zoomOutPath;
	}

	public void setZoomOutPath(Path zoomOutPath) {
		this.zoomOutPath = zoomOutPath;
	}

	private void appendFields(StringBuilder ret) {
		ret.append(expn);
		ret.append(ExpressionUtils.savedFieldDelim);
		ret.append(zoomOutPath);
		ret.append(ExpressionUtils.savedFieldDelim);

		// proofelement fields
		ret.append(super.getDirection());
		ret.append(ExpressionUtils.savedComponentDelim);		
		ret.append(super.getJustification().toSaveString());
	}
    
	public Suggestions getSuggestions() {
		return this.suggestions;
	}

   private boolean suggestionTrivial(Map<Expression, Step> uniqueSteps, Suggestion s){
    	Step step = s.getStep();
    	
    	if (!step.getFocus().equals(this.getExpn()) &&
                !uniqueSteps.containsKey(step.getFocus()) &&
                !(step.getFocus().equals(ExpressionUtils.top) && 
                  step.getDop().equals(ExpressionUtils.impDir)) &&
                !(step.getFocus().equals(ExpressionUtils.bottom) && 
                	step.getDop().equals(ExpressionUtils.revImpDir))) 
    	{
    		uniqueSteps.put(s.getStep().getFocus(), s.getStep());
    		return false; 
    	}
    	
    	return true;
    }
    
   private boolean suggestionSafe(Suggestion s, LawBlob laws, LawBlob context){
    	Step step = s.getStep();
    	
    	if (step.getSubtitution() != null) {
    		Map<Variable,Expression> subst = step.getSubtitution().getSubst();
    		//return typeCheck(subst, laws.inputLaws, step.law) || typeCheck(subst, context.inputLaws, step.law);
    		//TODO proper type making and checking

    		List<Expression> allLaws = new ArrayList<Expression>();
    		allLaws.addAll(context.getInputLaws());
    		allLaws.addAll(laws.getInputLaws());
    		
    		return typeCheck(subst, allLaws, step.getLaw());
    	}
    	
    	return true;
    }
    
	private static boolean typeCheck(Map<Variable, Expression> subst,	List<Expression> laws, Expression law) {
		boolean allGood = true;
		SpecialTypeChecker checker = new SpecialTypeChecker();
		
		for (Variable v : subst.keySet()) {
			Expression expn = subst.get(v);
			assert v.getType() != null;
			
			if (expn != null && expn.getType() != null) {
				Application inclusion = new Application(new Variable(Identifiers.colon), expn.getType(), v.getType());
				
				if (checker.generate(inclusion, ExpressionUtils.revImpDir, laws).size() != 0) {
					continue;
				}
			}
			
			allGood = false;
			break;
		}
		
		return allGood;
	}
   
   public void generateSuggestions(Direction requiredDir, LawBlob laws, LawBlob context) {
    	this.generateSuggestions(this.getExpn(), requiredDir, laws, context);
    }
    
   private void generateSuggestions(Expression expn, Direction requiredDir, LawBlob laws, LawBlob context) {    	
    	List<Suggestion> ret = new ArrayList<Suggestion>();
    	List<Suggestion> greyRet = new ArrayList<Suggestion>();

		Map<Expression, Step> uniqueSteps = new HashMap<Expression, Step>();
		expn = expn.clone();
		expn.makeTypes(context.getInputLaws());
		
		for (SuggestionGenerator gen : ProofRepresentationBits.generators) {
			List<Suggestion> generatedSuggestions = gen.generate(expn, requiredDir, laws, context);
			
			for (Suggestion s : generatedSuggestions) {
				if (!this.suggestionTrivial(uniqueSteps,s)) {
					if (this.suggestionSafe(s, laws, context)) {
						ret.add(s);
					} else {
						greyRet.add(s);
					}
				}
			}
		}
    	
    	this.suggestions.setMostRecentSugs(ret);
    	this.suggestions.setGreySugs(greyRet);
    }

    /**
     * Check whether the proof is zoomoutable from this line
     * @return Whether the user should be allowed to zoom out from this line
     */
	public boolean isZoomOutable() {
		return this.getParent() != null && this.getParent().isZoomInProof() && this.isLastLineInSubProof();
	}

    /**
     * Try to find a justification for the transition between this line and the 
     * next (or equivalently between next and theis), substituting it in as the 
     * justification for this line. Sets
     * justification to InvalidJustification if none can be found.
     * @return Whether a valid justification was found
     */
	public Justification tryToFindJustification(LawBlob lawBlob) {
		// Assume we're ok if we've been called without the justification having
		// been set to InvalidJustification
		if (!super.getJustification().isInvalid()) {
			return super.getJustification();
		}

		ProofElement nl = this.getNextElement();
		Justification just = null;

		if (nl != null && nl instanceof ProofLine) {
			ProofLine pl = (ProofLine) nl;
			
			//Make sure nl is not an expressionInputBox
			if (!(pl.getPanel() instanceof ExpressionInputBox)) {
				just = this.attemptVerify(this.getExpn(),	pl.getExpn(), pl.getDirection(), lawBlob);

				if (just == null) {
					just = this.attemptVerify(pl.getExpn(), this.getExpn(), pl.getDirection().reverse(), lawBlob);
				}
			}
		}

		if (just != null) {
			this.setJustification(just);
		}

		if (this.getParent().isProofDebt() && this.getParent().getPreviousLine().getJustification().isInvalid()) {
			if (!((Proof) this.getParent()).containsAnyInvalidSteps()) {
				this.getParent().getPreviousLine().setJustification(new Justification("Lemma", true));
			}
		}

		return just;
	}
    
    
    /**
     * Attempt to find a proof of expn0 dir expn1
     * 
     * @param expn0
     * @param expn1
     * @param dir
     * @return
     */
	private Justification attemptVerify(Expression expn0, Expression expn1, Direction dir, LawBlob lawblob) {
    	//grrr! this is slow, must use blob instead! 
        //List<Law> laws = new ArrayList<Law>(this.getFullContext().get(0));      
        //laws.addAll(Proof.laws);
        
        List<Expression> context = new ArrayList<Expression>(super.getParent().getFullContext().get(0));        
        
        //TODO: find a way to get the law blobs for the current open tab
        //LawBlob lawblob = Proof.gui.getLaws();
        LawBlob contextblob = new LawBlob();
        for (Expression law:context) {
        	contextblob.add(law);
        }
        
        generateSuggestions(expn0, dir, lawblob, contextblob);
        List<Suggestion> sug = this.suggestions.getMostRecentSugs();        
        for (Suggestion s:sug) {
        	if (s.getExpn().equals(expn1) /*&& localDir.isCompatible(s.getDir())*/) {            		
//        		this.setJustification(s.getJust());        		
        		return s.getJust();
        	}
        }
        
        return null;
    }

	public Map<Integer,ZoomInfo> getZoomMap() {
		return this.zoomMap;
	}
	
	public Map<Integer,ZoomInfo> getDeepZoomMap() {
		return this.deepZoomMap;
	}
	
	/**
	 * Add a zoom-in proof under this element, if it is a reasonable thing to do
	 * @param part The part to zoom in on
	 * @return The line that should be selected in the gui after creation of the subproof
	 * @throws Exception If this is not a reasonable place to start a zoom-in proof
	 */
	public Proof addZoomInProof(Path path) {
		Expression expn = getExpn(); 
		Expression newExpn = expn.clone().at(path.clone());
		newExpn.makeTypes(new ArrayList<Expression>());

		//ProofElement prev = this.getPreviousElement();
		ProofLine firstLine;

		// Build a new proof to go under the focused line
		MonotonicContext newMono = this.getParent().getProofOptions().getMonotonicContext();
		Direction newDirection = getParent().getDirection();

		for(int j = 0 ; j < path.size() ; j++) {
			newMono = ProofRep.determineMono(expn, path.get(j));
			newDirection = ProofRep.modifyDirection(newDirection, expn, path.get(j));
			expn = expn.getChild(path.get(j));
		}
		
		firstLine = new ProofLine(newDirection, newExpn, ProofRepresentationBits.InvalidJustification);
		Proof newProof = new Proof(newDirection, null, new ProofOptions(newMono, ProofSubtype.ZOOMIN, false));
		newProof.add(firstLine);

		this.insertAsNextLine(newProof);
		this.setJustification(new ZoomInJustification(path));
		newProof.generateScopeVars();
		this.getParent().getFullContext(); //generate context if it has not yet been done

		return newProof;
	}
	

	public ProofLine finishZoomOutProof() throws Exception {
		// make sure we're in a sane scenario for zooming out
		if (!isZoomOutable()) {
			throw new Exception("Cannot zoom out from here.");
		}

		this.setJustification(ProofRepresentationBits.ZoomOutJustification);

		// Get the data that we need to perform surgery on the proof tree
		Proof zoomProof = this.getParent();
		ProofLine prev = (ProofLine) zoomProof.getPreviousElement();
		if (!(prev.getJustification() instanceof ZoomInJustification)) {
			throw new Exception("Can't find the zoom in line.");
		}

		// Build the expression that will now go under the zoom proof
		Path path = ((ZoomInJustification) prev.getJustification()).getPath();
		Expression newexpn = prev.getExpn().replace(path, this.getExpn());

		if (!(prev.getJustification() instanceof ZoomInJustification)) {
			throw new Exception("Can't zoom out of a non-zoom-in proof.");
		}

		Direction pdir = zoomProof.getParent().getDirection();
		if (zoomProof.isEqualsProof()) {
			pdir = ExpressionUtils.eqDir;
		}

		ProofLine newLine = new ProofLine(pdir, newexpn, ProofRepresentationBits.InvalidJustification);
		newLine.setZoomOutPath(path);
		zoomProof.insertAsNextLine(newLine);

		return newLine;
	}
	
	public void initZoomMap() {
		Map<Interval,Path> intervals = this.getExpn().allIntervals();

		//TODO sort intervals and adjust for extra spaces
		ArrayList<Interval> l = new ArrayList<Interval>(intervals.keySet());
		Collections.sort(l);
		for(Interval inter : l) {
			int startShift = ProofRep.calcShiftStart(this.getExpn(), intervals.get(inter));
			int endShift = ProofRep.calcShiftEnd(this.getExpn(), intervals.get(inter)) + startShift;
			int start = inter.getStart() + startShift;
			int end = inter.getEnd() + endShift;

			for (int i=start;i<end;i++) {
				deepZoomMap.put(i, new ZoomInfo(start, end, intervals.get(inter)));
			}
		}

		int zoomCount = 0;
		for (Object part: getExpn().toParts()) {
			if (part instanceof Expression) {
				if (!ExpressionUtils.isZoomable(getExpn(), zoomCount)) {
					zoomCount++;
					continue;
				}

				assert (Expression)part == getExpn().getChild(zoomCount);
				ZoomInfo zi = ProofRep.calcOffsets(getExpn(), new Path(zoomCount));		
				//from start to end index add zi to zoom map
				//TODO have a sorted list of zoom info and do a log time lookup
				for(int j = zi.getStart() ; j < zi.getEnd() ; j++) {
					zoomMap.put(j, zi);
				}
				zoomCount++;
			} else {
				assert part instanceof String;
			}
		}
	}
}
