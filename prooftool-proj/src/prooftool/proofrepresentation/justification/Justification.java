package prooftool.proofrepresentation.justification;

import java.util.StringTokenizer;

import prooftool.backend.laws.Expression;
import prooftool.proofrepresentation.ProofRepresentationBits;
import prooftool.proofrepresentation.SavableElement;
import prooftool.util.ExpressionUtils;
import prooftool.util.Main;

/**
 * A class to give complete reasons for the deduction of some expression
 * given some predecessor and some law.
 * @author dave
 * @author evm
 */
public class Justification implements SavableElement, Cloneable {

	private boolean isInvalid;
	
	/**
	 * The name of the law that justified the assertion of this expression.
	 * In the event that the law used came from context, this will indicate
	 * that. In the even that there is no justification, it is a user hack
	 * type justification, this can be set to contain their reason.
	 */
	private String lawName;
	/**
	 * The expression that constitutes the identity of the law. If this is a
	 * user hack type justification, this will be set to null, indicating that
	 * there is no formal justification for a statement having this as its
	 * Justification.
	 */
	private Expression law;

	public Justification(Expression law) {
		this.lawName = (law == null? "Unnamed Law":law.getLawName());
		this.law = law;
		this.isInvalid = false;
	}
	
	public Justification(String lawName, boolean isValid) {
		this.lawName = lawName;
		this.isInvalid = !isValid;
	}

	public Justification(String loadString) throws Exception {
		this.loadElement(loadString);
	}

	@Override
	public String toString() {
		return lawName;
	}

	@Override
	public Object clone()  {
		// Don't return a different justification if this is THE invalidjustification
		if (this.equals(ProofRepresentationBits.InvalidJustification)) {
			return ProofRepresentationBits.InvalidJustification;
		}
		// Ditto ZoomOutJustification
		if (this.equals(ProofRepresentationBits.ZoomOutJustification)) {
			return ProofRepresentationBits.ZoomOutJustification;
		}
		// Ditto InvalidDirection
		if (this.equals(ProofRepresentationBits.InvalidDirection)) {
			return ProofRepresentationBits.InvalidDirection;
		}
		
		Expression nl;
		Justification ret;
		if (law != null) {
			Expression expn = law.clone();
			expn.setLawName(lawName);        	
			nl = expn;            
			ret = new Justification(/*dir,*/ nl);
		} else {
			ret = new Justification(lawName, true);
		}

		return ret;
	}

	@Override
	public String toSaveString() {
		StringBuilder ret = new StringBuilder();
		int index = 0;
		ret.append(index);
		ret.append(ExpressionUtils.savedFieldDelim);
		appendFields(ret);
		return ret.toString();
	}

	public boolean isInvalid() {
		return this.isInvalid;
	}
	
	public boolean equals(Object o) {
		if (o instanceof Justification) {
			Justification temp = (Justification) o;
			
			return ((this.law == null && temp.law==null) || this.law.equals(temp.law)) 
					&& this.lawName.equals(temp.lawName)
					&& this.isInvalid()==temp.isInvalid();
		} else {
			return false;
		}
	}

	public String getLawName() {
		return lawName;
	}

	public Expression getLaw() {
		return this.law;
	}

	public void loadElement(String s) throws Exception {
		StringTokenizer st = new StringTokenizer(s, ExpressionUtils.savedFieldDelim);
		st.nextToken();

		this.isInvalid = Boolean.parseBoolean(st.nextToken());	 		 
		this.lawName = st.nextToken();

		String field = st.nextToken();
		this.law = (field.equals("null")? null : Main.parseSingle(field));
	}

	protected void appendFields(StringBuilder ret) {	
		ret.append(isInvalid);
		ret.append(ExpressionUtils.savedFieldDelim);
		ret.append(lawName);
		ret.append(ExpressionUtils.savedFieldDelim);
		ret.append(law);    	
	}
}

