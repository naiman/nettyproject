package prooftool.proofrepresentation.justification;

import java.util.Scanner;

import prooftool.util.ExpressionUtils;
import prooftool.util.objects.Path;

/**
 *
 * @author dave
 * @author evm
 */
public class ZoomInJustification extends Justification {

	/**
     * The part that had been zoomed into
     */
    private Path zoomInPath;
    
    public ZoomInJustification(Path path) {
        super("Zoom In", true);
        this.zoomInPath = path;
        assert path!=null;
    }
    
    private ZoomInJustification() {
    	super("Zoom In", true);
    }
    
    public ZoomInJustification(String s) throws Exception {
    	super(s);
    	this.loadElement(s);
	}

	@Override
    public Object clone() {
        return new ZoomInJustification(zoomInPath);
    }    
    
    public Path getPath() {
    	return this.zoomInPath;
    }
    
    @Override
    public String toSaveString() {
    	StringBuilder ret = new StringBuilder();
		int index = 1;
    	ret.append(index);
    	ret.append(ExpressionUtils.savedFieldDelim);
    	
    	this.appendFields(ret);
    	return ret.toString();
    }
    
    @Override
    public void loadElement(String s) throws Exception {    	
    	int i = ExpressionUtils.lastnthIndexOf(s, ExpressionUtils.savedFieldDelim, 0);
    	String parentString = s.substring(0,i);
    	String myString = s.substring(i + 1);
    	super.loadElement(parentString);
    	
    	Scanner pathScan = new Scanner(myString);
		pathScan.useDelimiter(",");
		this.zoomInPath = new Path();	
		
		while(pathScan.hasNext()) {
			this.zoomInPath.push(Integer.parseInt(pathScan.next()));
		}
		
		//Fix potential resource leak
		pathScan.close();
		
		if (this.zoomInPath.isEmpty()) {
			throw new Exception("cannot have an empty zoom path");
		}
    }
    
    @Override
    protected void appendFields(StringBuilder ret) {
    	super.appendFields(ret);

    	ret.append(ExpressionUtils.savedFieldDelim);
    	assert this.zoomInPath !=null;
    	ret.append(this.zoomInPath.toString());
    }
}
