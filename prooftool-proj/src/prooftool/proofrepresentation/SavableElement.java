package prooftool.proofrepresentation;

public interface SavableElement {

	public String toSaveString();
	public void loadElement(String s) throws Exception;	
	
}
