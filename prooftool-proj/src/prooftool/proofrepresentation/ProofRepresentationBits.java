package prooftool.proofrepresentation;

import java.util.ArrayList;
import java.util.List;

import prooftool.backend.direction.Direction;
import prooftool.backend.direction.ExpnDirection;
import prooftool.backend.suggestions.SuggestionGenerator;
import prooftool.proofrepresentation.justification.Justification;

public class ProofRepresentationBits {
	public enum ProofSubtype {
		SUBPROOF, DIRECTPROOF, LEMMA, ZOOMIN, INCLUSION
	}

	public enum MonotonicContext {
		POSITIVE, NEGATIVE, NEUTRAL
	}

	public static final Direction equalsDir = new ExpnDirection("=");
	public static final Direction leftDir = new ExpnDirection("⇐");
	public static final Direction rightDir = new ExpnDirection("⇒");
	
	public static final String tabCharacter = "\t";
	
   public static final List<SuggestionGenerator> generators = new ArrayList<SuggestionGenerator>();
    
	// This si THE invalid justification
	public static final Justification InvalidJustification = new Justification("No justification", false);
	public static final Justification InvalidDirection = new Justification("Invalid direction", false);
	public static final Justification ZoomOutJustification = new Justification("Zoom Out", true);
}
