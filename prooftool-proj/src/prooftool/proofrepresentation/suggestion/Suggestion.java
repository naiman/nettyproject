package prooftool.proofrepresentation.suggestion;

import prooftool.backend.Step;
import prooftool.backend.direction.Direction;
import prooftool.backend.laws.Expression;
import prooftool.proofrepresentation.justification.Justification;

/**
 *
 * @author dave
 * @author evm
 */
public class Suggestion {

	private Justification just;
	private Step step;
	private String genSource;

	public Suggestion(Step s, Justification j, String genSource) {
		this.step = s;
		this.just = j;
		this.genSource = genSource;
	}

	@Override
	public String toString() {
		return getExpn().toString() + getJust().toString();
	}

	public Step getStep() {
		return step;
	}

	public void setStep(Step step) {
		this.step = step;
	}

	public Direction getDir() {
		return step.getDop();
	}

	public Expression getExpn() {
		return (Expression) step.getFocus();
	}

	public Justification getJust() {
		return just;
	}

	protected void setJust(Justification just) {
		this.just = just;
	}
	
	public String getSource() {
		return this.genSource;
	}
}
