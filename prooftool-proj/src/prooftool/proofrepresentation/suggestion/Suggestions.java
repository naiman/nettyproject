package prooftool.proofrepresentation.suggestion;

import java.util.ArrayList;
import java.util.List;

/*
 * @author evm
 */
public class Suggestions {
	
    private List<Suggestion> recentSugs;
    private List<Suggestion> greyRecentSugs; 
	
	public Suggestions() {
		this.recentSugs = new ArrayList<Suggestion>();
		this.greyRecentSugs = new ArrayList<Suggestion>();
	}
	
	public List<Suggestion> getMostRecentSugs() {
        return this.recentSugs;
    }

    public void setMostRecentSugs(List<Suggestion> mostRecentSugs) {
        this.recentSugs = mostRecentSugs;
    }
    
    public List<Suggestion> getGreySugs() {
        return this.greyRecentSugs;
    }

    public void setGreySugs(List<Suggestion> mostRecentSugs) {
        this.greyRecentSugs = mostRecentSugs;
    }
}
