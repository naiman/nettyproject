%Written by David Szeto
%This is a law file for Netty.
%Records the bunch laws as specified by Eric Hehner's aPToP, chapter 11.4.3
%NOT DONE; distributivity rules at the end must be filled in from 11.8

∀ x,y:all· (x:y) = (x=y) NAMED "Elementary"
∀ x,A,B:all· (x: A,B) = ((x: A) ∨ (x: B)) NAMED "Compound"
∀ A:all· A,A = A NAMED "Idempotence"
∀ A,B:all· A,B = B,A NAMED "Symmetry"
∀ A,B,C:all· A,(B,C) = (A,B),C NAMED "Associativity"
∀ A:all· A‘A = A NAMED "Idempotence"
∀ A,B:all· A‘B = B‘A NAMED "Symmetry"
∀ A,B,C:all· A‘(B‘C) = (A‘B)‘C NAMED "Associativity"
∀ A,B,C:all· A,B: C = (A:C ∧ B:C) NAMED "Antidistributivity"
∀ A,B,C:all· A: B‘C = (A: B ∧ A: C) NAMED "Distributivity"
∀ A,B:all· A: A,B NAMED "Generalization"
∀ A,B:all· A‘B: A NAMED "Specialization"
∀ A:all· A: A NAMED "Reflexivity"
∀ A,B:all· (A: B ∧ B: A) = (A=B) NAMED "Antisymmetry"
∀ A,B,C:all· A: B ∧ B: C ⇒ A: C NAMED "Transitivity"

¢ null = 0 NAMED "Size"
∀ x:all· ¢x = 1 NAMED "Size (only applicable to elements)"
∀ A,B:all· (¢(A, B) + ¢(A‘B)) = ¢A + ¢B NAMED "Size"
∀ A,x:all· ¬(x: A) ⇒ (¢(A‘x) = 0) NAMED "Size"
∀ A,B:all· A: B ⇒ ¢A ≤ ¢B NAMED "Size"

∀ A,B:all· A,(A‘B) = A NAMED "Absorption"
∀ A,B:all· A‘(A,B) = A NAMED "Absorption"
∀ A,B:all· (A: B) = (A,B = B) NAMED "Inclusion"
∀ A,B:all· (A,B = B) = (A = A‘B) NAMED "Inclusion"
∀ A,B:all· (A: B) = (A = A‘B) NAMED "Inclusion"
∀ A,B,C:all· A,(B,C) = (A,B),(A,C) NAMED "Distributivity"
∀ A,B,C:all· A,(B‘C) = (A,B)‘(A,C) NAMED "Distributivity"
∀ A,B,C:all· A‘(B,C) = (A‘B), (A‘C) NAMED "Distributivity"
∀ A,B,C:all· A‘(B‘C) = (A‘B)‘(A‘C) NAMED "Distributivity"
∀ A,B,C,D:all· A: B ∧ C: D ⇒ A,C: B,D NAMED "Conflation, monotonicity"
∀ A,B,C,D:all· A: B ∧ C: D ⇒ A‘C: B‘D NAMED "Conflation, monotonicity"
∀ A:all· null: A NAMED "Induction"
∀ A:all· A, null = A NAMED "Identity"
∀ A:all· A ‘ null = null NAMED "Base"
∀ A:all· (¢A = 0) = (A = null) NAMED "Size"
% ∀ i:all· ∀ x, y:int· x≤y ⇒ ((i: x,..y) = (i: int ∧ x≤i<y))
% x: int ∧ y: xint ∧ x≤y ⇒ ¢(x,..y) = y–x

%Distributivity over bunches
−null = null NAMED "Distribution"
∀ A,B:all· −(A, B) = (−A,−B) NAMED "Distribution"
∀ A:all· A+null = null NAMED "Distribution"
∀ A:all· null+A = null NAMED "Distribution"
∀ A,B,C,D:all· (A, B)+(C, D) = A+C, A+D, B+C, B+D NAMED "Distribution"
%%TO DO: fill in the rest of the distributivity rules



% These are some useful unicode symbols of operators
% binary: ⊤ ⊥ ¬ ∧ ∨ ⇐ ⇒ = ≠
% numeric: + − × ∕ ^ ≤ < ≥ >
% bunch: , ‘ ,.. : ¢ ⚡
% set: # ~ { } ∈ ⊆ ⊂ ⋃ ⋂
% strings: ; ;.. ∗ " " _ ⊲ ⊳ $ [ ]
% extra: ♮ ⟷
% functions: ⟨ → ⟩ ↦ | ∆
% quantifiers: ∀ ∃ ∑ § ∏ ·  
% programming: x′ := ok . ||
