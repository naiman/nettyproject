% Tip of the day: keep symbols that aren't on your keyboard in the comments.
% So you can copy and paste them as needed.

%%% some binary warmup %%%
% symbols: ⊤⊥¬∧∨⇐⇒=≠
b=¬b   			% bug#0 – worked around
a∧b⇒c∧b=(¬b)
a≠b=c≠d
⊤∧⊥∨(a⇒b)≠(¬¬b∧c∨¬¬(¬c⇐(c⇒¬c))=⊤)  
a⇒c ⇒⇒ a∧b⇒c∧b
if a then b else c end == (¬c⇒a) ∧ (a⇒b)

%%% some numeric rocking %%%
% symbols: + − × ∕ ^ ≤< ≥>
% note there's no decimal point. 
% for instance, x:=1.1=x represents a sequential composition
1+2×3≤a/b
-1--2∨0=5⇒--a
2^2^2≠(2^2)^2
2/3^2-5/4*2

%%% some bunch and set doodling %%%
% , ‘ ,.. : 
% {} ∈⊆⊂⋃⋂
12:1,2:alpha=5
(1+2,2+1)+(1,..4):nat×2
{1,..n}‘(⟨k→{1,..k}⟩nat)
A⋃B⋂C ⊆ B⋂C⋃A

%%% strings and lists %%%
% ; ;.. [] _ ⊲⊳ | → ""
[1;..10](2,4,5) : [(1;..20)_(1,..7)]
[s]|i→j = [s⊲i⊳j]
% I find that the triangles together with precedence have a big
% potential for confusion. Let's try it out.
"black pearl"_(1;..5;7;..11)⊲0⊳"j"⊲5⊳"f" ∨ a

%%% functions and quantifiers %%%
%%% ∀ ∃ ∑ § · ⟨→⟩
%∀⟨ b:0,..n → foo ⟩			%bug#1
∀⟨b:(0,..5)→b∧(a∧b)⟩		
∀ y,z ↦ f y ≤ 10
∀ y,z : D · f y ≤ 10
∀ y · f y ≤ 10 ⇒ ∃⟨z → z×y = 0⟩
∃⟨ A:D → A≠null ⟩
%∀ y · f y ≤ 10 ⇒ ∃ z · z×y = 0
∀⟨b:bool→∀⟨a:int→ −∞ ≤ a+b ⟩⟩
⟨a:D→b:d,e,f⟩|x→y

%%% programming and refinemets at last %%%
% x′ := ok . ||
P ⇐⇐ if n=0 then ok else s:=s+L(n) . n:=n-1 . P end
a[i]:=j == a′ = a|i→j
P||Q = P.R||Q.R

%%% now, let's just trash the place!! %%%
⊤∧⊥∨(a⇒b)≠(¬¬b∧c∨¬¬(¬c⇐(c⇒¬c))=⊤)∨(1+2,2+1)+(1,..4):nat×2.∀⟨b:(0,..5)→b∧(a∧b)⟩||a[i]:=j == a′ = a|i→j||[s]|i→j = [s⊲i⊳j]
